@extends('layouts.back_master') @section('title','Dashboard')
@section('current_title','Dashboard')

@section('css')
<style type="text/css">
    .info-box-number{
        font-size: 40px;
    }

    .badge{
        padding: 3px 12px;
        font-size: 15px;
    }

    .sub-title{
        margin-right: 5%;padding: 5px;
    }

    .link{
        text-decoration: underline;
    }
</style>  
@stop

@section('content')
<!-- Content-->
<section>
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Welcome To
      <small>O|Lead</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Dashboard</li>
    </ol>
  </section>
  <!-- !!Content Header (Page header) -->

  <!-- Main content -->
<section class="content" >
    <!-- Info boxes -->

    
    <div class="row">        
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-folder"></i></span>

                <div class="info-box-content">
                    <a class='link' href="{{url('admin/inquiry/list')}}?status=0" target="new"><span class="info-box-text">Total Inquiry</span></a>
                    <span class="info-box-number">{{ $all_inquiry }}</span>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="fa fa-folder-open"></i></span>

                <div class="info-box-content">
                    <a class='link' href="{{url('admin/inquiry/list')}}?status=1" target="new"><span class="info-box-text">Pending Inquiries</span></a>
                    <span class="info-box-number">{{ $pending_inquiry }}</span>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-check-square"></i></span>

                <div class="info-box-content">
                    <a class='link' href="{{url('admin/inquiry/list')}}?status=2" target="new"><span class="info-box-text">Approved Inquiries</span></a>
                    <span class="info-box-number">{{ $approved_inquiry }}</span>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-history"></i></span>

                <div class="info-box-content">
                    <a class='link' href="{{url('admin/inquiry/list')}}?status=3" target="new"><span class="info-box-text">Reject Inquiries</span></a>
                    <span class="info-box-number">{{ $reject_inquiry }}</span>
                </div>
            </div>
        </div>

    </div>

    <div class="row">        
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    @foreach($all_detail as $tmp)
                        <div class="form-group">                        
                            <a class='link' href="{{url('admin/inquiry/list')}}?status=0&sector={{$tmp['id']}}" target="new">
                                <h4><span class="col-sm-6 col-lg-6 label bg-aqua sub-title">{{$tmp['name']}}</span></h4>
                            </a>                            
                            <span class="badge">{{$tmp['count']}}</span>
                            
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    @foreach($pending_detail as $tmp)
                        <div class="form-group">                        
                            <a class='link' href="{{url('admin/inquiry/list')}}?status=1&sector={{$tmp['id']}}" target="new">
                                <h4><span class="col-sm-6 col-lg-6 label bg-yellow sub-title">{{$tmp['name']}}</span></h4>
                            </a>
                            <span class="badge">{{$tmp['count']}}</span>
                            
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    @foreach($approved_detail as $tmp)
                        <div class="form-group">                        
                            <a class='link' href="{{url('admin/inquiry/list')}}?status=2&sector={{$tmp['id']}}" target="new">
                                <h4><span class="col-sm-6 col-lg-6 label bg-green sub-title">{{$tmp['name']}}</span></h4>
                            </a>
                            <span class="badge">{{$tmp['count']}}</span>
                            
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    @foreach($reject_detail as $tmp)
                        <div class="form-group">                        
                            <a class='link' href="{{url('admin/inquiry/list')}}?status=3&sector={{$tmp['id']}}" target="new">
                                <h4><span class="col-sm-6 col-lg-6 label bg-red sub-title">{{$tmp['name']}}</span></h4>
                            </a>
                            <span class="badge">{{$tmp['count']}}</span>
                            
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

    </div>    

</section>
  <!-- !!Main content -->

</section>
<!-- !!!Content -->

@stop
@section('js')
  <!-- CORE JS -->
  
  <!-- //CORE JS -->
@stop
