<style type="text/css">
    .dropdown-menu > li > a:hover, .dropdown-menu > li > a:focus {
        color: #D96557;
        text-decoration: none;
        background-color: #f5f5f5;
    }

    .dropdown-menu > li > a {
        display: block;
        padding: 5px 10px;
        clear: both;
        font-weight: normal;
        line-height: 1.42857143;
        color: #333;
        white-space: nowrap;
    }

    .dropdown-menu > li > a {
        padding-right: 15px;
        padding-left: 15px;
        color: #616161;
        font-size: 13px;

    }
</style>

<li>
    <a href="javascript:;" data-toggle="dropdown">
        <span class="pull-left">@if(isset($user)) {{{$user->username}}} @endif</span>
    </a>        
</li>