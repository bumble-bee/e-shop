@extends('layouts.front_master') 
@section('current_title','O|ITS')   

@section('css')
<style type="text/css">  
</style>
@stop

@section('bg')
<!-- <div  class="hero">
    <img src="{{url('assets/front/img/bg.jpg')}}" style="width:100%;height:100%">    
</div> -->
@stop

@section('content') 
 <div class="masthead text-center">
    <div class="container">
        <div class="row" style="margin-top:5%;">
            <div class="col-md-8 col-md-offset-2">
                <h1 class="text-muted">{{config('app.app_name')}}</h1>
                <p class="lead text-muted">O|LEAD enhancing the Customer Relationships with connecting people.</p>
                <!-- <form>
                    <input class="search-field" placeholder="Search Something ... " type="text">
                    <button type="submit"><i class="fa fa-search"></i></button>
                </form> -->
                <a href="{{url('admin/inquiry/add')}}" class="btn btn-hero"><span class="icon-git"></span>Create Inquiry<span class="icon-right"></span></a>
            </div>
        </div>
    </div>
</div>


@stop


@section('js')
 
@stop
