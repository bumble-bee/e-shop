<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <title>Orel|IT</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width">
  <link rel="shortcut icon" href="/favicon.ico">

  <!-- BOOTSTRAP -->
  <link rel="stylesheet" href="{{asset('assets/dist/bootstrap/css/bootstrap.min.css')}}">
  <!-- //BOOTSTRAP -->

  <!-- FONTS -->
  <link rel="stylesheet" href="{{asset('assets/fonts/roboto/roboto.css')}}">
  <link rel="stylesheet" href="{{asset('assets/fonts/fontawesome/font-awesome.css')}}">
  <!-- //FONTS -->


<style type="text/css">
  body,html{
      background-color: #fff;
      width: 100%;
      height: 100%;
  }

 .with-bg-size {
    background-image: url('{{asset('assets/adminlte/img/bg.jpg')}}');
    width: 100%;
    height: 100%;
    background-position: center;
    /* Make the background image cover the area of the <div>, and clip the excess */
    background-size: cover;
  }

  .login{
    height: 100%;
  }

  .login-form{
    background: #fffffff5;
    padding: 20px;
  }

  .outer {
      display: table;
      position: absolute;
      height: 100%;
      width: 100%;
  }

  .middle {
      display: table-cell;
      vertical-align: middle;
  }

  .inner {
      margin-left: auto;
      margin-right: auto; 
      width: 30%; /*whatever width you want*/
  }

  .login-form-text{
    margin-bottom: 20px;
  }

  .login-form-text h3{
    margin-top: 8px;
  }

  .form-control {
    display: block;
    width: 100%;
    height: 40px;
    padding: 4px 7px;
    font-size: 12px;
    line-height: 1.5;
    color: #666;
    background-color: #fff;
    background-image: none;
    border: 1px solid #d0d0d0;
    border-radius: 3px;
     -webkit-box-shadow: none; 
     box-shadow: none; 
     -webkit-transition: none; 
    -o-transition: none;
     transition: none; 
}
  
  .btn-info {
    color: #fff;
    background-color: #9581d8;
    border-color: #9581d8;
}

.form-control:focus {
     border-color: none; 
    outline: 0;
    -webkit-box-shadow: none;
    box-shadow: none;
}

.spinner{
    position: absolute;
    height: 100%;
    width: 100%;
    top: 0;
    left: 0;
    background: #fff;
}


    .sk-folding-cube {
      margin: 20px auto;
      width: 40px;
      height: 40px;
      position: relative;
      -webkit-transform: rotateZ(45deg);
              transform: rotateZ(45deg);
    }

    .sk-folding-cube .sk-cube {
      float: left;
      width: 50%;
      height: 50%;
      position: relative;
      -webkit-transform: scale(1.1);
          -ms-transform: scale(1.1);
              transform: scale(1.1); 
    }
    .sk-folding-cube .sk-cube:before {
      content: '';
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      background-color: #2ed872;
      -webkit-animation: sk-foldCubeAngle 2.4s infinite linear both;
              animation: sk-foldCubeAngle 2.4s infinite linear both;
      -webkit-transform-origin: 100% 100%;
          -ms-transform-origin: 100% 100%;
              transform-origin: 100% 100%;
    }
    .sk-folding-cube .sk-cube2 {
      -webkit-transform: scale(1.1) rotateZ(90deg);
              transform: scale(1.1) rotateZ(90deg);
    }
    .sk-folding-cube .sk-cube3 {
      -webkit-transform: scale(1.1) rotateZ(180deg);
              transform: scale(1.1) rotateZ(180deg);
    }
    .sk-folding-cube .sk-cube4 {
      -webkit-transform: scale(1.1) rotateZ(270deg);
              transform: scale(1.1) rotateZ(270deg);
    }
    .sk-folding-cube .sk-cube2:before {
      -webkit-animation-delay: 0.3s;
              animation-delay: 0.3s;
    }
    .sk-folding-cube .sk-cube3:before {
      -webkit-animation-delay: 0.6s;
              animation-delay: 0.6s; 
    }
    .sk-folding-cube .sk-cube4:before {
      -webkit-animation-delay: 0.9s;
              animation-delay: 0.9s;
    }
    @-webkit-keyframes sk-foldCubeAngle {
      0%, 10% {
        -webkit-transform: perspective(140px) rotateX(-180deg);
                transform: perspective(140px) rotateX(-180deg);
        opacity: 0; 
      } 25%, 75% {
        -webkit-transform: perspective(140px) rotateX(0deg);
                transform: perspective(140px) rotateX(0deg);
        opacity: 1; 
      } 90%, 100% {
        -webkit-transform: perspective(140px) rotateY(180deg);
                transform: perspective(140px) rotateY(180deg);
        opacity: 0; 
      } 
    }

    @keyframes sk-foldCubeAngle {
      0%, 10% {
        -webkit-transform: perspective(140px) rotateX(-180deg);
                transform: perspective(140px) rotateX(-180deg);
        opacity: 0; 
      } 25%, 75% {
        -webkit-transform: perspective(140px) rotateX(0deg);
                transform: perspective(140px) rotateX(0deg);
        opacity: 1; 
      } 90%, 100% {
        -webkit-transform: perspective(140px) rotateY(180deg);
                transform: perspective(140px) rotateY(180deg);
        opacity: 0; 
      }
    }
</style>
</head>

<body>

  <div class="with-bg-size">      
      <div class="container-fluid">

        <div class="outer">
          <div class="middle">
            <div class="inner text-center">
             <div class="login-form">
                <div class="login-form-text">
                  <h3><b>Olead</b></h3>
                  <p>CRM cloud for sales</p>
                </div>

                <form role="form" action="{{URL::to('user/login')}}" method="post">
                  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                  @if($errors->has('login'))
                    <div class="alert alert-danger">
                      Oh snap! {{$errors->first('login')}}
                    </div>
                  @endif

                  <div class="form-group has-feedback">
                    <input type="text" name="username" class="form-control" placeholder="Username" 
                      autocomplete="off" value="{{{Input::old('username')}}}" 
                      @if(empty(Input::old('username'))) autofocus @endif>
                    <span class="fa fa-user form-control-feedback"></span>
                  </div>
                  <div class="form-group has-feedback">            
                    <input type="password" name="password" class="form-control" placeholder="Password"
                       @if(!empty(Input::old('username'))) autofocus @endif>
                    <span class="fa fa-lock form-control-feedback"></span>
                  </div>

                  <div class="form-group has-feedback">            
                    <button type="submit" class="btn btn-info btn-block btn-lg btn-flat">Sign In</button>
                  </div>

                  <div class="form-group has-feedback">
                        <div class="checkbox">
                         <label>
                            <input type="checkbox" name="remember" value="{{Input::old('remember')}}" >
                             <span style="margin-top: ">Remember Me</span>
                         </label>
                       </div>
                  </div>

                </form>
              </div>

            </div>
          </div>
        </div>
      </div>
  </div>

    <div class="spinner text-center">
        <div class="outer">
          <div class="middle">
            <div class="inner text-center">
                <div class="sk-folding-cube">
                  <div class="sk-cube1 sk-cube"></div>
                  <div class="sk-cube2 sk-cube"></div>
                  <div class="sk-cube4 sk-cube"></div>
                  <div class="sk-cube3 sk-cube"></div>
                </div>
            </div>
          </div>
        </div>
       
    </div>

    <!-- jquery -->      
    <script src="{{asset('assets/dist/jquery/js/jquery-1.12.3.min.js')}}"></script>

  
  <script type="text/javascript">
    
    $(window).load(function(){
       // PAGE IS FULLY LOADED  
       // FADE OUT YOUR OVERLAYING DIV
       $('.spinner').fadeOut();
    });
  </script>
  <!-- endbuild -->
</body>

</html>
