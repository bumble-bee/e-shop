<!doctype html>
<html class="no-js" lang="">

    <head>
        <meta charset="utf-8">
        <title>O|ITS</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <link rel="shortcut icon" href="/favicon.ico">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- BOOTSTRAP -->
        <link rel="stylesheet" href="{{asset('assets/dist/bootstrap/css/bootstrap.min.css')}}">
        <!-- //BOOTSTRAP -->

        <!-- FONTS -->
        <link rel="stylesheet" href="{{asset('assets/fonts/fontawesome/css/font-awesome.css')}}">
        <!-- //FONTS -->

        <!-- STYLE -->
        <link rel="stylesheet" href="{{asset('assets/adminlte/css/AdminLTE.css')}}">
        <link rel="stylesheet" href="{{asset('assets/core/css/style.css')}}">
        <link rel="stylesheet" href="{{asset('assets/adminlte/css/skins/skin-green.css')}}">

        <!-- Sweet Alert  -->
        <link rel="stylesheet" href="{{asset('assets/dist/sweetalert/css/sweet-alert.css')}}">
        <link rel="stylesheet" href="{{asset('assets/dist/toaster/toastr.min.css')}}">


        <style type="text/css"> 
            .form-group.has-error .chosen-container{
                border: 1px solid #dd4b39;
                box-shadow: none;
            }

            .header-btn{
                border: 1px solid;
                padding: 5px;

            }
        </style>

        <!-- INCLUDE css -->
        @yield('css')
        <!-- INCLUDE css -->

    </head>


    <body class="hold-transition skin-green sidebar-mini">
        <div class="wrapper">

            <header class="main-header">

                <!-- Logo -->
                <a href="{{url('/')}}" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini">{{config('app.app_name_short')}}</span>
                    <!-- logo for regular state and mobile devices -->
                    {{config('app.app_name')}}
                </a>

                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top" role="navigation">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button"><span class="sr-only">Toggle navigation</span></a>
                    <a href="{{url('/admin')}}" class="hidden-sm hidden-xs" style="padding: 15px 15px;float: left;color: #fff"><span >Dashbaord</span></a>
                    <a class="hidden-sm hidden-xs" href="{{url('/admin/inquiry/add')}}" style="padding: 15px 15px;float: left;color: #fff">
                        <span class="header-btn" >New Lead +</span>
                    </a>

                    <!-- Navbar Right Menu -->
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">

                            @include('includes.user')
                        <!-- Messages:-->
                            <!-- <li class="dropdown messages-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-envelope-o"></i><span class="label label-success">4</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="header">You have 4 messages</li>
                                    <li>
                                        <ul class="menu">
                                            <li>
                                                <a href="#">
                                                    <div class="pull-left">
                                                        <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                                                    </div>
                                                    <h4>Support Team<small><i class="fa fa-clock-o"></i> 5 mins</small></h4>
                                                    <p>Why not buy a new awesome theme?</p>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="footer"><a href="#">See All Messages</a></li>
                                </ul>
                            </li>
 -->
                            <!-- Notifications-->
                            <li class="dropdown notifications-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-bell-o"></i>
                                    <span class="label label-warning">10</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="header">You have 10 notifications</li>
                                    <li>
                                        <ul class="menu">
                                            <li>
                                                <a href="#"><i class="fa fa-users text-aqua"></i> 5 new members joined today</a>
                                            </li>                      
                                        </ul>
                                    </li>
                                    <li class="footer"><a href="#">View all</a></li>
                                </ul>
                            </li>


                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown notifications-menu" onclick="logout()">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-power-off" style="color: #fff;font-size: 16px;margin-top:2px;padding:0;margin:0;top:2px;position:relative"></i>
                                </a>
                            </li>

                        </ul>
                    </div>
                </nav>
            </header>


            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar -->
                @include('includes.menu')
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                @yield('content')
            </div><!-- /.content-wrapper -->

            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> {{config('app.version')}}
                </div>  
                Copyright &copy; 2016-2017 <a href="#">OrangeIT Solutions</a>. All rights reserved.
            </footer>
        
        </div><!-- ./wrapper -->


        <!-- modernizr -->
        <script src="{{asset('assets/dist/core/js/modernizr.js')}}"></script>
        <!-- jquery -->
        <script src="{{asset('assets/dist/jquery/js/jquery-1.12.3.min.js')}}"></script>
        <!-- bootstrap -->
        <script src="{{asset('assets/dist/bootstrap/js/bootstrap.min.js')}}"></script>
        <!-- AdminLTE App -->
        <script src="{{asset('assets/adminlte/js/app.min.js')}}"></script>
        <!-- SlimScroll 1.3.0 -->
        <script src="{{asset('assets/dist/slimScroll/jquery.slimscroll.min.js')}}"></script>
        <!-- sweet-alert -->
        <script src="{{asset('assets/dist/sweetalert/js/sweet-alert.min.js')}}"></script>

        <!-- MY-SCRIPTS -->
        <script src="{{asset('assets/core/js/custom_functions.js')}}"></script>

        <!-- //PUSHER -->
        <script src="https://js.pusher.com/4.1/pusher.min.js"></script>

        <script src="{{asset('assets/dist/toaster/toastr.min.js')}}"></script>

        <script type="text/javascript">
            $(document).ready(function(){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                toastr.options = {
                  "closeButton": true,
                  "debug": false,
                  "newestOnTop": false,
                  "progressBar": false,
                  "positionClass": "toast-bottom-right",
                  "preventDuplicates": false,
                  "onclick": null,
                  "showDuration": "300",
                  "hideDuration": "1000",
                  "timeOut": "5000",
                  "extendedTimeOut": "1000",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
                }

                @if(session('success'))
                    sweetAlert('{{session('success.title')}}', '{!!session('success.message')!!}',0);
                @elseif(session('error'))
                    sweetAlert('{{session('error.title')}}','{!!session('error.message')!!}',2);
                @elseif(session('warning'))
                    sweetAlert('{{session('warning.title')}}','{!!session('warning.message')!!}',3);
                @elseif(session('info'))
                    sweetAlert('{{session('info.title')}}','{!!session('info.message')!!}',1);
                @elseif(session('link'))
                    sweetAlertLink('{{session('link.title')}}','{!!session('link.message')!!}','{!!session('link.link')!!}','{!!session('link.linktitle')!!}',0);
                @endif
            });

            function logout(){
                window.location.href="{{{url('user/logout')}}}";
            }

            // Enable pusher logging - don't include this in production
            Pusher.logToConsole = false;

            var pusher = new Pusher('86c4cda8e5f55482fb13', {
              cluster: 'ap1',
              encrypted: true
            });

            var channel = pusher.subscribe('notify-{{$user->id}}');
            channel.bind('App\\Events\\NotificationEvent', function(data) {
                toastr.info(data.msg)
            });

            channel.bind('notify', function(data) {
                toastr.info(data.msg)
            });

        </script>

        @yield('js')

    </body>
</html>
