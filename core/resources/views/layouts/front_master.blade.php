  
<!--
##############################################################
#     Admin Panel - Laravel 5.1                              #
#                                                            #
#     Author - Yasith Samarawickrama <yazith11@gmail.com>    #
#     Version 1.0                                            #
#     Copyright Sammy 2015                                   #
##############################################################
-->

<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <title>O|ITS</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="shortcut icon" href="/favicon.ico">

  <!-- BOOTSTRAP -->  
  <link rel="stylesheet" href="{{asset('assets/dist/bootstrap/css/bootstrap.min.css')}}">  
  <!-- //BOOTSTRAP -->

  <!-- FLAT UI -->  
  <link rel="stylesheet" href="{{asset('assets/dist/flat-ui/dist/css/flat-ui.min.css')}}">  
  <!-- //FLAT UI -->

  <!-- FONTS -->
  <link rel="stylesheet" href="{{asset('assets/fonts/fontawesome/css/font-awesome.css')}}">
  <!-- //FONTS -->

   <!-- <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'> -->
    <link href="css/main.css" rel="stylesheet">

  <!-- FRONT -->
  <link rel="stylesheet" href="{{asset('assets/front/css/main.css')}}">
  <!-- //FRONT -->
  
  </style>

  @yield('css')

</head>

<body >

  <div class="container-fluid">      
      @yield('bg')
      <nav class="navbar @yield('navbar_')" style="margin-bottom: 0px">
          <div class="container">
              <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
                      <span class="sr-only">Toggle navigation</span>
                      <span class="fa fa-bars"></span>
                  </button>
                  <a href="" class="brand">
                     <span style="font-size: 46px;position: relative;top: 0;color:#fff">O</span>
                     <span style="top: -4px;position: relative;font-size: 30px;color:rgba(255, 255, 255, 0.75)">|LEAD</span>
                  </a>
              </div>
              <div class="navbar-collapse collapse" id="navbar-collapse">
                  <ul class="nav navbar-nav navbar-right">
                      <li>
                          <a href="{{url('/')}}">
                              Home
                          </a>
                      </li>
                      @if(Sentinel::check())
                        @if(Sentinel::getUser()->hasAnyAccess(['admin']))
                        <li>
                            <a href="{{url('/admin')}}">
                                Admin
                            </a>
                        </li>
                        @endif
                      @endif

                      @if(Sentinel::check())
                        @if(Sentinel::getUser()->hasAnyAccess(['admin']) || Sentinel::getUser()->hasAnyAccess(['front']))
                        <li>
                            <a href="{{url('inquiry/index')}}">
                                Inquiries
                            </a>
                        </li>
                        @endif
                      @endif

                      @if(Sentinel::check())
                        @if(Sentinel::getUser()->hasAnyAccess(['front']))
                        <li>
                            <a href="{{url('/profile')}}">
                                Welcome, {{ucfirst(Sentinel::getUser()->first_name)}}
                            </a>
                        </li>
                        @endif
                      @endif

                      @if(!Sentinel::check())
                      <li>
                          <a href="{{url('/login')}}" class="btn nav-btn">
                              Login
                          </a>
                      </li>
                      @endif

                      @if(Sentinel::check())
                      <li>
                          <a href="{{url('/logout')}}">
                              Signout
                          </a>
                      </li>
                      @endif

                      @if(!Sentinel::check())
                      <!-- <li>
                           <a href="{{url('/signup')}}" class="btn nav-btn">Sign Up</a>
                      </li> -->
                      @endif



                      @if(Sentinel::check())
                      <li>
                           <a href="{{url('admin/inquiry/add')}}" class="btn nav-btn">+ Inquiry</a>
                      </li>
                      @endif


                  </ul>
              </div>
          </div>  
      </nav>
        
    <div class="container">
      @yield('content')
    </div>

  </div>

  <!-- modernizr -->
  <script src="{{asset('assets/dist/core/js/modernizr.js')}}"></script>
  <!-- jquery -->      
  <script src="{{asset('assets/dist/jquery/js/jquery-1.12.3.min.js')}}"></script>
  <!-- bootstrap -->      
  <script src="{{asset('assets/dist/bootstrap/js/bootstrap.min.js')}}"></script>  
  <!-- flat ui -->
  <script src="{{asset('assets/dist/flat-ui/dist/js/flat-ui.min.js')}}"></script> 


  <script type="text/javascript">
    $(document).ready(function(){
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });     
    });
  </script>

  @yield('js')
</body>

</html>

