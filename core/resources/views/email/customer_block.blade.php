<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<!--[[display]]
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
<title>Notify 2</title>[[display]]-->

<style type="text/css">
body{
	margin: 0;
}
div, p, a, li, td { -webkit-text-size-adjust:none; }

*{
-webkit-font-smoothing: antialiased;
-moz-osx-font-smoothing: grayscale;
}

.ReadMsgBody
{width: 100%; background-color: #ffffff;}
.ExternalClass
{width: 100%; background-color: #ffffff;}
/*body*/ #tc_central{width: 100%; height: 100%; background-color: #ffffff; margin:0; padding:0; -webkit-font-smoothing: antialiased;}
/*html*/ #tc_central{width: 100%; background-color: #ffffff;}

@font-face {font-family: 'proxima_novalight';src: url('http://rocketway.net/themebuilder/products/font/proximanova-light-webfont.eot');src: url('http://rocketway.net/themebuilder/products/font/proximanova-light-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/products/font/proximanova-light-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/products/font/proximanova-light-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}

@font-face {font-family: 'proxima_nova_rgregular'; src: url('http://rocketway.net/themebuilder/products/font/proximanova-regular-webfont.eot');src: url('http://rocketway.net/themebuilder/products/font/proximanova-regular-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/products/font/proximanova-regular-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/products/font/proximanova-regular-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}

@font-face {font-family: 'proxima_novasemibold';src: url('http://rocketway.net/themebuilder/products/font/proximanova-semibold-webfont.eot');src: url('http://rocketway.net/themebuilder/products/font/proximanova-semibold-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/products/font/proximanova-semibold-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/products/font/proximanova-semibold-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}
    
@font-face {font-family: 'proxima_nova_rgbold';src: url('http://rocketway.net/themebuilder/products/font/proximanova-bold-webfont.eot');src: url('http://rocketway.net/themebuilder/products/font/proximanova-bold-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/products/font/proximanova-bold-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/products/font/proximanova-bold-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}
	
@font-face {font-family: 'proxima_novathin';src: url('http://rocketway.net/themebuilder/products/font/proximanova-thin-webfont.eot');src: url('http://rocketway.net/themebuilder/products/font/proximanova-thin-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/products/font/proximanova-thin-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/products/font/proximanova-thin-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}
   
@font-face {font-family: 'proxima_novaextrabold';src: url('http://rocketway.net/themebuilder/products/font/proximanova-extrabold-webfont.eot');src: url('http://rocketway.net/themebuilder/products/font/proximanova-extrabold-webfont.eot?#iefix') format('embedded-opentype'),url('http://rocketway.net/themebuilder/products/font/proximanova-extrabold-webfont.woff2') format('woff2'),url('http://rocketway.net/themebuilder/products/font/proximanova-extrabold-webfont.woff') format('woff'),url('http://rocketway.net/themebuilder/products/font/proximanova-extrabold-webfont.ttf') format('truetype');font-weight: normal;font-style: normal;}  


p {padding: 0!important; margin-top: 0!important; margin-right: 0!important; margin-bottom: 0!important; margin-left: 0!important; }

.hover:hover {opacity:0.85;filter:alpha(opacity=85);}

.image73 img {width: 73px; height: auto;}
.image42 img {width: 42px; height: auto;}
.image400 img {width: 400px; height: auto;}
.icon49 img {width: 49px; height: auto;}
.image113 img {width: 113px; height: auto;}
.image70 img {width: 70px; height: auto;}
.image67 img {width: 67px; height: auto;}
.image80 img {width: 80px; height: auto;}
.image35 img {width: 35px; height: auto;}
.icon49 img {width: 49px; height: auto;}

</style>



<!--SMALLONEO
<style type="text/css">
		/*body*/ #tc_central{width:auto!important;}
		table[class=full] {width: 100%!important; clear: both; }
		table[class=mobile] {width: 100%!important; padding-left: 30px; padding-right: 30px; clear: both; }
		table[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
		td[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
		*[class=erase] {display: none;}
		*[class=buttonScale] {float: none!important; text-align: center!important; display: inline-block!important; clear: both;}
		.image400 img {width: 100%!important; height: auto;}
		}
</style>
CSMALLONE--> 


<!--[[display]]</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" yahoo="fix">[[display]]-->

<div class="ui-sortable" id="sort_them"> 
<!-- Notification 1  -->
<div style="display: none" id="element_02829949666685194"><p></p></div><!-- End Notification 1 -->

<!-- Notification 2  -->
<div style="display: none" id="element_08351921680905554"><p></p></div><!-- End Notification 2 -->

<!-- Notification 3  -->
<div style="display: none" id="element_015945998256712568"><p></p></div><!-- End Notification 3 -->

<!-- Notification 4  -->
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full" object="drag-module" bgcolor="#69b8f1" c-style="bgcolor" style="background-color: rgb(242, 244, 249);">
	<tbody><tr mc:repeatable="">
		<td align="center" style="background-image: url(&quot;http://rocketway.net/themebuilder/products/notifications/templates/notify2/images/not4_bg_image.jpg&quot;); background-size: cover; background-position: center center; background-repeat: no-repeat; background-color: rgb(242, 244, 249);" id="not4ChangeBG" c-style="bgcolor">
		<div mc:hideable="">
			
			<!-- Mobile Wrapper -->
			<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
				<tbody><tr>
					<td width="100%" align="center">
					
						<!-- SORTABLE -->
						<div class="sortable_inner ui-sortable">
												
						<!-- Space -->
						<table width="400" border="0" cellpadding="0" cellspacing="0" align="center" class="full" object="drag-module-small">
							<tbody><tr>
								<td width="100%" height="50"></td>
							</tr>
						</tbody></table><!-- End Space -->
						
						<!-- Space -->
						<table width="400" border="0" cellpadding="0" cellspacing="0" align="center" class="full" object="drag-module-small">
							<tbody><tr>
								<td width="100%" height="50"></td>
							</tr>
						</tbody></table><!-- End Space -->
						
						<!-- Main -->
						<table width="400" border="0" cellpadding="0" cellspacing="0" align="center" class="full" style="-webkit-border-radius: 6px; -moz-border-radius: 6px; border-radius: 6px;">
							<tbody><tr>
								<td width="100%" style="border-radius: 6px; background-color: rgb(255, 255, 255);" bgcolor="#ffffff" c-style="not4Body">
						
									<!-- Start Top -->
									<table width="400" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#3d85b8" c-style="not4BlueBG" object="drag-module-small" style="border-top-right-radius: 6px; border-top-left-radius: 6px; background-color: rgb(183, 46, 46);">
										<tbody><tr>
											<td width="100%" valign="middle" align="center">
												
												<!-- Header Text --> 
												<table width="280" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
													<tbody><tr>
														<td width="100%" height="35"></td>
													</tr>
													<tr>
														<td valign="middle" width="100%" style="text-align: center; font-family: 'Open Sans', Helvetica, Arial, sans-serif; font-size: 18px; color: #ffffff; line-height: 26px; font-weight: 700;" class="fullCenter" t-style="not4WhiteText" mc:edit="27" object="text-editable">
															<singleline>Customer Blocked</singleline>
														</td>
													</tr>
													<tr>
														<td width="100%" height="10" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
													</tr>
												</tbody></table>
												
											</td>
										</tr>
									</tbody></table>
						
									<!-- Image 113px -->
									<table width="400" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#3d85b8" style="background-image: url(&quot;http://rocketway.net/themebuilder/products/notifications/templates/notify2/images/overlay1.png&quot;); background-repeat: repeat-x; background-position: center bottom; background-color: rgb(183, 46, 46);" c-style="not4BlueBG" object="drag-module-small">
										<tbody><tr>
											<td width="400" height="15" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
										</tr>
										<tr>
											<td width="400" valign="middle" style="text-align: center; line-height: 1px;" align="center">
											
												<table width="100" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
													<tbody><tr>
														<td width="100" height="35" align="center" object="image-editable" class="image113">
															<a href="#">
																</a><a href="#" style="text-decoration: none;"><img editable="true" src="https://image.flaticon.com/icons/svg/236/236831.svg" width="113" alt="" border="0" mc:edit="28"></a>
															
														</td>
													</tr>
												</tbody></table>
																			
											</td>
										</tr>
									</tbody></table><!-- End Image 113px -->
									
									<table width="400" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#ffffff" c-style="not4Body" object="drag-module-small" style="background-color: rgb(255, 255, 255);">
										<tbody><tr>
											<td width="100%" valign="middle" align="center">
											
												<table width="300" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
													<tbody><tr>
														<td width="400" height="30" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
													</tr>
													<tr>
														<td valign="middle" width="100%" style="text-align: center; font-family: 'Open Sans', Helvetica, Arial, sans-serif; font-size: 34px; color: #b72e2e; line-height: 20px; font-weight: 100;" class="fullCenter" t-style="not4BlueText" mc:edit="29" object="text-editable">
															<singleline style="font-size: 15px;font-weight: bold;">{{$customer->code}}<br/>{{$customer->first_name}}</singleline>
														</td>
													</tr>
												</tbody></table>
												
											</td>
										</tr>
									</tbody></table>
									
									<table width="400" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#ffffff" c-style="not4Body" object="drag-module-small" style="background-color: rgb(255, 255, 255);">
										<tbody><tr>
											<td width="100%" valign="middle" align="center">
											
												<table width="300" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
													<tbody><tr>
														<td width="100%" height="30" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
													</tr>
													<tr>
														<td valign="middle" width="100%" style="text-align: center; font-family: 'Open Sans', Helvetica, Arial, sans-serif; font-size: 13px; color: #5f6a74; line-height: 24px; font-weight: 400;" class="fullCenter" t-style="not4Text" mc:edit="30" object="text-editable">
															<singleline>You have uploaded a PO for above customer & He/She has been blocked due to some reasons. Please take necessary actions before you can proceed to the Invoice.</singleline>
														</td>
													</tr>
												</tbody></table>
																			
											</td>
										</tr>
									</tbody></table>
									
									<table width="400" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#ffffff" c-style="not4Body" object="drag-module-small" style="background-color: rgb(255, 255, 255);">
										<tbody><tr>
											<td width="100%" valign="middle" align="center">
											
												<table width="300" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
													<tbody><tr>
														<td width="100%" height="35" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
													</tr>
												</tbody></table>
																			
											</td>
										</tr>
									</tbody></table>
									
									<table width="400" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#ffffff" c-style="not4Body" object="drag-module-small" style="background-color: rgb(255, 255, 255);">
										<tbody><tr>
											<td width="100%" valign="middle" align="center">
											
												<table width="300" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
													<tbody><tr>
														<td width="100%" align="center"> 
															<table border="0" cellpadding="0" cellspacing="0" align="center" class="buttonScale">
																<tbody>
															</tbody></table>
			
														</td>
													</tr>
												</tbody></table>
																			
											</td>
										</tr>
									</tbody></table>
									
									<table width="400" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#ffffff" c-style="not4Body" object="drag-module-small" style="border-bottom-right-radius: 6px; border-bottom-left-radius: 6px; background-color: rgb(255, 255, 255);">
										<tbody><tr>
											<td width="100%" valign="middle" align="center">
											
												<table width="300" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
													<tbody><tr>
														<td width="100%" height="40"></td>
													</tr>
												</tbody></table>
																				
											</td>
										</tr>
									</tbody></table>
									
								</td>
							</tr>
						</tbody></table><!-- End Main -->
						
						<!-- CopyRight -->
						<table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" object="drag-module-small">
							<tbody><tr>
								<td width="100%" height="25"></td>
							</tr>
							<tr>
								<td valign="middle" width="100%" style="text-align: center; font-family: 'Open Sans', Helvetica, Arial, sans-serif; color: #ffffff; font-size: 12px; font-weight: 400; line-height: 18px;" class="fullCenter" t-style="not4Footer" mc:edit="33" object="text-editable">
									<singleline><i>OSales Application</i><br> A Product of OCloud | Powered By OrelIT</singleline>
								</td>
							</tr>
						</tbody></table>
						
						<table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" object="drag-module-small">
							<tbody><tr>
								<td width="100%" height="20" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
							</tr>
						</tbody></table>
						
						<table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" object="drag-module-small">
							<tbody><tr>
								<td width="100%" height="60" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
							</tr>
							<tr>
								<td width="100%" height="1" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
							</tr>
						</tbody></table><!-- End CopyRight -->
						</div>
			
					</td>
				</tr>
			</tbody></table>
			
		</div>
		</td>
	</tr>
</tbody></table><!-- End Notification 4 -->

<!-- Notification 5  -->
<div style="display: none" id="element_07849687625515862"><p></p></div><!-- End Notification 5 -->

<!-- Notification 6  -->
<div style="display: none" id="element_022715650303309554"><p></p></div><!-- End Notification 6 -->


</div>
<!--[[display]]</body>[[display]]-->
</body>
</html>