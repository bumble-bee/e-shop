@extends('layouts.back_master_dash') @section('title','Dashboard')
@section('current_title','Dashboard')

@section('css')
<style type="text/css">
    .stats .info-box-icon{
        background: none;
    }

    .stats .info-box-text{
        margin-top: 8px;
    }

    .stats .info-box-number{
        font-weight: 800;
    }

    .sector-performance a{
        padding: 5px 10px !important;
    }

    .dis-excced{
        color: red;
    }

    .btn{
        background: none;
    }

    .box-green{
      border-color: #00a65a;
    }

    .users-list-img {
        border-radius: 50%;
        max-width: 30%;
        height: auto;
    }

    .info-box-content{
        margin-left: 0px !important;
        color: #73879C;
        padding: 12px 10px;
        text-align: center;
        color: #ffffff;
    }

    .blue{
        background-color: #33B2FF;
    }

    .yellow{
        background-color: #FFC733;
    }

    .green{
        background-color: #239D0D;
    }

    .pink{
        background-color: #96228D;
    }

    .navy{
        background-color: #1C2EE2;
    }

    .icon-box{
        font-size: 17px;        
        font-weight: 600;        
    }

    .icon-padding{
        padding-right: 5px;        
    }

    .info-box-number{
        font-weight: 600;
        font-size: 25px;
    }

</style>  
@stop

@section('content')
<!-- Content-->
<section>
    
    <!-- Main content -->
    <section class="content" >
    <!-- Info boxes -->
        @if($user->hasAnyAccess(['dashboard.overallcounts', 'admin']))  
        <div class="row stats">
            <div class="col-md-2 col-sm-4 col-xs-12">
                <div class="info-box blue">
                    
                    <div class="info-box-content">
                        <span class="info-box-text icon-box"><i class="icon-padding fa fa-user"></i>Leads</span>
                        <span class="info-box-number">{{$leads['inquiries']}} / {{$leads['amount']}}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                  <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-2 col-sm-4 col-xs-12">
                <div class="info-box yellow">
                    
                    <div class="info-box-content">
                        <span class="info-box-text icon-box"><i class="icon-padding fa fa-file-text"></i>Quotes (Leads)</span>
                        <span class="info-box-number">{{$quotes['quotes']}} / {{$quotes['amount']}}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                  <!-- /.info-box -->
            </div>
            <!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>

            <div class="col-md-2 col-sm-4 col-xs-12">
                <div class="info-box yellow">

                    <div class="info-box-content">
                        <span class="info-box-text icon-box"><i class="icon-padding fa fa-book"></i>Quotes (Dir)</span>
                        <span class="info-box-number">{{$quotes_direct['quotes']}} / {{$quotes_direct['amount']}}</span>
                    </div>
                <!-- /.info-box-content -->
                </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-2 col-sm-4 col-xs-12">
                <div class="info-box green">

                    <div class="info-box-content">
                        <span class="info-box-text icon-box"><i class="icon-padding fa fa-file-pdf-o"></i>Invoices</span>
                        <span class="info-box-number">{{$invoices['invoices']}} / {{$invoices['amount']}}</span>
                    </div>
                <!-- /.info-box-content -->
                </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>

            <div class="col-md-2 col-sm-4 col-xs-12">
                <div class="info-box pink">

                    <div class="info-box-content">
                        <span class="info-box-text icon-box"><i class="icon-padding fa fa-bus"></i>Delivered</span>
                        <span class="info-box-number">{{$invoices_delivered['invoices']}} / {{$invoices_delivered['amount']}}</span>
                    </div>
                <!-- /.info-box-content -->
                </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-2 col-sm-4 col-xs-12">
                <div class="info-box navy">

                    <div class="info-box-content">
                        <span class="info-box-text icon-box"><i class="icon-padding fa fa-money"></i>Collection</span>
                        <span class="info-box-number">{{$collection['pending_collection']}} / {{$collection['total_collection']}}</span>
                    </div>
                <!-- /.info-box-content -->
                </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
        </div>  
        @endif

        @if($user->hasAnyAccess(['dashboard.sector.overoll-performance', 'admin']))  
        <div class="row">
            
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <div class="box box-green">
               
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div id="sector-overoll-perform" style="min-width: 310px; height: 280px; margin: 0 auto"></div>
                        <!-- /.row -->
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>        

            @if($user->hasAnyAccess(['dashboard.sector.performance', 'admin']))
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <div class="box box-green">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div id="overall-sector" style="min-width: 500px; height: 280px; max-width: 500px; margin: 0 auto"></div>
                    </div>
                    
                </div>
            </div>
            @endif
            
        </div>
        @endif

        @if($user->hasAnyAccess(['dashboard.leadtimes', 'admin']))  
        <div class="row">
            
            @if($user->hasAnyAccess(['dashboard.sales.recap', 'admin']))
            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                <div class="box">
                <!-- /.box-header -->
                    <div class="box-body">
                        <div id="monthly-sales-recap" style="min-width: 280px; height: 250px; margin: 0 auto"></div>
                    </div>
                    <!-- ./box-body -->
                    <div class="box-footer">
                        
                        <!-- /.row -->
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /.box -->
            </div>
            @endif

            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <div class="box box-green">
                    <div class="box-header with-border">
                        <h3 class="box-title">Average Lead Times</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <div class="table-responsive" style="height: 248px">
                            <table class="table no-margin">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Milestone</th>
                                        <th class="text-center">Lead Time</th>
                                    </tr>
                                </thead>
                                
                                <tbody>
                                    @if(count($milestone)>0)
                                        @foreach($milestone as $value)
                                            <tr>
                                                <td><strong>{{$value['no']}}</strong></td>
                                                <td><strong>{{$value['name']}}</strong></td>
                                                <td class="text-center"><strong>{{$value['lead']}}</strong></td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                      <!-- /.table-responsive -->
                    </div>
                    <!-- /.box-body -->
                <!-- /.box-footer -->
                </div>
            </div>

        </div>
        @endif        

    </section>
    <!-- !!Main content -->

</section>
<!-- !!!Content -->

@stop
@section('js')
  <!-- CORE JS -->
    <script src="{{asset('assets/front/highchart.js')}}"></script>
    <script src="{{asset('assets/dist/jquery-knob/dist/jquery.knob.min.js')}}"></script>

    <script type="text/javascript">

        perform_chart_overroll      =   {!! json_encode($perform_chart_overroll) !!};
        
        sector_perform_pie_chart    =   {!! json_encode($sector_perform_chart) !!};        

        $(document).ready(function(){
            $(".dial").knob({
                'format' : function (value) {
                    return value + '%';
                }
            });

            $('[data-toggle="popover"]').popover(); 

            @if($user->hasAnyAccess(['dashboard.sales.recap', 'admin'])) 

                dd=[];
                year = (new Date()).getFullYear();

                for (var i = 1 ; i < 13; i++) {
                    dd.push(Date.UTC(year,i));
                }

                $.ajax({
                    url: "{{url('admin/dashboard2/getSalesCap')}}",
                    type: 'GET',
                    data: {'dates': dd},
                    success: function(data) {

                        Highcharts.chart('monthly-sales-recap', {
                            
                            title: {
                                text: '12 Month Sales Recap'
                            },

                            subtitle: {
                                text: year+' Jan To '+year+' Dec'
                            },

                            yAxis: {
                                title: {
                                    text: 'Value'
                                }
                            },
                            legend: {
                                layout: 'vertical',
                                align: 'right',
                                verticalAlign: 'middle'
                            },

                            xAxis: {
                              type: 'datetime',
                              min: Date.UTC(2018, 1),
                              max: Date.UTC(2018, 11),
                              labels: {
                                  step: 1,
                                  style: {
                                      fontSize: '13px',
                                      fontFamily: 'Arial,sans-serif'
                                  }
                              },
                              dateTimeLabelFormats: { // don't display the dummy year
                                  month: '%b \'%y',
                                  year: '%Y'
                              }
                            },
               
                            series: data,

                            responsive: {
                                rules: [{
                                    condition: {
                                        maxWidth: 500
                                    },
                                    chartOptions: {
                                        legend: {
                                            layout: 'horizontal',
                                            align: 'center',
                                            verticalAlign: 'bottom'
                                        }
                                    }
                                }]
                            }
                          });
                    },error: function(data){

                    }
                });

              
            @endif

            @if($user->hasAnyAccess(['dashboard.sector.performance', 'admin']))  
                Highcharts.chart('sector-overoll-perform', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Overoll Sector Performance'
                    },
                    xAxis: {
                        categories: perform_chart_overroll.categories,
                        crosshair: true
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Count'
                        }
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
                        footerFormat: '</table>',
                        shared: true,
                        useHTML: true
                    },
                    plotOptions: {
                        column: {
                            pointPadding: 0.2,
                            borderWidth: 0
                        },series: {
                            borderColor: '#303030'
                        }
                    },
                    series: perform_chart_overroll.series
                });
              
            @endif

            @if($user->hasAnyAccess(['dashboard.sector.performance', 'admin']))  
              
              Highcharts.chart('overall-sector', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                }, 
                legend: {
                    enabled: false
                },
                title: {
                    text: 'Leads Of Sectors'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            }
                        },
                    }
                },
                series: [{
                    name: 'Brands',
                    colorByPoint: true,
                    size: '60%',
                    innerSize: '50%',
                    data: sector_perform_pie_chart
                }]
              });
            @endif            

            
        });
    </script>
  <!-- //CORE JS -->
@stop
