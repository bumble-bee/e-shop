@extends('layouts.back_master_dash') @section('title','Month-Analize')
@section('current_title','Dashboard')

@section('css')
    <link rel="stylesheet" href="{{url('assets/front/css/animate.css')}}">
    <style type="text/css">
        .stats .info-box-icon{
            background: none;
        }

        .stats .info-box-text{
            margin-top: 8px;
        }

        .stats .info-box-number{
            font-weight: 800;
        }

        .sector-performance a{
            padding: 5px 10px !important;
        }

        .dis-excced{
            color: red;
        }

        .btn{
            background: none;
        }

        .box-green{
          border-color: #00a65a;
        }

        .users-list-img {
            border-radius: 50%;
            max-width: 30%;
            height: auto;
        }


        .circle {
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            background-color: white;
            color: #333;
            border-radius: 50%;
            box-sizing: border-box;
            text-align: center;
            display: block;
            padding: 5px;
            &:before {
                content: '';
                display: inline-block;
                height: 100%;
                vertical-align: middle;
                margin-right: -2%;
            }
            border: 5px solid darken(white, 5%);
            -webkit-transition: all 2s ease-out;
            -moz-transition: all 2s ease-out;
            -o-transition: all 2s ease-out;
            transition: all 2s ease-out;
            &:hover {
                background-color: maroon;
                border: 5px solid darken(maroon, 10%);
                color: #fff;
            }
        }
 

        .outline {
            width: 10%;
            border: 5px solid black;
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            border-radius: 50%;
            box-sizing: border-box;
        }

        .box-image{
            width: 125px;
            height: 125px;
            position: absolute;
            bottom: 10px;
            margin: auto;
            left: 0%;
            right: 0%;
        }

        .box-content{
            height: 700px;
            position: relative;
            text-align: center;
        }

        .footer-text{
            position: absolute;
            bottom: 5px;    
            padding-left: 15px;          
        }

        .color-block-dark {
            margin: auto;
            height: 80px;
            width: 100px;
            text-align: center;
            color: #fff;
            padding: 25px;
            background-color: #9933CC !important;
        }

        .warning-color-dark {
            padding: 10px !important;
            background-color: #FF8800 !important;
        }

        .warning-color-light {
            padding: 10px !important;
            background-color: #ffbb33 !important;
        }

        .round-box {
            border-radius: 53px;
        }

        .z-depth-2{
            box-shadow: 0 8px 17px 0 rgba(0,0,0,.2), 0 6px 20px 0 rgba(0,0,0,.19);
        }

        h5{
            font-size: 1.64rem;
            margin: .82rem 0 .656rem;
            font-weight: 800;
        }

        .box-title-text{
            font-size: 2.5rem;
            color: #73879C;
            border-bottom: 3px solid #E6E9ED;
            padding: 1px 5px 6px;
            padding-top: 10px;
            margin-top: 35px;
        }

        .line {
            margin-top:9px;
            width:90px;
            background:blue;
            height:8px;
            float:left;
        }
        .point {    
            width: 0;
            height: 0; 
            border-top: 16px solid transparent;
            border-bottom: 10px solid transparent;
            border-left: 30px solid blue;
            float:right;
        }

        .arrow {
            width:120px;
        }

        .color-box{
            height: 65px !important;
            width: 150px;       
        }
    }

</style>  
@stop

@section('content')
<!-- Content-->
<section>
    <!-- Content Header (Page header) -->
    <!-- <section class="content-header">
        <h1>
            Welcome To
            <small>O|Lead</small>
        </h1> -->
        <!-- <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol> -->
    <!-- </section> -->
    <!-- !!Content Header (Page header) -->

    <!-- Main content -->
    <section class="content" style="padding-top: 0 !important">
    <!-- Info boxes -->

         <div class="row stats">
            <div class="col-lg-2 col-sm-12 col-xs-12" style="padding: 5px !important">
                <div class="info-box box-content" style="text-align: center">
                    <h2 class="box-title-text">LEADS</h2>
                    <br><br>
                    <div class="color-block-dark warning-color-light color-box" style="background-color: #ffffff !important;">
                        <h5>75</h5>
                    </div>
                    <br><br>
                    <div id="leads-chart" style="height: 290px;"></div>
                    
                    <svg width="300" height="100" style="position: absolute;margin: 2% 0 0 -20px;z-index: 1;">
                        <defs>
                            <marker id="arrow" markerWidth="13" markerHeight="13" refx="2" refy="6" orient="auto">
                                <path d="M2,1 L2,10 L10,6 L2,2" style="fill:black;" />
                            </marker>
                        </defs>
                        <path d="M20,50 L150,50" style="stroke:black; stroke-width: 1.25px; fill: none;marker-end: url(#arrow);"/>
                    </svg>

                    <svg width="300" height="100" style="position: absolute;margin: -20% 0 0 -33%;z-index: 1;">
                        <defs>
                            <marker id="arrow" markerWidth="13" markerHeight="13" refx="2" refy="6" orient="auto">
                                <path d="M2,1 L2,10 L10,6 L2,2" style="fill:black;" />
                            </marker>
                        </defs>
                        <path d="M75,0 L75,120" style="stroke:black; stroke-width: 1.25px; fill: none;"/>                            
                    </svg>
                    
                </div>
                  <!-- /.info-box -->
            </div>            

            <div class="col-lg-2 col-sm-12 col-xs-12" style="padding: 5px !important">
                <div class="info-box box-content">
                    <h2 class="box-title-text">QUOTATION</h2>
                    <br><br>
                    <div class="color-block-dark warning-color-light z-depth-2 color-box">
                        <span>FROM DIRECT</span>
                        <h5 id="direct-quotes-count" name="direct-quotes-count">0</h5>                        
                    </div>
                    <svg width="300" height="100" style="position: absolute;margin: 11% 0 0 -33%;z-index: 1;">

                        <defs>
                            <marker id="arrow" markerWidth="13" markerHeight="13" refx="2" refy="6" orient="auto">
                                <path d="M2,1 L2,10 L10,6 L2,2" style="fill:black;" />
                            </marker>
                        </defs>

                        <path d="M75,0 L75,55" style="stroke:black; stroke-width: 1.25px; fill: none;marker-end: url(#arrow);"/>
                            
                    </svg>
                    <br><br><br>
                    <svg width="300" height="100" style="position: absolute;margin: 74px 0 0 60px;z-index: 1;">

                        <defs>
                            <marker id="arrow" markerWidth="13" markerHeight="13" refx="2" refy="6" orient="auto">
                                <path d="M2,1 L2,10 L10,6 L2,2" style="fill:black;" />
                            </marker>
                        </defs>

                        <path d="M20,50 L70,50" style="stroke:black; stroke-width: 1.25px; fill: none;marker-end: url(#arrow);"/>
                            
                    </svg>
                    <div id="quote-chart" style="height: 250px;"></div>
                    <br><br><br>
                    <svg width="300" height="100" style="position: absolute;margin: -40% 0 0 -45%;z-index: 1;">
                        <defs>
                            <marker id="arrow" markerWidth="13" markerHeight="13" refx="2" refy="6" orient="auto">
                                <path d="M2,1 L2,10 L10,6 L2,2" style="fill:black;" />
                            </marker>
                        </defs>
                        <path d="M100,75 L100,10" style="stroke:black; stroke-width: 1.25px; fill: none;marker-end: url(#arrow);"/>
                    </svg>
                    <div class="color-block-dark warning-color-light z-depth-2 color-box" style="background-color: #0091ea !important;">
                        <span>FROM LEAD</span>
                        <h5 id="lead-quotes-count" name="lead-quotes-count">0</h5>
                    </div>    

                    
                </div>
                  <!-- /.info-box -->
            </div>            
            <!-- /.col -->
            <div class="col-lg-2 col-sm-12 col-xs-12" style="padding: 5px !important">
                <div class="info-box box-content">
                    <h2 class="box-title-text">PO</h2>

                    <svg width="300" height="100" style="position: absolute;margin: 177% 0 0 -33%;z-index: 0;">
                        <defs>
                            <marker id="arrow" markerWidth="13" markerHeight="13" refx="2" refy="6" orient="auto">
                                <path d="M2,1 L2,10 L10,6 L2,2" style="fill:black;" />
                            </marker>
                        </defs>
                        <path d="M75,0 L75,120" style="stroke:black; stroke-width: 1.25px; fill: none;"/>                            
                    </svg>
                    <svg width="300" height="100" style="position: absolute;margin: 200% 0 0 -18px;z-index: 1;">
                        <defs>
                            <marker id="arrow" markerWidth="13" markerHeight="13" refx="2" refy="6" orient="auto">
                                <path d="M2,1 L2,10 L10,6 L2,2" style="fill:black;" />
                            </marker>
                        </defs>
                        <path d="M20,50 L150,50" style="stroke:black; stroke-width: 1.25px; fill: none;marker-end: url(#arrow);"/>
                    </svg>
                    <br><br><br><br><br><br><br><br><br><br><br><br><br><br>
                    <div class="color-block-dark warning-color-light z-depth-2 color-box" style="background-color: #9933CC !important">
                        <span>TOTAL PO</span>
                        <h5 id="po-count" name="po-count">0</h5>
                    </div>                    
                    
                    
                </div>
                  <!-- /.info-box -->
            </div>            
            <!-- /.col -->
            <div class="col-lg-2 col-sm-12 col-xs-12" style="padding: 5px !important">
                <div class="info-box box-content">
                <h2 class="box-title-text" >INVOICES</h2>
                    <br><br>
                    <div class="color-block-dark warning-color-light z-depth-2 color-box">
                        <span>FROM DIRECT</span>
                        <h5 id="direct-invoice-count" name="direct-invoice-count">0</h5>
                    </div>
                    <svg width="300" height="100" style="position: absolute;margin: 11% 0 0 -33%;z-index: 1;">

                        <defs>
                            <marker id="arrow" markerWidth="13" markerHeight="13" refx="2" refy="6" orient="auto">
                                <path d="M2,1 L2,10 L10,6 L2,2" style="fill:black;" />
                            </marker>
                        </defs>

                        <path d="M75,0 L75,90" style="stroke:black; stroke-width: 1.25px; fill: none;marker-end: url(#arrow);"/>
                            
                    </svg>
                    <br><br><br><br><br><br><br><br>
                    <div class="color-block-dark warning-color-light z-depth-2 color-box" style="background-color: #1C2331 !important;margin-top: 7px">
                        <span>TOTAL</span>
                        <h5 id="total-invoice-count" name="total-invoice-count">0</h5>
                    </div>
                    <br><br><br><br><br><br>
                    
                    <svg width="300" height="100" style="position: absolute;margin: -35% 0 0 -44%;z-index: 1;">
                        <defs>
                            <marker id="arrow" markerWidth="13" markerHeight="13" refx="2" refy="6" orient="auto">
                                <path d="M2,1 L2,10 L10,6 L2,2" style="fill:black;" />
                            </marker>
                        </defs>
                        <path d="M100,170 L100,10" style="stroke:black; stroke-width: 1.25px; fill: none;marker-end: url(#arrow);"/>
                    </svg>
                    
                    <br><br>
                    <div class="color-block-dark warning-color-light z-depth-2 color-box" style="background-color: #0091ea !important;">
                        <span>FROM PO</span>
                        <h5 id="po-invoice-count" name="po-invoice-count">0</h5>
                    </div>
                    <svg width="300" height="100" style="position: absolute;margin: -130% 0 0 70px;z-index: 1;">

                        <defs>
                            <marker id="arrow" markerWidth="13" markerHeight="13" refx="2" refy="6" orient="auto">
                                <path d="M2,1 L2,10 L10,6 L2,2" style="fill:black;" />
                            </marker>
                        </defs>

                        <path d="M20,50 L65,50" style="stroke:black; stroke-width: 1.25px; fill: none;marker-end: url(#arrow);"/>
                            
                    </svg>
                    
                    
                </div>
                  <!-- /.info-box -->
            </div>            
            <!-- /.col -->
            <div class="col-lg-2 col-sm-12 col-xs-12" style="padding: 5px !important">
                <div class="info-box box-content">
                    <h2 class="box-title-text" >DELIVERY</h2>
                    <br><br>
                    <div class="color-block-dark warning-color-light color-box" style="background-color: #ffffff !important;">
                        <h5>75</h5>
                    </div>
                    <br><br>
                    <div id="delivery-chart" style="height: 290px;"></div>
                    <svg width="300" height="100" style="position: absolute;margin: -90% 0 0 80px;z-index: 1;">

                        <defs>
                            <marker id="arrow" markerWidth="13" markerHeight="13" refx="2" refy="6" orient="auto">
                                <path d="M2,1 L2,10 L10,6 L2,2" style="fill:black;" />
                            </marker>
                        </defs>

                        <path d="M20,50 L40,50" style="stroke:black; stroke-width: 1.25px; fill: none;marker-end: url(#arrow);"/>
                            
                    </svg>
                    
                    
                </div>
                  <!-- /.info-box -->
            </div>            
            <!-- /.col -->
            <div class="col-lg-2 col-sm-12 col-xs-12" style="padding: 5px !important">
                <div class="info-box box-content">
                    <h2 class="box-title-text" >COLLECTION</h2>
                    <br><br>
                    <div class="color-block-dark warning-color-light color-box" style="background-color: #ffffff !important;">
                        <h5>75</h5>
                    </div>
                    <br><br>
                    <div id="collection-chart" style="height: 290px;"></div>
                    
                    
                </div>
                  <!-- /.info-box -->
            </div>            
            <!-- /.col -->
        </div>   

    </section>
    <!-- !!Main content -->

</section>

@stop
@section('js')
    <!-- CORE JS -->

    <!-- <script src="{{asset('assets/dist/flot/jquery.flot.js')}}"></script> -->
    <!-- FLOT RESIZE PLUGIN - allows the chart to redraw when the window is resized -->
    <!-- <script src="{{asset('assets/dist/flot/jquery.flot.resize.js')}}"></script> -->
    <!-- FLOT PIE PLUGIN - also used to draw donut charts -->
    <!-- <script src="{{asset('assets/dist/flot/jquery.flot.pie.js')}}"></script> -->
    <script src="{{asset('assets/dist/amchart/amcharts.js')}}"></script>
    <script src="{{asset('assets/dist/amchart/pie.js')}}"></script>

    <script type="text/javascript">
        $(function () {

            leads               = {!! json_encode($leads) !!};
            quotes              = {!! json_encode($quotes) !!};
            leadQuotesCount     = {!! json_encode($leadQuotesCount) !!};
            directQuotesCount   = {!! json_encode($directQuotesCount) !!};
            poCount             = {!! json_encode($poCount) !!};
            invoices            = {!! json_encode($invoices) !!};

            $("#lead-quotes-count").text(leadQuotesCount.quotes);
            $("#direct-quotes-count").text(directQuotesCount.quotes);
            $("#po-count").text(poCount[0].count);

            total_quotes = leadQuotesCount.quotes+directQuotesCount.quotes;
            total_leads = 0;

            $("#direct-invoice-count").text(invoices[0].ln);
            $("#po-invoice-count").text(invoices[0].olead);
            
            total_invoice = invoices[0].ln+invoices[0].olead;
            $("#total-invoice-count").text(total_invoice);

            $.each(leads,function(key,value){
                total_leads += value.data;
            });

            
            $.ajax({
                url: "{{url('admin/dashboard3/getLeadsData')}}",
                type: 'GET',
                success: function(data) {
                    
                    AmCharts.makeChart("leads-chart", {
                        "type": "pie",
                        "theme": "light",
                        
                        "allLabels": [{
                            "text": total_leads,
                            "align": "center",
                            "bold": true,
                            "y": 130
                        }],
                        "dataProvider": leads,
                        
                        "titleField": "label",
                        "valueField": "data",
                        "colorField": "color",
                        "radius": "27%",
                        "innerRadius": "60%",
                        "labelText": "[[value]]",
                        "fontSize" : "15",
                        "balloon": {
                            "fixedPosition": true
                        }
                    });

                    AmCharts.makeChart("quote-chart", {
                        "type": "pie",
                        "theme": "light",
                        
                        "allLabels": [{
                            "text": total_quotes,
                            "align": "center",
                            "bold": true,
                            "y": 110
                        }],
                        "dataProvider": quotes,
                        
                        "titleField": "label",
                        "valueField": "data",
                        "colorField": "color",
                        "radius": "27%",
                        "innerRadius": "60%",
                        "labelText": "[[value]]",
                        "fontSize" : "15",
                        "balloon": {
                            "fixedPosition": true
                        }
                    });

                    AmCharts.makeChart("delivery-chart", {
                        "type": "pie",
                        "theme": "light",
                        
                        "allLabels": [{
                            "text": "0",
                            "align": "center",
                            "bold": true,
                            "y": 125
                        }],
                        "dataProvider": [{
                            "label": "Delivered",
                            "data": 30,
                            "color": '#00C851'
                        },{
                            "label": "On Delivery",
                            "data": 55,
                            "color": '#FF8800'
                        },{
                            "label": "Pending",
                            "data": 15,
                            "color": 'red'
                        }],
                        
                        "titleField": "label",
                        "valueField": "data",
                        "colorField": "color",
                        "labelRadius": -130,
                        "radius": "27%",
                        "innerRadius": "60%",
                        "labelText": "",
                        "balloon": {
                            "fixedPosition": true
                        }
                    });

                    AmCharts.makeChart("collection-chart", {
                        "type": "pie",
                        "theme": "light",
                        
                        "allLabels": [{
                            "text": "0",
                            "align": "center",
                            "bold": true,
                            "y": 125
                        }],
                        "dataProvider": [{
                            "label": "Delivered",
                            "data": 35,
                            "color": '#00C851'
                        },{
                            "label": "On Delivery",
                            "data": 30,
                            "color": '#FF8800'
                        },{
                            "label": "Pending",
                            "data": 45,
                            "color": 'red'
                        }],
                        
                        "titleField": "label",
                        "valueField": "data",
                        "colorField": "color",
                        "labelRadius": -130,
                        "radius": "27%",
                        "innerRadius": "60%",
                        "labelText": "",
                        "balloon": {
                            "fixedPosition": true
                        }
                    });                    
                }
            });
        });

    </script>
    <!-- //CORE JS -->
@stop
