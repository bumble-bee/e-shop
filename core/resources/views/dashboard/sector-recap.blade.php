@extends('layouts.back_master') @section('title','Dashboard')
@section('current_title','Dashboard')
<link rel="stylesheet" type="text/css" src="{{asset('assets/dist/bootstrap-datepicker/css/bootstrap-datepicker.css')}}"/>
@section('css')
<style type="text/css">
    .stats .info-box-icon{
        background: none;
    }

    .stats .info-box-text{
        margin-top: 8px;
    }

    .stats .info-box-number{
        font-weight: 800;
    }

    .sector-performance a{
        padding: 5px 10px !important;
    }

    .dis-excced{
        color: red;
    }

    .btn{
        background: none;
    }

    .box-green{
      border-color: #00a65a;
    }

    .users-list-img {
        border-radius: 50%;
        max-width: 30%;
        height: auto;
    }

    .info-box-content{
        margin-left: 0px !important;
        color: #73879C;
        padding: 12px 10px;
        text-align: center;
        color: #ffffff;
    }

    .blue{
        background-color: #33B2FF;
    }

    .yellow{
        background-color: #FFC733;
    }

    .green{
        background-color: #239D0D;
    }

    .pink{
        background-color: #96228D;
    }

    .navy{
        background-color: #1C2EE2;
    }

    .icon-box{
        font-size: 17px;        
        font-weight: 600;        
    }

    .icon-padding{
        padding-right: 5px;        
    }

    .info-box-number{
        font-weight: 600;
        font-size: 25px;
    }

</style>  
@stop

@section('content')
<!-- Content-->
<section>
    
    <!-- Main content -->
    <section class="content" >
    <!-- Info boxes -->

        <form action="{{url('admin/sales-recap')}}" method="get">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="box box-green">
                        <div class="box-header">
                            <div class="row form-group">
                                <div class="col-md-4">
                                    <label for="" class="control-label">From</label>
                                    <input id="from" name="from" type="text" class="form-control datepick" value="{{$old['from']}}">
                                </div>
                                <div class="col-md-4">
                                    <label for="" class="control-label">To</label>
                                    <input type="text" class="form-control datepick" name="to" id="to" value="{{$old['to']}}">
                                </div>
                                <div class="col-md-1">
                                    <button class="btn btn-default pull-right" type="submit" style="margin-top: 20px"><i class="fa fa-search"></i> Filter</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
            
        @if($user->hasAnyAccess(['dashboard3.leads-performance', 'admin']))  
        <div class="row">
            
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="box box-green">
                    <div class="box-body">
                        <div id="leads-overoll-perform" style="height: 280px; margin: 0 auto"></div>
                    </div>
                </div>
            </div>
        </div>
        @endif

        @if($user->hasAnyAccess(['dashboard2.quote-performance', 'admin']))  
        <div class="row">
            
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="box box-green">
               
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div id="quote-overoll-perform" style="height: 280px; margin: 0 auto"></div>
                        <!-- /.row -->
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
        @endif

        @if($user->hasAnyAccess(['dashboard2.po-performance', 'admin']))  
        <div class="row">
            
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="box box-green">
               
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div id="po-overoll-perform" style="height: 280px; margin: 0 auto"></div>
                        <!-- /.row -->
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
        @endif

    </section>
    <!-- !!Main content -->

</section>
<!-- !!!Content -->

@stop
@section('js')
  <!-- CORE JS -->
    <script src="{{asset('assets/front/highchart.js')}}"></script>
    <script src="{{asset('assets/front/data.js')}}"></script>
    <script src="{{asset('assets/front/drilldown.js')}}"></script>
    <script src="{{asset('assets/dist/jquery-knob/dist/jquery.knob.min.js')}}"></script>
    <script src="{{asset('assets/dist/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>

    <script type="text/javascript">

        leads_generation_summery    =   {!! json_encode($leads_generation_summery) !!};
        quotes_generation_summery   =   {!! json_encode($quotes_generation_summery) !!};
        po_generation_summery       =   {!! json_encode($po_generation_summery) !!};

        $(document).ready(function(){
            $(".dial").knob({
                'format' : function (value) {
                    return value + '%';
                }
            });

            $('.datepick').datepicker({
                  keyboardNavigation: false,
                  forceParse: false,
                  format: 'yyyy-mm-dd'
              });

            $('[data-toggle="popover"]').popover(); 

            @if($user->hasAnyAccess(['dashboard3.leads-performance', 'admin']))

                Highcharts.setOptions({
                    lang: {
                        drillUpText: '<< back'
                    }
                });

                Highcharts.chart('leads-overoll-perform', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Lead generation in each channel'
                    },
                    subtitle: {
                        text: 'Click the columns to view employee performance.'
                    },
                    xAxis: {
                        type: 'category',
                        title: {
                            text: 'Employees'
                        }
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Total Lead Count'
                        }

                    },
                    legend: {
                        enabled: false
                    },
                    plotOptions: {
                        series: {
                            borderWidth: 0,
                            dataLabels: {
                                enabled: true,
                            }
                        }
                    },

                    tooltip: {
                        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
                    },

                    series: [
                        {
                            "name": "Browsers",
                            "colorByPoint": true,
                            "data": leads_generation_summery.main
                        }
                    ],
                    drilldown: {
                        drillUpButton: {
                            relativeTo: 'spacingBox',
                            position: {
                                y: 0,
                                x: 0
                            },
                            theme: {
                                fill: 'white',
                                'stroke-width': 1,
                                stroke: 'silver',
                                r: 0,
                                states: {
                                    hover: {
                                        fill: '#a4edba'
                                    },
                                    select: {
                                        stroke: '#039',
                                        fill: '#a4edba'
                                    }
                                }
                            }

                        },series: leads_generation_summery.sub
                    }
                });              
            @endif

            @if($user->hasAnyAccess(['dashboard3.quote-performance', 'admin']))

                Highcharts.setOptions({
                    lang: {
                        drillUpText: '<< back'
                    }
                });

                Highcharts.chart('quote-overoll-perform', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Quotes generation in each channel'
                    },
                    subtitle: {
                        text: 'Click the columns to view employee performance.'
                    },
                    xAxis: {
                        type: 'category',
                        title: {
                            text: 'Employees'
                        }
                    },
                    yAxis: {
                        title: {
                            text: 'Total Quotation Count'
                        }

                    },
                    legend: {
                        enabled: false
                    },
                    plotOptions: {
                        series: {
                            borderWidth: 0,
                            dataLabels: {
                                enabled: true,
                            }
                        }
                    },

                    tooltip: {
                        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
                    },

                    series: [
                        {
                            "name": "Browsers",
                            "colorByPoint": true,
                            "data": quotes_generation_summery.main
                        }
                    ],
                    drilldown: {
                        drillUpButton: {
                            relativeTo: 'spacingBox',
                            position: {
                                y: 0,
                                x: 0
                            },
                            theme: {
                                fill: 'white',
                                'stroke-width': 1,
                                stroke: 'silver',
                                r: 0,
                                states: {
                                    hover: {
                                        fill: '#a4edba'
                                    },
                                    select: {
                                        stroke: '#039',
                                        fill: '#a4edba'
                                    }
                                }
                            }

                        },series: quotes_generation_summery.sub
                    }
                });
              
            @endif

            @if($user->hasAnyAccess(['dashboard3.po-performance', 'admin']))

                Highcharts.setOptions({
                    lang: {
                        drillUpText: '<< back'
                    }
                });

                Highcharts.chart('po-overoll-perform', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Purchase Order generation in each channel'
                    },
                    subtitle: {
                        text: 'Click the columns to view employee performance.'
                    },
                    xAxis: {
                        type: 'category',
                        title: {
                            text: 'Employees'
                        }
                    },
                    yAxis: {
                        title: {
                            text: 'Total Purchase Order Count'
                        }

                    },
                    legend: {
                        enabled: false
                    },
                    plotOptions: {
                        series: {
                            borderWidth: 0,
                            dataLabels: {
                                enabled: true,
                            }
                        }
                    },

                    tooltip: {
                        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
                    },

                    series: [
                        {
                            "name": "Browsers",
                            "colorByPoint": true,
                            "data": po_generation_summery.main
                        }
                    ],
                    drilldown: {
                        drillUpButton: {
                            relativeTo: 'spacingBox',
                            position: {
                                y: 0,
                                x: 0
                            },
                            theme: {
                                fill: 'white',
                                'stroke-width': 1,
                                stroke: 'silver',
                                r: 0,
                                states: {
                                    hover: {
                                        fill: '#a4edba'
                                    },
                                    select: {
                                        stroke: '#039',
                                        fill: '#a4edba'
                                    }
                                }
                            }

                        },series: po_generation_summery.sub
                    }
                });
              
            @endif
        });
    </script>
  <!-- //CORE JS -->
@stop
