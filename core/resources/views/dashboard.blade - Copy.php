@extends('layouts.back_master') @section('title','Dashboard')
@section('current_title','Dashboard')

@section('css')
<style type="text/css">
    .stats .info-box-icon{
        background: none;
    }

    .stats .info-box-text{
        margin-top: 8px;
    }

    .stats .info-box-number{
        font-weight: 800;
    }

    .sector-performance a{
        padding: 5px 10px !important;
    }

    .dis-excced{
        color: red;
    }

    .btn{
        background: none;
    }

    .box-green{
      border-color: #00a65a;
    }

    .users-list-img {
        border-radius: 50%;
        max-width: 30%;
        height: auto;
    }

    .info-box-content{
        margin-left: 0px !important;
        color: #73879C;
        padding: 12px 10px;
        text-align: center;
        color: #ffffff;
    }

    .blue{
        background-color: #33B2FF;
    }

    .yellow{
        background-color: #FFC733;
    }

    .green{
        background-color: #239D0D;
    }

    .pink{
        background-color: #96228D;
    }

    .navy{
        background-color: #1C2EE2;
    }

    .icon-box{
        font-size: 17px;        
        font-weight: 600;        
    }

    .icon-padding{
        padding-right: 5px;        
    }

    .info-box-number{
        font-weight: 600;
        font-size: 25px;
    }
</style>  
@stop

@section('content')
<!-- Content-->
<section>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Welcome To
            <small>O|Lead</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>
    <!-- !!Content Header (Page header) -->

    <!-- Main content -->
    <section class="content" >
    <!-- Info boxes -->

        @if($user->hasAnyAccess(['dashboard.overallcounts', 'admin']))  
        <div class="row stats">
            <div class="col-md-2 col-sm-4 col-xs-12">
                <div class="info-box blue">
                    
                    <div class="info-box-content">
                        <span class="info-box-text icon-box"><i class="icon-padding fa fa-user"></i>Leads</span>
                        <span class="info-box-number">{{$leads['inquiries']}} / {{$leads['amount']}}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                  <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-2 col-sm-4 col-xs-12">
                <div class="info-box yellow">
                    
                    <div class="info-box-content">
                        <span class="info-box-text icon-box"><i class="icon-padding fa fa-file-text"></i>Quotes (Leads)</span>
                        <span class="info-box-number">{{$quotes['quotes']}} / {{$quotes['amount']}}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                  <!-- /.info-box -->
            </div>
            <!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>

            <div class="col-md-2 col-sm-4 col-xs-12">
                <div class="info-box yellow">

                    <div class="info-box-content">
                        <span class="info-box-text icon-box"><i class="icon-padding fa fa-book"></i>Quotes (Dir)</span>
                        <span class="info-box-number">{{$quotes_direct['quotes']}} / {{$quotes_direct['amount']}}</span>
                    </div>
                <!-- /.info-box-content -->
                </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-2 col-sm-4 col-xs-12">
                <div class="info-box green">

                    <div class="info-box-content">
                        <span class="info-box-text icon-box"><i class="icon-padding fa fa-file-pdf-o"></i>Invoices</span>
                        <span class="info-box-number">{{$invoices['invoices']}} / {{$invoices['amount']}}</span>
                    </div>
                <!-- /.info-box-content -->
                </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>

            <div class="col-md-2 col-sm-4 col-xs-12">
                <div class="info-box pink">

                    <div class="info-box-content">
                        <span class="info-box-text icon-box"><i class="icon-padding fa fa-bus"></i>Delivered</span>
                        <span class="info-box-number">{{$invoices_delivered['invoices']}} / {{$invoices_delivered['amount']}}</span>
                    </div>
                <!-- /.info-box-content -->
                </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-2 col-sm-4 col-xs-12">
                <div class="info-box navy">

                    <div class="info-box-content">
                        <span class="info-box-text icon-box"><i class="icon-padding fa fa-money"></i>Collection</span>
                        <span class="info-box-number">{{$collection['pending_collection']}} / {{$collection['total_collection']}}</span>
                    </div>
                <!-- /.info-box-content -->
                </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
        </div>  
        @endif

        @if($user->hasAnyAccess(['dashboard.sector.performance', 'admin']))  
        <div class="row">
            
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="box box-green">
               
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div id="sector-overoll-perform" style="min-width: 310px; height: 350px; margin: 0 auto"></div>
                        <!-- /.row -->
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>        
            
        </div>
        @endif

        @if($user->hasAnyAccess(['dashboard.sector.performance', 'admin']))  
        <div class="row">
            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                <div class="box box-green">
               
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div id="sector-wise-user" style="min-width: 310px; height: 350px; margin: 0 auto"></div>
                        <!-- /.row -->
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
         
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <div class="box box-green">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div id="overall-sector" style="min-width: 262px; height: 262px; max-width: 262px; margin: 0 auto"></div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer no-padding sector-performance">
                        <ul class="nav nav-pills nav-stacked">
                            <?php $k=0; ?>
                            @foreach($sector_perform_chart as $value)
                                @if ($k == 3)
                                    <?php break; ?>
                                @endif                                
                                <li><a href="#">{{$value['name']}}<span class="pull-right text-red">{{$value['y']}}</span></a></li>
                                <?php $k++; ?>
                            @endforeach
                        </ul>
                    </div>
                    <!-- /.footer -->
                </div>
            </div>
        </div>
        @endif        

        @if($user->hasAnyAccess(['dashboard.lead.approvals.todo', 'admin']))  
        <div class="row">
            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                
                <div class="box box-green" style="min-height: 385px">
                    <div class="box-header with-border">
                        <h3 class="box-title">Latest Approval Need Leads</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <div class="table-responsive">
                            <table class="table no-margin">
                                <thead>
                                    <tr>
                                        <th style="text-align: left">Lead No</th>
                                        <th style="text-align: left">Project</th>
                                        <th style="text-align: left">Identified By</th>
                                        <th style="text-align: left">Sector</th>
                                        <th style="text-align: center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($leads_approval)>0)
                                        @foreach($leads_approval as $value)
                                        <tr>
                                            <td style="text-align: left"><a href="{{url('admin/inquiry/show')}}/{{$value['id']}}" target="_blank">{{$value['inquiry_code']}}</a></td>
                                            <td style="text-align: left">{{$value['title']}}</td>                    
                                            <td style="text-align: left">{{$value['leadOwner']->first_name}} {{$value['leadOwner']->last_name}}</td>
                                            <td style="text-align: left">{{$value['sectors']->name}}</td>
                                            <td style="text-align: center">
                                                <a href="{{url('admin/inquiry/show')}}/{{$value['id']}}" target="_blank" class="btn btn-default btn-xs"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    @else
                                        <tr><td rowspan="5">No Approved Leads</td></tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix">
                        <a href="{{url('admin/inquiry/list')}}" target="_blank" class="btn btn-sm btn-default btn-flat pull-right">View All</a>
                    </div>
                    <!-- /.box-footer -->
                </div>
            </div>
            
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <div class="box box-green" style="min-height: 385px">
                    <div class="box-header">
                        <i class="ion ion-clipboard"></i>
                        <h3 class="box-title">To Do</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- See dist/js/pages/dashboard.js to activate the todoList plugin -->
                        <div>
                            <!-- Progress bars -->
                            <div class="clearfix">
                                <span class="pull-left">{{$to_do['approve_pending_inquiry'][0]}} Leads to approve</span>
                                <small class="pull-right  text-red">{{$to_do['approve_pending_inquiry'][1]}}</small>
                            </div>
                            <div class="progress xs">
                                <div class="progress-bar progress-bar-red" style="width: {{$to_do['approve_pending_inquiry'][1]}};"></div>
                            </div>

                            <div class="clearfix">
                                <span class="pull-left">{{$to_do['unassigned_inquiry'][0]}} Leads Un-Assigned</span>
                                <small class="pull-right text-green">{{$to_do['unassigned_inquiry'][1]}}</small>
                            </div>
                    
                            <div class="progress xs">
                                <div class="progress-bar progress-bar-green" style="width: {{$to_do['unassigned_inquiry'][1]}};"></div>
                            </div>

                            <div class="clearfix">
                                <span class="pull-left">{{$to_do['approve_pending_quote'][0]}} Quotation to approve</span>
                                <small class="pull-right text-green">{{$to_do['approve_pending_quote'][1]}}</small>
                            </div>
                    
                            <div class="progress xs">
                                <div class="progress-bar progress-bar-green" style="width: {{$to_do['approve_pending_quote'][1]}};"></div>
                            </div>

                            <div class="clearfix">
                                <span class="pull-left">0 Quotation to Invoice</span>
                                <small class="pull-right text-red">0%</small>
                            </div>
                    
                            <div class="progress xs">
                                <div class="progress-bar progress-bar-red" style="width: 0%;"></div>
                            </div>

                            <div class="clearfix">
                                <span class="pull-left">0 Invoice to be delivered</span>
                                <small class="pull-right text-yellow">0%</small>
                            </div>

                            <div class="progress xs">
                                <div class="progress-bar progress-bar-yellow" style="width: 0%;"></div>
                            </div>

                            <div class="clearfix">
                                <span class="pull-left">0 invoice collection pending</span>
                                <small class="pull-right text-yellow">0%</small>
                            </div>

                            <div class="progress xs">
                                <div class="progress-bar progress-bar-yellow" style="width: 0%;"></div>
                            </div>
                        </div>

                    </div>            
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
        @endif

        @if($user->hasAnyAccess(['dashboard.quto.approvals', 'admin']))  
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="box box-green">
                    <div class="box-header with-border">
                        <h3 class="box-title">Latest Approval Need Quotations</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <div class="table-responsive">
                            <table class="table no-margin">
                                <thead>
                                    <tr>
                                        <th>Quotation</th>
                                        <th>Customer</th>
                                        <th>Value</th>
                                        <th class="text-center">Total Items</th>
                                        <th class="text-center">Date</th>
                                        <th class="text-right">Action</th>
                                    </tr>
                                </thead>
                                
                                <tbody>
                                    @if(count($quotes_approval)>0)
                                        @foreach($quotes_approval as $value)
                                            <tr>
                                                <td>
                                                    <a target="_blank" href="{{url('admin/quotation/listdetail')}}/{{$value['id']}}">{{$value['quote_no']}}</a><br>
                                                    <span><strong>{{$value['created_by']}}</strong></span>
                                                </td>
                                                <td>{{$value['customer']}} <br> <small><strong>{{strtoupper($value['vat_type'])}}</strong></small></td>                    
                                                <td>{{number_format($value['value'],2)}}</td>
                                                <td class="text-center">{{$value['items']}} Items</td>
                                                <td class="text-center">{{$value['date']}}</td>
                                                <td class="text-right">
                                                    <a target="_blank" href="{{url('admin/quotation/show')}}/{{$value['id']}}" class="btn btn-default btn-xs"><i class="fa fa-check" aria-hidden="true"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                      <!-- /.table-responsive -->
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix">
                        <a target="_blank" href="{{url('admin/quotation/list')}}" class="btn btn-sm btn-default btn-flat pull-right">View All</a>
                    </div>
                <!-- /.box-footer -->
                </div>
            </div>
        </div>
        @endif

        @if($user->hasAnyAccess(['dashboard.leadtimes', 'admin']))  
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="box box-green">
                    <div class="box-header with-border">
                        <h3 class="box-title">Average Lead Times</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <div class="table-responsive">
                            <table class="table no-margin">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Milestone</th>
                                        <th>Start(A)</th>
                                        <th>End(B)</th>
                                        <th class="text-center">Lead Time(Average Days)</th>
                                    </tr>
                                </thead>
                                
                                <tbody>
                                    @if(count($milestone)>0)
                                        @foreach($milestone as $value)
                                            <tr>
                                                <td><strong>{{$value['no']}}</strong></td>
                                                <td><strong>{{$value['name']}}</strong></td>
                                                <td><strong>{{$value['start']}}</strong></td>
                                                <td><strong>{{$value['end']}}</strong></td>
                                                <td class="text-center"><strong>{{$value->value}}</strong></td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                      <!-- /.table-responsive -->
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix">
                        
                    </div>
                <!-- /.box-footer -->
                </div>
            </div>
        </div>
        @endif

        @if($user->hasAnyAccess(['dashboard.user.ratio', 'admin']))  
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                
                <div class="box box-green">
                    <div class="box-header with-border">
                        <h3 class="box-title">User wise success ratio</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->

                    <div class="box-body no-padding">
                        <div class="table-responsive">
                            <table class="table no-margin">
                                <thead>
                                    <tr>
                                        <th>User</th>
                                        <th >User's Sector</th>
                                        <th >Other Sectors</th>
                                        <th >Overall</th>
                                    </tr>
                                </thead>
                                
                                <tbody>
                                    <tr>
                                        <td ><strong>Isuru Butler</strong><br>100258<br>Total 25 Leads</td>
                                        <td class="text-center">
                                            <div>
                                                <div class="pull-left" style="margin-right:10px">
                                                    <input type="text" data-linecap="round" data-anglearc="250" data-angleoffset="-125" class="dial" value="20" data-width="50" data-height="50" data-fgcolor="#f39c12" data-readonly="true" readonly="readonly"><br>                            
                                                    <a data-toggle="popover" title="Popover Header" data-content="Some content inside the popover">Hospitality <br><small>0.4k</small></a>
                                                </div>
                                                <div class="pull-left"  style="margin-left:10px">
                                                    <input type="text" data-linecap="round"  data-anglearc="250" data-angleoffset="-125" class="dial" value="5" data-width="50" data-height="50" data-fgcolor="#dd4b39" data-readonly="true" readonly="readonly"><br>
                                                    <a data-toggle="popover" title="Popover Header" data-content="Some content inside the popover">Health Care<br><small>1k</small></a>                            
                                                </div>
                                            </div>
                                        </td>                    
                                        
                                        <td class="text-center">
                                            <div>
                                                <div class="pull-left" style="margin-right:10px">
                                                    <input type="text" data-linecap="round" class="dial"  data-anglearc="250" data-angleoffset="-125" value="30" data-width="50" data-height="50" data-fgcolor="#3c8dbc" data-readonly="true" readonly="readonly"><br>
                                                    <a data-toggle="popover" title="Popover Header" data-content="Some content inside the popover">Data center <br><small>0.2k</small></a>                            
                                                </div>                         
                                            </div>
                                        </td>
                                        
                                        <td class="text-center">
                                            <div>
                                                <div class="pull-left">
                                                    <input type="text" data-linecap="round" class="dial"  data-anglearc="250" data-angleoffset="-125" value="25" data-width="50" data-height="50" data-fgcolor="#f39c12" data-readonly="true" readonly="readonly"><br>
                                                    <a data-toggle="popover" title="Popover Header" data-content="Some content inside the popover">Overall <br><small>5k</small></a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td><strong>Veronica perera</strong><br>100258<br>Total 25 Leads</td>
                                        <td class="text-center">
                                            <div>
                                                <div class="pull-left" style="margin-right:10px">
                                                    <input type="text" data-linecap="round" data-anglearc="250" data-angleoffset="-125" class="dial" value="50" data-width="50" data-height="50" data-fgcolor="#00a65a " data-readonly="true" readonly="readonly"><br>
                                                    <a data-toggle="popover" title="Popover Header" data-content="Some content inside the popover">Education<br><small>2k</small></a>                            
                                                </div>
                                                
                                                <div class="pull-left"  style="margin-left:10px">
                                                    <input type="text" data-linecap="round"  data-anglearc="250" data-angleoffset="-125" class="dial" value="50" data-width="50" data-height="50" data-fgcolor="#00a65a " data-readonly="true" readonly="readonly"><br>
                                                    <a data-toggle="popover" title="Popover Header" data-content="Some content inside the popover">Comercial<br><small>0.8k</small></a>  
                            
                                                </div>    
                                            </div>
                                        </td>                    
                                        <td class="text-center">
                                            <div>
                                                <div class="pull-left" style="margin-right:10px">
                                                    <input type="text" data-linecap="round" class="dial"  data-anglearc="250" data-angleoffset="-125" value="4" data-width="50" data-height="50" data-fgcolor="#3c8dbc" data-readonly="true" readonly="readonly"><br>
                                                    <a data-toggle="popover" title="Popover Header" data-content="Some content inside the popover">Data center<br><small>2k</small></a> 
                            
                                                </div>

                                                <div class="pull-left" style="margin-left:10px">
                                                    <input type="text" data-linecap="round" class="dial"  data-anglearc="250" data-angleoffset="-125" value="5" data-width="50" data-height="50" data-fgcolor="#3c8dbc" data-readonly="true" readonly="readonly"><br>

                                                    <a data-toggle="popover" title="Popover Header" data-content="Some content inside the popover">Public<br><small>5k</small></a> 
                            
                                                </div>                          
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <div>
                                                <div class="pull-left">
                                                    <input type="text" data-linecap="round" class="dial"  data-anglearc="250" data-angleoffset="-125" value="60" data-width="50" data-height="50" data-fgcolor="#00a65a" data-readonly="true" readonly="readonly"><br>

                                                    <a data-toggle="popover" title="Popover Header" data-content="Some content inside the popover">overall<br><small>10k</small></a> 
                                                </div>
                                            </div>  
                                        </td>
                                    </tr>


                                    <tr>
                                        <td><strong>James Perise</strong><br>100258<br>Total 25 Leads</td>
                                        <td class="text-center">
                                            <div>
                                                <div class="pull-left" style="margin-right:10px">
                                                    <input type="text" data-linecap="round" data-anglearc="250" data-angleoffset="-125" class="dial" value="8" data-width="50" data-height="50" data-fgcolor="#dd4b39" data-readonly="true" readonly="readonly"><br>

                                                    <a data-toggle="popover" title="Popover Header" data-content="Some content inside the popover">Recedence<br><small>0.05k</small></a> 
                                                </div>
                                            </div>
                                        </td>                    
                                      
                                        <td class="text-center">
                                            <div>
                                                <div class="pull-left" style="margin-right:10px">
                                                    <input type="text" data-linecap="round" class="dial"  data-anglearc="250" data-angleoffset="-125" value="45" data-width="50" data-height="50" data-fgcolor="#3c8dbc" data-readonly="true" readonly="readonly"><br>

                                                    <a data-toggle="popover" title="Popover Header" data-content="Some content inside the popover">Hospitality<br><small>0.05k</small></a> 
                                                </div>                          
                                            </div>
                                        </td>
                                      
                                        <td class="text-center" >
                                            <div>
                                                <div class="pull-left">
                                                    <input type="text" data-linecap="round" class="dial"  data-anglearc="250" data-angleoffset="-125" value="20" data-width="50" data-height="50" data-fgcolor="#dd4b39" data-readonly="true" readonly="readonly"><br>

                                                    <a data-toggle="popover" title="Popover Header" data-content="Some content inside the popover">Overall<br><small>0.8k</small></a> 
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
        @endif

        @if($user->hasAnyAccess(['dashboard.product.moevments', 'admin']))  
        <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <div class="box box-green">  
                    <div class="box-body">
                        <div id="product-sector-wise" style="min-width: 310px; height: 350px; margin: 0 auto"></div>
                    </div>
                </div>
            </div>
            
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <div class="box box-green">  
                    <div class="box-body">
                        <div id="allproduct-sector-wise" style="min-width: 310px; height: 350px; margin: 0 auto"></div>
                    </div>
                </div>
            </div>
        </div>
        @endif

        @if($user->hasAnyAccess(['dashboard.sales.recap', 'admin']))  
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                <!-- /.box-header -->
                    <div class="box-body">
                        <div id="monthly-sales-recap" style="min-width: 280px; height: 280px; margin: 0 auto"></div>
                    </div>
                    <!-- ./box-body -->
                    <div class="box-footer">
                        
                        <!-- /.row -->
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        @endif

    </section>
    <!-- !!Main content -->

</section>
<!-- !!!Content -->

@stop
@section('js')
  <!-- CORE JS -->
    <script src="{{asset('assets/front/highchart.js')}}"></script>
    <script src="{{asset('assets/dist/jquery-knob/dist/jquery.knob.min.js')}}"></script>

    <script type="text/javascript">

        perform_chart_overroll  =   {!! json_encode($perform_chart_overroll) !!};
        perform_chart_user      =   {!! json_encode($perform_chart_user) !!};
        sector_perform_chart    =   {!! json_encode($sector_perform_chart) !!};
        
        $('body').addClass('sidebar-collapse');

        $(document).ready(function(){
            $(".dial").knob({
                'format' : function (value) {
                    return value + '%';
                }
            });

            $('[data-toggle="popover"]').popover(); 

            @if($user->hasAnyAccess(['dashboard.sales.recap', 'admin'])) 

                dd=[];
                year = (new Date()).getFullYear();

                for (var i = 1 ; i < 13; i++) {
                    dd.push(Date.UTC(year,i));
                }

                $.ajax({
                    url: "{{url('admin/dashboard2/getSalesCap')}}",
                    type: 'GET',
                    data: {'dates': dd},
                    success: function(data) {

                        Highcharts.chart('monthly-sales-recap', {
                            title: {
                                text: '12 Month Sales Recap'
                            },

                            subtitle: {
                                text: year+' Jan To '+year+' Dec'
                            },

                            yAxis: {
                                title: {
                                    text: 'Value'
                                }
                            },
                            legend: {
                                layout: 'vertical',
                                align: 'right',
                                verticalAlign: 'middle'
                            },

                            xAxis: {
                              type: 'datetime',
                              min: Date.UTC(2018, 1),
                              max: Date.UTC(2018, 11),
                              labels: {
                                  step: 1,
                                  style: {
                                      fontSize: '13px',
                                      fontFamily: 'Arial,sans-serif'
                                  }
                              },
                              dateTimeLabelFormats: { // don't display the dummy year
                                  month: '%b \'%y',
                                  year: '%Y'
                              }
                            },
               
                            series: data,

                            responsive: {
                                rules: [{
                                    condition: {
                                        maxWidth: 500
                                    },
                                    chartOptions: {
                                        legend: {
                                            layout: 'horizontal',
                                            align: 'center',
                                            verticalAlign: 'bottom'
                                        }
                                    }
                                }]
                            }
                          });
                    },error: function(data){

                    }
                });

              
            @endif

            @if($user->hasAnyAccess(['dashboard.sector.performance', 'admin']))  
              Highcharts.chart('sector-overoll-perform', {
                chart: {
                  type: 'column'
                },
                title: {
                  text: 'Overoll Sector Performance'
                },
                xAxis: {
                  categories: perform_chart_overroll.categories,
                  crosshair: true
                },
                yAxis: {
                  min: 0,
                  title: {
                    text: 'Leads'
                  }
                },
                tooltip: {
                  headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                  pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
                  footerFormat: '</table>',
                  shared: true,
                  useHTML: true
                },
                plotOptions: {
                  column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                  }
                },
                series: perform_chart_overroll.series
              });
              
            @endif

            @if($user->hasAnyAccess(['dashboard.sector.performance', 'admin']))  
              Highcharts.chart('sector-wise-user', {
                chart: {
                  type: 'column'
                },
                title: {
                  text: 'User Wise Sector Performance'
                },
                xAxis: {
                  categories: perform_chart_user.categories,
                  crosshair: true
                },
                yAxis: {
                  min: 0,
                  title: {
                    text: 'Leads'
                  }
                },
                tooltip: {
                  headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                  pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
                  footerFormat: '</table>',
                  shared: true,
                  useHTML: true
                },
                plotOptions: {
                  column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                  }
                },
                series: perform_chart_user.series
              });

              Highcharts.chart('overall-sector', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                }, 
                legend: {
                    enabled: false
                },
                title: {
                    text: 'Leads Of Sectors'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true
                    }
                },
                series: [{
                    name: 'Brands',
                    colorByPoint: true,
                    data: sector_perform_chart
                }]
              });
            @endif            

            @if($user->hasAnyAccess(['dashboard.product.moevments', 'admin']))  
              Highcharts.chart('product-sector-wise', {
                chart: {
                  type: 'column'
                },
                title: {
                  text: 'Top Product Movement Sector Wise'
                },
                xAxis: {
                  categories: [
                    'Hospitatlity',
                    'Health Care',
                    'Education',
                    'Commercial',
                    'Residencial',
                    'Data Center',
                    'Public',
                    'Industrial'
                  ],
                  crosshair: true
                },
                yAxis: {
                  min: 0,
                  title: {
                    text: 'Movement'
                  }
                },
                tooltip: {
                  headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                  pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
                  footerFormat: '</table>',
                  shared: true,
                  useHTML: true
                },
                plotOptions: {
                  column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                  }
                },
                series: [{
                  name: 'X05-0001',
                  data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5]

                }, {
                  name: 'X05-0258',
                  data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3]

                }, {
                  name: 'X05-324424',
                  data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6]

                }, {
                  name: 'AKOYA-2588',
                  data: [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4, 60.4]

                }, {
                  name: 'SINT-2588',
                  data: [12.4, 55.2, 24.5, 79.7, 22.6, 75.5, 57.4, 60.4]

                }]
              });

              Highcharts.chart('allproduct-sector-wise', {
                chart: {
                  type: 'column'
                },
                title: {
                  text: 'Top Product Movement In Sectors'
                },
                xAxis: {
                  categories: [
                   'x05-2588',
                   'x07-0258',
                   'AKOYA-258',
                   'SENTILA-258',
                   'XX-2588'
                  ],
                  crosshair: true
                },
                yAxis: {
                  min: 0,
                  title: {
                    text: 'Movement'
                  }
                },
                tooltip: {
                  headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                  pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
                  footerFormat: '</table>',
                  shared: true,
                  useHTML: true
                },
                plotOptions: {
                  column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                  }
                },
                series: [{
                  name: 'Hospitatlity',
                  data: [49.9, 71.5, 106.4, 129.2, 144.0]

                }, {
                  name: 'Health Care',
                  data: [83.6, 78.8, 98.5, 93.4, 106.0]

                }, {
                  name: 'Education',
                  data: [48.9, 38.8, 39.3, 41.4, 47.0]

                }, {
                  name: 'Commercial',
                  data: [48.9, 38.8, 39.3, 41.4, 47.0]

                }, {
                  name: 'Residencial',
                  data: [48.9, 38.8, 39.3, 41.4, 47.0]

                }, {
                  name: 'Data Center',
                  data: [42.4, 33.2, 34.5, 39.7, 52.6]

                }, {
                  name: 'Public',
                  data: [12.4, 55.2, 24.5, 79.7, 22.6]

                }, {
                  name: 'Industrial',
                  data: [12.4, 55.2, 24.5, 79.7, 22.6]

                }]
              });
            @endif
        });
    </script>
  <!-- //CORE JS -->
@stop
