<?php
namespace App\Classes;

use App\Modules\EmployeeManage\Models\AppUser;
use Core\EmployeeManage\Models\Employee;
use App\Models\InquiryTransaction;
use App\Models\User;

class Functions
{

    //unixTimePhptoJava
    public static function unixTimePhpToJava($time)
    {
        $conTime = $time * 1000;
        return $conTime;
    }

    public static function unixTimeJavaToPhp($time)
    {
        $conTime = $time / 1000;
        return $conTime;
    }

    public static function formatDateToPhp($date)
    {
        $date_format = substr($date, 0, strlen($date) - 3);
        $time_format = substr($date, 13, strlen($date));
        return date("Y-m-d", strtotime(substr($date_format, 0, strlen($date_format) - 8))).' '.date("H:i:s", strtotime($time_format));
    }

     /**
     * user token check
     * @param $token
     * @return count of users
     *
     */
    public static function checkToken($token)
    {
        $appuser = AppUser::where('app_user.token', $token)->toSql();
        
        return count($appuser) > 0; //TRUE OR false
    }

     /**
     * get employee from mobile token
     * @param $token
     * @return object
     *
     */
    public static function getEmployeeFromToken($token)
    {
        $appuser = AppUser::where('token', $token)->first();
        $emp     = null;
        
        if(count($appuser)>0)
        {
            $emp = EmployeeManage::find($appuser->id);
        }

        return $emp;
    }

    public static function getEmployeeRepIds($employee_id){
        try{
            $pro = Employee::find($employee_id)->descendantsAndSelf()->get()->lists('id');
        }catch(\Exception $e){
            $pro = null;
        }
        return $pro;
    }

    //validate input
    public static function is_valid($request)
    {
        $request = trim($request);

        if(!empty($request) && isset($request))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static function set_transation($inquiry_id,$event_id,$reference)
    {
        InquiryTransaction::where('inquiry_id',$inquiry->id)->delete();
                
        $usr=User::find(Sentinel::getUser()->id);

        $trans=InquiryTransaction::create([
            'inquiry_id'    =>  $inquiry_id,
            'event_id'      =>  $event_id,
            'action_by'     =>  $usr->employee_id,
            'reference'     =>  $reference
        ]);

        if($trans){
            return true;
        }else{
            return false;
        }
    }

}