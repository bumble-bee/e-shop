@extends('layouts.back_master') @section('title','Add Employee Type')
@section('css')
<style type="text/css">
	.panel.panel-bordered {
	    border: 1px solid #ccc;
	}

	.btn-primary {
	    color: white;
	    background-color: #005C99;
	    border-color: #005C99;
	}

	.chosen-container{
		font-family: 'FontAwesome', 'Open Sans',sans-serif;
	}

	b, strong {
		font-weight: bold;
	}
	.repeat:hover {
		background-color:#EFEFEF;
	}
	#scrollArea {
		height: 200px;
		overflow: auto;
	}
	.switch.switch-sm{
		width: 30px;
		height: 16px;
	}
	.switch :checked + span {

		border-color: #689A07;
		-webkit-box-shadow: #689A07 0px 0px 0px 21px inset;
		-moz-box-shadow: #2ecc71 0px 0px 0px 21px inset;
		box-shadow: #689A07 0px 0px 0px 21px inset;
		-webkit-transition: border 300ms, box-shadow 300ms, background-color 1.2s;
		-moz-transition: border 300ms, box-shadow 300ms, background-color 1.2s;
		-o-transition: border 300ms, box-shadow 300ms, background-color 1.2s;
		transition: border 300ms, box-shadow 300ms, background-color 1.2s;
		background-color: #689A07;

	}

	.bootstrap-tagsinput.error{
		border-color: #d96557;
	}

	.error{
		color:#ef2a2a;
	}

	.top{
		margin-top: 10px;
	}

</style>
@stop
@section('content')
<section class="content-header">
	<ol class="breadcrumb">
		<li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
	  	<li><a href="javascript:;">Employee Management</a></li>
	  	<li class="active">Add Employee Type</li>
	</ol>
</section>

<section class="content">
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Add Employee Type</h3>
		</div>
		<div class="box-body">
			
			<form role="form" class="form-horizontal form-validation" method="post">
	        {!!Form::token()!!}
			
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="row top">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<label class=" control-label required" >Name</label>
							<input type="text" class="form-control @if($errors->has('name')) error @endif" name="name" placeholder="Employee Type" required value="{{Input::old('name')}}">
	            			@if($errors->has('name'))
	            				<label id="label-error" class="error" for="label">{{$errors->first('name')}}</label>
	            			@endif
						</div>
					</div>

					<div class="row top">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<label class=" control-label required" >Parent</label>
							@if($errors->has('parent'))
								{!! Form::select('parent',$parentList, [],['class'=>'form-control chosen error','style'=>'width:100%;','required','data-placeholder'=>'Choose parent']) !!}
								<label id="label-error" class="error" for="label">{{$errors->first('parent')}}</label>
							@else
								{!! Form::select('parent',$parentList, [],['class'=>'chosen form-control','style'=>'width:100%;','required','data-placeholder'=>'Choose parent']) !!}
							@endif
						</div>
					</div>

					<div class="row top">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<button type="submit" class="pull-right btn btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
						</div>
					</div>
				</div>

			</form>

		</div>
	</div>
</section>
@stop
@section('js')
<script type="text/javascript">
	$(document).ready(function(){
	});
</script>
@stop