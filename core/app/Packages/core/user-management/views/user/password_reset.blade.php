@extends('layouts.back_master') @section('title','Password Reset')
@section('css')
<link rel="stylesheet" href="{{asset('assets/dist/chosen/bootstrap-chosen.css')}}">
<style type="text/css">
	
</style>
@stop
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Web User<small>Management</small></h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li><a href="{{url('user/list')}}">Web User Management</a></li>
		<li class="active"><a href="{{ url('user/password/reset', 10) }}">Password Reset</a></li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<!-- Default box -->
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Password Reset</h3>
			<div class="box-tools pull-right" style="">
                <a href="{{ URL::previous() }}" class="btn btn-info"><span class="fa fa-caret-left"></span></a>
				<!-- <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
				<button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button> -->
			</div>
		</div>
		<div class="box-body">
            <div class="container-fluid">
                <br>
                <form action="{{ url('user/password/reset', ['id' => request()->id]) }}" method="post">
                    {!! Form::token() !!}
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                          
                          <div class="row" style="margin-bottom: 4px;">
                                
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <label class="control-label required">New Password</label>
                                    <input type="password" class="form-control" name="password" id="password" placeholder="Enter Password" value="{{ Input::old('password') }}">
                                    @if($errors->has('password'))
                                        <label id="label-error" class="has-error help-block" for="label">{{$errors->first('password')}}</label>
                                    @endif
                                </div>

                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <label class="control-label required">Confirm Password</label>
                                    <input type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" value="">
                                    @if($errors->has('password_confirmation'))
                                        <label id="label-error" class="error help-block" for="label">{{$errors->first('password_confirmation')}}</label>
                                    @endif
                                </div>

                            </div><!--/.row -->
                            <div class="row">
                                <div class="col-lg-12">
                                    <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-floppy-o"></i> Save</button>
                                </div>
                            </div>
                            <br>
                        </div>
                    </div>
                    
                </form>
            </div>
		</div>
	</div>	
</section>	

@stop
@section('js')


<script src="{{asset('assets/dist/chosen/chosen.jquery.min.js')}}"></script>

<script type="text/javascript">
$(document).ready(function() {
  $(".chosen").chosen();
});
	
</script>
@stop
