<?php
namespace Core\UserManage\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Menu Model Class
 *
 *
 * @category   Models
 * @package    Model
 * @author     Tharindu lakshan <dev.tharindumac@gmail.com>
 * @copyright  Copyright (c) 2015, Tharindu lakshan
 * @version    v1.0.0
 */
class Employee extends Model{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'employee';

    protected $guarded = array('id');


}
