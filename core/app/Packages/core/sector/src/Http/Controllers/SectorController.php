<?php
namespace Core\Sector\Http\Controllers;

use Core\Sector\Models\Sector;

use App\Http\Controllers\Controller;
use Core\Sector\Http\Requests\SectorRequest;
use Illuminate\Http\Request;
use Response;

use Sentinel;

class SectorController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Sector Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('guest');
	}

	/**
	 * Show the sector add screen to the user.
	 *
	 * @return Response
	 */
	public function addView()
	{
		return view( 'sector::sector.add' );
	}

	/**
	 * Add new menu data to database
	 *
	 * @return Redirect to menu add
	 */
	public function add(Request $request)
	{

		$name = $request->get( 'name' );

		$sector = Sector::create([
			'name'	=> $name
		]);

		if($sector){
			return redirect( 'sector/add' )->with([ 'success' => true,
				'success.message' => 'Sector added successfully!',
				'success.title'   => 'Well Done!' ]);			
		}else{
			return redirect( 'sector/add' )->with([ 'error' => true,
				'error.message' => 'Sector Faild!',
				'error.title'   => 'Oops !' ]);			
		}

	}

	/**
	 * Show the sector add screen to the user.
	 *
	 * @return Response
	 */
	public function listView()
	{

		$sectors=Sector::whereNull('deleted_at')->paginate(10);

		return view( 'sector::sector.list' )->with(['sectors'=>$sectors]);
	}

	/**
	 * Show the sector edit screen to the user.
	 *
	 * @return Response
	 */
	public function editView($id)
	{
		$sector = Sector::find($id);

		if($sector){
			return view( 'sector::sector.edit' )->with([ 'sector' => $sector]);
		}else{
			return view( 'errors.404' );
		}
	}

	/**
	 * Add new sector data to database
	 *
	 * @return Redirect to sector add
	 */
	public function edit(Request $request, $id)
	{
		$sector = Sector::find($id);

		$sector->name	= $request->get( 'name' );
		$sector->description	= $request->get( 'description' );

		$bool=$sector->save();

		if($bool){
			return redirect( 'sector/list' )->with([ 'success' => true,
				'success.message'=> 'Sector updated successfully!',
				'success.title' => 'Good Job!' ]);			
		}else{
			return redirect( 'sector/edit/'.$id )->with([ 'error' => true,
				'error.message'=> 'Sector update fail!',
				'error.title' => 'Oops !' ]);
		}

	}
}
