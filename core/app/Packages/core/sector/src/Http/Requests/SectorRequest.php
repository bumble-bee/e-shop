<?php
namespace Core\Sector\Http\Requests;

use App\Http\Requests\Request;

class SectorRequest extends Request {

	public function authorize(){
		return true;
	}

	public function rules(){

		$id=$this->id;

		if($this->is('sector/add')){
			
			$rules = ['name' => 'required|unique:sector,name,'.$this->name];

		}else if($this->is('sector/edit/'.$id)){
			
			$rules = ['name' => 'required|unique:sector,name,'.$id];

		}
		
		return $rules;
	}

}
