<?php
/**
 * SECTOR MANAGEMENT ROUTES
 *
 * @version 1.0.0
 * @author Sriya <csriyarathne@gmail.com>
 * @copyright 2015 Yasith Samarawickrama
 */

/**
 * SECTOR MIDDLEWARE
 */
Route::group(['middleware' => ['auth']], function()
{
    Route::group(['prefix' => 'sector/', 'namespace' => 'Core\Sector\Http\Controllers'], function(){
      /**
       * GET Routes
       */
      Route::get('add', [
        'as' => 'sector.add', 'uses' => 'SectorController@addView'
      ]);

      Route::get('list', [
        'as' => 'sector.list', 'uses' => 'SectorController@listView'
      ]);

      Route::get('edit/{id}', [
        'as' => 'sector.edit', 'uses' => 'SectorController@editView'
      ]);

      /**
       * POST Routes
       */
      Route::post('add', [
        'as' => 'sector.add', 'uses' => 'SectorController@add'
      ]);

      Route::post('edit/{id}', [
        'as' => 'sector.edit', 'uses' => 'SectorController@edit'
      ]);
      
    });
});