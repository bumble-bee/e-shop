@extends('layouts.back_master') @section('title','Sectors List')
@section('css')
<link rel="stylesheet" href="{{asset('assets/dist/datatables/dataTables.bootstrap.css')}}">
<style type="text/css">

</style>
@stop
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
	Sector 
	<small> Management</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Sector Management</a></li>
		<li class="active">Sector List</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<!-- Default box -->
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Sector List</h3>
			<div class="box-tools pull-right">
				<!-- <a href="{{url('permission/add')}}" class="btn btn-success btn-sm" style="    margin-top: 2px;">Add Permission</a> -->
			</div>
		</div>
		<div class="box-body">
			<div class="table-wrapper">
				<table class="table">
	              	<thead>
		                <tr>
		                  	<th rowspan="2" class="text-left" width="4%">#</th>
		                  	<th rowspan="2" class="text-left" style="font-weight:normal;">Name</th>
		                  	<th rowspan="2" class="text-left" style="font-weight:normal;">Description</th>
		                  	<th rowspan="2" class="text-left" width="4%" style="font-weight:normal;">Action</th>
		                </tr>
	              	</thead>
	              	<tbody>
	              	@if(sizeof($sectors) > 0)
		              	<?php $i=1; ?>
		              	@foreach($sectors as $sector)
		              		<tr>
		              			<td class="text-left">{{$i}}</td>
		              			<td class="text-left">{{$sector->name}}</td>
		              			<td class="text-left">{{$sector->description}}</td>
		              			<td class="text-center">
		              				@if($user->hasAnyAccess(['admin','sector.edit']))
										<a href="{{ url('sector/edit')}}/{{$sector->id}}" class="btn btn-xs btn-default">
											<span class="fa fa-pencil" style="color: blue"></span>
										</a>
									@else
										<a class="btn btn-xs btn-default disabled">
											<span class="fa fa-pencil"></span>
										</a>
									@endif
		              			</td>
		              		</tr>
		              		<?php $i++; ?>
		              	@endforeach

	              	@else
						<tr>
							<td colspan="10" class="text-center">
								<h4>No Data!</h4>
								<h6>Nothing to display. No data found.</h6>
							</td>
						</tr>
              		@endif

	              	</tbody>
	            </table>
	            <div class="row">
					<div class="col-md-12 text-right">
						<?php echo $sectors->render(); ?>
					</div>
				</div>
            </div>
		</div><!-- /.box-body -->
	</div><!-- /.box -->
</section><!-- /.content -->


@stop
@section('js')

<!-- datatables -->
<script src="{{asset('assets/dist/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/dist/datatables/dataTables.bootstrap.min.js')}}"></script>

<script type="text/javascript">
	var id = 0;
	var table = '';
	$(document).ready(function(){
		
	});
</script>
@stop
