@extends('layouts.back_master') @section('title','Add Sector')
@section('css')
<style type="text/css">
	.panel.panel-bordered {
	    border: 1px solid #ccc;
	}

	.btn-primary {
	    color: white;
	    background-color: #005C99;
	    border-color: #005C99;
	}

	.chosen-container{
		font-family: 'FontAwesome', 'Open Sans',sans-serif;
	}
</style>
@stop
@section('content')
<ol class="breadcrumb">
	<li>
    	<a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a>
  	</li>
  	<li>
    	<a href="javascript:;">Sector Management</a>
  	</li>
  	<li class="active">Add Sector</li>
</ol>
<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-bordered">
      		<div class="panel-heading border">
        		<strong>Add Sector</strong>
      		</div>
          	<div class="panel-body">
          		<form role="form" class="form-horizontal form-validation" method="post">
          		{!!Form::token()!!}
          			<div class="form-group">
	            		<label class="col-sm-2 control-label required">Sector</label>
	            		<div class="col-sm-10">
	            			<input type="text" class="form-control @if($errors->has('name')) error @endif" name="name" placeholder="Name of Sector" required value="{{Input::old('name')}}">
	            			@if($errors->has('name'))
	            				<label id="label-error" class="error" for="label">{{$errors->first('name')}}</label>
	            			@endif
	            		</div>
	                </div>

	                <div class="form-group">
	            		<label class="col-sm-2 control-label">Description</label>
	            		<div class="col-sm-10">
	            			<textarea type="text" class="form-control @if($errors->has('description')) error @endif" name="description" placeholder="Description of sector">{{Input::old('description')}}</textarea>
	            			@if($errors->has('description'))
	            				<label id="label-error" class="error" for="label">{{$errors->first('description')}}</label>
	            			@endif
	            		</div>
	                </div>

	                <div class="pull-right">
	                	<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
	                </div>
            	</form>
          	</div>
        </div>
	</div>
</div>
@stop
@section('js')

<script type="text/javascript">
	$(document).ready(function(){
	});
</script>
@stop
