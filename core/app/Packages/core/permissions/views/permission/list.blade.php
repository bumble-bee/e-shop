@extends('layouts.back_master') @section('title','Permission List')
@section('css')
<link rel="stylesheet" href="{{asset('assets/dist/datatables/dataTables.bootstrap.css')}}">

<style type="text/css">
	

</style>
@stop
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
	Permission 
	<small> Management</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li class="active">Permission List</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<!-- Default box -->
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Permission List</h3>
			<div class="box-tools pull-right">
				<!-- <a href="{{url('permission/add')}}" class="btn btn-success btn-sm" style="    margin-top: 2px;">Add Permission</a> -->
			</div>
		</div>
		<div class="box-body">
			<div class="table-wrapper">
				<table class="table table-bordered bordered table-striped table-condensed datatable">
	              	<thead>
		                <tr>
		                  	<th rowspan="2" class="text-center" width="4%">#</th>
		                  	<th rowspan="2" class="text-center" style="font-weight:normal;">Name</th>
		                  	<th rowspan="2" class="text-center" style="font-weight:normal;">Description</th>
		                  	<th rowspan="2" class="text-center" width="6%" style="font-weight:normal;">Status</th>
		                  	<th colspan="2" class="text-center" width="4%" style="font-weight:normal;">Action</th>
		                </tr>
		                <tr style="display: none;">
		                	<th style="display: none;" width="2%"></th>
		                	<th style="display: none;" width="2%"></th>
		                </tr>
	              	</thead>
	            </table>
            </div>
		</div><!-- /.box-body -->
	</div><!-- /.box -->
</section><!-- /.content -->


@stop
@section('js')

<!-- datatables -->
<script src="{{asset('assets/dist/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/dist/datatables/dataTables.bootstrap.min.js')}}"></script>

<script type="text/javascript">
	var id = 0;
	var table = '';
	$(document).ready(function(){
		table = generateTable('.datatable', '{{url('permission/json/list')}}',[3,4,5],[0,3,4,5],[],[],[0,"asc"]);

		table.on('draw.dt',function(){
			$("[data-toggle=tooltip]").tooltip();

			$('.permission-activate').change(function(){
				if($(this).prop('checked')==true){
					ajaxRequest( '{{url('permission/status')}}' , { 'id' : $(this).val() , 'status' : 1 }, 'post', successFunc);
				}else{
					ajaxRequest( '{{url('permission/status')}}' , { 'id' : $(this).val() , 'status' : 0 }, 'post', successFunc);
				}
			});

			$('.permission-delete').click(function(e){
				e.preventDefault();
				id = $(this).data('id');
				sweetAlertConfirm('Delete Permission', 'Are you sure?',2, deleteFunc);
			});
		});
	});

	/**
	 * Delete the menu
	 * Call to the ajax request menu/delete.
	 */
	function deleteFunc(){
		ajaxRequest( '{{url('permission/delete')}}' , { 'id' : id  }, 'post', handleData);
	}

	/**
	 * Delete the menu return function
	 * Return to this function after sending ajax request to the menu/delete
	 */
	function handleData(data){
		if(data.status=='success'){
			sweetAlert('Delete Success','Record Deleted Successfully!',0);
			table.ajax.reload();
		}else if(data.status=='invalid_id'){
			sweetAlert('Delete Error','Permission Id doesn\'t exists.',3);
		}else{
			sweetAlert('Error Occured','Please try again!',3);
		}
	}

	function successFunc(data){
		table.ajax.reload();
	}
</script>
@stop
