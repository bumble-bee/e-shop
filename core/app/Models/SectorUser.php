<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SectorUser extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sector_user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];


    public function sector()
    {
         return $this->belongsTo('App\Models\Sector','sector_id','id');
    }
}
