<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Inquiry extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'inquiries';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public function sectors(){
        return $this->belongsTo('Core\Sector\Models\Sector','sector','id');
    }

    public function leadOwner(){
        return $this->belongsTo('Core\EmployeeManage\Models\Employee','created_by','id');
    }

    public function source(){
        return $this->belongsTo('App\Models\Source','source_id','id');
    }

    public function transaction(){
        return $this->hasMany('App\Models\InquiryTransaction','id','transaction_id')->orderBy('created_at','DESC')->withTrashed();
    }

    public function transactionInfo(){
        return $this->hasMany('App\Models\InquiryTransaction')->whereIn('event_id',[1,5,6])->orderBy('created_at','ASC')->withTrashed();
    }

    public function activeTransaction(){
        return $this->hasOne('App\Models\InquiryTransaction')->orderBy('created_at','DESC')->where('event_id',5)->with(['inquriyAssign.employee.type']);
    }

    public function image(){
        return $this->hasMany('App\Models\Image','inquiry_id','id');
    }

    public function quotationSubmission(){
        return $this->hasMany('App\Models\Quotation','inquiry_id','id');
    }
}
