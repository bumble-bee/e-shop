<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Quotation extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    
    protected $table = 'quotation';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];


    public function customer(){
        return $this->belongsTo('App\Modules\AdminCustomerManage\Models\Customer', 'customer_id', 'id');
    }

    public function details(){
        return $this->hasMany('App\Models\QuotationDetails', 'quotation_id', 'id');
    }

    public function createdBy(){
        return $this->belongsTo('Core\EmployeeManage\Models\Employee', 'action_by', 'id');
    }

    public function inquiry(){
        return $this->belongsTo('App\Models\Inquiry', 'inquiry_id', 'id');
    }

    public function customerPo(){
        return $this->hasOne('App\Modules\AdminQuotationManage\Models\CustomerPo', 'quotation_id', 'id')
                ->whereNull('ended_at');
    }

}
