<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'invoice';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public function details(){
        return $this->hasMany('App\Models\InvoiceDetail','invoice_id','id');
    }

    public function customer(){
        return $this->belongsTo('App\Modules\AdminCustomerManage\Models\Customer','customer_id','id');
    }
}
