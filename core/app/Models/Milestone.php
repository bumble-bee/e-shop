<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Milestone extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'milestone';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public function detail(){
        return $this->hasMany('App\Models\MilestoneDetail','milestone_id','id');
    }
}
