<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Permission Model Class
 *
 *
 * @category   Models
 * @package    Model
 * @author     Chandimal <chandimal@craftbyorange.com>
 * @copyright  Copyright (c) 2015, Chathura Chandimal
 * @version    v1.0.0
 */
class MetaData extends Model{

	protected $table = 'metadata';

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $guarded = ['id'];

}

