<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TargetCollection extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'target_collection';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];


}
