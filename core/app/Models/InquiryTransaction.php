<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InquiryTransaction extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'inquiry_transaction';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public function inquiry(){
        return $this->belongsTo('App\Models\Inquiry','inquiry_id','id');
    }

    public function users(){
        return $this->belongsTo('Core\EmployeeManage\Models\Employee','action_by','id');
    }

    public function events(){
        return $this->belongsTo('App\Models\Events','event_id','id');
    }

    public function eventInfo(){
        return $this->belongsTo('App\Models\Events','event_id','id');
    }

    public function inquriyAssign(){
        return $this->belongsTo('App\Models\InquiryUser','id','transaction_id')->withTrashed();
    }
}
