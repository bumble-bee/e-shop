<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalesOffice extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    
    protected $table = 'sales_office';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];


    public function product(){
        return $this->hasMany('App\Modules\AdminProductManage\Models\Product', 'sales_office_id', 'id');
    }

}
