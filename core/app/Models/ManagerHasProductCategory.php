<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ManagerHasProductCategory extends Model
{
    /**
     * table row delete
     */
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    
    protected $table = 'manager_has_product_category';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $fillable = ['employee_id','product_category_id', 'ended_at', 'created_at'];

    /**
     * get employee details
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function employee()
    {
        return $this->belongsTo('Core\EmployeeManage\Models\Employee','employee_id','id');
    }

    /**
     * get product category details
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function productCategory()
    {
        return $this->belongsTo('App\Modules\AdminProductCategory\Models\AdminProductCategory','product_category_id','id');
    }

}
