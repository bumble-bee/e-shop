<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MilestoneDetail extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'milestone_detail';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];
}
