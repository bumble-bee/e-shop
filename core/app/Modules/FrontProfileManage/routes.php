<?php

Route::group(['middleware' => ['auth.front']], function()
{	
	Route::group(['prefix'=>'/','namespace' => 'App\Modules\FrontProfileManage\Controllers'],function() {

	    Route::get('profile', [
	      'as' => 'front.profile', 'uses' => 'FrontProfileManageController@index'
	    ]);	
	    
	});

    
});
