
@extends('layouts.back_master') @section('title','List Product')
@section('css')
  <link rel="stylesheet" href="{{asset('assets/dist/chosen/bootstrap-chosen.css')}}">
  <style type="text/css">
    table .btn{
      padding: 2px 6px;
    }

    .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
      padding-top:5px;
      padding-bottom:5px;
    }
  </style>
@stop
@section('content')
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Mail<small>Management</small></h1>
    <ol class="breadcrumb">
      <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
      <li class="active">Mail Management</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">List Mail</h3>
      </div>
      <br>
      <div class="box-body">

        <form method="get">
          <div class="row form-group">
            <div class="col-sm-1">
              <label for="" class="control-label">From</label>
            </div>
            <div class="col-sm-3">
              <input type="text" class="form-control datepick" name="from" id="from" value="@if(isset($from)){{$from}}@else{{date('Y-m-01')}}@endif">
            </div>
            <div class="col-sm-1">
              <label for="" class="control-label">To</label>
            </div>
            <div class="col-sm-3">
              <input type="text" class="form-control datepick" name="to" id="to" value="@if(isset($to)){{$to}}@else{{date('Y-m-31')}}@endif">
            </div>
            <div class="col-md-4">
              <button class="btn btn-default pull-right" type="submit"><i class="fa fa-search"></i> Filter</button>
            </div>
          </div>
        </form>

        <table class="table table-bordered">
          <thead>
          <tr class="table-header">
            <th rowspan="2" class="text-center">#</th>
            <th rowspan="2" class="text-center">From</th>
            <th rowspan="2" class="text-center">To</th>
            <th rowspan="2" class="text-center">Created_at</th>
            <th rowspan="2" class="text-center">Title</th>
            <th rowspan="2" class="text-center">Message</th>
            <th colspan="1" class="text-center">Action</th>
          </tr>
          </thead>
          <tbody>
          @if(count($data) > 0)
            @foreach($data as $key => $val)
              <tr>
                <td style="text-align: center">{{$key + 1}}</td>
                <td style="text-align: left">{{$val->from_email}}</td>
                <td style="text-align: left">{{$val->sender_email}}</td>
                <td style="text-align: left">{{$val->created_at}}</td>
                <td style="text-align: left">{{$val->title}}</td>
                <td style="text-align: left">{{$val->message}}</td>
                <td style="text-align: center">
                  <a title="Attachment" class="btn btn-xs btn-default" href="{{ url('/'.$val->attachment)}}">
                    <span class="fa fa-folder-open"></span>
                  </a>
                </td>
              </tr>
            @endforeach
          @else
            <tr>
              <td colspan="9" align="center"> - No Data to Display - </td>
            </tr>
          @endif
          </tbody>
        </table>
        <div class="row">
          <div class="col-md-12 text-right">
              <?php echo $data->render(); ?>
          </div>
        </div>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </section><!-- /.content -->

@stop
@section('js')

  <script src="{{asset('assets/dist/chosen/chosen.jquery.min.js')}}"></script>
  <script src="{{asset('assets/dist/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>

  <script type="text/javascript">
      $(document).ready(function() {
          $('.datepick').datepicker({
              keyboardNavigation: false,
              forceParse: false,
              format: 'yyyy-mm-dd',
          });
      });
  </script>
@stop

