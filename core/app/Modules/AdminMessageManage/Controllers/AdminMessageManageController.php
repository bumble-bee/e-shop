<?php namespace App\Modules\AdminMessageManage\Controllers;


/**
* Controller class
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\AdminMessageManage\Models\AdminMessageManage;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Null_;
use DB;

class AdminMessageManageController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
        $from = $request->from;
        $to = $request->to;
        if($from == ""){
            $from = date('Y-m-01');
        }

        if($to == ""){
            $to = date('Y-m-d');
        }

        $data = AdminMessageManage::orderBy('created_at','Asc');

        if(!empty($from) && !empty($to)){
            $data->whereBetween(DB::raw('DATE_FORMAT(created_at, \'%Y-%m-%d\')'),[$from, $to]);
        }
        $data = $data->paginate(5);
		return view("AdminMessageManage::index")->with(['data'=>$data]);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function composeView()
	{	
		

		return view("AdminMessageManage::compose");
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function readView($id)
	{
		
		$mailbox = new Mailbox('{imap.gmail.com:993/imap/ssl}INBOX', 'help.oticket', 'orange@it', __DIR__);
		$total = $mailbox->getTotal();
		 $mail = $mailbox->getMail($id);
		return json_encode($mail);
		return view("AdminMessageManage::read")->with(['total'=>$total,'mail'=>$mail]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
