<?php


Route::group(['middleware' => ['auth']], function(){

	Route::group(array('prefix'=>'admin/quotation/','module' => 'AdminQuotationManage', 'namespace' => 'App\Modules\AdminQuotationManage\Controllers'), function() {

		/*** GET Routes**/

		Route::get('list', [
	      'as' => 'quotation.list', 'uses' => 'AdminQuotationManageController@listView'
	    ]);

	    Route::get('create', [
	      'as' => 'quotation.create', 'uses' => 'AdminQuotationManageController@createView'
	    ]);

	    Route::get('approval', [
	      'as' => 'quotation.approval', 'uses' => 'AdminQuotationManageController@approveView'
	    ]);

	    Route::get('quotation_pdf/{id}', [
	      'as' => 'quotation.view', 'uses' => 'AdminQuotationManageController@quotationPdf'
	    ]);

	    Route::get('po/{id}', [
	      'as' => 'quotation.po', 'uses' => 'AdminQuotationManageController@poView'
	    ]);

	    Route::get('po/attachments/{id}', [
	      'as' => 'quotation.po', 'uses' => 'AdminQuotationManageController@poAttachmentsView'
	    ]);

	    Route::get('performa_pdf/{id}', [
	      'as' => 'quotation.view', 'uses' => 'AdminQuotationManageController@performaPdf'
	    ]);

		Route::get('listdetail/{id}', [
		  'as' => 'quotation.list.detail', 'uses' => 'AdminQuotationManageController@listViewDetail'
	    ]);

	    Route::get('view/{id}', [
		  'as' => 'quotation.view', 'uses' => 'AdminQuotationManageController@quotationView'
	    ]);

		Route::get('show/{id}', [
            'as' => 'quotation.approval', 'uses' => 'AdminQuotationManageController@show' 
        ]);

		/***JSON Routes**/
        Route::get('getItems', [
            'as' => 'quotation.create', 'uses' => 'AdminQuotationManageController@getItems' 
        ]);

        Route::get('getCustomers', [
            'as' => 'quotation.create', 'uses' => 'AdminQuotationManageController@getCustomers' 
        ]);

		/***POST Routes**/

        Route::post('edit', [
        	'as' => 'quotation.edit', 'uses' => 'AdminQuotationManageController@edit' 
        ]);

        Route::post('create', [
        	'as' => 'quotation.create', 'uses' => 'AdminQuotationManageController@create' 
        ]);
			
	}); 

}); 