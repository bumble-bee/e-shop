<?php namespace App\Modules\AdminQuotationManage\Controllers;


/**
* Controller class
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Modules\AdminQuotationManage\BusinessLogics\QuotationLogic;
use App\Models\Vat;
use App\Models\User;
use Sentinel;
use PDF;
use App\Models\Quotation;
use App\Models\QuotationDetails;

class AdminQuotationManageController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	protected $quotationLogic;

	public function __construct(QuotationLogic $quotationLogic){
		$this->quotationLogic = $quotationLogic;
	}

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listView(Request $request)
	{
		$old = $request;

		$action_by = $this->quotationLogic->getEmployees();

		$data = $this->quotationLogic->getQuotationRecords($old);

		return view("AdminQuotationManage::list", compact('data','old','action_by'));
	}

	public function approveView(Request $request)
	{
		$old = $request;
		$action_by = $this->quotationLogic->getEmployees();
		$data = $this->quotationLogic->getApprovalQuotations($old);
		return view("AdminQuotationManage::approval", compact('data','old','action_by'));
	}


	public function createView(Request $request)
	{
		$delivery_terms = $this->quotationLogic->getDeliveryTerms();
		$vat = $this->quotationLogic->getCurrentVat()[0];
		$nbt = $this->quotationLogic->getCurrentNbt()[0];
		$user = User::with('emp')->find(Sentinel::getUser()->id);

		return view("AdminQuotationManage::create",compact('delivery_terms','vat','nbt','user'));
	}

	public function quotationPdf(Request $request,$id)
	{
		return  $this->quotationLogic->getQuotationPdfOutput($id);
	}

	public function poView(Request $request,$id)
	{
		$list  = $this->quotationLogic->getQuotationById($id);
		$po_list  = $this->quotationLogic->getUploadedPOList($id);
		return view("AdminQuotationManage::po",compact('list','po_list'));
	}

	public function poAttachmentsView(Request $request,$id)
	{
		$attachments  = $this->quotationLogic->getPoAttachmets($id);
		$list  = $this->quotationLogic->getQuotationById($id);
		$po  = $this->quotationLogic->getPoById($id);
		return view("AdminQuotationManage::attachments",compact('attachments','list','po'));
	}

	public function performaPdf(Request $request,$id)
	{
		return  $this->quotationLogic->getPerfromaPdfOutput($id);
	}

	public function create(Request $request)
	{
		$quotation = $this->quotationLogic->createNewQuotation($request);
		$url = url('admin/quotation/quotation_pdf/')."/".$quotation->id;
		return redirect('admin/quotation/create')->with([
			'link'           => true,
			'link.link'      => $url,
			'link.linktitle' => ' Show Quotation',
			'link.message'   => 'New Quotation saved! #('.$quotation->reference_no.')',
			'link.title'     => 'Success..!'
        ]);

	}

	public function listViewDetail($id, Request $request)
	{
		$id        = $request->id;
		$details   = $this->quotationLogic->getQuotationDetailRecords($id);
		$list      = $this->quotationLogic->getQuotationById($id);

		if($list->vat_type == 'vat' || $list->vat_type == 'svat'){
			$vat = Vat::where('id', $list->vat_id)->first();
			$list = $this->quotationLogic->calculateVatCustomerQuotation($list);
		}else{
			$list = $this->quotationLogic->calculateNonVatCustomerQuotation($list);
			$vat = null;
		}

		return view("AdminQuotationManage::details", compact('details', 'list', 'vat'));
	}

	public function show($id, Request $request)
	{
		$user_role = $this->quotationLogic->getUserRole();
		$max_discount = ($user_role)?$user_role->discount:0;
		/*$quotation = $this->quotationLogic->getRecordsQuotationId($id);

		$quotation_detail = $this->quotationLogic->getRecordsQuotationDetailId($id);
		
		$inquiry_id = $quotation->inquiry_id;
		$data = $this->quotationLogic->getInquiryData($inquiry_id);*/

		$id      = $request->id;
		$details = $this->quotationLogic->getQuotationDetailRecords($id);
		$list    = $this->quotationLogic->getQuotationById($id);

		if($list->quotation_status != PENDING || 
			$max_discount < 1 || 
			$list->approval_role_id != $user_role->id){
			return view('errors.404');
		}

		$vat = Vat::where('id', $list->vat_id)->first();
		$nbt = Vat::where('id', $list->nbt_id)->first();

		if($list->vat_type == 'vat' || $list->vat_type == 'svat'){
			$list = $this->quotationLogic->calculateVatCustomerQuotation($list);
		}else{
			$list = $this->quotationLogic->calculateNonVatCustomerQuotation($list);
			//$vat = null;
		}

		//return $list;

		/*return view("AdminQuotationManage::approvaldetail", compact('data','quotation','quotation_detail','max_discount'));*/

		return view("AdminQuotationManage::approve_view", compact('details', 'list', 'vat', 'max_discount', 'nbt'));
	}

	public function edit(Request $request)
	{
		if($request->aprove){
			try {
		        $booh = $this->quotationLogic->aproveQuotationDiscount($request);		        
		        return redirect('admin/quotation/approval')->with([
	                'success' => true,
	                'success.message' => 'Approval Proceeded!',
	                'success.title'   => 'Success..!'
	            ]);
		    }catch(Exception $e){
	            return redirect('admin/quotation/show',['id' => $request->quotation_id])->with([
	                'error' => true,
	                'error.title' => 'Error..!',
	                'error.message' => $e->getMessage()
	            ])->withInput();
	        }

		}elseif($request->reject) {
			try{
		        $booh = $this->quotationLogic->rejectQuotationDiscount($request);		        
		        return redirect('admin/quotation/approval')->with([
	                'success' => true,
	                'success.message' => 'Approval Rejected!',
	                'success.title'   => 'Success..!'
	            ]);
		    }catch(Exception $e){
	            return redirect('admin/quotation/show',['id' => $request->quotation_id])->with([
	                'error' => true,
	                'error.title' => 'Error..!',
	                'error.message' => $e->getMessage()
	            ])->withInput();
	        }
		}
	}


	public function quotationView($id, Request $request){
		$old = $request;

		$quotation = $this->quotationLogic->getQuotationById($id);

		$page = view("AdminQuotationManage::quotation-pdf", compact('quotation'))->render();

		PDF::AddPage();
        PDF::writeHTML($page);
        PDF::Output(public_path('quotation/quotation-'.$id.'.pdf'),'F');
	}

	public function getItems(Request $request){
		return $this->quotationLogic->getItems(50,$request->search);
	}

	public function getCustomers(Request $request){
		return $this->quotationLogic->getCustomers(50,$request->search);
	}
}
