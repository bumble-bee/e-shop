@extends('layouts.back_master') @section('title','List Quotation')
@section('css')
<link rel="stylesheet" href="{{asset('assets/dist/chosen/bootstrap-chosen.css')}}">
<link rel="stylesheet" type="text/css" src="{{asset('assets/dist/bootstrap-datepicker/css/bootstrap-datepicker.css')}}"/>

<style type="text/css">
  table .btn{
    padding: 2px 6px;
  }

  .table>tbody>tr>td, 
  .table>tbody>tr>th, 
  .table>tfoot>tr>td, 
  .table>tfoot>tr>th, 
  .table>thead>tr>td, 
  .table>thead>tr>th{
    padding-top:5px; 
    padding-bottom:5px; 
  }
</style>
@stop
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Quotation<small>Management</small></h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
    <li><a href="{{ route('quotation.list') }}">Quotation List</a></li>
		<li class="active">Details</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<!-- Default box -->
	<div class="box">
		<div class="box-body">
      @if(isset($list) && !empty($list))
      <div class="row">
        <div class="col-md-12">
          <strong>
            <h3 class="text-center">
              {{($list->vat_type != 'novat')?strtoupper($list->vat_type):''}} QUOTATION
            </h3>
          </strong>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <h3 style="margin-top: 2px;margin-bottom: 0px;">{{ $list->reference_no?:'-' }}</h3>
          <h4 style="margin-bottom: 0;margin-top: 8px;">{{ $list->customer_name?:'-' }}</h4>
          <h5 style="margin-top: 5px;">{{ $list->customer_address?:'-' }}</h5>
          @if($list->approved_emp != $list->action_emp)
          <span>Approved By: {{$list->approved_emp}}</span>
          @endif
        </div>
        <div class="col-md-6">
          <h4 class="text-right" style="margin-bottom: 0">
            {{ ($list->quotation_date)?date('jS M, Y',strtotime($list->quotation_date)):'-' }}
          </h4>
          <h5 class="text-right" style="margin-top:3px;margin-bottom: 0;">
            {{ $list->action_emp?:'-' }}
          </h5>
          <h5 class="text-right" style="margin-top: 8px;">
            @if(PENDING == $list->quotation_status)
              <span class="text-warning">Pending</span>
            @elseif(APPROVED == $list->quotation_status)
              <span class="text-success">Approved</span>
            @elseif(REJECTED == $list->quotation_status)
              <span class="text-danger">Rejected</span>
            @elseif(REVISED == $list->quotation_status)
              <span class="text-primary">Revised</span>
            @elseif(CLOSED == $list->quotation_status)
              <span class="text-success">Closed</span>
            @elseif(INVOICED == $list->quotation_status)
              <span class="text-success">Invoiced</span>
            @else
              -
            @endif
          </h5>
          @if($list->quotation_status != PENDING && $list->quotation_status != REJECTED)
          <div class="text-right">
            <a href="{{url('admin/quotation/quotation_pdf/')}}/{{$list->id}}" target="_blank">Quotation PDF</a> | 
            <a href="{{url('admin/quotation/performa_pdf/')}}/{{$list->id}}" target="_blank">Proforma Invoice PDF</a>
          </div>
          @endif
        </div>
      </div>
    @endif
    <br>
    <br>
		  <table class="table table-bordered">
        <thead>
          <tr>
            <th class="text-center">#</th>
            <th class="text-center" width="15%">Product Code</th>
            <th class="text-center">Product</th>
            <th class="text-center">Unit Price (Rs)</th>
            <th class="text-center">UDP (%)</th>
            <th class="text-center">Additional (%)</th>
            <th class="text-center">Discount (Rs)</th>
            <th class="text-center">Qty</th>
            <th class="text-center">Amount (Rs)</th>
          </tr>
        </thead>
        <tbody>
          @if(count($list->details) > 0)
            @foreach($list->details as $key => $detail)
              <tr>
                <td>{{ (++$key) }}</td>
                <td>{{ $detail->product->code?:'-' }}</td>
                <td>{{ $detail->product->name?:'-' }}</td>
                <td align="right">{{ number_format($detail->unitPrice, 2)?:'-' }}</td>
                <td align="right">{{ number_format($detail->udp, 2)?:'-' }}</td>
                <td align="right">{{ number_format($detail->discount, 2)?:'-' }}</td>
                <td align="right">{{ number_format($detail->discountAmount, 2)?:'-' }}</td>
                <td align="right">{{ number_format($detail->qty, 2)?:'-' }}</td>
                <td align="right">{{ number_format($detail->totalAmount, 2)?:'-' }}</td>
              </tr>
            @endforeach
            <tr>
              <td colspan="8" align="right">Amount</td>
              <td align="right">{{ number_format($list->details->sum('totalAmount'), 2)?:'-' }}</td>
            </tr>
            <tr>
              <td colspan="8" align="right">Discount ({{number_format(($list->details->sum('totalDiscount')/$list->details->sum('totalAmount'))*100,2)}}%)</td>
              <td align="right">{{ number_format($list->details->sum('totalDiscount'), 2)?:'-' }}</td>
            </tr>
            @if($list->vat_type=='vat')
            <tr>
              <td colspan="8" align="right">VAT {{$vat->value}}%</td>
              <td align="right">{{ number_format($list->details->sum('totalVat'), 2)?:'-' }}</td>
            </tr>
            @elseif($list->vat_type=='svat')
            <tr>
              <td colspan="8" align="right">SVAT {{$vat->value}}%</td>
              <td align="right">{{ number_format($list->details->sum('totalVat'), 2)?:'-' }}</td>
            </tr>
            @endif
            <tr>
              <td colspan="8" align="right"><strong>Total Amount</strong></td>
              <td align="right"><strong>{{ number_format(
                $list->details->sum('totalAmount') - 
                $list->details->sum('totalDiscount') +
                $list->details->sum('totalVat'), 2)?:'-' }}</strong></td>
            </tr>
          @else
            <tr>
              <td colspan="9" align="center"> - No Data to Display - </td>
            </tr>
          @endif  
        </tbody>
      </table>
		</div><!-- /.box-body -->
    <div class="overlay" style="display: none;">
      <i class="fa fa-spin fa-refresh"></i>
    </div>
	</div><!-- /.box -->
</section><!-- /.content -->


@stop
@section('js')
<script src="{{asset('assets/dist/chosen/chosen.jquery.min.js')}}"></script>
<script src="{{asset('assets/dist/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>

<script type="text/javascript">
$(document).ready(function() {
  $(".chosen").chosen();

  $('.datepick').datepicker({
      keyboardNavigation: false,
      forceParse: false,
      format: 'yyyy-mm-dd'
  });
});
</script>
@stop
