<style>
  table{
    font-size:8px;
  }
  .full-border{
    border: 1px solid #000;
  }
  .border{
    border-color: #000;
    border-style: solid;
  }
</style>

<img src="assets/front/img/invoice-header-pi.jpg">
<span style="font-size: 7px;">No.34,Old Road,Nawinna,Maharagama. Tel: +94 11 4792 100 Fax: +94 11 4792 128 Email: sales@orelpower.com Web: www.orelpower.com VAT Reg.No: 114721255 7000</span>

<div></div>

<table>
  <tr>
    <td class="full-border" style="width:49%;font-size:9px;line-height:2;text-align:center">
      CUSTOMER DETAILS
    </td>
    <td style="width:2%;"></td>
    <td class="full-border" style="width:49%;font-size:9px;line-height:2;text-align:center">
      PAYMENT & BANK DETAILS
    </td>
  </tr>
  <tr>
    <td style="border-left-width:1px;border-right-width:1px;">
      Customer: {{ $quotation->bpr }}
      <p style="font-size:8px;padding-left:5em;">
        {{ $quotation->customer_name }} <br>
        {{ $quotation->customer_address }}
        @if($quotation->attn && $quotation->attn != "")
          <br>
          {{ 'Attn:'.$quotation->attn }}
        @endif
      </p>
    </td>
    <td></td>
    <td style="border-left-width:1px;border-right-width:1px">
      Payment Term : Within {{ $quotation->credit_period }} Days
    </td>
  </tr>
  <tr>
    <td style="border-left-width:1px;border-right-width:1px;border-bottom-width:1px">
      Sales Executive: {{ $quotation->action_emp }}
    </td>
    <td></td>
    <td style="border-left-width:1px;border-right-width:1px;border-bottom-width:1px;font-size:7px;">
      All payment to be made in a/c payee cheque in favour of "OREL CORPORATION (PVT) LTD"<br>
      Bank Name : Sampath Bank<br>
      Bank A/C : 0001 1008 3351<br>
      *This proforma Invoice is issued subject to clearance of outstanding payments due upon receipt of order.<br>
      <b>Items are non-returnable, unless otherwise due to a manufacturing defect.</b>
    </td>
  </tr>
</table>

<div></div>

<table class="full-border">
  <tr style="background-color:#000;line-height:2;">
    <th style="color:#fff;text-align:center;">Proforma No</th>
    <th style="color:#fff;text-align:center;">Proforma Date</th>
    <th style="color:#fff;text-align:center;">Expire Date</th>
    <th style="color:#fff;text-align:center;">Delivery Term</th>
  </tr>
  <tr>
    <td style="border-right-width:1px;text-align:center">{{ $quotation->reference_no }}</td>
    <td style="border-right-width:1px;text-align:center">{{ date_format(new DateTime($quotation->quotation_date), 'F j, Y, g:i a') }}</td>
    <td style="border-right-width:1px;text-align:center">{{ date('F j, Y, g:i a',strtotime("+1 MONTH",strtotime(date_format(new DateTime($quotation->quotation_date), 'Y-m-d H:i:s')))) }}</td>
    <td style="text-align:center">{{ $quotation->delivery_term }}</td>
  </tr>
</table>

<div></div>

<table class="full-border">
    <tr style="background-color:#000;line-height: 2;">
        <th style="width:5%;color:#fff;text-align: center;">#</th>
        <th style="width:15%;color:#fff;text-align: center;">SKU</th>
        <th style="width:35%;color:#fff;text-align: center;">Product Name</th>
        <th style="width:10%;color:#fff;text-align: center;">Qty.</th>
        <th style="width:5%;color:#fff;text-align: center;">Unit</th>
        <th style="width:15%;color:#fff;text-align: center;">Price (Rs.)</th>
        <th style="width:15%;color:#fff;text-align: center;">Amount (Rs.)</th>
    </tr>
    <?php $count = 0; ?>
    <?php $total = 0; ?>
    @if(isset($quotation->details) && count($quotation->details) > 0)
        <?php $total = 0; ?>
        <?php $hidden_total = 0; ?>
        <?php $i=1; ?>
        <?php $vat_checked = 0; ?>
        <?php $count = count($quotation->details); ?>
        <?php $discount_value = 0; ?>
        @foreach($quotation->details as $quotationDetails)
            <?php
            $hidden_price = 0;
            if((is_null($quotationDetails->product->is_nbt)) || ($quotationDetails->product->is_nbt == 0)){
              $price = $quotationDetails->price;
            }else{
              $price = ($quotationDetails->price) + ($quotationDetails->price * $nbt/100);
            }

            if($quotation->vat_type == 'novat'){
              if($quotationDetails->product->is_vat == 1) {
                $price  = $price + $price * $vat/100;

                $discount_value += ($price * ($quotationDetails->udp+$quotationDetails->discount)/100) * $quotationDetails->qty;           
                $vat_checked = 1;
              }else{
                $discount_value += ($price * ($quotationDetails->udp+$quotationDetails->discount)/100) * $quotationDetails->qty;           

                $vat_checked = 0;
              }  
            }elseif($quotation->vat_type == 'vat'){
              if($quotationDetails->product->is_vat == 1) {
                
                $hidden_price = $price - $price * ($quotationDetails->udp+$quotationDetails->discount)/100;
                $discount_value += ($price * ($quotationDetails->udp+$quotationDetails->discount)/100) * $quotationDetails->qty;           

                $vat_checked = 1;
              }else{
                $discount_value += ($price * ($quotationDetails->udp+$quotationDetails->discount)/100) * $quotationDetails->qty;           

                $vat_checked = 0;
              } 
            }elseif($quotation->vat_type == 'svat'){
              if($quotationDetails->product->is_vat == 1) {
                
                $hidden_price = $price - $price * ($quotationDetails->udp+$quotationDetails->discount)/100;
                $discount_value += ($price * ($quotationDetails->udp+$quotationDetails->discount)/100) * $quotationDetails->qty;           

                $vat_checked = 1;
              }else{
                $discount_value += ($price * ($quotationDetails->udp+$quotationDetails->discount)/100) * $quotationDetails->qty;           

                $vat_checked = 0;
              } 
            } 

            if($i == $count){
              $border = '';
            }
            else{
              $border = 'border-bottom:1px solid #aaa;';
            }
            ?>
            <tr style="line-height: 2;">
                <td style="width:5%;border-right:1px solid #000;text-align: center;{{ $border }}">{{ ($i) }}</td>
                <td style="width:15%;border-right:1px solid #000;{{ $border }}">{{ $quotationDetails->product->code }}</td>
                <td style="width:35%;border-right:1px solid #000;{{ $border }}">
                  {{ $quotationDetails->product->name }}
                  @if($vat_checked == 0) {{'*'}} @endif
                </td>
                <td style="width:10%;border-right:1px solid #000;text-align: center;{{ $border }}">{{ $quotationDetails->qty }}</td>
                <td style="width:5%;border-right:1px solid #000;text-align: center;{{ $border }}">{{'pcs'}}</td>
                <td style="width:15%;border-right:1px solid #000;text-align: right;{{ $border }}">{{ number_format($price,2) }}</td>
                <td style="width:15%;border-right:1px solid #000;text-align: right;{{ $border }}">
                    {{ number_format(($quotationDetails->qty*$price),2) }}
                </td>
            </tr>
            <?php
              if($quotation->vat_type == 'vat'){
                  $hidden_total += $hidden_price * $quotationDetails->qty;
              }else if($quotation->vat_type == 'svat'){
                  $hidden_total += $hidden_price * $quotationDetails->qty;
              }else{

              }

              $total += $price * $quotationDetails->qty; 
              $i++;
            ?>
        @endforeach
        <?php $vat_value   = 0; ?>
        <?php $svat_value  = 0; ?>
        <?php 
          if($quotation->vat_type == 'vat'){
              $vat_value  = $hidden_total * $vat/100;
          }else if($quotation->vat_type == 'svat'){
             $vat_value  = $hidden_total * $vat/100;
          }else{

          }              

        ?>
    @else
        <p class="text-center" style="margin: 10px 0">No products found.</p>
    @endif

</table>

<div></div>

@if($total>0)
<table>
  <tr>
    <td style="width:80%;text-align:right;">Amount</td>
    <td style="width:20%;text-align:right;">{{ number_format($total,2) }}</td>
  </tr>
  <tr>
    <td style="width:80%;text-align:right;">Discount</td>
    <td style="width:20%;text-align:right;">{{ number_format($discount_value,2) }}</td>
  </tr>
</table>

<div></div>

<table>
  @if($quotation->vat_type == "vat")
    <tr>
      <td style="width:80%;text-align:right;">VAT {{ number_format($vat,2) }}%</td>
      <td style="width:20%;text-align:right;">{{ number_format($vat_value,2) }}</td>
    </tr>
  @elseif($quotation->vat_type == "svat")
    <tr>
      <td style="width:80%;text-align:right;">SVAT {{ number_format($vat,2) }}%</td>
      <td style="width:20%;text-align:right;">{{ number_format($vat_value,2) }}</td>
    </tr>
  @endif
  
  <tr>
    <td style="width:80%;text-align:right;"><b>Total Amount</b></td>
    <td style="width:20%;text-align:right;">{{ number_format((($total - $discount_value) + $vat_value),2) }}</td>
  </tr>
</table>

<div><br /></div>

@endif

<span style="font-size: 8px;text-align:center;">
  Conditions :- Prevailing statutory levies will be applicable at the time of invoicing and prices will vary subject to USD fluctuations
</span>

<div></div>

<span style="font-size: 8px;text-align:center;">
  Should you require further information, please call 0114 792221.<br />
Whilst assuaring you of our best services at all times, we look forward your most valued order.
</span>

<div><br /></div>

<?php $wrap = (13 - $count-1);  ?>
@for ($i = 0; $i < $wrap; $i++)
    <br />
@endfor

<img style="bottom:0;" src="assets/front/img/invoice-footer.jpg">
