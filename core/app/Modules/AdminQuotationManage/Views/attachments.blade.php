@extends('layouts.back_master') @section('title','List Quotation')
@section('css')
<link rel="stylesheet" href="{{asset('assets/dist/chosen/bootstrap-chosen.css')}}">


<style type="text/css">
  .card {
    position: relative;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-direction: column;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-color: #fff;
    background-clip: border-box;
    border: 1px solid rgba(0,0,0,.125);
    border-radius: .25rem;
}

.card-body {
    -ms-flex: 1 1 auto;
    flex: 1 1 auto;
    padding: 1.25rem;
}

.btn-outline-secondary {
    color: #6c757d;
    background-color: transparent;
    background-image: none;
    border-color: #6c757d;
}

.card-img-top {
    width: 100%;
    border-top-left-radius: calc(.25rem - 1px);
    border-top-right-radius: calc(.25rem - 1px);
}



</style>
@stop
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Quotation<small>Management</small></h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
    <li><a href="{{ route('quotation.list') }}">Quotation List</a></li>
		<li class="active">PO Details</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <!-- Default box -->
      <div class="box">
        <div class="box-body">
          @if(isset($list) && !empty($list))
          <div class="row">
            <div class="col-md-6">
              <h3 style="margin-top: 2px;margin-bottom: 0px;">{{ $list->reference_no?:'-' }}</h3>
              <h4 style="margin-bottom: 0;margin-top: 8px;">{{ $list->customer_name?:'-' }}</h4>
              <h5 style="margin-bottom: 0;margin-top: 8px;">PO NUMBER #{{ $po->po_number?:'-' }}</h5>
              <h5 style="margin-top: 5px;">{{ $list->customer_address?:'-' }}</h5>
              @if($list->approved_emp != $list->action_emp)
              <span>Approved By: {{$list->approved_emp}}</span>
              @endif
            </div>
            <div class="col-md-6">
              <h4 class="text-right" style="margin-bottom: 0">
                {{ ($list->quotation_date)?date('jS M, Y',strtotime($list->quotation_date)):'-' }}
              </h4>
              <h5 class="text-right" style="margin-top:3px;margin-bottom: 0;">
                {{ $list->action_emp?:'-' }}
              </h5>
              <h5 class="text-right" style="margin-top: 8px;">
                @if(PENDING == $list->quotation_status)
                  <span class="text-warning">Pending</span>
                @elseif(APPROVED == $list->quotation_status)
                  <span class="text-success">Approved</span>
                @elseif(REJECTED == $list->quotation_status)
                  <span class="text-danger">Rejected</span>
                @elseif(REVISED == $list->quotation_status)
                  <span class="text-primary">Revised</span>
                @elseif(CLOSED == $list->quotation_status)
                  <span class="text-success">Closed</span>
                @elseif(INVOICED == $list->quotation_status)
                  <span class="text-success">Invoiced</span>
                @else
                  -
                @endif
              </h5>
              @if($list->quotation_status != PENDING && $list->quotation_status != REJECTED)
              <div class="text-right">
                <a href="{{url('admin/quotation/quotation_pdf/')}}/{{$list->id}}" target="_blank">Quotation PDF</a> | 
                <a href="{{url('admin/quotation/performa_pdf/')}}/{{$list->id}}" target="_blank">Proforma Invoice PDF</a>
              </div>
              @endif
            </div>
          </div>
        @endif
        </div><!-- /.box -->
      </div> 
    </div>
  </div>

  <div class="row">
      <!-- Default box -->
        @if(count($attachments)>0)
          @foreach($attachments as $attachment)
            @if($attachment->file_ext=="pdf")
              <div class="col-md-4">
                <div class="card mb-4 box-shadow">
                  <div class="card-img-top"  style="height: 225px; width: 100%; display: block;">
                    <img class="img-responsive" src="{{asset('assets/adminlte/img/pdf.png')}}" style="margin: 14% auto;">
                    
                  <hr style="color: #ddd">
                  </div>
                  <div class="card-body"> 
                    <p class="card-text">Uploaded By {{$po->uploadedby->first_name}} {{$po->uploadedby->last_name}}
                      <br><small class="text-muted">{{$attachment->created_at}}</small></p>
                    <div class="d-flex justify-content-between align-items-center">
                      <div class="btn-group">
                        <a target="_blank" href="{{url('core/storage/'.$attachment->file)}}" class="btn btn-sm btn-outline-secondary">View</a>
                      </div>                  
                    </div>
                  </div>
                </div>
              </div>
            @else
              <div class="col-md-4">
                <div class="card mb-4 box-shadow">
                  <img class="card-img-top" src="{{url('core/storage/'.$attachment->file)}}" 
                  data-holder-rendered="true" style="height: 225px; width: 100%; display: block;">
                  <div class="card-body"> 
                    <p class="card-text">Uploaded By {{$po->uploadedby->first_name}} {{$po->uploadedby->last_name}}
                      <br><small class="text-muted">{{$attachment->created_at}}</small></p>
                    <div class="d-flex justify-content-between align-items-center">
                      <div class="btn-group">
                        <a target="_blank" href="{{url('core/storage/'.$attachment->file)}}" class="btn btn-sm btn-outline-secondary">View</a>
                      </div>                  
                    </div>
                  </div>
                </div>
              </div>
            @endif
          @endforeach
        @else
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                <h3>No Attachments</h3>
                <h5>No Attachments found for the po</h5>
            </div>
        @endif
  </div>



</section><!-- /.content -->




@stop
@section('js')
<script src="{{asset('assets/dist/chosen/chosen.jquery.min.js')}}"></script>

<script type="text/javascript">
$(document).ready(function() {


});
</script>
@stop
