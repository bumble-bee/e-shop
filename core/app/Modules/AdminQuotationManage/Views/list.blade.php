@extends('layouts.back_master') @section('title','List Quotation')
@section('css')
<link rel="stylesheet" href="{{asset('assets/dist/chosen/bootstrap-chosen.css')}}">
<link rel="stylesheet" type="text/css" src="{{asset('assets/dist/bootstrap-datepicker/css/bootstrap-datepicker.css')}}"/>

<style type="text/css">
  table .btn{
    padding: 2px 6px;
  }

  .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
    padding-top:5px; 
    padding-bottom:5px; 
  }
</style>
@stop
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Quotation<small>Management</small></h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li class="active">Quotation Management</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<!-- Default box -->
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">List Quotaton</h3>
		</div>
		<br>
		<div class="box-body">

      <form method="get">
        <div class="row form-group">
          <div class="col-sm-4">
            <label for="" class="control-label">Reference No. (wildcard % supported)</label>
            <input type="text" class="form-control" name="reference" id="reference" value="{{$old->reference}}">
          </div>
          <div class="col-sm-4">
            <label for="" class="control-label">Customer</label>
            <input type="text" class="form-control" name="customer" id="customer" value="{{$old->customer}}">
          </div>
          <div class="col-sm-4">
            <label for="" class="control-label">Type</label>
            <select name="type" id="type" class="form-control chosen">
              <option value="" @if($old->type == "") selected @endif >Select an Option</option>  
              <option value="1" @if($old->type == "1") selected @endif >Special</option>  
              <option value="0" @if($old->type == "2") selected @endif >Normal</option>  
            </select>
          </div>
        </div>
        <div class="row form-group">
          <div class="col-sm-4">
            <label for="" class="control-label">From</label>
            <input id="from" name="from" type="text" class="form-control datepick" value="@if(isset($old->from)){{$old->from}}@else{{''}}@endif">
          </div>
          <div class="col-sm-4">
            <label for="" class="control-label">To</label>
            <input type="text" class="form-control datepick" name="to" id="to" value="@if(isset($old->to)){{$old->to}}@else{{date('Y-m-d')}}@endif">
          </div>
          <div class="col-sm-4">
            <label for="" class="control-label">Status</label>
            <select name="status" id="status" class="form-control chosen">
              <option value="" @if($old->status == "") selected @endif >Select an Option</option>  
              <option value="0" @if($old->status == PENDING && $old->status != "") selected @endif >Pending</option>  
              <option value="1" @if($old->status == APPROVED) selected @endif >Approved</option>  
              <option value="-1" @if($old->status == REJECTED) selected @endif >Rejected</option>  
              <option value="2" @if($old->status == REVISED) selected @endif >Revised</option>  
              <option value="3" @if($old->status == CLOSED) selected @endif >Closed</option>  
              <option value="4" @if($old->status == INVOICED) selected @endif >Invoiced</option>  
              <option value="5" @if($old->status == PO_PROCESSING) selected @endif >PO Processing</option>  
            </select>
          </div>
        </div>
        <div class="row form-group">
          <div class="col-sm-4">
            <label for="" class="control-label">Action By</label>
            <select name="action" id="action" class="form-control chosen">
              <option value="" @if($old->type == "") selected @endif >Select an Option</option>  
              @foreach($action_by as $val)
                <option value="{{$val->id}}" @if($val->id == $old->action) selected @endif >{{$val->first_name}} {{$val->last_name}}</option>  
              @endforeach
            </select>
          </div>
        </div>

        <div class="row form-group">
          <div class="col-md-12">
            <button class="btn btn-default pull-right" type="submit"><i class="fa fa-search"></i> Filter</button>
          </div>
        </div>
      </form>

		  <table class="table table-bordered">
        <thead>
          <tr>
            <th class="text-center">#</th>
            <th class="text-center">Reference No.</th>
            <th class="text-center">Customer</th>
            <th class="text-center">Date</th>
            <th class="text-center">Amount (Rs)</th>
            <th class="text-center">Action By</th>
            <th class="text-center">Type</th>
            <th class="text-center">Status</th>
            <th class="text-center">PO</th>
            <th class="text-center">Actions</th>
          </tr>
        </thead>
        <tbody>
          @if(count($data) > 0)
            @foreach($data as $key => $val)
              <tr>
                <td align="center">{{ (($data->currentPage()-1)*$data->perPage())+($key+1) }}</td>
                <td align="left">{{$val->reference_no}}</td>
                <td align="left">{{$val['customer']->first_name}} {{$val['customer']->last_name}}</td>
                <td align="left">{{explode(" ",$val->quotation_date)[0]}}</td>
                <td align="right">{{number_format($val->total_amount,2)}}</td>
                <td align="left">{{$val['createdBy']->first_name}} {{$val['createdBy']->last_name}}</td>
                <td align="left">@if($val->quotation_type == "1") Special @else Normal @endif </td>
                <td align="left">@if($val->status == PENDING) Pending 
                                 @elseif($val->status == APPROVED) Approved 
                                 @elseif($val->status == REJECTED) Rejected
                                 @elseif($val->status == REVISED) Revised
                                 @elseif($val->status == CLOSED) Closed
                                 @elseif($val->status == INVOICED) Invoiced 
                                 @elseif($val->status == PO_PROCESSING) PO Processing 
                                 @endif 
                </td>
                <td align="center">@if(!$val->customerPo) -
                                   @else<a href="{{url('admin/quotation/po')}}/{{$val->id}}" target=_blank>{{$val->customerPo->po_number}}</a><br>Verified
                                   @endif
                </td>
                <td align="center">
                  <a href="{{ route('quotation.list.detail', $val->id) }}"><i class="fa fa-th" aria-hidden="true"></i></a>
                </td>
              </tr>
            @endforeach
          @else
            <tr>
              <td colspan="9" align="center"> - No Data to Display - </td>
            </tr>
          @endif  
        </tbody>
      </table>
      <div class="row">
        <div class="col-sm-12 text-right">
          {!! $data->appends($old->except('page'))->render() !!}
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12 text-right">
          
        </div>
      </div>
		</div><!-- /.box-body -->
    <div class="overlay" style="display: none;">
      <i class="fa fa-spin fa-refresh"></i>
    </div>
	</div><!-- /.box -->
</section><!-- /.content -->

<!-- Default box -->
<div class="modal fade" id="detail_div" name="detail_div1" hidden="true">
  <form  class="form-horizontal form-validation" action="#" role="form"  method="post" id="stageForm">
    {!!Form::token() !!}
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title">Quotation Detail</h4>
              </div>
              <div class="modal-body">
                  <div class="form-group">
                      <table class="table table-bordered table-hover">
                          <thead class="table-header">
                              <tr>
                                  <td class="text-center">Product Code</td>
                                  <td class="text-center">Product</td>
                                  <td class="text-center">Price</td>
                                  <td class="text-center">Qty</td>
                                  <td class="text-center">Total</td>
                                  <td class="text-center">Discount</td>
                                  <td class="text-center">Amount</td>
                              </tr>
                          </thead>
                          <tbody id="tbdy"></tbody>
                      </table>
                  </div>
              </div>
          </div>
      </div>
  </form>
</div>


@stop
@section('js')

<script src="{{asset('assets/dist/chosen/chosen.jquery.min.js')}}"></script>
<script src="{{asset('assets/dist/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>


<script type="text/javascript">
$(document).ready(function() {
  $(".chosen").chosen();

  $('.datepick').datepicker({
      keyboardNavigation: false,
      forceParse: false,
      format: 'yyyy-mm-dd'
  });
});

function deleteData(_url){
  swal({
    title: "Are you sure?",
    text: "you wanna delete this product?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes, delete it!",
    closeOnConfirm: false
  },function(){
    $.ajax({
      url: _url,
      method: 'get',
      cache: false,
      data: [],
      success: function(response){
        if(response.status == 1){
          swal("Done!", "product has been deleted!.", "success");
          location.reload();
        }else{
          swal("Error!", response.message, "error");
        }
      },
      error: function(xhr){
        console.log(xhr);
        swal("Error!", 'Error occurred. Please try again', "error");
      } 
    });
    //window.open(_url, '_self');
  });
}

function showModal(id){
  $('.overlay').show();
  $.ajax({
        url: "{{URL::to('admin/quotation/listdetail')}}",
        method: 'GET',
        data: {'id': id},
        async: true,
        success: function (data) {
          $("#tbdy").empty();
          for(var i=0;i<data.data.length;i++){
            var amount = parseFloat(data.data[i]['price']) * parseInt(data.data[i]['qty']);
            var discount = (parseFloat(amount)*parseFloat(data.data[i]['discount']))/100;
            $("#tbdy").append(
                "<tr>"
                    +"<td>"+data.data[i]['product'][0]['code']+"</td>"
                    +"<td>"+data.data[i]['product'][0]['name']+"</td>"
                    +"<td>"+parseFloat(data.data[i]['price'])+"</td>"
                    +"<td>"+parseInt(data.data[i]['qty'])+"</td>"
                    +"<td>"+parseFloat(amount)+"</td>"
                    +"<td>"+parseFloat(discount)+"</td>"
                    +"<td>"+parseFloat(amount - discount)+"</td>"
                +"</tr>"
              );
          }  
          $('#detail_div').modal('show');
          $('.overlay').hide();
        },
        error: function () {
            alert('error');
        }
    });
}

</script>
@stop
