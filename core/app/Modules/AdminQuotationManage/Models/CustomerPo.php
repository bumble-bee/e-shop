<?php namespace App\Modules\AdminQuotationManage\Models;

/**
*
* Model
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerPo extends Model {

	protected $table = 'customer_po';

	protected $guarded = ['id'];


	public function uploadedby(){
        return $this->belongsTo('Core\EmployeeManage\Models\Employee','employee_id','id');
    }

    public function attachments(){
        return $this->hasMany('App\Modules\AdminQuotationManage\Models\PoAttachments','po_id','id');
    }
}
