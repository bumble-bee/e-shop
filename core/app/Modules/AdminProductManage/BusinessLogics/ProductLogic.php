<?php namespace App\Modules\AdminProductManage\BusinessLogics;


/**
* Business Logics 
* Define all the busines logics in here
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/
use Illuminate\Database\Eloquent\Model;

use App\Modules\AdminProductManage\Models\Product;

use DB;
use Excel;
use File;

class ProductLogic{

	private $product;

	public function __construct(Product $product){
		$this->product = $product;
	}

	public function saveProduct($productData, $isUpdate = false){

		$product = $this->product;

		if(isset($productData->code) && $productData->code != ''){
			$product->code = trim($productData->code);
		}elseif($isUpdate && !$productData->code){
			$product->code = '';
		}

		if(isset($productData->mrp) && $productData->mrp != ''){
			$product->mrp = $productData->mrp;
		}elseif($isUpdate && !$productData->mrp){
			$product->mrp = 0;
		}

		if(isset($productData->vat) && $productData->vat != ''){
			$product->is_vat = $productData->vat;
		}elseif($isUpdate && !$productData->vat){
			$product->is_vat = 0;
		}

		if(isset($productData->name) && $productData->name != ''){
			$product->name = $productData->name;
		}elseif($isUpdate && !$productData->name){
			$product->name = '';
		}

		if(isset($productData->description) && $productData->description != ''){
			$product->description = $productData->description;
		}elseif($isUpdate && !$productData->description){
			$product->description = '';
		}

		if(isset($productData->max_discount) && $productData->max_discount != ''){
			$product->max_discount = $productData->max_discount;
		}elseif($isUpdate && !$productData->max_discount){
			$product->max_discount = '';
		}

		if(isset($productData->udp) && $productData->udp != ''){
			$product->udp = $productData->udp;
		}elseif($isUpdate && !$productData->udp){
			$product->udp = '';
		}

		if(isset($productData->cash_udp) && $productData->cash_udp != ''){
			$product->cash_udp = $productData->cash_udp;
		}elseif($isUpdate && !$productData->cash_udp){
			$product->cash_udp = '';
		}

		if(isset($productData->category) && $productData->category != ''){
			$product->product_category_id = $productData->category;
		}elseif($isUpdate && !$productData->category){
			$product->product_category_id = null;
		}

		if(isset($productData->stock) && $productData->stock != ''){
			$product->stock = $productData->stock;
		}elseif($isUpdate && !$productData->stock){
			$product->stock = '';
		}

		if(isset($productData->sales_office) && $productData->sales_office != ''){
			$product->sales_office_id = $productData->sales_office;
		}elseif($isUpdate && !$productData->sales_office){
			$product->sales_office_id = '';
		}

		if(isset($productData->tax) && $productData->tax != ''){
			$is_vat = 0;
    		$is_nbt = 0;

    		if(strpos($productData->tax, 'NBT') !== FALSE){
    			$is_nbt = 1;
    		}

    		if(strpos($productData->tax, 'VAT') !== FALSE){
    			$is_vat = 1;
    		}

			$product->is_vat = $is_vat;
			$product->is_nbt = $is_nbt;
		}


		if(!$product->save()){
			throw new \Exception('P1-Could not save product');
		}

		return 1;

	}

	public function getProductById($id){

		$product = Product::find($id);

		if(!$product){
			throw new \Exception('P2-Product not found for ID:'.$id);
		}

		return $product;
	}

	public function getProductByCode($code){

		$product = Product::where('code',$code)->first();

		if(!$product){
			throw new \Exception('P2-Product not found for Code:'.$code);
		}

		return $product;
	}

	public function getProductsByCategory($category){

		$products = Product::where('product_category_id',$category)->get();

		if(!$products){
			throw new \Exception('P3-Products not found for category_id:'.$category);
		}

		return $products;
	}

	public function updateProduct($id, $productData, $isUpdate=true){
		$product = $this->getProductById($id);
		$this->product = $product;
		$this->saveProduct($productData, $isUpdate);
		return 1;
	}

	public function deleteProduct($id){
		$product = $this->getProductById($id);

		if(!$product->delete()){
			throw new \Exception('P3-Could not delete Product ID:'.$id);
		}

		return 1;
	}

	public function getRecords($filters, $perPage = 25){
		$products = Product::with(['category','mrpprice'])->orderBy('id','asc');

		if($filters->category != ''){
			$products->where('product_category_id', $filters->category);
		}

		if($filters->code != ''){
			$products->where('code','LIKE','%'.$filters->code.'%');
		}

		if($filters->name != ''){
			$products->where('name','LIKE','%'.$filters->name.'%');
		}

		if($filters->keyword != ''){
			$products->where(function($query) use ($filters){
				return $query->where('name', 'LIKE', '%'.$filters->keyword.'%')
						->orWhere('description', 'LIKE', '%'.$filters->keyword.'%');
			});
		}

		$products = $products->paginate($perPage);

		return $products;
	}

	public function getStockRecords($filters, $perPage = 25){
		$products = Product::with(['category'])->select([
			'code', 
			'name', 
			'product_category_id', 
			'stock'
		])->orderBy('id','asc');

		if($filters->category != ''){
			$products->where('product_category_id', $filters->category);
		}

		if($filters->code != ''){
			$products->where('code','LIKE','%'.$filters->code.'%');
		}

		if($filters->keyword != ''){
			$products->where(function($query) use ($filters){
				return $query->where('name', 'LIKE', '%'.$filters->keyword.'%')
						->orWhere('description', 'LIKE', '%'.$filters->keyword.'%');
			});
		}

		$products = $products->paginate($perPage);

		return $products;
	}

	public function uploadExcel($category, $file, $isMaxDis = false){
		$data = [];
		Excel::load($file, function($reader) use (&$data, $category, $isMaxDis){

			$reader->each(function($row) use (&$data, $category, $isMaxDis){
				try{

					$row->category = $category;
					$this->validateRequiredData($row, $isMaxDis);

					try{
						$product = $this->getProductByCode($row->code);
					}catch(\Exception $e){
						$product = null;
					}

					if(!$product && !$isMaxDis){
						$this->product = new Product;
						$this->saveProduct($row);
						array_push($data, [
							'code' => $row->code, 
							'name' => $row->name,
							'status' => 1,
							'message' => 'Done'
						]);
					}else{
						$this->updateProduct($product->id, $row);

						array_push($data, [
							'code' => $row->code, 
							'name' => $row->name,
							'status' => 1,
							'message' => 'Updated existing data.'
						]);
					}
				}catch(\Exception $e){
					array_push($data, [
						'code' => $row->code, 
						'name' => $row->name,
						'status' => 0,
						'message' => $e->getMessage()
					]);
				}
			});
		});

		return $data;
	}

	public function uploadMaxDiscountExcel($file){
		$data = [];
		Excel::load($file, function($reader) use (&$data){
			$reader->each(function($row) use (&$data){
				try{
					$product = null;	
					try{
						$product = $this->getProductByCode($row->code);
					}catch(\Exception $e){
						array_push($data, [
							'code' => $row->code, 
							'name' => 'Unknown',
							'status' => 0,
							'message' => 'Product not found'
						]);
					}

					if($product){
						$this->updateProduct($product->id, $row, false);
						array_push($data, [
							'code' => $row->code, 
							'name' => $row->name,
							'status' => 1,
							'message' => 'Updated existing data.'
						]);
					}					
				}catch(\Exception $e){
					array_push($data, [
						'code' => $row->code, 
						'name' => $row->name,
						'status' => 0,
						'message' => $e->getMessage()
					]);
				}
			});
		});

		return $data;
	}


	public function validateRequiredData($data, $isMaxDis = false){
		if(!$data->code)
			throw new \Exception('PV1-Short Code is required');

		if(!$data->name && !$isMaxDis)
			throw new \Exception('PV2-Product Name is required');

		if(!$data->category && !$isMaxDis)
			throw new \Exception('PV3-Product Category is required');

		if($isMaxDis && !$data->max_discount)
			throw new \Exception('PV4-Max Discount is required');

		return 1;
	}

}
