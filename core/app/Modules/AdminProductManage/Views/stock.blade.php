@extends('layouts.back_master') @section('title','List Stock')
@section('css')
<link rel="stylesheet" href="{{asset('assets/dist/chosen/bootstrap-chosen.css')}}">
<style type="text/css">
  table .btn{
    padding: 2px 6px;
  }

  .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
    padding-top:5px; 
    padding-bottom:5px; 
  }

  .box-header, .box-body {
    padding: 20px;
  }
</style>
@stop
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Product<small>Management</small></h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
    <li><a href="{{url('admin/product/list')}}">Product Management</a></li>
		<li class="active">Stock List</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<!-- Default box -->
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Stock List</h3>
		</div>
		<br>
		<div class="box-body">

      <form method="get">
        <div class="row form-group">
          <div class="col-sm-4">
            <label for="" class="control-label">Category</label>
            <select name="category" class="form-control chosen">
              @if(count($categories) > 0)
                @foreach($categories as $key => $value)
                <option value="{{$key}}" @if($key == $old->category) selected @endif>{{$value}}</option>
                @endforeach
              @else
              <option value="">All Categories</option>
              @endif
            </select>
          </div>
          <div class="col-sm-4">
            <label for="" class="control-label">Short Code</label>
            <input type="text" class="form-control" name="code" value="{{$old->code}}">
          </div>
          <div class="col-sm-4">
            <label for="" class="control-label">Keyword</label>
            <input type="text" class="form-control" name="keyword" value="{{$old->keyword}}">
          </div>
        </div>

        <div class="row form-group">
          <div class="col-md-12">
            <button class="btn btn-default pull-right" type="submit"><i class="fa fa-search"></i> Filter</button>
          </div>
        </div>
      </form>

		  <table class="table table-bordered">
        <thead>
          <tr>
            <th class="text-center">#</th>
            <th class="text-center">Short Code</th>
            <th class="text-center">Name</th>
            <th class="text-center">Category</th>
            <th class="text-center">Stock</th>
          </tr>
        </thead>
        <tbody>
          @if(count($data) > 0)
            @foreach($data as $key => $row)
            <tr>
              <td class="text-center">{{ (($data->currentPage()-1)*$data->perPage())+($key+1) }}</td>
              <td>{{ ($row->code != '') ? $row->code : '-' }}</td>
              <td>{{ ($row->name != '') ? $row->name : '-' }}</td>
              <td>{{ ($row->category) ? $row->category->name : '-' }}</td>
              <td class="text-right">{{ ($row->stock != '') ? $row->stock : '-' }}</td>
            </tr>
            @endforeach
          @else
            <tr>
              <td colspan="8" align="center"> - No Data to Display - </td>
            </tr>
          @endif
        </tbody>
      </table>
      <div class="row">
        <div class="col-sm-12 text-right">
          {!! $data->appends($old->except('page'))->render() !!}
        </div>
      </div>
		</div><!-- /.box-body -->
	</div><!-- /.box -->
</section><!-- /.content -->

@stop
@section('js')

<script src="{{asset('assets/dist/chosen/chosen.jquery.min.js')}}"></script>

<script type="text/javascript">
$(document).ready(function() {
  $(".chosen").chosen();
});

function deleteData(_url){
  swal({
    title: "Are you sure?",
    text: "you wanna delete this product?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes, delete it!",
    closeOnConfirm: false
  },
  function(){
    $.ajax({
      url: _url,
      method: 'get',
      cache: false,
      data: [],
      success: function(response){
        if(response.status == 1){
          swal("Done!", "product has been deleted!.", "success");
          location.reload();
        }else{
          swal("Error!", response.message, "error");
        }
      },
      error: function(xhr){
        console.log(xhr);
        swal("Error!", 'Error occurred. Please try again', "error");
      } 
    });
    //window.open(_url, '_self');
  });
}
</script>
@stop
