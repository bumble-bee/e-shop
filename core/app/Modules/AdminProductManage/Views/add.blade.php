@extends('layouts.back_master') @section('title','Add Product')
@section('css')
<link rel="stylesheet" href="{{asset('assets/dist/chosen/bootstrap-chosen.css')}}">
<style type="text/css">
	.box-header, .box-body {
		padding: 20px;
	}
  	.has-error .help-block, .has-error .control-label{
    	color:#e41212;
  	}
  	.has-error .chosen-container{
    	border:1px solid #e41212;
  	}
</style>
@stop
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Product<small>Management</small></h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li><a href="{{url('admin/product/list')}}">Product Management</a></li>
		<li class="active">Add Product</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<!-- Default box -->
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Add Product</h3>
		</div>
		<br>
		<form action="" class="form-horizontal" method="post">
			<div class="box-body">
				{!!Form::token()!!}
				<div class="form-group">
					<div class="@if($errors->has('category')) has-error @endif">
	              		<label for="" class="col-sm-2 control-label required">Product Category</label>
	              		<div class="col-sm-4">
	                		<select name="category" class="form-control chosen">
	                			@if(count($categories) > 0)
	                				@foreach($categories as $key => $value)
	                				<option value="{{$key}}" @if($key == old('category')) selected @endif>{{$value}}</option>
	                				@endforeach
	                			@else
	                			<option value="">Select a Category</option>
	                			@endif
	                		</select>
	                		@if($errors->has('category'))
	                		<span class="help-block">{{$errors->first('category')}}</span>
	                		@endif
	              		</div>
              		</div>
              		<div class="@if($errors->has('code')) has-error @endif">
	              		<label for="" class="col-sm-2 control-label required">Short Code</label>
	              		<div class="col-sm-4">
	                		<input type="text" class="form-control" name="code" value="{{old('code')}}">
	                		@if($errors->has('code'))
	                		<span class="help-block">{{$errors->first('code')}}</span>
	                		@endif
	              		</div>
              		</div>
              	</div>
            	<div class="form-group @if($errors->has('name')) has-error @endif">
              		<label for="" class="col-sm-2 control-label required">Product Name</label>
              		<div class="col-sm-10">
                		<input type="text" class="form-control" name="name" value="{{old('name')}}">
                		@if($errors->has('name'))
                		<span class="help-block">{{$errors->first('name')}}</span>
                		@endif
              		</div>
            	</div>
            	<div class="form-group @if($errors->has('description')) has-error @endif">
              		<label for="" class="col-sm-2 control-label">Product Description</label>
              		<div class="col-sm-10">
                		<textarea name="description" class="form-control" rows="4">{{old('description')}}</textarea>
                		@if($errors->has('description'))
                		<span class="help-block">{{$errors->first('description')}}</span>
                		@endif
              		</div>
            	</div>
            	<div class="form-group">
            		<div class="@if($errors->has('vat')) has-error @endif">
	              		<label for="" class="col-sm-2 control-label required">VAT</label>
	              		<div class="col-sm-4">
	                		<select name="vat" class="form-control chosen">
	                			<option value="0" @if(old('vat') == 0) selected @endif>No</option>
	                			<option value="1" @if(old('vat') == 1) selected @endif>Yes</option>
	                		</select>
	                		@if($errors->has('vat'))
	                		<span class="help-block">{{$errors->first('vat')}}</span>
	                		@endif
	              		</div>
	              	</div>
              		<div class="@if($errors->has('sales_office')) has-error @endif">
	              		<label for="" class="col-sm-2 control-label required">Sales Office</label>
	              		<div class="col-sm-4">
	                		<select name="sales_office" class="form-control chosen">
	                			@if(count($sales_offices) > 0)
	                				@foreach($sales_offices as $key => $value)
	                				<option value="{{$key}}" @if($key == old('sales_office')) selected @endif>{{$value}}</option>
	                				@endforeach
	                			@else
	                			<option value="">Select a Sales Office</option>
	                			@endif
	                		</select>
	                		@if($errors->has('sales_office'))
	                		<span class="help-block">{{$errors->first('sales_office')}}</span>
	                		@endif
	              		</div>
              		</div>
            	</div>
            	<div class="form-group">
            		<div class="@if($errors->has('max_discount')) has-error @endif">
	              		<label for="" class="col-sm-2 control-label">Max Discount (%)</label>
	              		<div class="col-sm-4">
	                		<input type="number" min="0" max="100" class="form-control" name="max_discount" value="{{old('max_discount')}}">
	                		@if($errors->has('max_discount'))
	                		<span class="help-block">{{$errors->first('max_discount')}}</span>
	                		@endif
	              		</div>
              		</div>
              		<div class="@if($errors->has('udp')) has-error @endif">
	              		<label for="" class="col-sm-2 control-label">UDP (%)</label>
	              		<div class="col-sm-4">
	                		<input type="number" min="0" max="100" class="form-control" name="udp" value="{{old('udp')}}">
	                		@if($errors->has('udp'))
	                		<span class="help-block">{{$errors->first('udp')}}</span>
	                		@endif
	              		</div>
              		</div>
            	</div>
			</div><!-- /.box-body -->
			<div class="box-footer">
				<button type="submit" class="btn btn-default pull-right btn-save">Save</button>
			</div>
			<div class="overlay" style="display:none;">
              <i class="fa fa-refresh fa-spin"></i>
            </div>
		</form>
	</div><!-- /.box -->
</section><!-- /.content -->

@stop
@section('js')

<script src="{{asset('assets/dist/chosen/chosen.jquery.min.js')}}"></script>

<script type="text/javascript">
$(document).ready(function() {
  $(".chosen").chosen();

  $('.btn-save').click(function(){
  	$('.overlay').show();
  });
});
</script>
@stop
