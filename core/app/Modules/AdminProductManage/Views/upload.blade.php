@extends('layouts.back_master') @section('title','Upload Products')
@section('css')
<link rel="stylesheet" href="{{asset('assets/dist/chosen/bootstrap-chosen.css')}}">
<style type="text/css">
  table .btn{
    padding: 2px 6px;
  }

  .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
    padding-top:5px; 
    padding-bottom:5px; 
  }

  .box-header, .box-body {
    padding: 20px;
  }
  .has-error .help-block, .has-error .control-label{
    color:#e41212;
  }
  .has-error .chosen-container{
    border:1px solid #e41212;
  }
</style>
@stop
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Product<small>Management</small></h1>
	<ol class="breadcrumb">
		<li><a href="{{url('/')}}"><i class="fa fa-home mr5"></i>Home</a></li>
    <li><a href="{{url('admin/product/list')}}">Product Management</a></li>
		<li class="active">Upload Products</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<!-- Default box -->
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Upload Products</h3>
		</div>
		<br>
    <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
  		<div class="box-body">
        {!!Form::token()!!}
        <div class="form-group @if($errors->has('category')) has-error @endif">
            <label for="" class="col-sm-2 control-label required">Product Category</label>
            <div class="col-sm-10">
              <select name="category" class="form-control chosen">
                @if(count($categories) > 0)
                  @foreach($categories as $key => $value)
                  <option value="{{$key}}" @if($key == old('category')) selected @endif>{{$value}}</option>
                  @endforeach
                @else
                <option value="">Select a Category</option>
                @endif
              </select>
              @if($errors->has('category'))
              <span class="help-block">{{$errors->first('category')}}</span>
              @endif
            </div>
        </div>
        <div class="form-group">
            <label for="" class="col-sm-2 control-label">Download</label>
            <div class="col-sm-10">
              <a href="{{asset('assets/excel_formats/product_upload.xls')}}">
                <i class="fa fa-download"></i> Sample Excel
              </a>
            </div>
        </div>
        <div class="form-group @if($errors->has('file')) has-error @endif">
            <label for="" class="col-sm-2 control-label required">Browse Excel</label>
            <div class="col-sm-10">
              <input type="file" name="file">
              @if($errors->has('file'))
              <span class="help-block">{{$errors->first('file')}}</span>
              @endif
            </div>
        </div>
        <button class="btn btn-default pull-right btn-save" type="submit">Upload</button><br/><br/><br/>
        @if(count(session('data')) > 0)
  		  <table class="table table-bordered" align="center" style="width:90%">
          <thead>
            <tr>
              <th>Code</th>
              <th>Name</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>
            @foreach(session('data') as $row)
              <tr>
                <td width="20%">{{ $row['code'] }}</td>
                <td>{{ $row['name'] }}</td>
                <td><span class="{{($row['status'])? 'text-success':'text-danger'}}">{{ $row['message'] }}</span></td>
              </tr>
            @endforeach
          </tbody>
        </table>
        @endif
  		</div><!-- /.box-body -->
      <div class="overlay" style="display:none;">
        <i class="fa fa-refresh fa-spin"></i>
      </div>
    </form>
	</div><!-- /.box -->
</section><!-- /.content -->

@stop
@section('js')

<script src="{{asset('assets/dist/chosen/chosen.jquery.min.js')}}"></script>

<script type="text/javascript">
$(document).ready(function() {
  $(".chosen").chosen();

  $('.btn-save').click(function(){
    $('.overlay').show();
  });
});
</script>
@stop
