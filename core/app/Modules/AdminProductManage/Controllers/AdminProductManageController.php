<?php namespace App\Modules\AdminProductManage\Controllers;


/**
* Controller class
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Modules\AdminProductManage\BusinessLogics\ProductLogic;
use App\Modules\AdminProductCategory\BusinessLogics\ProductCategoryLogic;


use App\Models\SalesOffice;

use Illuminate\Http\Request;
use Excel;	

class AdminProductManageController extends Controller {

	protected $productLogic;
	protected $categoryLogic;

	public function __construct(ProductLogic $productLogic, ProductCategoryLogic $categoryLogic){
		$this->productLogic = $productLogic;
		$this->categoryLogic = $categoryLogic;
	}

	public function addView()
	{
		$categories = $this->categoryLogic->getCategoryList()->prepend('Select a Category', '');
		$salesOffices = SalesOffice::lists('code','id')->prepend('Select a Sales Office', '');

		return view("AdminProductManage::add")->with([
			'categories' => $categories,
			'sales_offices' => $salesOffices
		]);
	}

	public function add(Request $request)
	{
		$this->validate($request, [
			'code' => 'required|unique:product,code,NULL,id,deleted_at,NULL',
			'name' => 'required',
			'category' => 'required',
			'max_discount' => 'max:100|numeric',
			'udp' => 'max:100|numeric',
			'sales_office' => 'required'
		]);

		try{
			$product = $this->productLogic->saveProduct($request);

			return redirect('admin/product/add')->with([
				'success' => true,
				'success.message' => 'Product added successfully',
				'success.title' => 'Successfull'
			]);

		}catch(\Exception $e){
			return redirect('admin/product/add')->with([
				'error' => true,
				'error.message' => $e->getMessage(),
				'error.title' => 'Error Occured'
			])->withInput();
		}
	}

	public function editView($id)
	{
		try{
			$product = $this->productLogic->getProductById($id);
		}catch(\Exception $e){
			return redirect('admin/product/list')->with([
				'error' => true,
				'error.title' => 'Error Occurred',
				'error.message' => $e->getMessage()
			]);
		}

		$categories = $this->categoryLogic->getCategoryList()->prepend('Select a Category', '');
		$salesOffices = SalesOffice::lists('code','id')->prepend('Select a Sales Office', '');

		return view("AdminProductManage::edit")->with([
			'product' => $product,
			'categories' => $categories,
			'sales_offices' => $salesOffices
		]);
	}

	public function edit($id, Request $request)
	{
		$this->validate($request, [
			'code' => 'required|unique:product,code,'.$id.',id,deleted_at,NULL',
			'name' => 'required',
			'category' => 'required',
			'max_discount' => 'max:100|numeric',
			'udp' => 'max:100|numeric',
			'sales_office' => 'required'
		]);

		try{
			$product = $this->productLogic->updateProduct($id,$request);

			return redirect('admin/product/list')->with([
				'success' => true,
				'success.message' => 'Product updated successfully',
				'success.title' => 'Successfull'
			]);

		}catch(\Exception $e){
			return redirect('admin/product/edit/'.$id)->with([
				'error' => true,
				'error.message' => $e->getMessage(),
				'error.title' => 'A1-Error Occured'
			])->withInput();
		}
	}

	public function listView(Request $request)
	{
		$old = $request;

		$categories = $this->categoryLogic->getCategoryList()->prepend('All Categories', '');

		$data = $this->productLogic->getRecords($old);

		return view("AdminProductManage::list", compact('old', 'data', 'categories'));
	}

	public function stockView(Request $request)
	{
		$old = $request;

		$categories = $this->categoryLogic->getCategoryList()->prepend('All Categories', '');

		$data = $this->productLogic->getStockRecords($old);

		return view("AdminProductManage::stock", compact('old', 'data', 'categories'));
	}

	public function uploadView(Request $request)
	{
		$categories = $this->categoryLogic->getCategoryList()->prepend('Select a Category', '');

		return view("AdminProductManage::upload")->with(compact('categories'));
	}

	public function upload(Request $request)
	{
		$this->validate($request, [
			'category' => 'required',
			'file' => 'required'
		]);

		set_time_limit(0);
		$data = $this->productLogic->uploadExcel($request->category, $request->file);

		return redirect('admin/product/upload')->with([
			'data' => $data
		]);
	}

	public function maxDiscountUploadView(Request $request)
	{
		$categories = $this->categoryLogic->getSessionCategoryList()->prepend('Select a Category', '');

		return view("AdminProductManage::max_discount_upload")->with(compact('categories'));
	}

	public function getMaxDiscountProducts(Request $request)
	{
		$this->validate($request, [
			//'category' => 'required'
		]);

		$products = $this->productLogic->getProductsByCategory($request->category);
		$category = $this->categoryLogic->getProductCategoryDetails($request->category);

		if(count($products) > 0){

		    // Initialize the array which will be passed into the Excel
		    // generator.
		    $productsArray = []; 

		    // Define the Excel spreadsheet headers
		    $productsArray[] = ['code','max_discount'];

		    // Convert each member of the returned collection into an array,
		    // and append it to the payments array.
		    foreach ($products as $product) {
		    	$dd = [];

		    	array_push($dd, $product->code);
		    	array_push($dd, $product->max_discount);

		    	array_push($productsArray, $dd);
		    }

		    // return $productsArray;

		    // Generate and return the spreadsheet
		    return Excel::create('products', function($excel) use ($productsArray, &$category) {

		        // Set the spreadsheet title, creator, and description
		        $excel->setTitle($category[0]->name.' Products');

		        // Build the spreadsheet, passing in the products array
		        $excel->sheet($category[0]->code.'-products sheet', function($sheet) use ($productsArray) {
		            $sheet->fromArray($productsArray, null, 'A1', false, false);
		        });

		    })->download('xls');
		}
		else{
			return redirect('admin/product/max_discount_upload')->with([
				'error' => true,
				'error.message' => 'Products Not Found.',
				'error.title' => 'Opz!'
			]);
		}
	}

	public function maxDiscountUpload(Request $request)
	{
		$this->validate($request, [
			'file' => 'required'
		]);

		set_time_limit(0);
		$data = $this->productLogic->uploadMaxDiscountExcel($request->file, true);

		return redirect('admin/product/max_discount_upload')->with([
			'data' => $data
		]);
	}

	public function delete(Request $request)
	{
		try{
			$this->productLogic->deleteProduct($request->id);
			return response()->json([
				'status' => 1
			]);
		}catch(\Exception $e){
			return response()->json([
				'status' => 0,
				'message' => $e->getMessage()
			]);
		}
	}
}
