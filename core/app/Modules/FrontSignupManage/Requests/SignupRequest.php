<?php
namespace App\Modules\FrontSignupManage\Requests;

use App\Http\Requests\Request;

class SignupRequest extends Request {

	public function authorize(){
		return true;
	}

	public function rules(){
		$rules = [
			'first_name'			=> 'required',
			'last_name'				=> 'required',
			'username'				=> 'required',
			'email'					=> 'required|email|unique:users,email',
			'password' 				=> 'required|confirmed|min:6',
			'password_confirmation'	=> 'required'
		];
		return $rules;
	}

}
