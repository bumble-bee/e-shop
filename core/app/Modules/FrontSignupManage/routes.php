<?php


// Route::group(['middleware' => ['auth','admin']], function()
// {	
	Route::group(['prefix'=>'/','namespace' => 'App\Modules\FrontSignupManage\Controllers'],function() {

	    Route::get('signup', [
	      'as' => 'front.signup', 'uses' => 'FrontSignupManageController@index'
	    ]);	

	    Route::post('signup', [
	      'as' => 'front.signup', 'uses' => 'FrontSignupManageController@create'
	    ]);	
	    
	});

    
// });
