<?php namespace App\Modules\FrontSignupManage\BusinessLogics;


/**
* Business Logics 
* Define all the busines logics in here
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Core\UserManage\Models\Employee;
use Core\UserManage\Models\User;
use Sentinel;
use DB;

class SignupLogic{

		
	public function addNewMember($member)
	{
		
		DB::beginTransaction();
		
		try{
			$user = Sentinel::registerAndActivate([
				'email'			=> $member->email,
				'username'		=> $member->username,
				'password'		=> $member->password
			]);

			if($member->sector!=0){
				User::where('id', $user->id)->update([
	                'first_name'  	=> $member->first_name,
	                'last_name'   	=> $member->last_name,
	                'sector_id' 	=> $member->sector,
	                'status'      	=> 1
	            ]);			

				$role = Sentinel::findRoleById($member->user_type);

			   	if(sizeof($role) > 0)	{
			   		$role->users()->attach($user);
			   	}else{
			   		throw new Exception("Role not found!.");
			   		DB::rollBack();
			   		
			   	}
			}else{
				throw new \Exception("Sector doesn't select");				
			}
		}catch(Exception $ex){
			throw new Exception($ex);
			DB::rollBack();
		}

		DB::Commit();

		return $user;
	}

	public function getMembers()
	{
		
	}

	public function getMember($member_id)
	{
		# code...
	}
}
