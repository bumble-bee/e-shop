<?php namespace App\Modules\FrontSignupManage\BusinessLogics;

/**
*
* Model
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;


class Member {

	public $first_name;
	public $last_name;
	public $sector;
	public $user_type;
	public $email;
	public $username;
	public $password;

}
