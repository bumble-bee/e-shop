@extends('layouts.front_master') @section('current_title','O|ITS') @section('navbar_','navbar-shrink')

@section('css')
<style type="text/css">  

</style>
@stop

@section('content')
<div class="row">	
	<div class="col-md-6 col-lg-6 col-md-offset-3 text-center" style="margin-top:5%">
        <form role="form" method="POST">
        {!! csrf_field() !!}
            <div class="card">
            @if(isset($success))
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Success!</strong> You have been signuped in successfully!
                </div>
            @elseif(isset($success))
                <div class="alert alert-danger" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Error!</strong> You have been faild signup!
                </div>
            @endif
                <form role="form" method="POST" >
                {!! csrf_field() !!}
                    <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }} has-feedback">
                        <input type="text" name="first_name" id="first_name" class="form-control" placeholder="First Name" value="{{Input::old('first_name')}}"> 
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        @if ($errors->has('first_name'))
                            <span class="help-block">{{ $errors->first('first_name') }}</span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }} has-feedback">
                        <input type="text" name="last_name" id="last_name" class="form-control" placeholder="Last Name" value="{{Input::old('last_name')}}"> 
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        @if ($errors->has('last_name'))
                            <span class="help-block">{{ $errors->first('last_name') }}</span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('sector') ? ' has-error' : '' }} has-feedback">
                        
                        @if($errors->has('sectors'))
                            <select name="sector" id="sector" class="form-control chosen">
                                <option value="0">-- Sector --</option>
                                @if(sizeof($sectors) > 0)
                                    @foreach($sectors as $key=>$sector)
                                        @if(Input::old('sector') == $key)
                                            <option value="{{ $key }}" selected>{{ $sector }}</option>
                                        @else
                                            <option value="{{ $key }}">{{ $sector }}</option>
                                        @endif
                                    @endforeach
                                @endif
                            </select>
                            <label id="sector-error" class="help-block" for="sector">
                                {{$errors->first('sector')}}
                            </label>
                        @else
                            <select name="sector" id="sector" class="form-control chosen">
                                <option value="0">-- Sector --</option>
                                @if(sizeof($sectors) > 0)
                                    @foreach($sectors as $key=>$sector)
                                        @if(Input::old('sector') == $key)
                                            <option value="{{ $key }}" selected>{{ $sector }}</option>
                                        @else
                                            <option value="{{ $key }}">{{ $sector }}</option>
                                        @endif
                                    @endforeach
                                @endif
                            </select>
                        @endif

                    </div>

                    <div class="form-group{{ $errors->has('sector') ? ' has-error' : '' }} has-feedback">
                        
                        @if($errors->has('user_type'))
                            <select name="user_type" id="user_type" class="form-control chosen">
                                <option value="">-- User Roles --</option>
                                @if(sizeof($user_types) > 0)
                                    @foreach($user_types as $user_type)
                                        @if(Input::old('user_type') == $user_type->id)
                                            <option value="{{ $user_type->id }}" selected>{{ $user_type->name }}</option>
                                        @else
                                            <option value="{{ $user_type->id }}">{{ $user_type->name }}</option>
                                        @endif
                                    @endforeach
                                @endif
                            </select>
                            <label id="user_type-error" class="help-block" for="user_type">
                                {{$errors->first('user_type')}}
                            </label>
                        @else
                            <select name="user_type" id="user_type" class="form-control chosen">
                                <option value="">-- User Roles --</option>
                                @if(sizeof($user_types) > 0)
                                    @foreach($user_types as $user_type)
                                        @if(Input::old('user_type') == $user_type->id)
                                            <option value="{{ $user_type->id }}" selected>{{ $user_type->name }}</option>
                                        @else
                                            <option value="{{ $user_type->id }}">{{ $user_type->name }}</option>
                                        @endif
                                    @endforeach
                                @endif
                            </select>
                        @endif

                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} has-feedback">
                        <input type="text" name="email" id="email" class="form-control" placeholder="Email" value="{{Input::old('email')}}"> 
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        @if ($errors->has('email'))
                            <span class="help-block">{{ $errors->first('email') }}</span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }} has-feedback">
                        <input type="text" name="username" id="username" class="form-control" placeholder="User Name" value="{{Input::old('username')}}"> 
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        @if ($errors->has('username'))
                            <span class="help-block">{{ $errors->first('username') }}</span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} has-feedback">
                        <input type="password" name="password" id="password" class="form-control" placeholder="Password" value="{{Input::old('password')}}">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        @if ($errors->has('password'))
                            <span class="help-block">{{ $errors->first('password') }}</span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }} has-feedback">
                        <input type="password" name="password_confirmation" class="form-control" placeholder="Confirm Password" value="{{Input::old('password_confirmation')}}">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">{{ $errors->first('password_confirmation') }}</span>
                        @endif
                    </div>

                    <div class="row" style="margin-bottom:10px;">
                        <div class="col-xs-12 text-center">
                            <button type="submit" class="btn btn-primary btn-flat" style="width:150px ">Sign Up</button>
                        </div>
                        <!-- /.col -->
                    </div>
                    <p><a href="{{ url('/login') }}">Sign in to your account</a></p>
                </form>		        
		    </div>	        
	    </form> 	
	</div>
</div>
@stop

@section('js')
    <script type="text/javascript">
        window.setTimeout(function() {
            $(".alert").fadeTo(100, 0).slideUp(100, function(){
                $(this).remove(); 
            });
        }, 4000);
    </script>
@stop
