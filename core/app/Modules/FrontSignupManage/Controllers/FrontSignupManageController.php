<?php namespace App\Modules\FrontSignupManage\Controllers;

/**
* Controller class
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\FrontSignupManage\BusinessLogics\SignupLogic;
use App\Modules\FrontSignupManage\BusinessLogics\Member;

use Illuminate\Http\Request;
use App\Modules\FrontSignupManage\Requests\SignupRequest;

use Core\Sector\Models\Sector;
use Core\UserRoles\Models\UserRole;

class FrontSignupManageController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		$sectors   	= Sector::all()->lists('name','id');
		$roles 		= UserRole::select('name' , 'id' )->get();

		return view("FrontSignupManage::index")->with([
			'user_types'	=> $roles,
			'sectors'		=> $sectors
		]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		$newMember = new Member();
   		$newMember->first_name 	= $request->first_name;
   		$newMember->last_name 	= $request->last_name;
   		$newMember->sector 		= $request->sector;
   		$newMember->user_type 	= $request->user_type;
   		$newMember->email 		= $request->email;
   		$newMember->username 	= $request->username;
   		$newMember->password 	= $request->password;

		try{
			$logic = new  SignupLogic();	
			$res=$logic->addNewMember($newMember);
			if($res){
				$success=1;
			}else{
				$success=0;
			}

			$sectors   	= Sector::all()->lists('name','id');
			$roles 		= UserRole::select('name' , 'id' )->get();

			return view("FrontSignupManage::index")->with([
				'user_types'	=> $roles,
				'sectors'		=> $sectors,
				'success'		=>$success
			]);

		}catch(Exception $ex){

		}	

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
