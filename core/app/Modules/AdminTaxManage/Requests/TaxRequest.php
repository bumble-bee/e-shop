<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 12/10/2015
 * Time: 10:53 AM
 */

namespace App\Modules\AdminTaxManage\Requests;

use App\Http\Requests\Request;

class TaxRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'vat_value' => 'required'
        ];
        return $rules;
    }

}