<?php
namespace App\Modules\AdminTaxManage\Models;

/**
 * Permission Model Class
 *
 *
 * @category   Models
 * @package    Model
 * @author     Chathura Chandimal <chandimal@craftbyorange.com>
 *
 */
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tax extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'vat';

    /**
     * The attributes that are not assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

}