<?php namespace App\Modules\AdminCustomerManage\BusinessLogics;


/**
* Business Logics 
* Define all the busines logics in here
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/
use Illuminate\Database\Eloquent\Model;
use App\Modules\AdminCustomerManage\Models\Customer;
use DB;

class CustomerLogic {

	private $customer;

	public function __construct(Customer $customer){
		$this->customer = $customer;
	}

	public function getCustomerById($id){
		$customer = Customer::with(['pricebook'])->find($id);

		if(!$customer){
			throw new \Exception('Customer not found for ID:'.$id);
		}

		return $customer;
	}

	/**
     * This function is used to get all customer details
     * @parm -
     * @return customer object
     */
    public function getAllCustomers(){
        return Customer::paginate(10);
    }

    /**
     * This function is used to search customer
     * @param integer $name, $code
     * @response product category object
     */
    public function searchCustomer($request) {
        $search = Customer::select('*',DB::raw("CONCAT(`first_name`, ' ', `last_name`) as name"));

        if($request->name != ''){
            $ds = explode(' ', $request->name);
            if(count($ds) > 0){
                foreach($ds as $ss){
                    $search->where(function($q) use ($ss){
                        $q->where('code', 'like', '%'.$ss.'%')
                            ->orWhere('last_name','like','%'.$ss.'%')
                            ->orWhere('first_name','like','%'.$ss.'%');
                    });
                }
            }else{
                $search->where(function($q) use ($request){
                        $q->where('code', 'like', '%'.$request->name.'%')
                            ->orWhere('last_name','like','%'.$request->name.'%')
                            ->orWhere('first_name','like','%'.$request->name.'%');
                    });
            }
        }

        if($request->credit_limit != ''){
            $search->where('credit_limit', '=', $request->credit_limit);
        }

        if($request->credit_period != ''){
            $search = $search->where('credit_period', '=', $request->credit_period);
        }

        if($request->status != ''){
            $search = $search->where('blocked', '=', $request->status);
        }

        $search->orderBy('created_at','desc');
        return $search->paginate(25);
    }

    public function getCustomersWithPriceBook($priceBookId, $isPaginate = false, $hasFilters = false, $filters){
        $customer = Customer::with(['current_pricebook' => function($qry) use ($priceBookId){
                $qry->where('pricebook_id', $priceBookId);
            }])->where('status', 1);

        if($hasFilters){
            if($filters->keyword != ''){
                $customer->where(function($qry) use ($filters){
                    $qry->where('code','LIKE', '%'.$filters->keyword.'%')
                        ->orWhere('first_name','LIKE', '%'.$filters->keyword.'%')
                        ->orWhere('last_name','LIKE', '%'.$filters->keyword.'%');
                });
            }
        }

        if($isPaginate){
            $customer = $customer->paginate(25);
        }else{
            $customer = $customer->get();
        }


        return $customer;
    }

}
