@extends('layouts.back_master') @section('title','Admin - Customer Management')
@section('current_title','Customer List')

@section('css')
<style type="text/css">
  .box-header, .box-body {
    padding: 20px;
  }
</style>
@stop

@section('content')
<!-- Content-->
<section>
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Customer
      <small>Management</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li>Customer</li>
      <li class="active">List</li>
    </ol>
  </section>
  <!-- !!Content Header (Page header) -->

  <!-- Main content -->
  <section class="content">  
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">List Customer</h3>
        <!-- <button type="button" class="btn btn-primary btn-sm pull-right" onclick="window.location.href='{{url('admin/customer/create')}}'">Add Customer</button> -->
      </div>
      <div class="box-body">  
        <form id="form" role="form" method="get" action="">
          <div class="form-group">
            <div class="row">
              <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="form-group">
                  <label>Customer</label>
                  <input type="text" class="form-control input-sm" name="name" placeholder="" value="{{$old->name}}">
                </div>
              </div>
              <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="form-group">
                  <label>Credit Limit</label>
                  <input type="text" class="form-control input-sm" name="credit_limit" placeholder="" value="{{$old->credit_limit}}">
                </div>
              </div>
              <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="form-group">
                  <label>Credit Period</label>
                  <input type="text" class="form-control input-sm" name="credit_period" placeholder="" value="{{$old->credit_period}}">
                </div>
              </div>
              <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="form-group">
                  <label>Status</label>
                  <select class="form-control" name="status">
                    <option value="">All Status</option>
                    <option value="0" @if($old->status != '' && $old->status == 0) selected @endif>Available</option>
                    <option value="1" @if($old->status == 1) selected @endif>Blocked</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3 col-md-offset-9">
                <div class="pull-right">
                  <button type="submit" class="btn btn-default btn-sm" id="plan"><i class="fa fa-search" style="padding-right: 16px;width: 28px;"></i>Find</button>
                  <a href="index" class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top"><i class="fa fa-refresh" style="padding-right: 16px;width: 20px;"></i>Refresh</a>
                </div>
              </div>
            </div>
          </div>
        </form>

        <table class="table table-bordered bordered table-striped table-condensed" id="orderTable">
          <thead>
            <tr>
              <th width="5%">#</th>
              <th>Code</th>
              <th>Name</th>
              <th>Credit Limit</th>
              <th>Credit Period</th>
              <th>Status</th>
              <th>Created At</th>
            </tr>
          </thead>
          <tbody>
          <?php $i = 1;?>
            @if(count($customer_details) > 0)
              @foreach($customer_details as $result_val)
                @include('AdminCustomerManage::template.customer')
              <?php $i++;?>
              @endforeach
            @else
              <tr><td colspan="7" class="text-center">No data found.</td></tr>
            @endif
          </tbody>
        </table>

        @if($customer_details != null)
          <div style="float: right;">{!! $customer_details->render() !!}</div>
        @endif
      </div>
    </div>

  </section>
  <!-- !!Main content -->

</section>
<!-- !!!Content -->
@stop

@section('js')
@stop
