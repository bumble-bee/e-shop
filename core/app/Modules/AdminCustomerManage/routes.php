<?php


Route::group(['middleware' => ['auth']], function()
{	
	Route::group(['prefix'=>'admin/customer/','namespace' => 'App\Modules\AdminCustomerManage\Controllers'],function() {

        Route::get('index', [
            'as' => 'customer.index', 'uses' => 'AdminCustomerManageController@index'
        ]);

        Route::get('search', [
            'as' => 'customer.search', 'uses' => 'AdminCustomerManageController@searchCustomer'
        ]);

	});    
});