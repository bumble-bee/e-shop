<?php namespace App\Modules\AdminCustomerManage\Models;

/**
*
* Model
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class CustomerPriceBook extends Model {

	protected $table = 'customer_pricebooks';

	protected $fillable = [
		'customer_id',
		'pricebook_id',
		'ended_at',
	];

	public function pricebook(){
		return $this->belongsTo('App\Modules\AdminPriceManage\Models\PriceBook','pricebook_id', 'id');
	}

	public function customer(){
		return $this->belongsTo('App\Modules\AdminCustomerManage\Models\Customer','customer_id', 'id');
	}

}
