<?php namespace App\Modules\AdminCustomerManage\Models;

/**
*
* Model
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Customer extends Model {

	use SoftDeletes;

	protected $table = 'customer';

	protected $dates = ['deleted_at'];

	protected $fillable = [
		'code',
		'first_name',
		'last_name',
		'blocked',
		'credit_limit',
		'credit_period',
		'address',
		'contact_no',
		'status',
		'customer_group',
		'payment_terms'
	];

	public function pricebook(){
		return $this->belongsToMany('App\Modules\AdminPriceManage\Models\PriceBook',
				'customer_pricebooks', 
				'customer_id', 
				'pricebook_id')
			->whereNull('ended_at');
	}

	public function current_pricebook(){
		return $this->hasOne('App\Modules\AdminCustomerManage\Models\CustomerPriceBook','customer_id', 'id')
			->whereNull('ended_at');
	}

}
