<?php namespace App\Modules\AdminCustomerManage\Controllers;


/**
* Controller class
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Modules\AdminCustomerManage\BusinessLogics\CustomerLogic;

class AdminCustomerManageController extends Controller {

	protected $customer;

    public function __construct(CustomerLogic $customer){
        $this->customer = $customer;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		$customer_details = $this->customer->searchCustomer($request);

		return view("AdminCustomerManage::index")->with([
			'customer_details' 	=> $customer_details,
			'old' 				=> $request
		]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	/**
     * This function is used to search customer
     * @return Response
     */
    public function searchCustomer(Request $request){
        $customer_details = $this->customer->searchCustomer($request->get('name'), $request->get('credit_limit'), $request->get('credit_period'));
        
        return view("AdminCustomerManage::index")->with([
        	'customer_details' 	=> $customer_details,
        	'name' 				=> $request->get('name'),
        	'credit_limit' 		=> $request->get('credit_limit'),
			'credit_period' 	=> $request->get('credit_period')
        ]);
    }

}
