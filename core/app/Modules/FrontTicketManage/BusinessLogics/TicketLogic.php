<?php namespace App\Modules\FrontTicketManage\BusinessLogics;


/**
* Business Logics 
* Define all the busines logics in here
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/
use Illuminate\Database\Eloquent\Model;
use App\Models\Ticket;

class TicketLogic extends Model {

	

	public function addNewTicket($title,$category,$priority,$message,$user)
	{
		$ticket = Ticket::create([
            'title'        => $title,
            'user_id'      => $user->id,
            'category_id'  => $category,
            'priority_id'  => $priority,
            'message'      => $message,
        ]);
        
        $ticket->status_id = 1;
        $ticket->ticket_id = "TCK".str_pad($ticket->id, 6, '0', STR_PAD_LEFT);
        //$mailer->sendTicketInformation($user->id, $ticket);
        return $ticket->save();
	}

}
