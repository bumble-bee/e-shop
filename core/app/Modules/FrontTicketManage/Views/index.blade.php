@extends('layouts.front_master') 
@section('current_title','O|ITS')
@section('navbar_','navbar-shrink')

@section('css')

<style type="text/css">
  

</style>

@stop


@section('content')
<div class="row">	
	@if ($tickets->isEmpty())
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="margin-top:8%;">
            <p>You have not created any tickets.</p>
            <a href="{{ url('ticket/create') }}">
                Create Ticket
            </a>
        </div>
    @else
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <table class="table">
            <thead>
                <tr>
                    <th>Ticket</th>
                    <th>Title</th>
                    <th>Comments</th>
                    <th>Category</th>
                    <th>Status</th>
                    <th>Priority</th>
                    <th>Created</th>                                
                    <th>Last Updated</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($tickets as $ticket)
                <tr>
                    <td><span class="label label-default">#{{ $ticket->ticket_id }}</span></td>
                    <td>
                        <a href="{{ url('ticket/show/'. $ticket->ticket_id) }}">
                            {{ $ticket->title }}
                        </a>
                    </td>   
                    <td><span class="badge">{{ count($ticket->comments) }}</span></td>
                    <td>
                    @foreach ($categories as $category)
                        @if ($category->id === $ticket->category_id)
                            {{ $category->name }}
                        @endif
                    @endforeach
                    </td>
                    <td>
                    @foreach ($statuses as $status)
                        @if ($status->id === $ticket->status_id)
                            @if ($status->id === 1)
                            <span class="label label-info"> {{ $status->name }}</span>
                            @elseif ($status->id === 2)
                            <span class="label label-warning"> {{ $status->name }}</span>
                            @elseif ($status->id === 3)
                            <span class="label label-success"> {{ $status->name }}</span>
                            @else
                            <span class="label label-danger"> {{ $status->name }}</span>
                            @endif                                     
                        @endif
                    @endforeach
                    </td>
                    <td>
                    @foreach ($prioritys as $priority)
                        @if ($priority->id === $ticket->priority_id)
                            @if ($priority->id === 1)
                            <span class="label label-success"> {{ $priority->name }}</span>
                            @elseif ($priority->id === 2)
                            <span class="label label-warning"> {{ $priority->name }}</span>
                            @else 
                            <span class="label label-danger"> {{ $priority->name }}</span>
                            @endif                                        
                        @endif
                    @endforeach
                    </td>                                    
                    <td>{{ $ticket->created_at->diffForHumans() }}</td>
                    <td>{{ $ticket->updated_at->diffForHumans() }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div> 
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <hr style="margin-bottom:0">
    </div>
    <div class="col-md-12 text-right">
        <?php echo $tickets->render(); ?>
    </div>
    @endif
</div>
@stop


@section('js')
 
@stop
