@extends('layouts.front_master') 
@section('current_title','O|ITS')
@section('navbar_','navbar-shrink')

@section('css')

<link rel="stylesheet" href="{{asset('assets/dist/summernote/summernote.css')}}">
  
<style type="text/css">
  

</style>

@stop


@section('content')
<div class="row">        
    <form role="form" method="POST">
        {!! csrf_field() !!}
        <div class="row"> 
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                

                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <p>Title</p>
                            <input id="title" type="text" class="form-control" name="title" placeholder="Type in a title" value="{{ old('title') }}">
                            @if ($errors->has('title'))
                                <span class="help-block">
                                    {{ $errors->first('title') }}
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                            <p>Category</p>
                            <select id="category" type="category" class="form-control" name="category">
                                <option value="">Select Category</option>
                                @foreach ($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('category'))
                                <span class="help-block">
                                    {{ $errors->first('category') }}
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3"> 
                        <div class="form-group{{ $errors->has('priority') ? ' has-error' : '' }}">
                            <p>Priority</p>
                            <select id="priority" type="priority" class="form-control" name="priority">
                                <option value="">Select Priority</option>
                                @foreach ($prioritys as $priority)
                                <option value="{{ $priority->id }}">{{ $priority->name }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('priority'))
                                <span class="help-block">
                                    {{ $errors->first('priority') }}
                                </span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
                    <p>Message</p>
                    <textarea rows="5" id="summernote" class="form-control" placeholder="Type in a message" name="message"></textarea>
                    @if ($errors->has('message'))
                        <span class="help-block">
                            {{ $errors->first('message') }}
                        </span>
                    @endif
                </div>

                <div class="form-group pull-right">
                    <button type="submit" class="btn btn-success">
                         Open Ticket
                    </button>
                </div>   
                         
            </div>    
        </div>
    </form> 
</div>
@stop


@section('js')
<script src="{{asset('assets/dist/summernote/summernote.js')}}"></script> 
 <script type="text/javascript">
    $(document).ready(function () {
        $('#summernote').summernote({
          height: 200,                 // set editor height
          minHeight: null,             // set minimum height of editor
          maxHeight: null,             // set maximum height of editor
          focus: true,
          toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']]]
        });
        $('.dropdown-toggle').dropdown()
    });            
 </script>
@stop
