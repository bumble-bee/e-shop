<?php
namespace App\Modules\FrontTicketManage\Requests;

use App\Http\Requests\Request;

class TicketRequest extends Request {

	public function authorize(){
		return true;
	}

	public function rules(){
		$rules = [
			'title'				=> 'required',
			'category'			=> 'required|exists:categories,id',
			'priority'			=> 'required|exists:priorities,id',
			'message'			=> 'required',
		];
		return $rules;
	}

}
