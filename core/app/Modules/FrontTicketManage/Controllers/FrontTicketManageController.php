<?php namespace App\Modules\FrontTicketManage\Controllers;


/**
* Controller class
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Ticket;
use App\Models\Category;
use App\Models\Priority;
use App\Models\Status;
use Sentinel;
use App\Modules\FrontTicketManage\BusinessLogics\TicketLogic;
use App\Modules\FrontTicketManage\Requests\TicketRequest;

class FrontTicketManageController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{	
		$user = Sentinel::getUser();
		$tickets = Ticket::orderBy('created_at', 'desc')->where('user_id', $user->id)->paginate(1);
        $categories = Category::all();
        $prioritys = Priority::all();
        $statuses = Status::all();
		return view("FrontTicketManage::index",compact('tickets', 'categories', 'statuses', 'prioritys'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$categories = Category::all();
        $prioritys = Priority::all();
        $statuses = Status::all();
        return view("FrontTicketManage::create",compact('categories', 'statuses', 'prioritys'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(TicketRequest $request)
	{
		$user = Sentinel::getUser();
        $logic = new TicketLogic();
        //$title,$category,$priority,$message,$user
        $logic->addNewTicket(
        	$request->title,
        	$request->category,
        	$request->priority,
        	$request->message,
        	$user
        );
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($ticket_id)
	{	
		$user = Sentinel::getUser();
		$ticket = Ticket::where('ticket_id', $ticket_id)->where('user_id', $user->id)->firstOrFail();
        $category = $ticket->category;
        $comments = $ticket->comments;
        $status = $ticket->status;
        $priority = $ticket->priority;
        return view('FrontTicketManage::show', compact('ticket', 'category', 'status', 'priority', 'comments'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
