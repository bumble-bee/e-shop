<?php

Route::group(['middleware' => ['auth.front']], function()
{	
	Route::group(['prefix'=>'/','namespace' => 'App\Modules\FrontTicketManage\Controllers'],function() {

	    Route::get('mytickets', [
	      'as' => 'front.tickets', 'uses' => 'FrontTicketManageController@index'
	    ]);	

	    Route::get('ticket/create', [
	      'as' => 'front.create.ticket', 'uses' => 'FrontTicketManageController@create'
	    ]);	

	    Route::get('ticket/show/{id}', [
	      'as' => 'front.create.ticket', 'uses' => 'FrontTicketManageController@show'
	    ]);	

	    Route::post('mytickets', [
	      'as' => 'front.tickets', 'uses' => 'FrontTicketManageController@create'
	    ]);

	    Route::post('ticket/create', [
	      'as' => 'front.create.ticket', 'uses' => 'FrontTicketManageController@store'
	    ]);	    	


	});    
});
