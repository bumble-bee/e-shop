<?php


Route::group(array('prefix'=>'test','module' => 'AdminFollowUpManage', 'namespace' => 'App\Modules\AdminFollowUpManage\Controllers'), function() {

    Route::resource('AdminFollowUpManage', 'AdminFollowUpManageController');
    
}); 