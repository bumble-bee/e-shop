<?php namespace App\Modules\AdminFollowUpManage\Models;

/**
*
* Model
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class FollowUp extends Model {

	use SoftDeletes;
	
	protected $dates = ['deleted_at'];

	protected $table = 'followup';

	protected $fillable = [
		'title',
		'note',
		'remark',
		'followup_date',
		'action_time',
		'inquiry_id',
		'status',
		'followup_type_id',
		'action_by',
		'timestamp'
	];

	public function type(){
		return $this->belongsTo('App\Modules\AdminFollowUpManage\Models\FollowUpType','followup_type_id','id')->withTrashed();
	}

}
