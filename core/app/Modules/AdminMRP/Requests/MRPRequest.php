<?php
namespace App\Modules\AdminMRP\Requests;

use App\Http\Requests\Request;

class MRPRequest extends Request {

	public function authorize(){
		return true;
	}

	public function rules(){
		$rules = [
			'product_id'	=> 'required',
			'mrp'			=> 'required'
		];
		return $rules;
	}

}
