@extends('layouts.back_master') @section('title','Admin - MRP Management')
@section('current_title','MRP List')

@section('css')
<style type="text/css">
  .box-header, .box-body {
    padding: 20px;
  }
  .has-error .help-block, .has-error .control-label{
    color:#e41212;
  }
  .has-error .chosen-container{
    border:1px solid #e41212;
  }
</style>
@stop

@section('content')
<!-- Content-->
<section>
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      MRP
      <small>Management</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li>MRP</li>
      <li class="active">List</li>
    </ol>
  </section>
  <!-- !!Content Header (Page header) -->

  <!-- Main content -->
  <section class="content">  
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">List MRP</h3>
        <button type="button" class="btn btn-primary btn-sm pull-right" onclick="window.location.href='{{url('admin/mrp/create')}}'">Add MRP</button>
      </div>
      <div class="box-body">
        <form id="form" role="form" method="get" action="{{url('admin/mrp/search')}}">
          <div class="form-group">
            <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                  <label>Product Name</label>
                  <input type="text" class="form-control input-sm" name="name" placeholder="" value="{{$name}}">
                </div>
              </div>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                  <label>Product Code</label>
                  <input type="text" class="form-control input-sm" name="code" placeholder="" value="{{$code}}">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3 col-md-offset-9">
                <div class="pull-right">
                  <button type="submit" class="btn btn-default btn-sm" id="plan"><i class="fa fa-search" style="padding-right: 16px;width: 28px;"></i>Find</button>
                  <a href="index" class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top"><i class="fa fa-refresh" style="padding-right: 16px;width: 20px;"></i>Refresh</a>
                </div>
              </div>
            </div>
          </div>
        </form>

        <table class="table table-bordered bordered table-striped table-condensed" id="orderTable">
          <thead>
            <tr>
              <th width="5%">#</th>
              <th>Product Code</th>
              <th>Product Name</th>
              <th>MRP</th>
              <th>Created At</th>
              <th width="10%">Action</th>
            </tr>
          </thead>
          <tbody>
          <?php $i = 1;?>
            @if(count($mrp_details) > 0)
              @foreach($mrp_details as $result_val)
                @include('AdminMRP::template.mrp')
              <?php $i++;?>
              @endforeach
            @else
              <tr><td colspan="6" class="text-center">No data found.</td></tr>
            @endif
          </tbody>
        </table>

        @if($mrp_details != null)
          <div style="float: right;">{!! $mrp_details->render() !!}</div>
        @endif
      </div>
    </div>

  </section>
  <!-- !!Main content -->

</section>
<!-- !!!Content -->
@stop

@section('js')
<script type="text/javascript">
    /*$(document).ready(function(){
        $('.delete').click(function(e){
            e.preventDefault();
            id = $(this).data('id');
            $('#form').trigger('submit', [ { 'variable_name': true } ]);
        });
    });*/
</script>
@stop
