<?php namespace App\Modules\FrontLoginManage\Controllers;


/**
* Controller class
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Sentinel;
use Session;	
use Input;

class FrontLoginManageController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view("FrontLoginManage::index");
	}


	/**
	 * Validate front end login.
	 *
	 * @return Response
	 */
	public function login(Request $request)
	{
		$credentials = array(
			'username'    => $request->email,
			'password' => $request->password
		);

		$remember = false;

		try{
			$user = Sentinel::authenticate($credentials, $remember);

			if ($user){
				$redirect = Session::get('loginRedirect', 'admin');
				Session::forget('loginRedirect');
				return redirect('admin');

			}else{
				$msg = 'Invalid username/password. Try again!';
			}
		}catch(\Exception $e){
			$msg = $e->getMessage();
		}
		return redirect()->route('front.login')->withErrors(array('login' => $msg))->withInput(Input::except('password'));
	}

	/*
	*	@method logout()
	*	@description Logging out the logged in user
	*	@return URL redirection
	*/
	public function logout(){
		 Sentinel::logout();
		 return redirect()->route('front.index');

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
