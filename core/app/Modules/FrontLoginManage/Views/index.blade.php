@extends('layouts.front_master') 
@section('current_title','O|ITS')
@section('navbar_','navbar-shrink')

@section('css')

<style type="text/css">
  

</style>

@stop


@section('content')
<div class="row">	
	<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 col-md-offset-4 text-center" style="margin-top:5%">
	     <form role="form" method="POST">
	        {!! csrf_field() !!}
	        <div class="card">

	        	<p>Sign in to your account</p>

		        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} has-feedback">
			        <input type="text" name="email" id="email" class="form-control " placeholder="Email"> 
			        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
			            @if ($errors->has('email'))
			                <span class="help-block">{{ $errors->first('email') }}</span>
			            @endif
		        </div>
		            
		        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} has-feedback">
			        <input type="password" name="password" id="password" class="form-control search-field" placeholder="Password">
			        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
			            @if ($errors->has('password'))
			                <span class="help-block">{{ $errors->first('password') }}</span>
			            @endif
		        </div>
		                        

		      	<div class="row text-center" style="margin-bottom:10px;">
			          <button type="submit" class="btn btn-primary btn-flat" style="width:150px ">Sign In</button>
		      	</div>
		      	<div class="row">
		      		<p><a href="{{ url('/password/reset') }}">I forgot my password</a></p>
		      	</div>
		              
		        <!-- <div class="social-auth-links text-center">
		          <p>- OR -</p>
		            <a href="{{ url('/auth/facebook') }}" class="btn btn-block btn-social btn-facebook">
		            <i class="fa fa-facebook"></i>Sign in with Facebook</a>
		            

		            <a href="{{ url('/auth/google') }}" class="btn btn-block btn-social btn-google"><i class="fa fa-google-plus"></i>Sign in with Google</a>
		            
		                                  
		        </div> -->
		        <!-- /.social-auth-links -->
		    </div>	        
	    </form> 	
	</div>
</div>

@stop


@section('js')
 
@stop
