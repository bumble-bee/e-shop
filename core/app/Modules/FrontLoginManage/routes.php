<?php


Route::group(['prefix'=>'/','namespace' => 'App\Modules\FrontLoginManage\Controllers'],function() {

    Route::get('login', [
      'as' => 'front.login', 'uses' => 'FrontLoginManageController@index'
    ]);	

    Route::get('logout', [
      'as' => 'front.logout', 'uses' => 'FrontLoginManageController@logout'
    ]);	

    Route::post('login', [
      'as' => 'front.login', 'uses' => 'FrontLoginManageController@login'
    ]);	
	    
});
