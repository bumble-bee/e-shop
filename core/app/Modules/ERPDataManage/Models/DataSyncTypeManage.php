<?php namespace App\Modules\ERPDataManage\Models;

/**
*
* Model
* @nilusha De silva Author <thnsdesilva@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class DataSyncTypeManage extends Model {

    /**
     * table row delete
     */
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sync_type';

    /**
     * The attributes that are not assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];


    public function type()
    {
        return $this->hasMany('App\Modules\ERPDataManage\Models\DataSyncStatusManage','sync_type_id')->orderBy('created_at','desc');
    }

}
