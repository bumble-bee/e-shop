<?php

Route::group(['middleware' => ['auth']], function(){

    Route::group(array('prefix'=>'admin/erp','module' => 'ERPDataManage', 'namespace' => 'App\Modules\ERPDataManage\Controllers'), function() {

        /*** GET Routes**/

        Route::get('customers', ['uses' => 'ERPDataManageController@index']);

        Route::get('allProduct', ['uses' => 'ERPDataManageController@getAllProduct']);

        Route::get('list', [
            'as' => 'erp.sync', 'uses' => 'ERPDataManageController@listView'
        ]);

        Route::get('export', [
            'as' => 'erp.export', 'uses' => 'ERPDataManageController@exportView'
        ]);


        Route::get('export/csv', [
            'as' => 'erp.export', 'uses' => 'ERPDataManageController@exportCsv'
        ]);

        Route::post('export', [
            'as' => 'erp.export', 'uses' => 'ERPDataManageController@export'
        ]);

        /**
         * POST Routes
         */
        Route::any('add', [
            'as' => 'erp.sync', 'uses' => 'ERPDataManageController@create'
        ]);

    });

});