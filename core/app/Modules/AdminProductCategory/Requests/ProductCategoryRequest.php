<?php
namespace App\Modules\AdminProductCategory\Requests;

use App\Http\Requests\Request;

class ProductCategoryRequest extends Request {

	public function authorize(){
		return true;
	}

	public function rules(){
		$rules = [
			'name'				=> 'required',
			'code'				=> 'required'
			
		];
		return $rules;
	}

}
