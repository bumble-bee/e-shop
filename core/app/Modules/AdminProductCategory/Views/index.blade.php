@extends('layouts.back_master') @section('title','Admin - Product Category Management')
@section('current_title','Product Category List')

@section('css')
<style type="text/css">
  .box-header, .box-body {
    padding: 20px;
  }
</style>
@stop

@section('content')
<!-- Content-->
<section>
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Product Category
      <small>Management</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li>Product Category</li>
      <li class="active">List</li>
    </ol>
  </section>
  <!-- !!Content Header (Page header) -->

  <!-- Main content -->
  <section class="content">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">List Product Category</h3>
        <button type="button" class="btn btn-primary btn-sm pull-right" onclick="window.location.href='{{url('admin/product-category/create')}}'">Add Product Category</button>
      </div>
      <div class="box-body">  
        <form id="form" role="form" method="get" action="{{url('admin/product-category/search')}}">
          <div class="form-group">
            <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                  <label>Name</label>
                  <input type="text" class="form-control input-sm" name="name" placeholder="" value="{{$name}}">
                </div>
              </div>
              <div class="col-md-6 col-sm-6 col-xs-12">
               <div class="form-group">
                  <label>Code</label>
                  <input type="text" class="form-control input-sm" name="code" placeholder="" value="{{$code}}">
               </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3 col-md-offset-9">
                <div class="pull-right">
                  <button type="submit" class="btn btn-default btn-sm" id="plan"><i class="fa fa-search" style="padding-right: 16px;width: 28px;"></i>Find</button>
                  <a href="list" class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top"><i class="fa fa-refresh" style="padding-right: 16px;width: 20px;"></i>Refresh</a>
                </div>
              </div>
            </div>
          </div>
        </form>

        <table class="table table-bordered bordered table-striped table-condensed" id="orderTable">
          <thead>
            <tr>
              <th width="5%">#</th>
              <th>Product Category Code</th>
              <th>Product Category Name</th>
              <th>Product Category Parent</th>
              <th>Created At</th>
              <th width="10%">Action</th>
            </tr>
          </thead>
          <tbody>
          <?php $i = 1;?>
            @if(count($product_category_details) > 0)
              @foreach($product_category_details as $result_val)
                  @include('AdminProductCategory::template.category')
              <?php $i++;?>
              @endforeach
            @else
              <tr><td colspan="6" class="text-center">- No data found -</td></tr>
            @endif
          </tbody>
        </table>

        @if($product_category_details != null)
          <div style="float: right;">{!! $product_category_details->render() !!}</div>
        @endif
      </div>
    </div>

  </section>
  <!-- !!Main content -->

</section>
<!-- !!!Content -->
@stop

@section('js')
<script type="text/javascript">
    /*$(document).ready(function(){
        $('.delete').click(function(e){
            e.preventDefault();
            id = $(this).data('id');
            $('#form').trigger('submit', [ { 'variable_name': true } ]);
        });
    });*/
</script>
@stop
