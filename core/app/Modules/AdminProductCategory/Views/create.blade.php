@extends('layouts.back_master') @section('title','Admin - Product Category Management')
@section('current_title','Product Category Create')

@section('css')
<link rel="stylesheet" href="{{asset('assets/dist/chosen/bootstrap-chosen.css')}}">

<style type="text/css">
	.box-header, .box-body {
		padding: 20px;
	}
  .has-error .help-block, .has-error .control-label{
    color:#e41212;
  }
  .has-error .chosen-container{
    border:1px solid #e41212;
  }
</style>
@stop

@section('content')
<!-- Content-->
<section>
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      	Product Category
      	<small>Management</small>
    </h1>
    <ol class="breadcrumb">
      	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      	<li>Product Category</li>
      	<li class="active">Add</li>
    </ol>
  </section>
  <!-- !!Content Header (Page header) -->

  <!-- Main content -->
  <section class="content">

    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Add Product Category</h3>
      </div>
      <div class="box-body">
      	<form role="form" class="form-horizontal form-validation" method="post">
    			{!!Form::token()!!}      			   	                             
    			<div class="form-group">
            <div class="@if($errors->has('name')) has-error @endif">
          		<label class="col-sm-2 control-label required">Name</label>
          		<div class="col-sm-4">
          			<input type="text" class="form-control" name="name" placeholder="Name" value="{{Input::old('name')}}">
          			@if($errors->has('name'))
          				<span class="help-block">{{$errors->first('name')}}</span>
          			@endif
          		</div>
            </div>
            <div class="@if($errors->has('code')) has-error @endif">
              <label class="col-sm-2 control-label required">Code</label>
              <div class="col-sm-4">
                <input type="text" class="form-control" name="code" placeholder="Code" value="{{Input::old('code')}}">
                @if($errors->has('code'))
                  <span class="help-block">{{$errors->first('code')}}</span>
                @endif
              </div>
            </div>
          </div>            
        	<div class="form-group">
        		<label class="col-sm-2 control-label">Parent Category</label>
        		<div class="col-sm-4">            			
      				{!! Form::select('parent', $categories, Input::old('parent'),['class'=>'chosen','style'=>'width:100%;','required','data-placeholder'=>'Choose Parent']) !!}
        		</div>
            <label class="col-sm-2 control-label">Parent Order</label>
            <div class="col-sm-4">
              {!! Form::select('parent_order', $categories, Input::old('parent_order'),['class'=>'chosen','style'=>'width:100%;','data-placeholder'=>'Set After']) !!}
            </div>
          </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Sales Officers</label>
                <div class="col-sm-4">
                    {!! Form::select('sales_officers', $salesOfficers, Input::old('sales_officers'),['class'=>'chosen','style'=>'width:100%;','data-placeholder'=>'Choose Sales Officers']) !!}
                </div>
            </div>
          <div class="pull-right">
          	<button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Save</button>
          </div>
      	</form>
      </div>
    </div>

  </section>
  <!-- !!Main content -->

</section>
<!-- !!!Content -->
@stop

@section('js')
	<script src="{{asset('assets/dist/chosen/chosen.jquery.min.js')}}"></script>
  	<!-- CORE JS -->
	<script type="text/javascript">
    	$(document).ready(function() {
	  		$(".chosen").chosen();
	  	});
	</script>
  	<!-- //CORE JS -->
@stop
