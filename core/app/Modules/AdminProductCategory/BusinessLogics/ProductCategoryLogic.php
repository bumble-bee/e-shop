<?php namespace App\Modules\AdminProductCategory\BusinessLogics;


/**
* Business Logics 
* Define all the busines logics in here
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/
use Illuminate\Database\Eloquent\Model;
use App\Modules\AdminProductCategory\Models\AdminProductCategory;
use App\Models\ManagerHasProductCategory;
use Illuminate\Support\Facades\DB;
use Exception;
use Sentinel;
use App\Models\User;
use App\Exceptions\TransactionException;

class ProductCategoryLogic extends Model {

	public function add($data){

    	DB::transaction(function() use($data){
	    	$user = Sentinel::getUser();

	    	$parent = AdminProductCategory::find($data->parent);

			$sortAfter = AdminProductCategory::find($data->parent_order);

			$categoryCheck = AdminProductCategory::where('name', '=', $data->name)->get();

			if (count($categoryCheck) > 0) {
				throw new TransactionException('Category name already exists.', 101);
			}

	        $productCategory = AdminProductCategory::create([
	            'name'                =>  $data->name, 
	            'code'                =>  $data->code,
	            'created_by'          =>  $user->id,
                'sales_office_id'     => $data->sales_officers,
	        ]);

	        if ($parent != null) {
				$productCategory->makeChildOf($parent);
			} else {
				$productCategory->makeRoot();
			}

            if($sortAfter && $productCategory->id != $sortAfter->id){
                $productCategory->moveToRightOf($sortAfter);
            }           

    		AdminProductCategory::rebuild();

	        if($productCategory) {
	        	return $productCategory;
	        }else{
	            throw new TransactionException('Record wasn\'t updated', 101);
	        }
	    });
    }

    /**
     * This function is used to get all product category details
     * @parm  integer $id that need to get product category details
     * @return supplier object
     */
    public function getProductCategoryDetails($id){
        $product_category_details = AdminProductCategory::where('id','=',$id)->get();
        if(count($product_category_details) > 0){
            return $product_category_details;
        }else{
            return false;
        }
    }

    /**
     * This function is used to update product category
     * @param integer id and array $data
     * @response is boolean
     */
    public function updatePoductCategory($id, $data){

        DB::transaction(function() use($id, $data){
            
            $productCategory 		= AdminProductCategory::find($id);

			$parent 				= AdminProductCategory::find($data->parent);
			
            $sortAfter              = AdminProductCategory::find($data->parent_order);

			$productCategoryCheck   = AdminProductCategory::where('name', '=', $data->name)->where('id','!=',$id)->get();

			if (count($productCategoryCheck) > 0) {
				throw new TransactionException('Product category name already exists.', 101);
			}

			$productCategory->name = $data->name;
			$productCategory->code = $data->code;
            $productCategory->sales_office_id = $data->sales_officers;
			$productCategory->save();

			if ($parent != null) {
                $productCategory->makeChildOf($parent);
            } else {
                $productCategory->makeRoot();
            }

            if($sortAfter){
                if($productCategory->id != $sortAfter->id){
                    $productCategory->moveToRightOf($sortAfter);
                }
            }

            AdminProductCategory::rebuild();

            if($productCategory){
                return true;
            }else{
                throw new Exception("Error occurred while updating product category.");
            }
        });
    }

    /**
     * This function is used to get all product category details
     * @parm -
     * @return product category object
     */
    public function getAllProductCategories(){
        return AdminProductCategory::with(['parentProductCategory'])->paginate(10);
    }

    /**
     * This function is used to search prodcut category
     * @param integer $name, $code
     * @response product category object
     */
    public function searchProductCategory($name, $code) {
        $search = AdminProductCategory::with(['parentProductCategory']);
        if($name !== ''){
            $search = $search->where('name','LIKE', '%' . $name . '%');
        }
        if($code !== ''){
            $search = $search->where('code','LIKE', '%' . $code . '%');
        }
        $search->orderBy('created_at','desc');
        return $search->paginate(10);
    }

    public function getCategoryList(){
    	$categories = AdminProductCategory::lists('name', 'id');

    	return $categories;
    }

    public function getSessionCategoryList(){
    	$user = Sentinel::getUser();

    	$userDetails = User::with('emp')->find($user->id);

    	$categories = ManagerHasProductCategory::select('*')
    		->leftJoin('product_category as product_categories', function($product_category){
    			$product_category->on('manager_has_product_category.product_category_id', '=', 'product_categories.id');
    		})
    		->where('employee_id', $userDetails->emp->id)
    		->orderBy('product_categories.id')
    		->lists('product_categories.name','product_categories.id');

    	return $categories;
    }

}
