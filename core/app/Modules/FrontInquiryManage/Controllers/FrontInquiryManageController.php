<?php namespace App\Modules\FrontINquiryManage\Controllers;

/**
* Controller class
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Inquiry;
use App\Models\Source;
use App\Models\Image;

use Core\Sector\Models\Sector;

use Sentinel;

use Illuminate\Http\Request;

use App\Modules\FrontInquiryManage\BusinessLogics\InquiryLogic;
use App\Modules\FrontInquiryManage\Requests\InquiryRequest;

class FrontInquiryManageController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{	

		$sec=Sentinel::getUser()->sector_id;

		if($sec!=0){
			$inquiries=Inquiry::where('sector',$sec)->with(['sectors','source'])->orderBy('created_at', 'desc')->paginate(5);
		}else{
			$inquiries=Inquiry::with(['sectors','source'])->orderBy('created_at', 'desc')->paginate(5);
		}
		
		$logic = new InquiryLogic();
		$inquiries=$logic->prependUser($inquiries);

		return view("FrontInquiryManage::index",compact('inquiries'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$sectors = Sector::all()->lists('name','id');

		$sources = source::all()->lists('name','id');

		return view("FrontInquiryManage::create",compact('sectors','sources'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(InquiryRequest $request)
	{
		$user = Sentinel::getUser();
        $logic = new InquiryLogic();
        $bool=$logic->addNewInquiry($request);

        if($bool){
        	return redirect('inquiry/index')->with([
                'success' 			=> true,
                'success.message'	=> 'Inquiry created successfully!.',
                'success.title' 	=> 'Success...!'
            ]);
        }else{        	
        	return redirect('inquiry/create')->with([
                'error' 			=> true,
                'error.message'	=> 'Inquiry faild to create!.',
                'error.title' 	=> 'Oops...!'
            ]);
        }

	}

	/**
	 * Detail of Inquiry.
	 *
	 * @return Response
	 */
	public function getInquiryDetails(Request $request)
	{
		$inquiry=Inquiry::with('sectors')->find($request->inquiry);
		if($inquiry){
			$images=Image::where('inquiry_id',$inquiry->id)->get();
			return ['detail'=>$inquiry,'images'=>$images];
		}else{
			return 0;
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($ticket_id)
	{	
		return view('FrontInquiryManage::show');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
