<?php
namespace App\Modules\FrontInquiryManage\Requests;

use App\Http\Requests\Request;

class InquiryRequest extends Request {

	public function authorize(){
		return true;
	}

	public function rules(){
		$rules = [
			'project'				=> 'required',
			'sector'				=> 'required',
			'address'				=> 'required',
			'city'					=> 'required',
			'district'				=> 'required',
			'source'				=> 'required',
			
		];
		return $rules;
	}

}
