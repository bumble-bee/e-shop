@extends('layouts.front_master') 
@section('current_title','O|ITS')
@section('navbar_','navbar-shrink')

@section('css')
<style type="text/css">
  

</style>
@stop

@section('content')
<div class="row">	
	@if ($inquiries->isEmpty())
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="margin-top:8%;">
            <p>You have not created any inquiry.</p>
            <a href="{{ url('inquiry/create') }}">
                Create Inquiry
            </a>
        </div>
    @else
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <table class="table">
            <thead>
                <tr>
                    <th style="text-align: center">Inquiry</th>
                    <th style="text-align: center">Project</th>
                    <th style="text-align: center">Sector</th>
                    <th style="text-align: center">Address</th>
                    <th style="text-align: center">Other Contact</th>
                    <th style="text-align: center">Contact Person</th>
                    <th style="text-align: center">Source</th>
                    <th style="text-align: center">Oppertunity Identified By</th>
                    <th style="text-align: center">Status</th>
                    <th style="text-align: center">Createrd At</th>
                    <th style="text-align: center">Device</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($inquiries as $inquiry)
                <tr>
                    <td style="text-align: center"><span class="label label-default">#{{ $inquiry['inquiry_code'] }}</span></td>
                    <td style="text-align: center">{{ $inquiry['title'] }}</td>
                    <td style="text-align: center">{{ $inquiry['sectors']->name }}</td>
                    <td style="text-align: center">{{ $inquiry['address'] }}</td>
                    <td style="text-align: center">{{ $inquiry['contact'] }}</td>
                    <td style="text-align: center">{{ $inquiry['contact_person'] }}</td>
                    <td style="text-align: center">@if($inquiry['source']){{ $inquiry['source']->name }}@else Not Mentioned @endif</td>
                    <td style="text-align: center">{{ $inquiry['user_name']}}</td>
                    
                    <td style="text-align: center">
                    @if($inquiry['status_id']==0)
                        <span class="label label-warning">Pending</span>
                    @elseif($inquiry['status_id']==1)
                        <span class="label label-primary">Approved</span>
                    @elseif($inquiry['status_id']==2)
                        <span class="label label-danger">Rejected</span>
                    @elseif($inquiry['status_id']==3)
                        <span class="label label-success">Done</span>
                    @endif
                    </td>

                    <td style="text-align: center">{{ $inquiry['created_at'] }} <br>( {{ $inquiry['created_at']->diffForHumans() }} )</td>
                    <td style="text-align: center">{{ $inquiry['created_source']}}</td>
                    
                </tr>
            @endforeach
            </tbody>
        </table>
    </div> 
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <hr style="margin-bottom:0">
    </div>
    <div class="col-md-12 text-right">
        <?php echo $inquiries->render(); ?>
    </div>
    @endif
</div>

<div class="modal" id="detail_modal" name="detail_modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <form class="form-horizontal">
                    <h4 class="modal-title"><legend>Inquiry Details</legend></h4>
                    <fieldset>
                        <div class="row" id="data_set" name="data_set">
                            
                        </div>

                        <h2 class="modal-title"><legend>Attached Images</legend></h2>

                        <div class="row" id="imge_set" name="imge_set" style="max-height: 300px;overflow: overlay">
                            
                        </div>                            
                    </fieldset>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@stop

@section('js')
<script type="text/javascript">
    $(document).ready(function(){
        $('.view').click(function(){
            id = $(this).data('id');
            $('#data_set').html('');
            $('#imge_set').html('');
            $.ajax({
                url: "{{url('inquiry/get-details')}}" ,
                 type: 'get',
                 data: {'inquiry' :  id},
                 success: function(data) {

                    $('#data_set').append(
                        '<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">'
                            +'<div class="row">'
                                +'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">'
                                    +'<div class="form-group" style="margin-bottom: 0px">'
                                        +'<label class="col-lg-3 control-label">Inquiry </label>'
                                        +'<div class="col-lg-8 mediam" style="padding-top: 4px;padding-left: 20px;">'
                                            +'<span class="help-block" style="color: black; font-size: 18px">'+data.detail.inquiry_code+'</span>'
                                        +'</div>'
                                    +'</div>'
                                +'</div>'
                                +'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">'
                                    +'<div class="form-group" style="margin-bottom: 0px">'
                                        +'<label class="col-lg-3 control-label">Sector </label>'
                                        +'<div class="col-lg-8 mediam" style="padding-top: 4px;padding-left: 20px;">'
                                            +'<span class="help-block" style="color: black; font-size: 18px">'+data.detail.sectors.name+'</span>'
                                        +'</div>'
                                    +'</div>'
                                +'</div>'
                                +'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">'
                                    +'<div class="form-group" style="margin-bottom: 0px">'
                                        +'<label class="col-lg-3 control-label">Project </label>'
                                        +'<div class="col-lg-8 mediam" style="padding-top: 4px;padding-left: 20px;">'
                                            +'<span class="help-block" style="color: black; font-size: 18px">'+data.detail.title+'</span>'
                                        +'</div>'
                                    +'</div>'
                                +'</div>'
                                +'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">'
                                    +'<div class="form-group" style="margin-bottom: 0px">'
                                        +'<label class="col-lg-3 control-label">City </label>'
                                        +'<div class="col-lg-8 mediam" style="padding-top: 4px;padding-left: 20px;">'
                                            +'<span class="help-block" style="color: black; font-size: 18px">'+data.detail.city+'</span>'
                                        +'</div>'
                                    +'</div>'
                                +'</div>'
                                +'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">'
                                    +'<div class="form-group" style="margin-bottom: 0px">'
                                        +'<label class="col-lg-3 control-label">District </label>'
                                        +'<div class="col-lg-8 mediam" style="padding-top: 4px;padding-left: 20px;">'
                                            +'<span class="help-block" style="color: black; font-size: 18px">'+data.detail.district+'</span>'
                                        +'</div>'
                                    +'</div>'
                                +'</div>'
                            +'</div>'
                        +'</div>'

                        +'<div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">'
                            +'<div class="row">'
                                
                                +'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">'
                                    +'<div class="form-group" style="margin-bottom: 0px">'
                                        +'<label class="col-lg-3 control-label">Developer </label>'
                                        +'<div class="col-lg-8 mediam" style="padding-top: 4px;padding-left: 30px;">'
                                            +'<span class="help-block" style="color: black; font-size: 18px">'+data.detail.developer+'</span>'
                                            +'<span class="help-block" style="color: black; font-size: 18px">'+data.detail.developer_contact+'</span>'
                                        +'</div>'
                                    +'</div>'
                                +'</div>'

                                +'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">'
                                    +'<div class="form-group" style="margin-bottom: 0px">'
                                        +'<label class="col-lg-3 control-label">Constructor </label>'
                                        +'<div class="col-lg-8 mediam" style="padding-top: 4px;padding-left: 30px;">'
                                            +'<span class="help-block" style="color: black; font-size: 18px">'+data.detail.constructor+'</span>'
                                            +'<span class="help-block" style="color: black; font-size: 18px">'+data.detail.constructor_contact+'</span>'
                                        +'</div>'
                                    +'</div>'
                                +'</div>'

                                +'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">'
                                    +'<div class="form-group" style="margin-bottom: 0px">'
                                        +'<label class="col-lg-3 control-label">Architect </label>'
                                        +'<div class="col-lg-8 mediam" style="padding-top: 4px;padding-left: 30px;">'
                                            +'<span class="help-block" style="color: black; font-size: 18px">'+data.detail.architect+'</span>'
                                            +'<span class="help-block" style="color: black; font-size: 18px">'+data.detail.architect_contact+'</span>'
                                        +'</div>'
                                    +'</div>'
                                +'</div>'
                            +'</div>'
                        +'</div>'
                    );
                    
                    $.each(data.images,function(index,value){
                        $('#imge_set').append(
                            '<div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">'
                                +'<img src="{{url('core/storage/uploads/images/project')}}/'+value.filename+'" class="col-md-12 img-responsive">'
                            +'</div>'
                        );
                        
                    });

                    $('#detail_modal').modal();
                 },
                 error: function(xhr, textStatus, thrownError) {
                     console.log(thrownError);
                 }
             });
        });
    });
</script>
@stop
