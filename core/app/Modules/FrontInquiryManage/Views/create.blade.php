@extends('layouts.front_master') 
@section('current_title','O|ITS')
@section('navbar_','navbar-shrink')

@section('css')

<link rel="stylesheet" href="{{asset('assets/dist/summernote/summernote.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/dist/file/bootstrap-fileinput-master/css/fileinput.css')}}" media="all" />
  
<style type="text/css">

p{
    margin: 0px;
}


.kv-fileinput-caption{
    height: 42px !important;
}

.required{
    color: #D60404; 
    font-weight: bolder;
    font-size: 16px;
    content: "*";
}

</style>

@stop

@section('content')
<div class="row">        
    <form role="form" method="post" enctype="multipart/form-data">
        {!! csrf_field() !!}
        <div class="row" style="margin-top: 10px"> 
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">              

                <div class="row col-xs-offset-1">
                    
                    <div class="col-md-offset-1 col-md-3 col-lg-3">
                        <div class="form-group{{ $errors->has('project') ? ' has-error' : '' }}">
                            <p>Project <span style="color: red">*</span></p>
                            <input id="project" type="text" class="form-control" name="project" placeholder="Name of Project" value="{{ old('project') }}">
                            @if ($errors->has('project'))
                                <span class="help-block">
                                    {{ $errors->first('project') }}
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-3 col-lg-3">
                        <div class="form-group{{ $errors->has('estimate_value') ? ' has-error' : '' }}">
                            <p>Estimate Value <span style="color: red">*</span></p>
                            <input id="estimate_value" type="number" class="form-control" name="estimate_value" placeholder="Estimate Value" value="{{ old('estimate_value') }}">
                            @if ($errors->has('estimate_value'))
                                <span class="help-block">
                                    {{ $errors->first('estimate_value') }}
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-3 col-lg-3">
                        <div class="form-group{{ $errors->has('sector') ? ' has-error' : '' }}">
                            <p>Sector <span style="color: red">*</span></p>
                            <select id="sector" type="sector" class="form-control" name="sector">
                                <option value="">Select Sector</option>
                                @foreach ($sectors as $key=>$sector)
                                <option value="{{ $key }}">{{ $sector }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('sector'))
                                <span class="help-block">
                                    {{ $errors->first('sector') }}
                                </span>
                            @endif
                        </div>
                    </div>

                </div>

                <div class="row col-xs-offset-1">

                    <div class="col-md-offset-1 col-md-3 col-lg-3">
                        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                            <p>Location - Address  <span style="color: red">*</span> </p>
                            <input id="address" type="text" class="form-control" name="address" placeholder="Address of location" value="{{ old('address') }}">
                            @if ($errors->has('address'))
                                <span class="help-block">
                                    {{ $errors->first('address') }}
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3">
                        <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                            <p>Location - City <span style="color: red">*</span></p>
                            <input id="city" type="text" class="form-control" name="city" placeholder="Name of city" value="{{ old('city') }}">
                            @if ($errors->has('city'))
                                <span class="help-block">
                                    {{ $errors->first('city') }}
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3">
                        <div class="form-group{{ $errors->has('district') ? ' has-error' : '' }}">
                            <p>Location - District <span style="color: red">*</span></p>
                            <input id="district" type="text" class="form-control" name="district" placeholder="Name of district" value="{{ old('district') }}">
                            @if ($errors->has('district'))
                                <span class="help-block">
                                    {{ $errors->first('district') }}
                                </span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row col-xs-offset-1">
                    <div class="col-md-offset-1 col-md-3 col-lg-3">
                        <div class="form-group{{ $errors->has('developer') ? ' has-error' : '' }}">
                            <p>Developer</p>
                            <input id="developer" type="text" class="form-control" name="developer" placeholder="Name of Developer" value="{{ old('developer') }}">
                            @if ($errors->has('developer'))
                                <span class="help-block">
                                    {{ $errors->first('developer') }}
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-3 col-lg-3">
                        <div class="form-group{{ $errors->has('constructor') ? ' has-error' : '' }}">
                            <p>Contractor</p>
                            <input id="constructor" type="text" class="form-control" name="constructor" placeholder="Name of contractor" value="{{ old('constructor') }}">
                            @if ($errors->has('constructor'))
                                <span class="help-block">
                                    {{ $errors->first('constructor') }}
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-3 col-lg-3">
                        <div class="form-group{{ $errors->has('architect') ? ' has-error' : '' }}">
                            <p>Architect</p>
                            <input id="architect" type="text" class="form-control" name="architect" placeholder="Name of Architect" value="{{ old('architect') }}">
                            @if ($errors->has('architect'))
                                <span class="help-block">
                                    {{ $errors->first('architect') }}
                                </span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row col-xs-offset-1">
                    
                    <div class="col-md-offset-1 col-md-3 col-lg-3">
                        <div class="form-group{{ $errors->has('developer') ? ' has-error' : '' }}">
                            <p>Contact No - Developer</p>
                            <input id="developer_contact" type="text" class="form-control" name="developer_contact" placeholder="Contact No of Developer" value="{{ old('developer_contact') }}">
                            @if ($errors->has('developer_contact'))
                                <span class="help-block">
                                    {{ $errors->first('developer_contact') }}
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-3 col-lg-3">
                        <div class="form-group{{ $errors->has('constructor_contact') ? ' has-error' : '' }}">
                            <p>Contact No - Contractor</p>
                            <input id="constructor_contact" type="text" class="form-control" name="constructor_contact" placeholder="Contact No of Contractor" value="{{ old('constructor_contact') }}">
                            @if ($errors->has('constructor_contact'))
                                <span class="help-block">
                                    {{ $errors->first('constructor_contact') }}
                                </span>
                            @endif
                        </div>
                    </div>                    

                    <div class="col-md-3 col-lg-3">
                        <div class="form-group{{ $errors->has('architect_contact') ? ' has-error' : '' }}">
                            <p>Contact No - Architect</p>
                            <input id="architect_contact" type="text" class="form-control" name="architect_contact" placeholder="Contact No of Architect" value="{{ old('architect_contact') }}">
                            @if ($errors->has('architect_contact'))
                                <span class="help-block">
                                    {{ $errors->first('architect_contact') }}
                                </span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row col-xs-offset-1">

                    <div class="col-md-offset-1 col-md-3 col-lg-3">                    
                        <div class="form-group{{ $errors->has('contact_person') ? ' has-error' : '' }}">
                            <p>Other Details - Person</p>
                            <input id="contact_person" type="text" class="form-control" name="contact_person" placeholder="Person Name" value="{{ old('contact_person') }}">
                            @if ($errors->has('contact_person'))
                                <span class="help-block">
                                    {{ $errors->first('contact_person') }}
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-3 col-lg-3">
                        <div class="form-group{{ $errors->has('contact_person_designation') ? ' has-error' : '' }}">
                            <p>Designation</p>
                            <input id="contact_person_designation" type="text" class="form-control" name="contact_person_designation" placeholder="Designation" value="{{ old('contact_person_designation') }}">
                            @if ($errors->has('contact_person_designation'))
                                <span class="help-block">
                                    {{ $errors->first('contact_person_designation') }}
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-3 col-lg-3">                    
                        <div class="form-group{{ $errors->has('contact') ? ' has-error' : '' }}">
                            <p>Other Details - Contact</p>
                            <input id="contact" type="text" class="form-control" name="contact" placeholder="Person - Contact No" value="{{ old('contact') }}">
                            @if ($errors->has('contact'))
                                <span class="help-block">
                                    {{ $errors->first('contact') }}
                                </span>
                            @endif
                        </div>
                    </div>

                </div>

                <div class="row col-xs-offset-1">
                    <div class="col-md-offset-1 col-md-9 col-lg-9">
                        <div class="form-group{{ $errors->has('source') ? ' has-error' : '' }}">
                            <p>Source Identified <span style="color: red">*</span></p>
                            <select id="source" type="source" class="form-control" name="source">
                                <option value="">Select source</option>
                                @foreach ($sources as $key=>$source)
                                <option value="{{ $key }}">{{ $source }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('source'))
                                <span class="help-block">
                                    {{ $errors->first('source') }}
                                </span>
                            @endif
                        </div>
                    </div>                    
                </div>

                <div class="row col-xs-offset-1">
                    <div class="col-md-offset-1 col-md-9 col-lg-9">
                        <div class="form-group{{ $errors->has('inputficons1') ? ' has-error' : '' }}">
                            <p>Upload Image</p>
                            <input id="input-ficons-1" name="inputficons1[]" multiple type="file" style="" class="form-control file-loading">
                            @if ($errors->has('inputficons1'))
                                <span class="help-block">
                                    {{ $errors->first('inputficons1') }}
                                </span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row" style="margin: auto 45%">
                    <button type="submit" class=" btn btn-success">Add Inquiry</button>                    
                </div>
            </div>    
        </div>
    </form> 
</div>

@stop

@section('js')
<script src="{{asset('assets/dist/summernote/summernote.js')}}"></script>
<script src="{{asset('assets/dist/file/bootstrap-fileinput-master/js/fileinput.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#summernote').summernote({
          height: 200,                 // set editor height
          minHeight: null,             // set minimum height of editor
          maxHeight: null,             // set maximum height of editor
          focus: true,
          toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']]]
        });
        $('.dropdown-toggle').dropdown();

        $("#input-ficons-1").fileinput({
            uploadUrl: "",
            uploadAsync: true,
            showUpload:false,
            previewFileIcon: '<i class="fa fa-file"></i>',
            allowedPreviewTypes: null, // set to empty, null or false to disable preview for all types
            previewFileIconSettings: {
                'docx': '<i class="fa fa-file-word-o text-primary"></i>',
                'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
                'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
                'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
                'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
                'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
            }
        });
    });            
</script>
@stop
