<?php

Route::group(['middleware' => ['auth.front']], function()
{	
	Route::group(['prefix'=>'inquiry','namespace' => 'App\Modules\FrontInquiryManage\Controllers'],function() {

	    Route::get('index', [
	      'as' => 'front.index.inquiry', 'uses' => 'FrontInquiryManageController@index'
	    ]);

	    Route::get('create', [
	      'as' => 'front.create.inquiry', 'uses' => 'FrontInquiryManageController@create'
	    ]);

        Route::get('get-details', [
            'as' => 'front.index.inquiry', 'uses' => 'FrontInquiryManageController@getInquiryDetails' 
        ]);
	    
	    Route::post('create', [
	      'as' => 'front.create.inquiry', 'uses' => 'FrontInquiryManageController@store'
	    ]);

	});    
});