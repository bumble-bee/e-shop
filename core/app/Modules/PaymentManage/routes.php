<?php

Route::group(['middleware' => ['auth']], function(){

    Route::group(array('prefix'=>'payment','module' => 'EmailManage', 'namespace' => 'App\Modules\EmailManage\Controllers'), function() {

        /*** GET Routes**/

        Route::get('recipt/list', [
            'as' => 'emails.list', 'uses' => 'EmailManageController@listView'
        ]);        

    });

});