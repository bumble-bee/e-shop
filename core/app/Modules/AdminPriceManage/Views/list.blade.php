@extends('layouts.back_master') @section('title','List Price Book')
@section('css')
<link rel="stylesheet" href="{{asset('assets/dist/chosen/bootstrap-chosen.css')}}">
<style type="text/css">
  table .btn{
    padding: 2px 6px;
  }

  .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
    padding-top:5px; 
    padding-bottom:5px; 
  }
</style>
@stop
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Price Book<small>Management</small></h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li class="active">Price Book Management</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<!-- Default box -->
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">List Price Book</h3>
      <button type="button" class="btn btn-primary btn-sm pull-right" onclick="window.location.href='{{url('admin/price/add')}}'">Add Book</button>
		</div>
		<br>
		<div class="box-body">

      <form method="get">
        <div class="row form-group">
          <div class="col-sm-6">
            <label for="" class="control-label">Short Code</label>
            <input type="text" class="form-control" name="code" value="{{$old->code}}">
          </div>
          <div class="col-sm-6">
            <label for="" class="control-label">Keyword</label>
            <input type="text" class="form-control" name="keyword" value="{{$old->keyword}}">
          </div>
        </div>

        <div class="row form-group">
          <div class="col-md-12">
            <button class="btn btn-default pull-right" type="submit"><i class="fa fa-search"></i> Filter</button>
          </div>
        </div>
      </form>

		  <table class="table table-bordered">
        <thead>
          <tr>
            <th class="text-center">#</th>
            <th class="text-center">Short Code</th>
            <th class="text-center">Name</th>
            <th class="text-center">Status</th>
            <th class="text-center">Actions</th>
          </tr>
        </thead>
        <tbody>
          @if(count($data) > 0)
            @foreach($data as $key => $row)
            <tr>
              <td class="text-center">{{ (($data->currentPage()-1)*$data->perPage())+($key+1) }}</td>
              <td>{{ ($row->code != '') ? $row->code : '-' }}</td>
              <td>{{ ($row->name != '') ? $row->name : '-' }}</td>
              <td class="text-center">{!! ($row->status == 1) ? '<span class="text-success"><i class="fa fa-check-circle-o"></i> Active</span>' : '<span class="text-danger"><i class="fa fa-times-circle-o"></i> Inactive</span>' !!}</td>
              <td class="text-center" width="13%">
                <div class="btn-group">
                  @if($user->hasAnyAccess(['price.view', 'admin']))
                  <button class="btn btn-default" onclick="window.location.href='{{url('admin/price/view/'.$row->id)}}'"><i class="fa fa-eye"></i></button>
                  @else
                  <button class="btn btn-default" disabled><i class="fa fa-eye"></i></button>
                  @endif

                  @if($user->hasAnyAccess(['price.assign', 'admin']))
                  <button class="btn btn-default" onclick="window.location.href='{{url('admin/price/assign/'.$row->id)}}'"><i class="fa fa-user-plus"></i></button>
                  @else
                  <button class="btn btn-default" disabled><i class="fa fa-user-plus"></i></button>
                  @endif

                  @if($user->hasAnyAccess(['price.edit', 'admin']))
                  <button class="btn btn-default" onclick="window.location.href='{{url('admin/price/edit/'.$row->id)}}'"><i class="fa fa-pencil"></i></button>
                  @else
                  <button class="btn btn-default" disabled><i class="fa fa-pencil"></i></button>
                  @endif

                  @if($user->hasAnyAccess(['price.delete', 'admin']))
                  <!-- <button class="btn btn-default btn-delete" data-id="{{$row->id}}" onclick="deleteData('{{url('admin/price/delete?id='.$row->id)}}');"><i class="fa fa-trash-o"></i></button> -->
                  @else
                  <!-- <button class="btn btn-default" disabled><i class="fa fa-trash-o"></i></button> -->
                  @endif
                </div>
              </td>
            </tr>
            @endforeach
          @else
            <tr>
              <td colspan="5" align="center"> - No Data to Display - </td>
            </tr>
          @endif
        </tbody>
      </table>
      <div class="row">
        <div class="col-sm-12 text-right">
          {!! $data->appends($old->except('page'))->render() !!}
        </div>
      </div>
		</div><!-- /.box-body -->
	</div><!-- /.box -->
</section><!-- /.content -->

@stop
@section('js')

<script src="{{asset('assets/dist/chosen/chosen.jquery.min.js')}}"></script>

<script type="text/javascript">
$(document).ready(function() {
  $(".chosen").chosen();
});

function deleteData(_url){
  swal({
    title: "Are you sure?",
    text: "you wanna delete this product?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes, delete it!",
    closeOnConfirm: false
  },
  function(){
    $.ajax({
      url: _url,
      method: 'get',
      cache: false,
      data: [],
      success: function(response){
        if(response.status == 1){
          swal("Done!", "product has been deleted!.", "success");
          location.reload();
        }else{
          swal("Error!", response.message, "error");
        }
      },
      error: function(xhr){
        console.log(xhr);
        swal("Error!", 'Error occurred. Please try again', "error");
      } 
    });
    //window.open(_url, '_self');
  });
}
</script>
@stop
