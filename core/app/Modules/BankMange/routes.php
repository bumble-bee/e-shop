<?php


Route::group(['middleware' => ['auth']], function()
{	
	Route::group(['prefix'=>'admin/bank/','namespace' => 'App\Modules\BankMange\Controllers'],function() {

	    Route::get('create', [
	      	'as' => 'bank.create', 'uses' => 'BankController@create'
	    ]);


	    Route::get('edit/{id}', [
	      	'as' => 'bank.edit', 'uses' => 'BankController@edit'
	    ]);

        Route::get('list', [
            'as' => 'bank.list', 'uses' => 'BankController@list'
        ]);

	    Route::post('create', [
	      	'as' => 'bank.create', 'uses' => 'BankController@store'
	    ]);

	    Route::post('edit/{id}', [
	      	'as' => 'bank.edit', 'uses' => 'BankController@update'
	    ]);

	    Route::get('delete/{id}', [
	      	'as' => 'bank.delete', 'uses' => 'BankController@delete'
	    ]);


	});    
});