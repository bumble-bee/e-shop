@extends('layouts.back_master') @section('title','Admin - MRP Management')
@section('current_title','Product Category Create')

@section('css')
<style type="text/css">
  .box-header, .box-body {
    padding: 20px;
  }
  .has-error .help-block, .has-error .control-label{
    color:#e41212;
  }
  .has-error .chosen-container{
    border:1px solid #e41212;
  }
</style>
@stop

@section('content')
<!-- Content-->
<section>
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Bank<small>Management</small></h1>
    <ol class="breadcrumb">
      	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      	<li>banks</li>
      	<li class="active">Add</li>
    </ol>
  </section>
  <!-- !!Content Header (Page header) -->

  <!-- Main content -->
  <section class="content">

    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Update Bank</h3>
      </div>
      <div class="box-body">
      	<form method="POST">
          {!! csrf_field() !!}
          <div class="form-group @if($errors->has('bank_name')) has-error @endif">
            <label class="control-label required">Bank Name</label>
            <input type="text" class="form-control" name="bank_name" placeholder="Bank Name"
            value="{{$bank->bank_name}}">
            @if($errors->has('bank_name'))
              <span class="help-block">{{$errors->first('bank_name')}}</span>
            @endif
          </div>

          <div class="form-group @if($errors->has('short_code')) has-error @endif">
            <label class="control-label required">Short Code</label>
            <input type="text" class="form-control" name="short_code" placeholder="Bank code" 
            value="{{$bank->short_code}}">
            @if($errors->has('short_code'))
              <span class="help-block">{{$errors->first('short_code')}}</span>
            @endif
          </div>
        
          
        
          <button type="submit" class="btn btn-primary pull-right">Submit</button>
        </form>
      </div>
    </div>

  </section>
  <!-- !!Main content -->

</section>
<!-- !!!Content -->

@stop
@section('js')  
  <!-- CORE JS -->
	<script type="text/javascript">
    	$(document).ready(function() {
        
	  	});
	</script>
  <!-- //CORE JS -->
@stop
