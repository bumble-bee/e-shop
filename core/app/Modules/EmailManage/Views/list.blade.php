@extends('layouts.back_master') @section('title','List Quotation')
@section('css')
<link rel="stylesheet" href="{{asset('assets/dist/chosen/bootstrap-chosen.css')}}">
<link rel="stylesheet" type="text/css" src="{{asset('assets/dist/bootstrap-datepicker/css/bootstrap-datepicker.css')}}"/>

<style type="text/css">
  table .btn{
    padding: 2px 6px;
  }

  .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
    padding-top:5px; 
    padding-bottom:5px; 
  }
</style>
@stop
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Quotation<small>Management</small></h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li class="active">Quotation Management</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<!-- Default box -->
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">List Quotaton</h3>
		</div>
		<br>
		<div class="box-body">

      <form method="get">
        <div class="row form-group">
          <div class="col-sm-4">
            <label for="" class="control-label">Reference No.</label>
          </div>
          <div class="col-sm-4">
            <label for="" class="control-label">Customer</label>
            <input type="text" class="form-control" name="customer" id="customer" value="{{$old->customer}}">
          </div>
          <div class="col-sm-4">
            <label for="" class="control-label">Type</label>

          </div>
        </div>
        <div class="row form-group">
          <div class="col-sm-4">
            <label for="" class="control-label">From</label>
          </div>
          <div class="col-sm-4">
            <label for="" class="control-label">To</label>
          </div>
          <div class="col-sm-4">
            <label for="" class="control-label">Status</label>
          </div>
        </div>
        <div class="row form-group">
          <div class="col-sm-4">
            <label for="" class="control-label">Action By</label>

          </div>
        </div>

        <div class="row form-group">
          <div class="col-md-12">
            <button class="btn btn-default pull-right" type="submit"><i class="fa fa-search"></i> Filter</button>
          </div>
        </div>
      </form>

		  <table class="table table-bordered">
        <thead>
          <tr>
            <th class="text-center">#</th>
            <th class="text-center">Reference No.</th>
            <th class="text-center">Customer</th>
            <th class="text-center">Date</th>
            <th class="text-center">Amount</th>
            <th class="text-center">Action By</th>
            <th class="text-center">Type</th>
            <th class="text-center">Status</th>
            <th class="text-center">Actions</th>
          </tr>
        </thead>
        <tbody>

        </tbody>
      </table>
      <div class="row">
        <div class="col-sm-12 text-right">
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12 text-right">
          
        </div>
      </div>
		</div><!-- /.box-body -->
    <div class="overlay" style="display: none;">
      <i class="fa fa-spin fa-refresh"></i>
    </div>
	</div><!-- /.box -->
</section><!-- /.content -->

<!-- Default box -->
<div class="modal fade" id="detail_div" name="detail_div1" hidden="true">
  <form  class="form-horizontal form-validation" action="#" role="form"  method="post" id="stageForm">
    {!!Form::token() !!}
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title">Quotation Detail</h4>
              </div>
              <div class="modal-body">
                  <div class="form-group">
                      <table class="table table-bordered table-hover">
                          <thead class="table-header">
                              <tr>
                                  <td class="text-center">Product Code</td>
                                  <td class="text-center">Product</td>
                                  <td class="text-center">Price</td>
                                  <td class="text-center">Qty</td>
                                  <td class="text-center">Total</td>
                                  <td class="text-center">Discount</td>
                                  <td class="text-center">Amount</td>
                              </tr>
                          </thead>
                          <tbody id="tbdy"></tbody>
                      </table>
                  </div>
              </div>
          </div>
      </div>
  </form>
</div>


@stop
@section('js')

<script src="{{asset('assets/dist/chosen/chosen.jquery.min.js')}}"></script>
<script src="{{asset('assets/dist/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>


<script type="text/javascript">
$(document).ready(function() {
  $(".chosen").chosen();

  $('.datepick').datepicker({
      keyboardNavigation: false,
      forceParse: false,
      format: 'yyyy-mm-dd'
  });
});


</script>
@stop
