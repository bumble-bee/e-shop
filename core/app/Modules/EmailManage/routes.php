<?php

Route::group(['middleware' => ['auth']], function(){

    Route::group(array('prefix'=>'emails','module' => 'EmailManage', 'namespace' => 'App\Modules\EmailManage\Controllers'), function() {

        /*** GET Routes**/

        Route::get('list', [
            'as' => 'emails.list', 'uses' => 'EmailManageController@listView'
        ]);        

    });

});