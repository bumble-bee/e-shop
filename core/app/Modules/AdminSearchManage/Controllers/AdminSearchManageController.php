<?php

namespace App\Modules\AdminSearchManage\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Session;
use App\Models\Inquiry;
use Sentinel;
use App\Models\User;
use Core\EmployeeManage\Models\Employee;
use Core\EmployeeManage\Models\EmployeeType;
use App\Classes\Functions;

class AdminSearchManageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    private $functions;

    public function __construct(Functions $functions){
        $this->functions = $functions;
    }  

    public function index(Request $request)
    {	
		$userRole         = Sentinel::getUser()->roles()->first();
		$usr              = Sentinel::getUser();
		$user             = User::with('sectors')->find($usr->id);
		// $role_id       = $userRole->id;

		if($userRole){
			$user_id          = $userRole->pivot->user_id;
		}else{
			$user_id          = Sentinel::getUser()->id;		
		}

		$employee_id      = $user->employee_id;
		$employee_detail  = Employee::where('id','=',$employee_id)->first();
		$employeeReps     = $this->functions->getEmployeeRepIds($employee_id);
		$data             = Inquiry::orderBy('created_at', 'desc')->with(['sectors', 'source','activeTransaction']);		
		$sec              = Sentinel::getUser()->sector_id;	
		$data             = $data->where('inquiry_code','like','%'.$request->keyword.'%');	
		$data             = $data->orWhere('title','like','%'.$request->keyword.'%');	
		$data             = $data->orWhere('project_value','like','%'.$request->keyword.'%');	
		$data             = $data->orWhere('contact','like','%'.$request->keyword.'%');	
		$data             = $data->orWhere('contact_person','like','%'.$request->keyword.'%');	
		$data             = $data->paginate(20);
        
        return view('AdminSearchManage::admin-search-manage.index')->with(['keyword'=>$request->keyword,'data'=>$data]);
    }

    
}
