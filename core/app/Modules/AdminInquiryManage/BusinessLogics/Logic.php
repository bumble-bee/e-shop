<?php namespace App\Modules\AdminInquiryManage\BusinessLogics;


/**
* Business Logics 
* Define all the busines logics in here
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/
use Illuminate\Database\Eloquent\Model;
use App\Models\Inquiry;
use App\Models\Image;
use App\Models\AppUser;
use App\Models\User;
use App\Models\InquiryTransaction;
use Core\EmployeeManage\Models\Employee;
use Sentinel;
use DB;
use App\Exceptions\TransactionException;

class Logic extends Model {

	public function addNewInquiry($data)
    {
    	try{
	    	DB::transaction(function() use ($data){
		        $inquiry = Inquiry::create([
		            'title'                 =>  $data->project, 
		            'project_value'         =>  $data->estimate_value, 
		            'sector'                =>  $data->sector, 
		            'address'               =>  $data->address, 
		            'city'                  =>  $data->city, 
		            'district'              =>  $data->district, 
		            'developer'             =>  $data->developer, 
		            'constructor'           =>  $data->constructor, 
		            'architect'             =>  $data->architect, 
		            'developer_contact'     =>  $data->developer_contact, 
		            'constructor_contact'   =>  $data->constructor_contact, 
		            'architect_contact'     =>  $data->architect_contact,
		            'contact_person'        =>  $data->contact_person, 
		            'designation'           =>  $data->contact_person_designation, 
		            'contact'               =>  $data->contact, 
		            'source_id'             =>  $data->source,
		            'created_by'            =>  Sentinel::getUser()->employee_id,
		            'timestamp'             =>  date('Y-m-d H:i:s')
		        ]);

		        if($inquiry){
		            $inquiry->status_id = 0;
		            
		            $tmp="INQ".str_pad($inquiry->id, 6, '0', STR_PAD_LEFT);

		            $inquiry->inquiry_code = $tmp;

		            $inquiry->save();

		            $file = $data->file('inputficons1');            

		            if(count($file)>0){
		                foreach ($file as $key => $value) {

		                    if($value){
		                        $destinationPath = storage_path('uploads/images/project');
		                        $extn = $value->getClientOriginalExtension();

		                        $fileName = $tmp.'-' . $key . '.' . $extn;
		                        $value->move($destinationPath, $fileName);

		                        $image = Image::create([
		                            'inquiry_id'  => $inquiry->id,
		                            'path'      => $destinationPath,
		                            'filename'  => $fileName,
		                        ]);

		                        if (!$image) {
		                            throw new TransactionException('Record wasn\'t updated', 100);
		                        }
		                    }
		                }
		            }

		            InquiryTransaction::where('inquiry_id',$inquiry->id)->delete();
		                
		            $usr=User::find(Sentinel::getUser()->id);

		            $trans=InquiryTransaction::create([
		                'inquiry_id'    =>  $inquiry->id,
		                'event_id'      =>  1,
		                'action_by'     =>  $usr->employee_id,
		                'reference'     =>  ''
		            ]);

		            if(!$trans){
		                throw new TransactionException("Faild to complete transaction", 102);
		            }
		        }else{
		            throw new TransactionException('Record wasn\'t updated', 101);
		        }
		    });
	    	return redirect('admin/inquiry/list')->with([
                'success' => true,
                'success.message' => 'Inquiry created successfully!',
                'success.title'   => 'Success..!'
            ]);
		}catch(\Exception $e){
            return redirect('admin/inquiry/add')->with([
                'error' => true,
                'error.title' => 'Error Occurred',
                'error.message' => $e->getMessage()
            ]);
        }
    }

    public function prependUser($inquiries)
    {
        $inquiries=$inquiries;
        foreach ($inquiries as $value) {
            if($value->created_source=="MOBILE"){
                $emp = Employee::find($value->created_by);
                $value->user_name = $emp->first_name." ".$emp->last_name;
            }else{
                $usr = Employee::find($value->created_by);
                $value->user_name=$usr->first_name." ".$usr->last_name;
            }
        }

        return $inquiries;
    }

}
