@extends('layouts.back_master') @section('title','Admin - Inquiry Management')
@section('current_title','Create Inquiry')
@section('css')
<link rel="stylesheet" href="{{asset('assets/dist/chosen/bootstrap-chosen.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/dist/file/bootstrap-fileinput-master/css/fileinput.css')}}" media="all" />
<style type="text/css">
    .port-image
    {
        width: 100%;
    }

    .gallery_product
    {
        margin-bottom: 30px;
    }

    .mediam
    {
        font-size: medium;
    }

    .kv-fileinput-caption{
        height: 34px !important;
    }
</style>  
@stop

@section('content')
<!-- Content-->
<section>
  <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Inquiry
            <small>Management</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/admin')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{url('/admin/inquiry/list')}}"> List</a></li>
            <li class="active">Add Inquiry</li>
        </ol>
    </section>
    <!-- !!Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">  
        <div class="box box-default">
            <div class="box-header">  
                <h4>New Inquiry</h4>
            </div>
            <div class="box-body">
                <form role="form" method="post" enctype="multipart/form-data">
                {!! csrf_field() !!}
                    <div class="row"> 
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">              

                            <div class="row">
                                
                                <div class="col-md-4 col-lg-4 form-group @if($errors->has('project')) has-error @endif">
                                    <label for="" class="control-label required">Project Name</label>
                                    <input type="text" class="form-control" name="project" placeholder="Project Name" 
                                    value="{{old('project')}}">
                                    @if($errors->has('project'))
                                        <span class="help-block">{{$errors->first('project')}}</span>
                                    @endif
                                </div>

                                <div class="col-md-2 col-lg-2 form-group @if($errors->has('estimate_value')) has-error @endif">
                                    <label for="" class="control-label">Value</label>
                                    <input type="text" class="form-control" name="estimate_value" id="estimate_value" placeholder="Project Estimate Value" value="{{old('estimate_value')}}">
                                    @if($errors->has('estimate_value'))
                                        <span class="help-block">{{$errors->first('estimate_value')}}</span>
                                    @endif
                                </div>

                                <div class="col-md-4 col-lg-4 form-group @if($errors->has('address')) has-error @endif">
                                    <label for="" class="control-label">Location - Address</label>
                                    <input type="text" class="form-control" name="address" value="{{old('address')}}" placeholder="Location - Address">
                                    @if($errors->has('address'))
                                        <span class="help-block">{{$errors->first('address')}}</span>
                                    @endif
                                </div>
                                
                                <div class="col-md-2 col-lg-2  form-group @if($errors->has('sector')) has-error @endif">
                                    <label for="" class="control-label required">Sector</label>
                                    <select name="sector" id="sector" class="form-control chosen">
                                        <option value="">-Select Sector-</option>
                                        @foreach($sectors as $key=>$sector)
                                            <option value="{{$key}}" @if(old('sector')==$key) selected @endif>{{$sector}}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('sector'))
                                        <span class="help-block">{{$errors->first('sector')}}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="row">
                                
                                <div class="col-md-4 col-lg-4 form-group @if($errors->has('contact_person')) has-error @endif">
                                    <label for="" class="control-label required">Contact Name</label>
                                    <input type="text" class="form-control" name="contact_person" value="{{old('contact_person')}}" placeholder="Enter Contact Name">
                                    @if($errors->has('contact_person'))
                                        <span class="help-block">{{$errors->first('contact_person')}}</span>
                                    @endif
                                </div>
                                
                                <div class="col-md-2 col-lg-2 form-group @if($errors->has('contact_person_designation')) has-error @endif">
                                    <label for="" class="control-label">Designation</label>
                                    <input type="text" class="form-control" name="contact_person_designation" value="{{old('contact_person_designation')}}" placeholder="Enter Designation">
                                    @if($errors->has('contact_person_designation'))
                                        <span class="help-block">{{$errors->first('contact_person_designation')}}</span>
                                    @endif
                                </div>
                                
                                <div class="col-md-2 col-lg-2 form-group @if($errors->has('contact')) has-error @endif">
                                    <label for="" class="control-label required">Contact No</label>
                                    <input type="text" class="form-control" name="contact" value="{{old('contact')}}" placeholder="Enter Contact No">
                                    @if($errors->has('contact'))
                                        <span class="help-block">{{$errors->first('contact')}}</span>
                                    @endif
                                </div>
                                
                                <div class="col-md-4 col-lg-4 form-group @if($errors->has('source')) has-error @endif">
                                    <label for="" class="control-label required">Source</label>
                                    <select name="source" id="source" class="form-control chosen">
                                        <option value="">-Select Source-</option>
                                        @foreach($sources as $key=>$source)
                                            <option value="{{$key}}" @if(old('source')==$key) selected @endif>{{$source}}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('source'))
                                        <span class="help-block">{{$errors->first('source')}}</span>
                                    @endif
                                </div>

                            </div>

                            <div class="row">

                                <div class="col-md-12 col-lg-12 form-group{{ $errors->has('inputficons1') ? ' has-error' : '' }}">
                                    <p>Upload Image</p>
                                    <input id="input-ficons-1" name="inputficons1[]" multiple type="file" class="form-control file-loading">
                                    @if ($errors->has('inputficons1'))
                                        <span class="help-block">
                                            {{ $errors->first('inputficons1') }}
                                        </span>
                                    @endif
                                </div>                            
                            </div>

                            <!-- <div class="row">                                
                                <div class="col-md-4 col-lg-4 form-group @if($errors->has('city')) has-error @endif">
                                    <label for="" class="control-label">Location - City</label>
                                    <input type="text" class="form-control" name="city" value="{{old('city')}}" placeholder="Location - city">
                                    @if($errors->has('city'))
                                        <span class="help-block">{{$errors->first('city')}}</span>
                                    @endif
                                </div>
                                
                                <div class="col-md-4 col-lg-4 form-group @if($errors->has('district')) has-error @endif">
                                    <label for="" class="control-label">Location - District</label>
                                    <input type="text" class="form-control" name="district" value="{{old('district')}}" placeholder="Location - District">
                                    @if($errors->has('district'))
                                        <span class="help-block">{{$errors->first('district')}}</span>
                                    @endif
                                </div>

                            </div> -->

                            <div class="row">
                                
                                <div class="col-md-4 col-lg-4 form-group @if($errors->has('developer')) has-error @endif">
                                    <label for="" class="control-label">Developer</label>
                                    <input type="text" class="form-control" name="developer" value="{{old('developer')}}" placeholder="Enter Developer Name">
                                    @if($errors->has('developer'))
                                        <span class="help-block">{{$errors->first('developer')}}</span>
                                    @endif
                                </div>
                                
                                <div class="col-md-4 col-lg-4 form-group @if($errors->has('constructor')) has-error @endif">
                                    <label for="" class="control-label">Contractor</label>
                                    <input type="text" class="form-control" name="constructor" value="{{old('
                                    constructor')}}" placeholder="Enter Contractor Name">
                                    @if($errors->has('constructor'))
                                        <span class="help-block">{{$errors->first('constructor')}}</span>
                                    @endif
                                </div>
                                
                                <div class="col-md-4 col-lg-4 form-group @if($errors->has('architect')) has-error @endif">
                                    <label for="" class="control-label">Architect</label>
                                    <input type="text" class="form-control" name="architect" value="{{old('architect')}}" placeholder="Enter Architect Name">
                                    @if($errors->has('architect'))
                                        <span class="help-block">{{$errors->first('architect')}}</span>
                                    @endif
                                </div>

                            </div>

                            <div class="row">
                                
                                <div class="col-md-4 col-lg-4 form-group @if($errors->has('developer_contact')) has-error @endif">
                                    <label for="" class="control-label">Contact - Developer</label>
                                    <input type="text" class="form-control" name="developer_contact" value="{{old('developer_contact')}}" placeholder="Enter Developer Contact">
                                    @if($errors->has('developer_contact'))
                                        <span class="help-block">{{$errors->first('developer_contact')}}</span>
                                    @endif
                                </div>
                                
                                <div class="col-md-4 col-lg-4 form-group @if($errors->has('constructor_contact')) has-error @endif">
                                    <label for="" class="control-label">Contact - Contractor</label>
                                    <input type="text" class="form-control" name="constructor_contact" value="{{old('constructor_contact')}}" placeholder="Enter Contractor Contact">
                                    @if($errors->has('constructor_contact'))
                                        <span class="help-block">{{$errors->first('constructor_contact')}}</span>
                                    @endif
                                </div>
                                
                                <div class="col-md-4 col-lg-4 form-group @if($errors->has('architect_contact')) has-error @endif">
                                    <label for="" class="control-label">Contact - Architect</label>
                                    <input type="text" class="form-control" name="architect_contact" value="{{old('architect_contact')}}" placeholder="Enter Architect Contact">
                                    @if($errors->has('architect_contact'))
                                        <span class="help-block">{{$errors->first('architect_contact')}}</span>
                                    @endif
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <button type="submit" class="btn btn-success pull-right">Save</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </form>
            </div>         
        </div>
    </section>
    <!-- !!Main content -->
</section>

@stop
@section('js')
<!-- CORE JS -->
    <script src="{{asset('assets/dist/file/bootstrap-fileinput-master/js/fileinput.min.js')}}" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function(){

            $("#input-ficons-1").fileinput({
                uploadUrl: "",
                uploadAsync: true,
                showUpload:false,
                previewFileIcon: '<i class="fa fa-file"></i>',
                allowedPreviewTypes: null, // set to empty, null or false to disable preview for all types
                previewFileIconSettings: {
                    'docx': '<i class="fa fa-file-word-o text-primary"></i>',
                    'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
                    'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
                    'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
                    'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
                    'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
                },
                overwriteInitial: false,
            });

            $('#estimate_value').keyup(function(event) {

                if(event.which >= 37 && event.which <= 40) return;
              
                $(this).val(function(index, value) {
                    return value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                });
            });
        });    
    </script>
<!-- //CORE JS -->
@stop