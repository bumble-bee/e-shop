@extends('layouts.back_master') @section('title','Admin - Inquiry Management')
@section('current_title','Edit Inquiry')
@section('css')
<link rel="stylesheet" href="{{asset('assets/dist/chosen/bootstrap-chosen.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/dist/file/bootstrap-fileinput-master/css/fileinput.css')}}" media="all" />
<style type="text/css">
    .port-image
    {
        width: 100%;
    }

    .gallery_product
    {
        margin-bottom: 30px;
    }

    .mediam
    {
        font-size: medium;
    }

    .kv-fileinput-caption{
        height: 34px !important;
    }
</style>  
@stop

@section('content')
<!-- Content-->
<section>
  <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Inquiry
            <small>Management</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Inquiry</li>
            <li class="active">Edit Inquiry</li>
        </ol>
    </section>
    <!-- !!Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">  
        <div class="box box-default">
            <div class="box-header">  
                <div class="pull-left text">
                    Inquiry Edit
                </div>
                <div class="pull-right tool-buttons">
                    <button type="submit" class="btn btn-primary btn-sm pull-right" style="margin-left:4px" onclick="window.location.href='{{url('admin/inquiry/list')}}'">
                        <i class="fa fa-bars" aria-hidden="true"></i> 
                        List
                    </button>            
                </div>
            </div>
            <div class="box-body">
                <form role="form" method="post" enctype="multipart/form-data">
                {!! csrf_field() !!}
                    <div class="row"> 
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">              
                            <div class="row">
                                <div class="col-md-2 col-lg-2 form-group @if($errors->has('inquiry_no')) has-error @endif">
                                    <label for="" class="control-label required">Inquiry No</label>
                                    <input type="text" class="form-control" disabled="true" name="inquiry_no" value="{{$inquiry->inquiry_code}}">
                                    @if($errors->has('inquiry_no'))
                                        <span class="help-block">{{$errors->first('inquiry_no')}}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3 col-lg-3 form-group @if($errors->has('project_name')) has-error @endif">
                                    <label for="" class="control-label required">Project Name</label>
                                    <input type="text" class="form-control" name="project_name" value="{{$inquiry->title}}">
                                    @if($errors->has('project_name'))
                                        <span class="help-block">{{$errors->first('project_name')}}</span>
                                    @endif
                                </div>

                                <div class="col-md-3 col-lg-3 form-group @if($errors->has('estimate_value')) has-error @endif">
                                    <label for="" class="control-label">Value</label>
                                    <input type="text" class="form-control" name="estimate_value" placeholder="Project Estimate Value" value="{{$inquiry->project_value}}">
                                    @if($errors->has('estimate_value'))
                                        <span class="help-block">{{$errors->first('estimate_value')}}</span>
                                    @endif
                                </div>

                                <div class="col-md-4 col-lg-4 form-group @if($errors->has('address')) has-error @endif">
                                    <label for="" class="control-label required">Location - Address</label>
                                    <input type="text" class="form-control" name="address" value="{{$inquiry->address}}">
                                    @if($errors->has('address'))
                                        <span class="help-block">{{$errors->first('address')}}</span>
                                    @endif
                                </div>
                                
                                <div class="col-md-2 col-lg-2 form-group @if($errors->has('sector')) has-error @endif">
                                    <label for="" class="control-label required">Sector</label>
                                    <select name="sector" id="sector" class="form-control chosen">
                                        <option value="0">-Select Sector-</option>
                                        @foreach($sectors as $key=>$sector)
                                            <option value="{{$key}}" @if($key==$inquiry['sector']) selected @endif>{{$sector}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                
                                <div class="col-md-3 col-lg-3 form-group @if($errors->has('contact_person')) has-error @endif">
                                    <label for="" class="control-label required">Contact Name</label>
                                    <input type="text" class="form-control" name="contact_person" value="{{$inquiry->contact_person}}">
                                    @if($errors->has('contact_person'))
                                        <span class="help-block">{{$errors->first('contact_person')}}</span>
                                    @endif
                                </div>
                                
                                <div class="col-md-3 col-lg-3 form-group @if($errors->has('contact_person_designation')) has-error @endif">
                                    <label for="" class="control-label">Designation</label>
                                    <input type="text" class="form-control" name="contact_person_designation" value="{{$inquiry->designation}}">
                                    @if($errors->has('contact_person_designation'))
                                        <span class="help-block">{{$errors->first('contact_person_designation')}}</span>
                                    @endif
                                </div>
                                
                                <div class="col-md-3 col-lg-3 form-group @if($errors->has('other_contact')) has-error @endif">
                                    <label for="" class="control-label required">Contact No</label>
                                    <input type="text" class="form-control" name="other_contact" value="{{$inquiry->contact}}">
                                    @if($errors->has('other_contact'))
                                        <span class="help-block">{{$errors->first('other_contact')}}</span>
                                    @endif
                                </div>
                                
                                <div class="col-md-3 col-lg-3 form-group @if($errors->has('source')) has-error @endif">
                                    <label for="" class="control-label required">Source</label>
                                    <select name="source" id="source" class="form-control chosen">
                                        <option value="0">-Select Source-</option>
                                        @foreach($sources as $key=>$source)
                                            <option value="{{$key}}" @if($key==$inquiry['source_id']) selected @endif>{{$source}}</option>
                                        @endforeach
                                    </select>
                                </div>

                            </div>

                            <!-- <div class="row">
                                                                
                                <div class="col-md-3 col-lg-3 form-group @if($errors->has('city')) has-error @endif">
                                    <label for="" class="control-label">Location - City</label>
                                    <input type="text" class="form-control" name="city" value="{{$inquiry->city}}">
                                    @if($errors->has('city'))
                                        <span class="help-block">{{$errors->first('city')}}</span>
                                    @endif
                                </div>
                                
                                <div class="col-md-3 col-lg-3 form-group @if($errors->has('district')) has-error @endif">
                                    <label for="" class="control-label">Location - District</label>
                                    <input type="text" class="form-control" name="district" value="{{$inquiry->district}}">
                                    @if($errors->has('district'))
                                        <span class="help-block">{{$errors->first('district')}}</span>
                                    @endif
                                </div>

                            </div> -->

                            <div class="row">
                                <div class="col-md-4 col-lg-4 form-group @if($errors->has('developer')) has-error @endif">
                                    <label for="" class="control-label">Developer</label>
                                    <input type="text" class="form-control" name="developer" value="{{$inquiry->developer}}">
                                    @if($errors->has('developer'))
                                        <span class="help-block">{{$errors->first('developer')}}</span>
                                    @endif
                                </div>
                                
                                <div class="col-md-4 col-lg-4 form-group @if($errors->has('contractor')) has-error @endif">
                                    <label for="" class="control-label">Contractor</label>
                                    <input type="text" class="form-control" name="contractor" value="{{$inquiry->constructor}}">
                                    @if($errors->has('contractor'))
                                        <span class="help-block">{{$errors->first('contractor')}}</span>
                                    @endif
                                </div>
                                
                                <div class="col-md-4 col-lg-4 form-group @if($errors->has('architect')) has-error @endif">
                                    <label for="" class="control-label">Architect</label>
                                    <input type="text" class="form-control" name="architect" value="{{$inquiry->architect}}">
                                    @if($errors->has('architect'))
                                        <span class="help-block">{{$errors->first('architect')}}</span>
                                    @endif
                                </div>

                            </div>

                            <div class="row">
                                
                                <div class="col-md-4 col-lg-4 form-group @if($errors->has('developer_contact')) has-error @endif">
                                    <label for="" class="control-label">Contact - Developer</label>
                                    <input type="text" class="form-control" name="developer_contact" value="{{$inquiry->developer_contact}}">
                                    @if($errors->has('developer_contact'))
                                        <span class="help-block">{{$errors->first('developer_contact')}}</span>
                                    @endif
                                </div>
                                
                                <div class="col-md-4 col-lg-4 form-group @if($errors->has('contractor_contact')) has-error @endif">
                                    <label for="" class="control-label">Contact - Contractor</label>
                                    <input type="text" class="form-control" name="contractor_contact" value="{{$inquiry->constructor_contact}}">
                                    @if($errors->has('contractor_contact'))
                                        <span class="help-block">{{$errors->first('contractor_contact')}}</span>
                                    @endif
                                </div>
                                
                                <div class="col-md-4 col-lg-4 form-group @if($errors->has('architect_contact')) has-error @endif">
                                    <label for="" class="control-label">Contact - Architect</label>
                                    <input type="text" class="form-control" name="architect_contact" value="{{$inquiry->architect_contact}}">
                                    @if($errors->has('architect_contact'))
                                        <span class="help-block">{{$errors->first('architect_contact')}}</span>
                                    @endif
                                </div>

                            </div>

                            <!-- <div class="row">
                                <div class="col-md-offset-1 col-md-9 col-lg-9 form-group{{ $errors->has('inputficons1') ? ' has-error' : '' }}">
                                    <p>Upload Image</p>
                                    <input id="input-ficons-1" name="inputficons1[]" multiple type="file" class="form-control file-loading">
                                    @if ($errors->has('inputficons1'))
                                        <span class="help-block">
                                            {{ $errors->first('inputficons1') }}
                                        </span>
                                    @endif
                                </div>
                            </div> -->

                            <div class="row">
                                <div class="col-md-offset-9">
                                    <button type="submit" class="btn btn-success">Update</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </form>
            </div> 
        
        </div>
    </section>
    <!-- !!Main content -->

</section>

@stop
@section('js')
<!-- CORE JS -->
    <script src="{{asset('assets/dist/file/bootstrap-fileinput-master/js/fileinput.min.js')}}" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function(){

            $("#input-ficons-1").fileinput({
                uploadUrl: "",
                uploadAsync: true,
                showUpload:false,
                previewFileIcon: '<i class="fa fa-file"></i>',
                allowedPreviewTypes: null, // set to empty, null or false to disable preview for all types
                previewFileIconSettings: {
                    'docx': '<i class="fa fa-file-word-o text-primary"></i>',
                    'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
                    'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
                    'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
                    'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
                    'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
                },
                overwriteInitial: false,
                initialPreview: [
                    "<img src='{{url("assets/adminlte/img/avatar.png")}}' class='file-preview-image' alt='Desert' title='Desert'>"
                ],
            });
        });    
    </script>
<!-- //CORE JS -->
@stop
