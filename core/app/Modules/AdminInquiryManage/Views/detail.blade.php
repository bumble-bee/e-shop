@extends('layouts.back_master') @section('title','Admin - Inquiry Details')
@section('current_title','Dashboard')

@section('css')
<style type="text/css">
    .port-image
    {
        width: 100%;
    }

    .gallery_product
    {
        margin-bottom: 30px;
    }

    .mediam
    {
        font-size: medium;
    }

    .timeline{position:relative;margin:0 0 30px 0;padding:0;list-style:none}

    .timeline:before{content:'';position:absolute;top:0;bottom:0;width:4px;background:#ddd;left:31px;margin:0;border-radius:2px}

    .timeline>li{position:relative;margin-right:10px;margin-bottom:15px}

    .timeline>li:before,.timeline>li:after{content:" ";display:table}

    .timeline>li:after{clear:both}

    .timeline>li>.timeline-item{-webkit-box-shadow:0 1px 1px rgba(0,0,0,0.1);box-shadow:0 1px 1px rgba(0,0,0,0.1);border-radius:3px;margin-top:0;background:#fff;color:#444;margin-left:60px;margin-right:15px;padding:0;position:relative}

    .timeline>li>.timeline-item>.time{color:#999;float:right;padding:10px;font-size:12px}

    .timeline>li>.timeline-item>.timeline-header{margin:0;color:#555;border-bottom:1px solid #f4f4f4;padding:10px;font-size:16px;line-height:1.1}

    .timeline>li>.timeline-item>.timeline-header>a{font-weight:600}

    .timeline>li>.timeline-item>.timeline-body,.timeline>li>.timeline-item>.timeline-footer{padding: 2px 10px 2px 10px;}

    .timeline>li>.fa,.timeline>li>.glyphicon,.timeline>li>.ion{width:30px;height:30px;font-size:15px;line-height:30px;position:absolute;color:#666;background:#d2d6de;border-radius:50%;text-align:center;left:18px;top:0}

    .timeline>.time-label>span{font-weight:600;padding:5px;display:inline-block;background-color:#fff;border-radius:4px}

    .timeline-inverse>li>.timeline-item{background:#f0f0f0;border:1px solid #ddd;-webkit-box-shadow:none;box-shadow:none}

    .timeline-inverse>li>.timeline-item>.timeline-header{border-bottom-color:#ddd}

    .profile-user-img {
        margin: 0px auto;
        width: 100px;
        height: 100px;
        padding: 3px;
        border: 3px solid #D2D6DE;
    }

    .img-circle {
        border-radius: 50%;
    }

    .inquiry-details span{
        font-size: 12px;
    }

    .inquiry-status{
        font-size: 12px
    }

    .mailbox-attachment img {
        max-width: 100%;
        max-height: 100%;
    }


    .square {
        height: 75px;
        width: 75px;
    }
    .mailbox-attachment-name{
        font-size: 12px;
    }
</style>  
@stop

@section('content')
<!-- Content-->
<section>
  <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Inquiry<small>Management</small></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Inquiry</li>
            <li class="active">list</li>
        </ol>
    </section>

    <!-- !!Content Header (Page header) -->
     <section class="content"> 
        <div class="box box-default">
            <div class="box-body">    
               <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="pull-left">                    
                            <img class="profile-user-img img-responsive img-circle" 
                          src="{{asset('assets/adminlte/img/place.jpg')}}" alt="User profile picture">
                        </div>            
                        <div class="pull-left" style="margin-left:20px;margin-top:10px">
                            <h3 class="profile-username">@if(isset($client)){{$client->first_name}} {{$client->last_name}}@else @endif</h3>                         
                            <span style="margin-top:-8px">
                                <p class="text-muted pull-left">Inquiry No: #{{$data->inquiry_code?$data->inquiry_code:"n/a"}}</p>
                                <p class="text-muted pull-left" style="margin-left:20px">Project : {{$data->title?$data->title:"no project name"}}</p>
                            </span>
                            <br>               
                            <span class="inquiry-details">                              
                                <span class="label label-primary pull-left" >Sector - {{$data['sectors']?$data['sectors']->name:"no sector"}}</span>
                                <span class="label label-primary pull-left" style="margin-left:10px">source - {{$data['source']?$data['source']->name:"no source"}}</span>
                                <span class="label label-primary pull-left" style="margin-left:10px">{{ $data->created_source}}</span>
                            </span>
                        </div>
                        @if($data['status_id']==0)
                            <span class="label label-warning pull-right inquiry-status" style="margin-top:15px;">Pending</span>
                        @elseif($data['status_id']==1)
                            <span class="label label-primary pull-right inquiry-status" style="margin-top:15px;">Approved</span>
                        @elseif($data['status_id']==2)
                            <span class="label label-danger pull-right inquiry-status" style="margin-top:15px;">Rejected</span>
                        @elseif($data['status_id']==3)
                            <span class="label label-danger pull-right inquiry-status" style="margin-top:15px;">Re-Open</span>    
                        @endif
                    </div>
               </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8">
                <div class="box box-default">
                    <div class="box-body">    
                        <p>Inquiry Details </p>
                        <hr style="margin-top:0">
                        <div class="row">                            
                            <div class="col-md-6">
                              <b>Inquiry No #{{$data->inquiry_code}}</b><br>
                              <br>
                              <b>Project :</b> {{$data->title?$data->title:"no title"}}<br>
                              <b>Project Value :</b> {{$data->project_value?$data->project_value:"Not Specified"}}<br>
                              <b>Sector :</b> {{$data['sectors']?$data['sectors']->name:"no sector"}}<br>
                              <b>Source :</b> {{$data['source']?$data['source']->name:"no source"}}<br>
                              <b>Inquiry From :</b> {{$data->created_source}} <br>
                              <b>Attachments :</b> @if(count($data['image'])>0) YES @else NO @endif<br><br>

                              <b>Developer : {{$data->developer?$data->developer:"no developer"}} {{$data->constructor_contact?", ".$data->constructor_contact:" , no contact"}}</b><br>
                              <b>Architect : {{$data->architect?$data->architect:"no Architect"}} {{$data->architect_contact?", ".$data->architect_contact:", no contact"}}</b><br>
                              <b>Contractor : {{$data->constructor?$data->constructor:"no contractor"}} {{$data->contractor?", ".$data->contractor_contact:", no contact"}}</b><br>
                              
                            </div>
                            <div class="col-md-6 text-right">
                              <b>{{$data->created_at->format('Y-m-d')}}</b><br>
                              <br>
                               <b>Address : {{$data->address?$data->address:"no adreess"}}</b><br>
                               <b>District : {{$data->district?$data->district:"no district"}}</b><br>
                               <b>City : {{$data->city?$data->city:"no city"}}</b><br>                              
                               <b></b><br>
                               <b></b><br>
                               <b></b><br>
                               <b></b><br>
                               <b>Contact Person : {{$data->contact_person?$data->contact_person:"not mentioned"}}</b><br>                              
                               <b>Designation    : {{$data->designation?$data->designation:"not mentioned"}}</b><br>                              
                               <b>Contact        : {{$data->contact?$data->contact:"not mentioned"}}</b><br>                              
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box box-default">
                    <div class="box-body">    
                        <p>Attachments <i class="fa fa-paperclip"></i></p>
                        @if(count($data['image'])>0)
                        <ul class="mailbox-attachments clearfix">
                            @foreach($data['image'] as $images)
                            <!-- <li>
                              <span class="mailbox-attachment-icon"><i class="fa fa-file-pdf-o"></i></span>
                              <div class="mailbox-attachment-info">
                                <a href="#" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> Sep2014-report.pdf</a>
                                <a href="#" class="btn btn-default btn-xs pull-right"><i class="fa fa-cloud-download"></i></a>
                              </div>
                            </li> -->
                            <li>
                              <span class="mailbox-attachment-icon has-img">
                                <img class="square" src="{{url('core/storage/uploads/images/project')}}/{{$images->filename}}" >
                              </span>
                              <div class="mailbox-attachment-info">
                                <a target="_blank" href="{{url('core/storage/uploads/images/project')}}/{{$images->filename}}" class="mailbox-attachment-name"><i class="fa fa-camera"></i> {{$images->filename}}</a>
                                <a target="_blank" href="{{url('core/storage/uploads/images/project')}}/{{$images->filename}}" class="btn btn-default btn-xs pull-right"><i class="fa fa-cloud-download"></i></a>
                              </div>
                            </li>
                            @endforeach
                        </ul>
                        @else
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                                    <h5>No Attachments</h5>
                                    <p>No Attachments found for this inquiry</p>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>

                    <div class="box box-default">
                            <div class="box-body">
                                <p>Follow Up </p>
                                @if(sizeof($followup) > 0)
                                    @foreach($followup as $key => $val)
                                        <hr style="margin-top:0">
                                        <div class="row">
                                            <div class="col-md-10">
                                              <b>Type :</b> {{$val->type->name}}<br>
                                              <b>Date :</b> {{$val->followup_date}}<br>
                                              <b>Title :</b> {{$val->title}} {{$val->note}}<br>
                                              <b>Remark :</b> {{$val->remark}} <br>
                                            </div>
                                            <div class="col-md-2 text-right">
                                               <b>PENDING</b><br>
                                            </div>
                                        </div>
                                    @endforeach
                                @else
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                                            <h5>No Follow Up</h5>
                                            <p>No Follow Up found for this inquiry</p>
                                        </div>
                                    </div>
                                @endif
                            </div>
                    </div>

                <div class="row">
                    <div class="col-md-12 text-right">
                        <?php echo $followup->render(); ?>
                    </div>
                </div>
            </div>


            <div class="col-md-4">
                <ul class="timeline">
                    @foreach($data['transaction'] as $timeline)
                        <li class="time-label">
                            @if($timeline->status_id==0)
                                <span class="bg-yellow">
                            @elseif($timeline->status_id==1)
                                <span class="bg-green">
                            @elseif($timeline->status_id==2)
                                <span class="bg-red">
                            @elseif($timeline->status_id==3)
                                <span class="bg-blue">
                            @endif
                            {{date_format($timeline->created_at, 'D d M-Y')}}
                            </span>
                        </li>
                        <li>
                            @if($timeline->status_id==0)
                                <i class="fa fa-bolt bg-yellow"></i>
                            @elseif($timeline->status_id==1)
                                <i class="fa fa-check-square bg-green"></i>
                            @elseif($timeline->status_id==2)
                                <i class="fa fa-close bg-red"></i>
                            @elseif($timeline->status_id==3)
                                <i class="fa fa-recycle bg-blue"></i>                                            
                            @endif
                            <div class="timeline-item">
                                <span class="time">
                                    <i class="fa fa-clock-o"></i> 
                                    {{date_format($timeline->created_at, 'h:i:s')}}
                                </span>

                                <h4 class="timeline-header"><a href="#">Inquiry</a></h4>

                                <div class="timeline-body">
                                    @if(count($timeline->inquriyAssign)>0)
                                        {{$timeline->events['name']}} to {{$timeline->inquriyAssign->employee['first_name']}} {{$timeline->inquriyAssign->employee['last_name']}}
                                    @else
                                        {{$timeline->events['name']}}
                                    @endif
                                </div>
                                <div class="timeline-footer">
                                    <p style="font-size:11px">By 
                                        @if($timeline['users']){{$timeline['users']->first_name}} {{$timeline['users']->last_name}}@else-@endif
                                    </p>
                                </div>
                            </div>
                        </li>
                    @endforeach 
               </ul> 
            </div>
        </div>
    </section>        


    <!-- Main content -->

</section>
<!-- !!!Content -->


@stop
@section('js')
  <!-- CORE JS -->
<script type="text/javascript">
    $(document).ready(function(){
        
        // $('#time_container').perfectScrollbar();
    });
</script>
  <!-- //CORE JS -->
@stop
