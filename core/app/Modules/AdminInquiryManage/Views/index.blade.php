@extends('layouts.back_master') @section('title','Admin - Inquiry Management')
@section('current_title','Dashboard')
@section('css')
<link rel="stylesheet" href="{{asset('assets/dist/chosen/bootstrap-chosen.css')}}">
<style type="text/css">
    .port-image
    {
        width: 100%;
    }

    .gallery_product
    {
        margin-bottom: 30px;
    }

    .mediam
    {
        font-size: medium;
    }
</style>  
@stop

@section('content')
<!-- Content-->
<section>
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Inquiry
      <small>Management</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li>Inquiry</li>
      <li class="active">list</li>
    </ol>
  </section>
  <!-- !!Content Header (Page header) -->

  <!-- Main content -->
  <section class="content">  
    <div class="box box-default">
        <div class="box-body">  
            <form  method="get" role="form">
                <div class="row">
                    <div class="col-xs-3 col-sm-3 col-md-2 col-lg-3">
                        <div class="form-group">
                            <label>Inquiry No</label>    
                            <input name="inquiry_no" type="text" class="form-control" id="inquiry_no" placeholder="Inquiry No" data-toggle="tooltip" data-placement="top" title="Inquiry No" value="{{$old['inquiry']}}">
                        </div> 
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-2 col-lg-3">
                        <div class="form-group">
                            <label>Sector</label>
                            <select name="sector" id="sector" class="form-control chosen">
                                <option value="0">-ALL-</option>
                                @foreach($sectors as $key=>$sector)
                                    <option value="{{$key}}" @if($key==$old['sector']) selected @endif>{{$sector}}</option>
                                @endforeach
                            </select>
                        </div> 
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
                        <div class="form-group">
                            <label>Source</label>
                            <select name="source" id="source" class="form-control chosen">
                                <option value="0">-ALL-</option>
                                @foreach($sources as $key=>$source)
                                    <option value="{{$key}}" @if($key==$old['source']) selected @endif>{{$source}}</option>
                                @endforeach
                            </select>
                        </div> 
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
                        <div class="form-group">
                            <label>Status</label>
                            <select name="status" id="status" class="form-control chosen">
                                <option value="0">-ALL-</option>
                                @foreach($statuses as $key=>$status)
                                    <option value="{{$key}}" @if($key==$old['status']) selected @endif>{{$status}}</option>
                                @endforeach
                            </select>
                        </div> 
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 text-right">
                        <div  style="margin-top:20px;">
                             <button type="submit" class="btn btn-primary btn-sm" 
                              data-toggle="tooltip" data-placement="top" title="Search">
                              <i class="fa fa-search" aria-hidden="true"></i> Search
                            </button> 
                        </div>    
                    </div>
                </div>  
            </form>
        </div>
    </div>


    <div class="box box-default">
        <div class="row" style="margin-top: 10px">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <h4>Inquiry List</h4>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right">               
                <button type="button" class="btn btn-primary btn-sm create">
                 <i class="fa fa-plus" aria-hidden="true"></i> Add
                </button> 
            </div>
            </div>
        </div>
        <div class="box-body table-responsive content-box-body">
            @if ($inquiries->isEmpty())
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="margin-top:8%;">
                    <p>You have not created any inquiry.</p>
                    <a href="{{ url('admin/inquiry/add') }}">
                        Create Inquiry
                    </a>
                </div>
            @else
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="text-align: center">Inquiry</th>
                            <th style="text-align: left" >Project</th>
                            <th style="text-align: left">Sector</th>
                            <th style="text-align: left">Contact Person</th>
                            <th style="text-align: left">Source</th>
                            <th style="text-align: left">Oppertunity Identified By</th>
                            <th style="text-align: left">Assigned To</th>
                            <th style="text-align: center" width="13%">Created At</th>
                            <th style="text-align: center" width="13%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($inquiries as $inquiry)
                        <tr>
                            <td style="text-align: center">
                                <a href="{{url('admin/inquiry/show/')}}/{{$inquiry->id}}" style="color:blue;">
                                   #{{ $inquiry['inquiry_code'] }}
                                </a>
                                <br>
                                @if($inquiry['status_id']==0)
                                    <span class="label label-warning">Pending</span>
                                @elseif($inquiry['status_id']==1)
                                    <span class="label label-primary">Approved</span>
                                @elseif($inquiry['status_id']==2)
                                    <span class="label label-danger">Rejected</span>
                                @endif         
                            </td>
                            <td style="text-align: left">
                                {{ $inquiry['title'] }} <br>
                                (Rs.{{ $inquiry['project_value']}})
                            </td>
                            <td style="text-align: left">{{ $inquiry['sectors']->name }}</td>
                            <td style="text-align: left">{{ $inquiry['contact_person'] }}</br>{{ $inquiry['contact'] }}
                            </td>
                            <td style="text-align: left">@if($inquiry['source']){{ $inquiry['source']->name }}@else Not Mentioned @endif</td>
                            <td style="text-align: left">{{ $inquiry['user_name']}}</td>
                            
                            <td style="text-align: left">
                                @if(count($inquiry['activeTransaction'])>0)
                                    {{$inquiry['activeTransaction']['inquriyAssign']['employee']->first_name}}
                                    {{$inquiry['activeTransaction']['inquriyAssign']['employee']->last_name}}
                                    </br>
                                    <span class="label label-warning" style="background-color: green !important">{{$inquiry['activeTransaction']['inquriyAssign']['employee']['type']->name}}</span>
                                @else
                                    not assigned                                        
                                @endif
                            </td>
                            <td style="text-align: center">{{ $inquiry['created_at']->format('j M Y') }} <br>
                                ( {{ $inquiry['created_at']->diffForHumans() }} )</td>
                            <td style="text-align: center;">
                                @if($inquiry['status_id']==0)  
                                    @if($user->hasAnyAccess(['inquiry.approve', 'admin']))
                                        <button data-id="{{$inquiry->id}}" class="btn btn-default btn-xs approve" data-toggle="tooltip" data-placement="top" title="Approve Inquiry"><i class="fa fa-check-circle" style="color: blue"></i></button>
                                    @else
                                        <button class="disabled btn btn-default btn-xs approve" data-toggle="tooltip" data-placement="top" title="Approve Disabled"><i class="fa fa-check-circle"></i></button>
                                    @endif
                                
                                    @if($user->hasAnyAccess(['inquiry.reject', 'admin']))
                                        <button data-id="{{$inquiry->id}}" class="btn btn-default btn-xs reject" data-toggle="tooltip" data-placement="top" title="Reject Inquiry"><i class="fa fa-times" style="color: red"></i></button>
                                    @else
                                        <button class="disabled btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="Reject Disabled"><i class="fa fa-times"></i></button>
                                    @endif
                                
                                    <button class="btn btn-default btn-xs disabled btn-actions" data-toggle="tooltip" data-placement="top" title="Assign Disabled"><i class="fa fa-arrow-circle-o-up"></i></button>

                                @elseif($inquiry['status_id']==1)
                                
                                    <button class="btn btn-default btn-xs disabled btn-actions" data-toggle="tooltip" data-placement="top" title="Approve Disabled"><i class="fa fa-check-circle"></i></button>
                                
                                    <button class="btn btn-default btn-xs disabled btn-actions" data-toggle="tooltip" data-placement="top" title="Reject Disabled"><i class="fa fa-times"></i></button>
                            
                                    <button data-id="{{$inquiry->id}}" class="btn btn-default btn-xs assign" data-toggle="tooltip" data-placement="top" title="Assign Inquiry"><i class="fa fa-arrow-circle-o-up" style="color: green"></i></button>

                                @elseif($inquiry['status_id']==2)
                                   
                                        <button class="btn btn-default btn-xs disabled btn-actions" data-toggle="tooltip" data-placement="top" title="Approve Disabled"><i class="fa fa-check-circle"></i></button>
                                    
                                        @if($user->hasAnyAccess(['inquiry.reopen', 'admin']))
                                            <button data-id="{{$inquiry->id}}" class="btn btn-default btn-xs reopen" data-toggle="tooltip" data-placement="top" title="Re-Open Inquiry"><i class="fa fa-recycle" style="color: green"></i></button>
                                        @else
                                            <button href="#" class="btn btn-default btn-xs disabled btn-actions" data-toggle="tooltip" data-placement="top" title="Re-Open Disabled"><i class="fa fa-recycle"></i></button>
                                        @endif
                                   
                                        <button class="disabled btn-actions" data-toggle="tooltip" data-placement="top" title="Assign Disabled"><i class="fa fa-arrow-circle-o-up"></i></button>
                                    
                                @endif

                                @if($user->hasAnyAccess(['inquiry.edit', 'admin']))
                                    <button data-id="{{$inquiry->id}}" class="btn btn-default btn-xs edit" data-toggle="tooltip" data-placement="top" title="Edit Inquiry"><i class="fa fa-pencil" style="color: brown"></i></button>
                                @else
                                    <button class="btn btn-default btn-xs disabled btn-actions" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></button>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div> 
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <p>Showing {{count($inquiries)}} lead(s)  </p>
            </div>
            <div class="col-md-6 text-right">
                <?php echo $inquiries->render(); ?>
            </div>
            @endif
        </div>
    </div>

  </section>
  <!-- !!Main content -->

</section>
<!-- !!!Content -->

<div class="modal" id="reject_modal" name="reject_modal" style="margin-top: 150px">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Reject Reason</h4>
            </div>
            <div class="modal-body">
                <div class="col-md-12 col-lg-12">
                    <div class="form-group">
                        <label>Reason <span style="color: red">*</span></label>
                        <textarea id="reject_reason" type="text" class="form-control" name="reject_reason" placeholder="Reason Reject">{{ old('reject_reason') }}</textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="reject_id" id="reject_id">
                <button type="button" class="btn btn-primary reject-inquiry">Reject</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="assign_modal" name="assign_modal" style="margin-top: 150px">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Inquity Assign</h4>
            </div>
            <div class="modal-body">
                <div class="col-md-12 col-lg-12">
                    <div class="form-group">
                        <label>Assign User <span style="color: red">*</span></label>
                        {!! Form::select('assign_user',[], [],['class'=>'form-control chosen search','style'=>'width:100%;','required','data-placeholder'=>'Choose User','id'=>'assign_user']) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="assign_id" id="assign_id">
                <button type="button" class="btn btn-primary assign-inquiry">Assign</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
    <div class="overlay" style="display:none;">
        <i class="fa fa-refresh fa-spin"></i>
    </div>
</div>

@stop
@section('js')
  <!-- CORE JS -->
<script type="text/javascript">
    $(document).ready(function(){
        $('.approve').click(function(e){
            e.preventDefault();
            id = $(this).data('id');
            ajaxRequest( '{{url('admin/inquiry/approve')}}' , { 'id' : id }, 'post', successFunc);            
        });

        $('.assign').click(function(e){
            e.preventDefault();
            id = $(this).data('id');
            $("#assign_id").val(id);
            ajaxRequest( '{{url('admin/inquiry/assign/json/user')}}' , { 'id' : id }, 'get', result);
            $('#assign_modal').modal();
        });

        $('.reject').click(function(e){
            e.preventDefault();
            id = $(this).data('id');
            $("#reject_id").val(id);
            $('#reject_modal').modal();
        });

        $('.reject-inquiry').click(function(e){
            e.preventDefault();
            id = $("#reject_id").val();
            reason = $('#reject_reason').val();
            if(reason!=""){
                console.log('sdfklnsdknfg');
                $('#reject_modal').modal('toggle');
                ajaxRequest( '{{url('admin/inquiry/reject')}}' , { 'id' : id, 'reason' : reason }, 'post', successFunc);            
            }else{
                swal('Warning!','Please enter reason to reject inquiry','error');    
            }
        });

        $('.reopen').click(function(e){
            e.preventDefault();
            id = $(this).data('id');
            ajaxRequest( '{{url('admin/inquiry/reopen')}}' , { 'id' : id }, 'post', successFunc);            
        });

        $('.assign-inquiry').click(function(e){
            $('.overlay').show();
            e.preventDefault();
            id      = $('#assign_id').val();
            user    = $("#assign_user").val();
            ajaxRequest( '{{url('admin/inquiry/assign')}}' , { 'id' : id, 'user' : user }, 'post', assignSuccessFunc);
        });

        $('.edit').click(function(e){
            // ajaxRequest( '{{url('admin/inquiry/edit')}}' , { 'id' : $(this).data('id') }, 'get');
            window.location.href='{{url('admin/inquiry/edit')}}/'+$(this).data('id');
        });

        $('.create').click(function(e){
            window.location.href='{{url('admin/inquiry/add')}}';
        });

    });

    /**
     * Delete the menu return function
     * Return to this function after sending ajax request to the menu/delete
     */
    function successFunc(data){
        if(data.status=='success'){
            swal(data.title,data.msg+' !','success');
            window.location.reload();
        }else if(data.status=='error'){
            swal('Oops',data.msg+' !','error');
        }
    }

    function assignSuccessFunc(data){
        $('.overlay').hide();
        $('#assign_modal').modal('toggle');
        if(data.status=='success'){
            swal(data.title,data.msg+' !','success');
            window.location.reload();
        }else if(data.status=='error'){
            swal('Oops',data.msg+' !','error');
        }
    }

    function result(data){
        $('#assign_user').html("");
        $('#assign_user').append('<option value="0">Select User</option>');
        $.each(data,function(key,value){
            if(value.name){
                $('#assign_user').append('<option value="'+value.id+'">'+value.code+" - "+value.name+'</option>');
            }else{                
                $('#assign_user').append('<option value="'+value.id+'">'+value.code+" - "+value.first_name+"  "+value.last_name+'</option>');
            }
        });
        $('#assign_user').trigger("chosen:updated");
    }
</script>
  <!-- //CORE JS -->
@stop
