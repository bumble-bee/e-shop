<?php namespace App\Modules\AdminInquiryManage\Controllers;
/**
* Controller class
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Core\EmployeeManage\Models\Employee;
use Core\EmployeeManage\Models\EmployeeType;
use Core\Sector\Models\Sector;

use App\Models\Source;
use App\Models\Status;
use App\Models\Inquiry;
use App\Models\InquiryTransaction;
use App\Models\Image;
use App\Models\User;
use App\Models\AppUser;
use App\Models\InquiryUser;
use App\Classes\Functions;

use App\Modules\AdminInquiryManage\BusinessLogics\Logic;
use App\Modules\AdminFollowUpManage\Models\FollowUp;
use App\Modules\AdminFollowUpManage\Models\FollowUpType;
use App\Events\NotificationEvent;
use App\Exceptions\TransactionException;
use PhpSpec\Exception\Exception;
use DB;
use Log;
use Sentinel;

class AdminInquiryManageController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

    private $functions;

    public function __construct(Functions $functions){
        $this->functions = $functions;
    }    

	public function index(Request $request)
	{	
        $userRole = Sentinel::getUser()->roles()->first();
        $usr     = Sentinel::getUser();
        $user    = User::with('sectors')->find($usr->id);

        // $role_id = $userRole->id;
        if($userRole){
            $user_id = $userRole->pivot->user_id;
        }else{
            $user_id = Sentinel::getUser()->id;
        }

        $employee = User::where('status','=',1)->where('id','=',$user_id)->first();
        $employee_id = $employee->employee_id;

        $employee_detail = Employee::where('id','=',$employee_id)->first();
        $employee_type_id = $employee_detail->employee_type_id;

        $employeeReps = $this->functions->getEmployeeRepIds($employee_id);

		$data=Inquiry::orderBy('created_at', 'desc')->with(['sectors', 'source','activeTransaction']);

        $inquiry_no = $request->inquiry_no;
        if($inquiry_no!=null && $inquiry_no!=""){
            $data = $data->where('inquiry_code', 'like', '%' .$inquiry_no. '%');
        }
        
        if(count($user->sectors)>0){
            //FILTER WITH SECTORS
            $data = $data->whereIn('sector',$user->sectors->lists('sector_id'))->orWhereIn('created_by', $employeeReps);
        }else{
            $data = $data->whereIn('created_by', $employeeReps);
        }      

        $sector = $request->sector;
        if($sector>0){
            $data = $data->where('sector', $sector);
        }

        $source = $request->source;
        if($source>0){
            $data = $data->where('source_id', $source);
        }

        $status = $request->status;
        if($status>-1){
            $data = $data->where('status_id', $status);
        }

        $sec=Sentinel::getUser()->sector_id;

        if($sec!=0){
            $sectors=Sector::where('id',$sec)->lists('name','id');
        }else{
            $sectors=Sector::all()->lists('name','id');
        }

        $data=$data->paginate(20);

		$logic = new Logic();

		$inquiries=$logic->prependUser($data);

		$sources=Source::all()->lists('name','id');

		$statuses=[0=>'Pending',1=>'Approved',2=>'Reject'];

		return view("AdminInquiryManage::index")->with([
			'inquiries'=>$inquiries,
			'sectors'=>$sectors,
			'sources'=>$sources,
			'statuses'=>$statuses,
			'old'=>['inquiry'=>$inquiry_no,'sector'=>$sector,'source'=>$source,'status'=>$status]
		]);
	}

    /**
     * Detail of Inquiry.
     *
     * @return Response
     */
    public function getInquiryDetails(Request $request)
    {
        $inquiry=Inquiry::with('sectors')->find($request->inquiry);
        if($inquiry){
            $images=Image::where('inquiry_id',$inquiry->id)->get();
            return ['detail'=>$inquiry,'images'=>$images];
        }else{
            return 0;
        }
    }

	/**
	 * Assign users for Inquiry.
	 *
	 * @return Response
	 */
	public function getAssignUsers(Request $request)
	{
        $inquiry=Inquiry::find($request->id);

        $res = InquiryTransaction::where('inquiry_id',$request->id)->where('event_id',5)->with(['inquriyAssign'])->withTrashed()->orderBy('id','ASC')->get();


        $emp = Employee::find(Sentinel::getUser()->employee_id);

        $empTypeSales=EmployeeType::where('name','like','%Sales Team Head%')->first();

        $empTypePmo=EmployeeType::where('name','like','%PMO%')->first();

        // return Employee::where('employee_type_id',[8,9,10])->select(DB::raw('CONCAT(first_name," ",last_name) as name'), 'id', 'code')->get();

        if($emp->employee_type_id==1){

            return Employee::where('employee_type_id',$empType->id)->select(DB::raw('CONCAT(first_name," ",last_name) as name'), 'id', 'code')->get();
        
        }elseif($emp->employee_type_id==$empTypePmo->id){

            $res1=InquiryTransaction::where('inquiry_id',$request->id)->where('event_id',5)->with(['inquriyAssign'])->orderBy('id','ASC')->get();

            // $empType1=EmployeeType::where('name','like','%Sales Team Head%')->first();

            $employee = Employee::whereIn('employee_type_id',function($query){
                $query->select('id')->from('employee_type')
                    ->where('name','like','%Sales Team Head%')
                    ->orWhere('name','like','%PMO%')
                    ->orWhere('name','like','%Sector Head%')
                    ->orWhere('name','like','%Sales Person%');
            })->orderBy('code','asc')->get();

            foreach ($employee as $key => $value) {

                if(isset($res1[0]) && $res1[0]->reference==$value['id']){
                    unset($employee[$key]);
                }
            }

            return $employee;

        }elseif($emp->employee_type_id==$empTypeSales->id){
            
            $res1=InquiryTransaction::where('inquiry_id',$request->id)->where('event_id',5)->with(['inquriyAssign'])->orderBy('id','ASC')->get();

            $employee = Employee::find($emp->id);

            $empType1=EmployeeType::where('name','like','%Sales Person%')->first();

            $employee = $employee->descendants()->where('employee_type_id',$empType1->id)->get();

            foreach ($employee as $key => $value) {

                if(isset($res1[0]) && $res1[0]->reference==$value['id']){
                    unset($employee[$key]);
                }
            }

            return $employee;

        }else{
            return [];
        }
	}

    /**
     * create view of Inquiry.
     *
     * @return Response
     */
    public function createView()
    {  
        $sectors = Sector::all()->lists('name','id');

        $sources = source::all()->lists('name','id');

        return view("AdminInquiryManage::create",compact('sectors','sources'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'project' => 'required',
            'sector' => 'required',
            'source' => 'required',
            'contact_person' => 'required',
            'contact' => 'required|numeric'
        ]);

        $logic = new Logic();
        return $bool=$logic->addNewInquiry($request);
    }

    /**
     * Edit view of Inquiry.
     *
     * @return Response
     */
    public function editView($id)
    {  
        $inquiry=Inquiry::find($id);

        $sectors=Sector::all()->lists('name','id');

        $sources=Source::all()->lists('name','id');

        return view("AdminInquiryManage::edit",compact('inquiry','sectors','sources'));
    }

    /**
     * Approve Inquiry.
     *
     * @return Response
     */
    public function approve(Request $request)
    {
        try{
            
            DB::transaction(function () use ($request) {

                $inquiry=Inquiry::find($request->id);
                
                if($inquiry){
                    
                    $inquiry->status_id=1;
                    $bool=$inquiry->save();

                    if($bool){
                        InquiryTransaction::where('inquiry_id',$inquiry->id)->delete();

                        $usr=User::find(Sentinel::getUser()->id);
                        
                        $emp=Employee::find($usr->employee_id);

                        $trans=InquiryTransaction::create([
                            'inquiry_id'    =>  $inquiry->id,
                            'event_id'      =>  INQUIRY_APPROVED,
                            'reference'     =>  '',
                            'action_by'     =>  $usr->employee_id
                        ]);

                        // $senduser=User::where('employee_id',$inquiry->created_by)->first();
                        // $msg = $emp->first_name.' '.$emp->last_name.' Accepted your inquiry #'.$inquiry->inquiry_code;
                        // event(new NotificationEvent($senduser,$msg));

                        if(!$trans){
                            throw new TransactionException("Faild to complete transaction", 102);
                        }

                    }else{                      
                        throw new TransactionException("Faild to approve inquiry", 100);
                    }
                }else{
                    throw new TransactionException("Faild to find inquiry", 101);
                }
            });

           return response()->json(['status' => 'success','msg' => 'Approve successfully completed','title' => 'Done !']);

        } catch (TransactionException $e) {
            if ($e->getCode() == 100) {
                return response()->json(['status' => 'error','msg'=>"Faild to approve inquiry"]);
            }else if ($e->getCode() == 101) {
                return response()->json(['status' => 'error','msg'=>"Faild to find Inquiry"]);
            }else if ($e->getCode() == 102) {
                return response()->json(['status' => 'error','msg'=>"Faild to complete transaction. Try Again"]);
            }
        } catch (Exception $e) {
            return response()->json(['status' => 'error','msg'=>"Transaction error"]);
        }
    }

	/**
	 * Assign Inquiry.
	 *
	 * @return Response
	 */
	public function assign(Request $request)
	{

        try{
            
            DB::transaction(function () use ($request) {

                $inquiry=Inquiry::find($request->id);
                
                if($inquiry){
                    
                    $inquiry->assigned_user = $request->user;
                    $inquiry->timestamp     = date('Y-m-d H:i:s');
                    $bool=$inquiry->save();

                    if($bool){
                    	InquiryTransaction::where('inquiry_id',$inquiry->id)->delete();

                        $usr=User::find(Sentinel::getUser()->id);

                        $trans=InquiryTransaction::create([
                            'inquiry_id'    =>  $inquiry->id,
                            'event_id'      =>  INQUIRY_ASSIGNED,
                            'reference'     =>  $request->user,
                            'action_by'     =>  $usr->employee_id
                        ]);

                        if(!$trans){
                            throw new TransactionException("Faild to complete transaction", 102);
                        }

                        InquiryUser::where('inquiry_id',$inquiry->id)->delete();

                        $inquiry_user=InquiryUser::create([
                        	'inquiry_id' 	=>	$inquiry->id,
                            'user_id'       =>  $request->user,
                        	'transaction_id'=>	$trans->id
                        ]);

                        if(!$inquiry_user){
                        	throw new TransactionException("Faild to complete transaction", 103);
                        }

                    }else{                    	
                        throw new TransactionException("Update faild in assign inquiry", 100);
                    }
                }else{
                    throw new TransactionException("Faild to find inquiry", 101);
                }
            });

    	   return response()->json(['status' => 'success','msg' => 'Approve successfully completed','title' => 'Done !']);

        } catch (TransactionException $e) {
            if ($e->getCode() == 100) {
                return response()->json(['status' => 'error','msg'=>"Faild to assign inquiry to user"]);
            }else if ($e->getCode() == 101) {
                return response()->json(['status' => 'error','msg'=>"Faild to find Inquiry"]);
            }else if ($e->getCode() == 102) {
                return response()->json(['status' => 'error','msg'=>"Faild to complete transaction. Try Again"]);
            }else if ($e->getCode() == 103) {
                return response()->json(['status' => 'error','msg'=>"Couldn't complete assign process. Try Again"]);
            }
        } catch (Exception $e) {
            return response()->json(['status' => 'error','msg'=>"Transaction error"]);
        }
	}

	/**
	 * Reject Inquiry.
	 *
	 * @return Response
	 */
	public function reject(Request $request)
	{
		try{
            
            DB::transaction(function () use ($request) {

                $inquiry=Inquiry::find($request->id);
                
                if($inquiry){
                    
                    $inquiry->status_id=2;
                    $bool=$inquiry->save();

                    $usr=User::find(Sentinel::getUser()->id);

                    if($bool){
                    	InquiryTransaction::where('inquiry_id',$inquiry->id)->delete();
                        $trans=InquiryTransaction::create([
                            'inquiry_id'    =>  $inquiry->id,
                            'event_id'      =>  INQUIRY_CANCELLED,
                            'reference'     =>  $request->reason,
                            'action_by'     =>  $usr->employee_id
                        ]);

                        if(!$trans){
                        	throw new TransactionException("Faild to complete transaction", 102);
                        }

                    }else{                    	
                        throw new TransactionException("Faild to reject inquiry", 100);
                    }
                }else{
                    throw new TransactionException("Faild to find inquiry", 101);
                }
            });

    	   return response()->json(['status' => 'success','msg' => 'Reject successfully completed','title' => 'Done !']);

        } catch (TransactionException $e) {
            if ($e->getCode() == 100) {
                return response()->json(['status' => 'error','msg'=>"Faild to reject inquiry"]);
            }else if ($e->getCode() == 101) {
                return response()->json(['status' => 'error','msg'=>"Faild to find Inquiry"]);
            }else if ($e->getCode() == 102) {
                return response()->json(['status' => 'error','msg'=>"Faild to complete transaction. Try Again"]);
            }
        } catch (Exception $e) {
            return response()->json(['status' => 'error','msg'=>"Transaction error"]);
        }
	}

	/**
	 * Reopen Inquiry.
	 *
	 * @return Response
	 */
	public function reopen(Request $request)
	{
		try{
            
            DB::transaction(function () use ($request) {

                $inquiry=Inquiry::find($request->id);
                
                if($inquiry){
                    
                    $inquiry->status_id=0;
                    $bool=$inquiry->save();

                    $usr=User::find(Sentinel::getUser()->id);

                    if($bool){
                    	InquiryTransaction::where('inquiry_id',$inquiry->id)->delete();
                        $trans=InquiryTransaction::create([
                        	'inquiry_id'    =>  $inquiry->id,
                            'event_id'      =>  INQUIRY_REOPENED,
                            'reference'     =>  'Re Opened',
                            'action_by'     =>  $usr->employee_id
                        ]);

                        if(!$trans){
                        	throw new TransactionException("Faild to complete transaction", 102);
                        }

                    }else{                    	
                        throw new TransactionException("Faild to reopen inquiry", 100);
                    }
                }else{
                    throw new TransactionException("Faild to find inquiry", 101);
                }
            });

    	   return response()->json(['status' => 'success','msg' => 'Reopen successfully completed','title' => 'Done !']);

        } catch (TransactionException $e) {
            if ($e->getCode() == 100) {
                return response()->json(['status' => 'error','msg'=>"Faild to reopen inquiry"]);
            }else if ($e->getCode() == 101) {
                return response()->json(['status' => 'error','msg'=>"Faild to find Inquiry"]);
            }else if ($e->getCode() == 102) {
                return response()->json(['status' => 'error','msg'=>"Faild to complete transaction. Try Again"]);
            }
        } catch (Exception $e) {
            return response()->json(['status' => 'error','msg'=>"Transaction error"]);
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $followup = FollowUp::with(['type'])->where('status','=',1)->where('inquiry_id','=',$id)->paginate(5);
        
        $inquiry=Inquiry::with(['sectors','source','transaction.users','transaction.events','transaction.inquriyAssign.employee','image'])->find($id);
        $client;
        if($inquiry->created_source=="MOBILE"){
            $client = User::find($inquiry->created_by); 
        }else{
            $client = Employee::find($inquiry->created_by); 
        }
 
		return view("AdminInquiryManage::detail")->with(['data'=>$inquiry,'client'=>$client,'followup'=>$followup]);
	}
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
    {
        // return $id;
        try{
            
            DB::transaction(function () use ($request,$id) {

                $inquiry=Inquiry::find($id);

                if($inquiry){
                    $inquiry->title                 =  $request->project_name;
                    $inquiry->project_value         =  $request->estimate_value;
                    $inquiry->sector                =  $request->sector;
                    $inquiry->address               =  $request->address;
                    $inquiry->city                  =  $request->city;
                    $inquiry->district              =  $request->district;
                    $inquiry->developer             =  $request->developer;
                    $inquiry->constructor           =  $request->contractor;
                    $inquiry->architect             =  $request->architect;
                    $inquiry->developer_contact     =  $request->developer_contact;
                    $inquiry->constructor_contact   =  $request->contractor_contact;
                    $inquiry->architect_contact     =  $request->architect_contact;
                    $inquiry->contact               =  $request->other_contact;
                    $inquiry->contact_person        =  $request->contact_person;
                    $inquiry->designation           =  $request->contact_person_designation;
                    $inquiry->source_id             =  $request->source;

                    $inquiry->save();
                }else{
                    throw new TransactionException("Faild to find Inquiry", 100);
                }
            });

            return redirect('admin/inquiry/list')->with([
                'success' => true,
                'success.message' => 'Inquiry Updated successfully!',
                'success.title'   => 'Success..!'
            ]);

        } catch (TransactionException $e) {
            if ($e->getCode() == 100) {
                
                return redirect('admin/inquiry/edit/'.$id)->with([
                    'error' => true,
                    'error.message' => 'Faild to find Inquiry!',
                    'error.title'   => 'Faild..!'
                ]);

            }
        } catch (Exception $e) {
            
            return redirect('admin/inquiry/edit/'.$id)->with([
                'error' => true,
                'error.message' => 'Transaction error!',
                'error.title'   => 'Faild..!'
            ]);
        }

        return $request->all();
    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
