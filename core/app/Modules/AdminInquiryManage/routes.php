<?php


Route::group(['middleware' => ['auth']], function()
{   
    Route::group(['prefix'=>'admin/inquiry/','namespace' => 'App\Modules\AdminInquiryManage\Controllers'],function() {

        Route::get('list', [
            'as' => 'inquiry.list', 'uses' => 'AdminInquiryManageController@index' 
        ]);

        Route::get('get-details', [
            'as' => 'inquiry.list', 'uses' => 'AdminInquiryManageController@getInquiryDetails' 
        ]);

        Route::get('show/{id}', [
            'as' => 'inquiry.show', 'uses' => 'AdminInquiryManageController@show' 
        ]);

        Route::get('assign/json/user', [
            'as' => 'inquiry.assign', 'uses' => 'AdminInquiryManageController@getAssignUsers' 
        ]);

        Route::get('edit/{id}', [
            'as' => 'inquiry.edit', 'uses' => 'AdminInquiryManageController@editView' 
        ]);

        Route::get('add', [
            'as' => 'inquiry.create', 'uses' => 'AdminInquiryManageController@createView' 
        ]);

        Route::post('approve', [
            'as' => 'inquiry.approve', 'uses' => 'AdminInquiryManageController@approve' 
        ]);

        Route::post('reject', [
            'as' => 'inquiry.reject', 'uses' => 'AdminInquiryManageController@reject' 
        ]);

        Route::post('reopen', [
            'as' => 'inquiry.reopen', 'uses' => 'AdminInquiryManageController@reopen'
        ]);

        Route::post('assign', [
            'as' => 'inquiry.assign', 'uses' => 'AdminInquiryManageController@assign'
        ]);

        Route::post('edit/{id}', [
            'as' => 'inquiry.edit', 'uses' => 'AdminInquiryManageController@update' 
        ]);

        Route::post('add', [
            'as' => 'inquiry.create', 'uses' => 'AdminInquiryManageController@store' 
        ]);

    });    
});
