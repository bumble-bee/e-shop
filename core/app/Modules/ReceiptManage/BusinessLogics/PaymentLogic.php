<?php namespace App\Modules\ReceiptManage\BusinessLogics;


/**
* Business Logics 
* Define all the busines logics in here
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/
use Illuminate\Database\Eloquent\Model;
use App\Modules\ReceiptManage\Models\Payment;
use App\Modules\ReceiptManage\Models\PaymentCash;
use App\Modules\ReceiptManage\Models\PaymentCheque;
use App\Modules\ReceiptManage\Models\PaymentDetails;
use App\Modules\ReceiptManage\Models\PaymentAttachment;
use Core\EmployeeManage\Models\Employee;
use Sentinel;
use DB;

class PaymentLogic {

	

	public function addPayment($data)
	{
		DB::transaction(function() use($data){
            $payment = Payment::create([
				'receipt_no'     => '',
				'payment_amount' => $data->payment_amount,
				'payment_date'   => $data->payment_date,
				'status'         => 0,
				'remarks'        => $data->remarks?$data->remarks:"",
				'collected_by'   => null,
				'rejected_at'    => null,
				'approvee_at'    => null,
				'hold_at'        => null,
            ]);

            $payment->receipt_no = 'RCPT/'.str_pad($payment->id,6,0,STR_PAD_LEFT);
			$payment->save();

	        if($payment) {                
	        	return $payment;
	        }else{
	            throw new TransactionException('Record wasn\'t inserted', 101);
	        }
	    });
	}


	public function addPaymentDetails($data)
	{
		DB::transaction(function() use($data){
            $payment_details = PaymentDetails::create([
				'payment_id' => $data->payment_id,
				'invoice_id' => $data->invoice_id,
				'amount'     => $data->amount,
				'status'     => 1
            ]);
			
	        if($payment_details) {                
	        	return $payment_details;
	        }else{
	            throw new TransactionException('Record wasn\'t inserted', 101);
	        }
	    });
	}

	public function addPaymentCash($data)
	{
		DB::transaction(function() use($data){
            $payment_cash = PaymentCash::create([
				'payment_id'  => $data->payment_id,
				'cash_amount' => $data->cash_amount,
				'remarks'     => ''
            ]);
			
	        if($payment_cash) {                
	        	return $payment_cash;
	        }else{
	            throw new TransactionException('Record wasn\'t inserted', 101);
	        }
	    });
	}

	public function addPaymentCheque($data)
	{
		DB::transaction(function() use($data){
            $payment_cheque = PaymentCheque::create([
				'payment_id'    => $data->payment_id,
				'cheque_amount' => $data->cheque_amount,
				'cheque_no'     => $data->cheque_no,
				'cheque_date'   => $data->cheque_date,
				'bank_id'       => $data->bank_id,
				'status'        => 0,
				'remarks'       => ''
            ]);
			
	        if($payment_cheque) {                
	        	return $payment_cheque;
	        }else{
	            throw new TransactionException('Record wasn\'t inserted', 101);
	        }
	    });
	}

	public function addPaymentAttachment($data)
	{
		DB::transaction(function() use($data){
            $payment_attach = PaymentAttachment::create([
				'payment_id' => $data->payment_id,
				'file_name'  => $data->file_name,
				'receipt_no' => $data->receipt_no,
				'file_size'  => $data->file_size,
				'path'       => $data->path
            ]);
			
	        if($payment_attach) {                
	        	return $payment_attach;
	        }else{
	            throw new TransactionException('Record wasn\'t inserted', 101);
	        }
	    });
	}

	public function getCollection($id)
	{
		return Payment::with('collectedby')->find($id);
	}

	public function getCollectionWithDetails($id)
	{
		return Payment::with(['collectedby','details.invoice.customer'])->find($id);
	}

	public function doApproveCollection($id)
	{
		DB::transaction(function() use($id){
            $col = $this->getCollection($id);

            $col->status = PAYMENT_APPROVED;
            $col->save();
			
	        if(!$col->save()){
				throw new \Exception('P3-Could not delete Product ID:'.$id);
			}

			return 1;
	    });
	}

	public function doRejectCollection($id)
	{
		DB::transaction(function() use($id){
            $col = $this->getCollection($id);

            $col->status = PAYMENT_REJECT;
            $col->save();
			
	        if(!$col->save()){
				throw new \Exception('P3-Could not delete Product ID:'.$id);
			}

			return 1;
	    });
	}

	public function doHoldCollection($id)
	{
		DB::transaction(function() use($id){
            $col = $this->getCollection($id);

            $col->status = PAYMENT_HOLD;
            $col->save();
			
	        if(!$col->save()){
				throw new \Exception('P3-Could not delete Product ID:'.$id);
			}

			return 1;
	    });
	}

	public function getCollectionList($data)
	{	
		$result = Payment::whereNull('deleted_at');
		//?recipt_no=&type=&status=&from=&to=

		if($data->recipt_no){
			$result = $result->where('receipt_no','like','%'.$data->recipt_no.'%');	
		}

		if($data->type){
			$result = $result->where('type',$data->type);	
		}

		if($data->status){
			$result = $result->where('status',$data->status);	
		}

		if($data->from && $data->to){
			$result = $result->whereBetween('payment_date',[$data->from,$data->to]);	
		}

		return $result->paginate(20);
	}

}
