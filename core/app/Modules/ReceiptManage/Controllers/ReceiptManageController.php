<?php namespace App\Modules\ReceiptManage\Controllers;


/**
* Controller class
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\ReceiptManage\BusinessLogics\PaymentLogic;
use Illuminate\Http\Request;

class ReceiptManageController extends Controller {


	protected $payment_logic;

	public function __construct(PaymentLogic $payment_logic){
		$this->payment_logic = $payment_logic;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function list(Request $request)
	{
		$collections = $this->payment_logic->getCollectionList($request);
		return view("ReceiptManage::list",compact('collections','request'));
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$collection = $this->payment_logic->getCollectionWithDetails($id);
		return view("ReceiptManage::details",compact('collection'));
	}

	public function doApproveCollection(Request $request)
	{
		try{
			$this->payment_logic->doApproveCollection($request->id);
			return response()->json([
				'status' => 1
			]);
		}catch(\Exception $e){
			return response()->json([
				'status' => 0,
				'message' => $e->getMessage()
			]);
		}
	}

	public function doRejectCollection(Request $request)
	{
		try{
			$this->payment_logic->doRejectCollection($request->id);
			return response()->json([
				'status' => 1
			]);
		}catch(\Exception $e){
			return response()->json([
				'status' => 0,
				'message' => $e->getMessage()
			]);
		}
	}

	public function doHoldCollection(Request $request)
	{
		try{
			$this->payment_logic->doHoldCollection($request->id);
			return response()->json([
				'status' => 1
			]);
		}catch(\Exception $e){
			return response()->json([
				'status' => 0,
				'message' => $e->getMessage()
			]);
		}
	}
}
