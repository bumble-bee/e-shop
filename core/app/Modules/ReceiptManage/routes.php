<?php

Route::group(['middleware' => ['auth']], function()
{	
	Route::group(['prefix'=>'admin','namespace' => 'App\Modules\ReceiptManage\Controllers'],function() {

	    Route::get('payment/recipt/list', [
	      'as' => 'payment.recipt.list', 'uses' => 'ReceiptManageController@list'
	    ]);	

		Route::get('payment/recipt/show/{id}', [
	      'as' => 'payment.recipt.show', 'uses' => 'ReceiptManageController@show'
	    ]);	

	    Route::get('payment/doApproveCollection', [
	      'as' => 'payment.recipt.list', 'uses' => 'ReceiptManageController@doApproveCollection'
	    ]);	

	    Route::get('payment/doRejectCollection', [
	      'as' => 'payment.recipt.list', 'uses' => 'ReceiptManageController@doRejectCollection'
	    ]);	

	    Route::get('payment/doHoldCollection', [
	      'as' => 'payment.recipt.list', 'uses' => 'ReceiptManageController@doHoldCollection'
	    ]);	

	    

	});    
});
