<?php namespace App\Modules\ReceiptManage\Models;

/**
*
* Model
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;


class PaymentCheque extends Model {

	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'payment_cheque';

    /**
     * The attributes that are not assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

}
