<?php namespace App\Modules\ReceiptManage\Models;

/**
*
* Model
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;


class Payment extends Model {

	
	 /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'payment';

    /**
     * The attributes that are not assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public function collectedby(){
        return $this->belongsTo('Core\EmployeeManage\Models\Employee','collected_by','id');
    }

    public function details(){
        return $this->hasMany('App\Modules\ReceiptManage\Models\PaymentDetails');
    }

}
