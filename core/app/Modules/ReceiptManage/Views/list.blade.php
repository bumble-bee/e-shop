@extends('layouts.back_master') @section('title','Admin - MRP Management')
@section('current_title','Product Category Create')



@section('css')
<link rel="stylesheet" type="text/css" src="{{asset('assets/dist/bootstrap-datepicker/css/bootstrap-datepicker.css')}}"/>

<style type="text/css">
  .box-header, .box-body {
    padding: 20px;
  }
  .has-error .help-block, .has-error .control-label{
    color:#e41212;
  }
  .has-error .chosen-container{
    border:1px solid #e41212;
  }
</style>
@stop

@section('content')
<!-- Content-->
<section>
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Receipt<small>Management</small></h1>
    <ol class="breadcrumb">
      	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      	<li>Payment</li>
      	<li>Receipt</li>
      	<li class="active">list</li>
    </ol>
  </section>
  <!-- !!Content Header (Page header) -->

  <!-- Main content -->
  <section class="content">

    <div class="box">
      <div class="box-body">
         <form method="get">
            <div class="row">
              <div class="col-sm-4">
                <label for="" class="control-label">Recipt No</label>
                <input type="text" class="form-control" name="recipt_no" value="{{$request->recipt_no}}">
              </div>
              <div class="col-sm-2">
                <label for="" class="control-label">Type</label>
                <select name="type" id="inputType" class="form-control" >
                  <option value="">-All-</option>
                  <option value="1" @if($request->type==1) selected @endif>Cash</option>
                  <option value="2" @if($request->type==2) selected @endif>Cheque</option>
                  <option value="3" @if($request->type==3) selected @endif>Advance</option>
                </select>
              </div>
              <div class="col-sm-2">
                <label for="" class="control-label">Status</label>
                <select name="status" id="inputType" class="form-control" >
                  <option value="">-All-</option>
                  <option value="0" @if($request->status==0) selected @endif>Pending</option>
                  <option value="1" @if($request->status==1) selected @endif>Approved</option>
                  <option value="2" @if($request->status==2) selected @endif>Hold</option>
                  <option value="-1" @if($request->status==-1) selected @endif>Rejected</option>
                </select>
              </div>
              <div class="col-sm-2">
                <label for="" class="control-label">From</label>
                <input id="from" name="from" type="text" class="form-control datepick" 
                value="@if(isset($request->from)){{$request->from}}@else{{''}}@endif">
              </div>
              <div class="col-sm-2">
                <label for="" class="control-label">To</label>
                <input type="text" class="form-control datepick" name="to" id="to" 
                value="@if(isset($request->to)){{$request->to}}@else{{''}}@endif">
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12 text-right">
                 <div style="margin-top: 21px;">
                   <button type="submit" class="btn btn-default btn-sm" id="plan"><i class="fa fa-search" 
                  style="padding-right: 16px;width: 28px;"></i>Find</button>
                  <a href="list" class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top">
                    <i class="fa fa-refresh" style="padding-right: 16px;width: 20px;"></i>Refresh</a>   
                 </div>           
              </div>
            </div>
          </form>
      </div>
    </div>

    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Receipt List</h3>
      </div>
      <div class="box-body">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th width="5%">#</th>
              <th>Recipt No</th>
              <th>Type</th>
              <th>Amount</th>
              <th>Date</th>
              <th>Status</th>
              <th>Remarks</th>
              <th>Attachment(s)</th>
              <th>Action By</th>
              <th width="12%" class="text-center">Actions</th>
            </tr>
          </thead>
          <tbody>
            @foreach($collections as $key=>$collection)
              <tr>
                  <td>{{$key+1}}</td>
                  <td>{{$collection->receipt_no}}</td>
                  <td>
                    @if($collection->type==1)
                      <span class="label label-default">Cash</span>
                    @elseif($collection->type==2)
                      <span class="label label-default">Cheque</span>
                    @elseif($collection->type==3)
                      <span class="label label-default">Advance</span>
                    @endif
                  </td>
                  <td>Rs.{{number_format($collection->payment_amount, 2, ',', ' ')}}</td>
                  <td>
                    {{date_format(date_create($collection->payment_date),'Y-m-d')}} <br>
                    <small>{{date_format(date_create($collection->payment_date),'H:i:s')}} </small>
                  </td>
                  <td>
                    @if($collection->status==0)
                      <span class="label label-default bg-purple">Pending</span>
                    @elseif($collection->status==1)
                      <span class="label label-default bg-green">Approved</span>
                    @elseif($collection->status==2)
                      <span class="label label-default bg-yellow">Hold</span>
                    @elseif($collection->status==-1)
                      <span class="label label-default bg-red">Rejected</span>
                    @endif
                  </td>
                  <td>{{$collection->remarks}}</td>
                  <td>No</td>
                  <td>-</td>
                  <td class="text-center">
                    <div class="btn-group">                      
                      @if($user->hasAnyAccess(['payment.recipt.reject', 'admin']))
                        <button class="btn btn-default btn-xs btn-reject" data-id="{{$collection->id}}"><i class="fa fa-times"></i></button>
                      @else
                        <button class="btn btn-default btn-xs" disabled><i class="fa fa-times"></i></button>
                      @endif 

                      @if($user->hasAnyAccess(['payment.recipt.approve', 'admin']))
                        <button class="btn btn-default btn-xs btn-approve" data-id="{{$collection->id}}"><i class="fa fa-check"></i></button>
                      @else
                        <button class="btn btn-default btn-xs" disabled><i class="fa fa-check"></i></button>
                      @endif

                      @if($user->hasAnyAccess(['payment.recipt.hold', 'admin']))
                        <button class="btn btn-default btn-xs btn-hold" data-id="{{$collection->id}}"><i class="fa fa-pause"></i></button>
                      @else
                        <button class="btn btn-default btn-xs" disabled><i class="fa fa-pause"></i></button>
                      @endif                          

                      <a class="btn btn-default btn-xs" href="{{url('admin/payment/recipt/show')}}/{{$collection->id}}"><i class="fa fa-eye"></i></a>
                 
                    </div>
                  </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>

  </section>
  <!-- !!Main content -->

</section>
<!-- !!!Content -->

@stop


@section('js')  
<script src="{{asset('assets/dist/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
  <!-- CORE JS -->
	<script type="text/javascript">
		$(document).ready(function() {

      $('.datepick').datepicker({
          keyboardNavigation: false,
          forceParse: false,
          format: 'yyyy-mm-dd'
      });

      $('.btn-reject').click(function(argument) {
        var id = $(this).data('id');
        swal({
          title: "Are you sure?",
          text: "Are you sure you wanna to reject this collection?",
          type: "warning",
          inputPlaceholder: "Reject Remarks",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes, reject it!",
          showLoaderOnConfirm: true,
          closeOnConfirm: true
        },function(inputValue){

          $.ajax({
            url: "{{url('admin/payment/doRejectCollection')}}",
            method: 'get',
            cache: false,
            data: {'id':id,'remark':inputValue},
            success: function(response){
              if(response.status == 1){
                swal("Done!", "Collection has been rejected!.", "success");
                location.reload();
              }else{
                swal("Error!", response.message, "error");
              }
            },
            error: function(xhr){
              console.log(xhr);
              swal("Error!", 'Error occurred. Please try again', "error");
            } 
          });
          //window.open(_url, '_self');
        });
      });

      $('.btn-approve').click(function(argument) {
        var id = $(this).data('id');
        swal({
          title: "Are you sure?",
          text: "Are you sure you wanna to approve this collection?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes, approve it!",
          closeOnConfirm: false
        },function(){
          $.ajax({
            url: "{{url('admin/payment/doApproveCollection')}}",
            method: 'get',
            cache: false,
            data:  {'id':id},
            success: function(response){
              if(response.status == 1){
                swal("Done!", "Collection has been Approved!.", "success");
                location.reload();
              }else{
                swal("Error!", response.message, "error");
              }
            },
            error: function(xhr){
              console.log(xhr);
              swal("Error!", 'Error occurred. Please try again', "error");
            } 
          });
          //window.open(_url, '_self');
        });
      });

      $('.btn-hold').click(function(argument) {
        var id = $(this).data('id');
        swal({
          title: "Are you sure?",
          text: "Are you sure you wanna to hold this collection?",
          type: "warning",
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes, hold it!",
          showCancelButton: true,
          closeOnConfirm: false
        },function(){
          $.ajax({
            url: "{{url('admin/payment/doHoldCollection')}}",
            method: 'get',
            cache: false,
             data: {'id':id,'remark':''},
            success: function(response){
              if(response.status == 1){
                swal("Done!", "Collection has been hold!.", "success");
                location.reload();
              }else{
                swal("Error!", response.message, "error");
              }
            },
            error: function(xhr){
              console.log(xhr);
              swal("Error!", 'Error occurred. Please try again', "error");
            } 
          });
          //window.open(_url, '_self');
        });
      });
    });
	</script>
  <!-- //CORE JS -->
@stop
