@extends('layouts.back_master') @section('title','Admin - MRP Management')
@section('current_title','Product Category Create')



@section('css')

<style type="text/css">
  .box-header, .box-body {
    padding: 20px;
  }
  .has-error .help-block, .has-error .control-label{
    color:#e41212;
  }
  .has-error .chosen-container{
    border:1px solid #e41212;
  }
</style>
@stop

@section('content')
<!-- Content-->
<section>
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Receipt<small>Management</small></h1>
    <ol class="breadcrumb">
      	<li></li>
      	<li>Payment</li>
      	<li><a href="{{url('amdin/payment/recipt/list')}}"><i class="fa fa-dashboard"></i> Receipt list</a></li>
      	<li class="active">details</li>
    </ol>
  </section>
  <!-- !!Content Header (Page header) -->

  
<!-- Main content -->
<section class="content">
  <!-- Default box -->
  <div class="box">
    <div class="box-body">
      @if(isset($collection) && !empty($collection))
      <div class="row">
        <div class="col-md-12">
          <strong>
            <h3 class="text-center">
              COLLECTION RECEIPT
            </h3>
          </strong>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <h3 style="margin-top: 2px;margin-bottom: 0px;">{{$collection->receipt_no}}</h3>
          <h4 style="margin-bottom: 0;margin-top: 8px;">
            @if($collection->type==1)
              Cash Collection</span>
            @elseif($collection->type==2)
              Cheque Collection
            @elseif($collection->type==3)
              Advance Payment
            @endif
          </h4>
        </div>
        <div class="col-md-6">
          <h4 class="text-right" style="margin-bottom: 0">
            {{($collection->payment_date)?date('jS M, Y',strtotime($collection->payment_date)):'-' }}
          </h4>
          <h5 class="text-right" style="margin-top:3px;margin-bottom: 0;">
            {{$collection->collectedby?$collection->collectedby->first_name.' '.$collection->collectedby->last_name:'-'}}
          </h5>
          <h5 class="text-right" style="margin-top: 8px;">
            @if(PAYMENT_PENDING == $collection->status)
              <span class="text-warning">Pending</span>
            @elseif(PAYMENT_APPROVED == $collection->status)
              <span class="text-success">Approved</span>
            @elseif(PAYMENT_HOLD == $collection->status)
              <span class="text-danger">Hold</span>
            @elseif(PAYMENT_REJECT == $collection->status)
              <span class="text-primary">Rejected</span>
            @else
              -
            @endif
          </h5>
          @if($collection->status != PAYMENT_APPROVED)
          <div class="text-right">
            <a href="" target="_blank">Show PDF</a>
          </div>
          @endif
        </div>
      </div>
    @endif
    <br>


    <table class="table table-bordered">
        <thead>
          <tr>
            <th class="text-center">#</th>
            <th class="text-center" width="15%">Invoice No</th>
            <th class="text-center">Amount (Rs)</th>
            <th class="text-center">Customer</th>
            <th class="text-center">Invoice Date</th>
            <th class="text-center">Collected Amount (Rs)</th>
          </tr>
        </thead>
        <tbody>
          @if(count($collection->details) > 0)
            @foreach($collection->details as $key => $detail)
              <tr>
                <td>{{ (++$key) }}</td>
                <td>{{ $detail->invoice->invoice_no?:'-' }}</td>
                <td align="right">{{ $detail->invoice->total_amount}}</td>
                <td align="center">{{ $detail->invoice->customer->first_name}}</td>
                <td align="center">{{ $detail->invoice->invoice_date}}</td>
                <td align="right">{{ number_format($detail->amount, 2)?:'-' }}</td>
              </tr>
            @endforeach
          @else
            <tr>
              <td colspan="9" align="center"> - No Data to Display - </td>
            </tr>
          @endif  
        </tbody>
      </table>
      
    </div><!-- /.box-body -->
    <div class="overlay" style="display: none;">
      <i class="fa fa-spin fa-refresh"></i>
    </div>
  </div><!-- /.box -->
</section><!-- /.content -->

</section>
<!-- !!!Content -->

@stop


@section('js')  
  <!-- CORE JS -->
	<script type="text/javascript">
		$(document).ready(function() {

    });
	</script>
  <!-- //CORE JS -->
@stop
