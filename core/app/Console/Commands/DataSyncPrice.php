<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Modules\ERPDataManage\Controllers\ERPDataManageController;
use Illuminate\Http\Request;

class DataSyncPrice extends Command
{
     /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:price';

    protected $erpData;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'turncate entire permisson table CAUTION';

    /**
     * Create a new command instance.
     */
    public function __construct(ERPDataManageController $erpData)
    {
         parent::__construct();
         $this->erpData = $erpData;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('price sync starting....');
        $request = new Request;
        $request->type = 3;
        $request->is_terminal = true;
        //$this->info($request->type);
        $data = $this->erpData->create($request);
        $this->info($data);
        $this->info('price sync finished');
    }
}
