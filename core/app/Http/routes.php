<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/**
 * USER AUTHENTICATION MIDDLEWARE
 */
Route::group(['middleware' => ['auth']], function()
{
    Route::get('/admin', [
      'as' => 'index', 'uses' => 'WelcomeController@admin'
    ]);
    
    Route::get('/', [
      'as' => 'index', 'uses' => 'WelcomeController@admin'
    ]);

    Route::get('/admin/test', [
      'as' => 'index', 'uses' => 'WelcomeController@adminTest'
    ]);

    Route::get('/admin/month-analizer', [
      'as' => 'index', 'uses' => 'WelcomeController@monthAnalize'
    ]);

    Route::get('/admin/dashboard2', [
      'as' => 'index', 'uses' => 'WelcomeController@adminDashboard2'
    ]);

    Route::get('/admin/dashboard2/getSalesCap', [
      'as' => 'index', 'uses' => 'WelcomeController@getSalesCap'
    ]);

    Route::get('/admin/dashboard3/getLeadsData', [
      'as' => 'index', 'uses' => 'WelcomeController@getLeadsData'
    ]);

    Route::get('/admin/sales-recap', [
      'as' => 'index', 'uses' => 'WelcomeController@adminSalesRecap'
    ]);
});

Route::get('privacy-policy', [
  'as' => 'index', 'uses' => 'WelcomeController@privacy'
]);


Route::get('user/login', [
  'as' => 'user.login', 'uses' => 'AuthController@loginView'
]);

Route::post('user/login', [
  'as' => 'user.login', 'uses' => 'AuthController@login'
]);

Route::get('user/logout', [
  'as' => 'user.logout', 'uses' => 'AuthController@logout'
]);

Route::get('test', [
  'as' => 'index', 'uses' => 'WelcomeController@test'
]);





Route::group(['prefix' => 'v1/api'], function () {
    /**
     * GET Routes
     */
    Route::any('checkUserAuthorize', 'ApiController@checkUserAuthorize');

    Route::any('inquiryList', 'ApiController@getInquiryList');

    Route::any('getSectors', 'ApiController@getSectors');

    Route::any('getSources', 'ApiController@getSources');

    Route::any('getMasterData', 'ApiController@getMasterData');

    Route::any('saveInquiry', 'ApiController@saveInquiry');
    
    Route::any('saveInquiryIos', 'ApiController@saveInquiryIos');

    Route::any('getInquiryDetails', 'ApiController@getInquiryDetails');

    Route::any('getInquiryTimeline', 'ApiController@getInquiryTimeline');

    Route::any('saveAttachments', 'ApiController@saveAttachments');

    Route::any('userSignup', 'ApiController@userSignup');


});

Route::group(['prefix' => 'v1/ios/api'], function () {
    /**
     * GET Routes
     */
    Route::any('checkUserAuthorize', 'IOSApiController@checkUserAuthorize');

    Route::any('inquiryList', 'IOSApiController@getInquiryList');

    Route::any('getSectors', 'IOSApiController@getSectors');

    Route::any('getSources', 'IOSApiController@getSources');

    Route::any('getMasterData', 'IOSApiController@getMasterData');

    Route::any('saveInquiry', 'IOSApiController@saveInquiry');
    
    Route::any('getInquiryDetails', 'IOSApiController@getInquiryDetails');

    Route::any('getInquiryTimeline', 'IOSApiController@getInquiryTimeline');

    Route::any('userSignup', 'IOSApiController@userSignup');


});

Route::group(['prefix' => 'v1/scol/api'], function () {
  
    /**
     * GET Routes
     */
    Route::any('checkUserAuthorize', 'SColApiController@checkUserAuthorize');

    Route::any('getCustomers', 'SColApiController@getCustomers');
    
    Route::any('getInvoices', 'SColApiController@getInvoices');

    Route::any('getBanks', 'SColApiController@getBanks');

    Route::any('getQuotations', 'SColApiController@getQuotations');
    
    Route::any('getRecipts', 'SColApiController@getRecipts');

    Route::any('savePayment', 'SColApiController@savePayment');


});

Route::group(['prefix' => 'v1/sales/api'], function () {
    /**
     * GET Routes
     */

    Route::any('getSources', 'ApiSalesAppController@getSources');

    Route::any('getCategories', 'ApiSalesAppController@getCategories');

    Route::any('getInquiries', 'ApiSalesAppController@getInquiry');

    Route::any('getProducts', 'ApiSalesAppController@getProducts');

    Route::any('getSector', 'ApiSalesAppController@getSector');

    Route::any('getProductDetails', 'ApiSalesAppController@getProductDetails');

    Route::any('addQuotation', 'ApiSalesAppController@addQuotation');

    Route::any('getFollowupTypes', 'ApiSalesAppController@getFollowupTypes');

    Route::any('getVat', 'ApiSalesAppController@getVat');

    Route::any('getVatType', 'ApiSalesAppController@getVatType');

    Route::any('checkUserAuthorize', 'ApiSalesAppController@checkUserAuthorize');

    Route::any('getCustomers', 'ApiSalesAppController@getCustomers');

    Route::any('sendFollowUp', 'ApiSalesAppController@sendFollowUp');

    Route::any('addCustomer', 'ApiSalesAppController@addCustomer');

    Route::any('getFollowUps', 'ApiSalesAppController@getFollowUps');

    Route::any('getQuotationPdf', 'ApiSalesAppController@getQuotationPdf');

    Route::any('getQuotations', 'ApiSalesAppController@getQuotations');

    Route::any('uploadPo', 'ApiSalesAppController@uploadPo');

    Route::any('sendmail', 'ApiSalesAppController@sendQuotationMail');

    Route::any('updateFollowUp', 'ApiSalesAppController@updateFollowUp');

    Route::any('getDeliveryTerms', 'ApiSalesAppController@getDeliveryTerms');
    
    Route::any('addReviseQuotation', 'ApiSalesAppController@addReviseQuotation');

    Route::any('checkVer', 'ApiSalesAppController@checkVersion');

});
