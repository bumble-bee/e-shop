<?php namespace App\Http\Logic;


use Core\MenuManage\Models\Menu;
use Core\UserManage\Models\User;
use Core\UserRoles\Models\UserRole;
use Core\Sector\Models\Sector;
use Core\EmployeeManage\Models\Employee;
use Core\EmployeeManage\Models\EmployeeType;

use App\Models\Inquiry;
use App\Models\InquiryTransaction;
use App\Models\Quotation;
use App\Models\Invoice;
use App\Models\Milestone;
use App\Models\MilestoneDetail;
use App\Modules\AdminQuotationManage\Models\CustomerPo;

use App\Models\TargetSales;
use App\Models\TargetCollection;

use App\Classes\Functions;
use App\Events\NotificationEvent;

use DB;
use Sentinel;
use DateTime;
use DateTimeZone;

class Dashboard {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */

	public static function getInquiries($dates){

		$inquiry = Inquiry::whereBetween('created_at',$dates)->get();

		$cc = 0;

		foreach ($inquiry->pluck('project_value') as $value) {
			$cc += intval(filter_var($value, FILTER_SANITIZE_NUMBER_INT));
		}

		$cc = self::number_format_short($cc);

		$tmp_code['inquiries'] = count($inquiry);		

		$tmp_code['amount'] = $cc;

		return $tmp_code;

	}

	public static function getQuotes($dates){

		$quotes = Quotation::whereNotIn('inquiry_id',[0])->whereBetween('quotation_date',$dates)->get();

		$cc = 0;

		foreach ($quotes->pluck('total_amount') as $value) {
			$cc += $value;
		}

		$cc = self::number_format_short($cc);

		$tmp_code['quotes'] = count($quotes);		

		$tmp_code['amount'] = $cc;		

		return $tmp_code;

	}

	public static function getDirectQuotes($dates){

		$quotes = Quotation::where('inquiry_id',0)->whereBetween('quotation_date',$dates)->get();

		$cc = 0;

		foreach ($quotes->pluck('total_amount') as $value) {
			$cc += $value;
		}

		$cc = self::number_format_short($cc);

		$tmp_code['quotes'] = count($quotes);		

		$tmp_code['amount'] = $cc;		

		return $tmp_code;
	}

	public static function getInvoices($dates){

		$invoices = Invoice::where('status',0)->whereBetween('invoice_date',$dates)->get();

		$cc = 0;

		foreach ($invoices->pluck('total_amount') as $value) {
			$cc += $value;
		}

		$cc = self::number_format_short($cc);

		$tmp_code['invoices'] = count($invoices);

		$tmp_code['amount'] = $cc;		

		return $tmp_code;
	}

	public static function getDeliveredInvoices(){

		$invoices = Invoice::where('status',1)->get();

		$cc = 0;

		foreach ($invoices->pluck('total_amount') as $value) {
			$cc += $value;
		}

		$cc = self::number_format_short($cc);

		$tmp_code['invoices'] = count($invoices);

		$tmp_code['amount'] = $cc;		

		return $tmp_code;
	}

	public static function getCollectionDetails(){

		// $invoices = Invoice::where('status',1)->get();

		$cc = 0;

		// foreach ($invoices->pluck('total_amount') as $value) {
		// 	$cc += $value;
		// }

		$cc = self::number_format_short($cc);

		$tmp_code['pending_collection'] = $cc;

		$tmp_code['total_collection'] = $cc;		

		return $tmp_code;
	}

	public static function performChartSectorOverroll($dates)
	{

		$sectors 		= 	Sector::whereNull('deleted_at')->get();

		$tmp_sec 		= $sectors->pluck('id');
		$tmp_sec_name 	= $sectors->pluck('name');

		$tmp_sec_name[] = 'Direct';

		$data_perform=[];


		$data_perform=[];

		$tmp_lead=[];
		$tmp_quote=[];
		$tmp_invoice=[];

		foreach ($sectors as $value) {

			$tmp_l 	= Inquiry::where('sector',$value->id)
							->whereBetween('created_at',$dates)
							->get()->count();

			$tmp_lead[]=$tmp_l;

			$tmp_q 	= 	Quotation::leftJoin('inquiries','quotation.inquiry_id','=','inquiries.id')
							->whereNotIn('inquiry_id',[0])
							->where('inquiries.sector',$value->id)
							->whereBetween('quotation_date',$dates)
							->groupBy('inquiry_id')
							->get()
							->count();

			$tmp_quote[]=$tmp_q;

			$tmp_i 	= 	Invoice::leftJoin('quotation',function($qry1){
							$qry1->on('invoice.quotation_no','=','quotation.reference_no');
						})
						->leftJoin('inquiries','quotation.inquiry_id','=','inquiries.id')
						->whereNotIn('inquiry_id',[0])
						->where('inquiries.sector',$value->id)
						->whereBetween('invoice_date',$dates)
						->groupBy('inquiry_id')
						->get()
						->count();

			$tmp_invoice[]=$tmp_i;
			
		}

		$tmp_lead[]=0;

		$tmp_quote[] = Quotation::where('inquiry_id',0)->whereBetween('quotation_date',$dates)->get()->count();

		$tmp_invoice[] = Invoice::leftJoin('quotation',function($qry1){
							$qry1->on('invoice.quotation_no','=','quotation.reference_no');
						})
						->where('inquiry_id',0)
						->whereBetween('invoice_date',$dates)
						->get()
						->count();


		$dd=[];

		$dd['name']='Leads';
		$dd['data']=$tmp_lead;

		array_push($data_perform, $dd);

		$dd['name']='Quotation';
		$dd['data']=$tmp_quote;

		array_push($data_perform, $dd);

		$dd['name']='Invoice';
		$dd['data']=$tmp_invoice;

		array_push($data_perform, $dd);

		return ['categories'=>$tmp_sec_name,'series'=>$data_perform];
	}

	public static function performChartSectorOverrollAmountWise($dates)
	{

		$sectors 		= Sector::whereNull('deleted_at')->get();

		$tmp_sec 		= $sectors->pluck('id');
		$tmp_sec_name 	= $sectors->pluck('name');

		$tmp_sec_name[] = 'Direct';

		$data_perform=[];


		$data_perform=[];

		$tmp_lead=[];
		$tmp_quote=[];
		$tmp_invoice=[];

		foreach ($sectors as $value) {

			$tmp_l 	= Inquiry::where('sector',$value->id)
							->whereBetween('created_at',$dates)
							->select('project_value')->get()->pluck('project_value');			
			$lk_leads = 0;
			foreach ($tmp_l as $project_value) {
				$lk_leads += intval(filter_var($project_value, FILTER_SANITIZE_NUMBER_INT));
			}

			$tmp_lead[] = $lk_leads;

			$tmp_q 	= 	Quotation::leftJoin('inquiries','quotation.inquiry_id','=','inquiries.id')
							->whereNotIn('inquiry_id',[0])
							->where('inquiries.sector',$value->id)
							->whereBetween('quotation_date',$dates)
							->groupBy('inquiry_id')
							->select('total_amount')
							->get()
							->pluck('total_amount');

			$lk_quotes = 0;
			
			foreach ($tmp_q as $quote_value) {
				$lk_quotes += floatval($quote_value);
			}

			$tmp_quote[]=$lk_quotes;

			$tmp_i 	= 	Invoice::leftJoin('quotation',function($qry1){
							$qry1->on('invoice.quotation_no','=','quotation.reference_no');
						})
						->leftJoin('inquiries','quotation.inquiry_id','=','inquiries.id')
						->whereNotIn('inquiry_id',[0])
						->where('inquiries.sector',$value->id)
						->whereBetween('invoice_date',$dates)
						->groupBy('inquiry_id')
						->select('invoice.total_amount')
						->get()
						->pluck('total_amount');

			$lk_invoices = 0;
			
			foreach ($tmp_i as $invoice_value) {
				$lk_invoices += floatval($invoice_value);
			}

			$tmp_invoice[]=$lk_invoices;
			
		}

		$tmp_lead[]=0;

		$tt_q = Quotation::where('inquiry_id',0)
					->whereBetween('quotation_date',$dates)
					->select('total_amount')
					->get()
					->pluck('total_amount');
		
		$drt_q = 0;

		foreach ($tt_q as $dir_quote_value) {
			$drt_q += floatval($dir_quote_value);
		}

		$tmp_quote[] = $drt_q;

		$tt_i = Invoice::leftJoin('quotation',function($qry1){
							$qry1->on('invoice.quotation_no','=','quotation.reference_no');
						})
						->where('inquiry_id',0)
						->whereBetween('invoice_date',$dates)
						->select('invoice.total_amount')
						->get()
						->pluck('total_amount');

		$drt_i = 0;

		foreach ($tt_i as $dir_invoice_value) {
			$drt_i += floatval($dir_invoice_value);
		}

		$tmp_invoice[] = $drt_i;

		$dd=[];

		$dd['name']='Leads';
		$dd['data']=$tmp_lead;

		array_push($data_perform, $dd);

		$dd['name']='Quotation';
		$dd['data']=$tmp_quote;

		array_push($data_perform, $dd);

		$dd['name']='Invoice';
		$dd['data']=$tmp_invoice;

		array_push($data_perform, $dd);

		return ['categories'=>$tmp_sec_name,'series'=>$data_perform];
	}

	public static function performChart($dates)
	{

		$sector_heads 	= 	Employee::where('employee_type_id',8)->lists('first_name','id');
		$sectors 		= 	Sector::whereNull('deleted_at')->get();

		$tmp_sec 		= $sectors->pluck('id');
		$tmp_sec_name 	= $sectors->pluck('name');

		$data_perform=[];

		foreach ($sector_heads as $key=>$value) {

			$tmp 	= Inquiry::where('created_by',$key)
						->whereBetween('inquiries.created_at',$dates)
						->leftJoin('sectors', 'inquiries.sector', '=', 'sectors.id')
						->select('sectors.id', DB::raw('count(*) as total'))
						->groupBy('sectors.id')
						->get();

			$dd=[];
			$dd['name']=$value;
			$dd['data']=[];
			
			foreach ($sectors as $sector) {
				foreach ($tmp as $key=>$detail) {
					if($detail->id==$sector->id){
						$dd['data'][$detail->id]=$detail->total;
					}
				}

				if(!array_key_exists($sector->id, $dd['data'])){
					$dd['data'][$sector->id]=0;
				}
			}

			$dd['data']=array_values($dd['data']);

			array_push($data_perform, $dd);
		}	

        return ['categories'=>$tmp_sec_name,'series'=>$data_perform];
	}

	public static function sectorPerformance($dates)
	{

		$data_perform = Sector::whereNull('sectors.deleted_at')
						->leftJoin('inquiries','sectors.id','=','inquiries.sector')
						->whereBetween('inquiries.created_at',$dates)
						->groupBy('sectors.id')
						->select('sectors.name',DB::raw('count(inquiries.id) as y'))
						->orderBy('y','desc')
						->get();

        return $data_perform;
	}

	public static function salesRecap($dates){
		
		$year 	= date('Y');
		$month 	= [];

		$sales_traget 			= [];
		$sales 					= [];
		$collection 			= [];
		$collection_target 		= [];

		$series 				= [];

		for ($i=1; $i <= 12; $i++) {
			$pre = "0";

			if($i<=9){
				$pre = $pre.$i;
			}else{
				$pre = $i;
			}

			$tmp  = $year."-".$pre;

			$tmp_utc  	= intval($dates[$i-1]);

			///////////////////////////////////Sales Details/////////////////////////////////

			$invoices 	= Invoice::where('invoice_date','like','%'.$tmp)->sum('total_amount');

			$dd_sales 	= [];

			$dd_sales[] = $tmp_utc;
			$dd_sales[] = ($invoices=="")?0:$invoices;

			$sales[] 	= $dd_sales;

			/////////////////////////////////////////////////////////////////////////////////

			////////////////////////////////Sales Target Details/////////////////////////////

			$trgt_sale 			= TargetSales::where('month','=',$tmp)->first();

			$dd_sales_trgt 		= [];

			$dd_sales_trgt[] 	= $tmp_utc;
			$dd_sales_trgt[] 	= ($trgt_sale)?intval($trgt_sale['target_value']):0;

			$sales_traget[] 	= $dd_sales_trgt;

			/////////////////////////////////////////////////////////////////////////////////

			////////////////////////////////Collection Details/////////////////////////////

			$invoice_collection	= [];

			$dd_collection 		= [];

			$dd_collection[] 	= $tmp_utc;
			$dd_collection[] 	= ($invoice_collection)?intval($invoice_collection):0;

			$collection[] 		= $dd_collection;

			/////////////////////////////////////////////////////////////////////////////////

			////////////////////////////////Collection Target Details/////////////////////////////

			$trgt_collection 		= TargetCollection::where('month','=',$tmp)->first();

			$dd_collection_trgt 	= [];

			$dd_collection_trgt[] 	= $tmp_utc;
			$dd_collection_trgt[] 	= ($trgt_collection)?intval($trgt_collection['target_value']):0;

			$collection_target[] 	= $dd_collection_trgt;

			/////////////////////////////////////////////////////////////////////////////////

		}

		array_push($series, ['name'=>'Sales Target','data'=>$sales_traget]);
		array_push($series, ['name'=>'Sales','data'=>$sales]);
		array_push($series, ['name'=>'Collection','data'=>$collection]);
		array_push($series, ['name'=>'Collection Target','data'=>$collection_target]);

		return $series;
	}

	public static function approvedLeads(){
		
		$result = 	Inquiry::leftJoin('inquiry_transaction',function($query){
						$query->on('inquiries.id','=','inquiry_transaction.inquiry_id')
							->where('event_id','=',INQUIRY_WEB_CREATE)
							->orWhere('event_id','=',INQUIRY_MOBILE_CREATE)
							->whereNUll('inquiry_transaction.deleted_at');
					})
					->where('status_id','=',0)
					->orderBy('inquiry_transaction.created_at','desc')
					->limit(6)
					->with('sectors','leadOwner')
					->select('inquiries.*')
					->get();

		return $result;
	}

	public static function toDoList(){

		$data = [];

		function dothemath($val,$tot){
			return number_format(($val/$tot)*100);
		}

		$tot_inq=Inquiry::all()->count();

		$count_inq_approve_pending = Inquiry::leftJoin('inquiry_transaction',function($query){
						$query->on('inquiries.id','=','inquiry_transaction.inquiry_id')
							->where('event_id','=',INQUIRY_WEB_CREATE)
							->orWhere('event_id','=',INQUIRY_MOBILE_CREATE)
							->whereNUll('inquiry_transaction.deleted_at');
					})
					->where('status_id','=',0)
					->orderBy('inquiry_transaction.created_at','desc')
					->get()->count();

		$data['approve_pending_inquiry']=[$count_inq_approve_pending,dothemath($count_inq_approve_pending,$tot_inq).'%'];

		$count_unassigned = Inquiry::rightJoin('inquiry_transaction',function($query){
						$query->on('inquiries.id','=','inquiry_transaction.inquiry_id')
							->where('event_id','=',INQUIRY_APPROVED)
							->whereNUll('inquiry_transaction.deleted_at');
					})
					->where('status_id','=',1)
					->orderBy('inquiry_transaction.created_at','desc')
					->get()->count();

		$data['unassigned_inquiry']=[$count_unassigned,dothemath($count_unassigned,$tot_inq).'%'];

		$tot_quote = Quotation::all()->count();

		$count_quote_pending = Quotation::where('status',0)->get()->count();

		$data['approve_pending_quote']=[$count_quote_pending,dothemath($count_quote_pending,$tot_quote).'%'];

		return $data;		
		
	}

	public static function approvalQuotes(){
		
		$result = Quotation::where('status',0)->limit(6)->with(['customer','details','createdBy'])->get();

		$data=[];

		foreach ($result as $key => $value) {
			$dd=[];

			$dd['id']=$value->id;
			$dd['quote_no']=$value->reference_no;
			$dd['created_by']=$value['createdBy']->first_name." ".$value['createdBy']->last_name;
			$dd['customer']=$value['customer']->first_name;
			$dd['vat_type']=$value->vat_type;
			$dd['value']=$value->total_amount;
			$dd['items']=count($value['details']);
			$dd['date']=$value->quotation_date;

			array_push($data, $dd);
		}

		return $data;
	}

	public static function milestone(){
		$ms = Milestone::whereNotIn('id',[7,8])->get();

		foreach ($ms as $value) {
			$dd = MilestoneDetail::where('milestone_id',$value->id)->avg('lead_time');
			if($dd){
				$value->value = number_format($dd,0);
			}else{
				$value->value = 0;
			}
		}

		return $ms;
	}

	public static function temp(){

		$tp = [];

		if(MilestoneDetail::all()->count()>0){
			return "not empty";
		}else{

			$allocated = [];

			$tmp1 = Inquiry::with('transactionInfo.eventInfo')->get();

			foreach ($tmp1 as $value) {

				if(count($value['transactionInfo'])>1){
					
					$tt = $value['transactionInfo'][1]->created_at->diff($value['transactionInfo'][0]->created_at)->format("%a");

					array_push($allocated, $tt);

				}

			}

			$tp['inquiry_generation'] = round(array_sum($allocated)/count($allocated),2);

			return $tmp2 = Inquiry::leftJoin('inquiry_transaction',function($query1){
					$query1->on('inquiry_transaction.inquiry_id','=','inquiries.id')
						->whereIn('event_id',[1,5,6]);
			})->get();
		}

		return $tp;
	}

	public static function TotalLeadValue($project_values_array){

		$cc = 0;

		foreach ($project_values_array as $value) {
			$cc += intval(str_replace(',', '', $value));
		}

		return self::number_format_short($cc);

	}

	public static function number_format_short( $n, $precision = 1 ) {
		if ($n < 900) {
			// 0 - 900
			$n_format = number_format($n, $precision);
			$suffix = '';
		} else if ($n < 900000) {
			// 0.9k-850k
			$n_format = number_format($n / 1000, $precision);
			$suffix = 'K';
		} else if ($n < 900000000) {
			// 0.9m-850m
			$n_format = number_format($n / 1000000, $precision);
			$suffix = 'M';
		} else if ($n < 900000000000) {
			// 0.9b-850b
			$n_format = number_format($n / 1000000000, $precision);
			$suffix = 'B';
		} else {
			// 0.9t+
			$n_format = number_format($n / 1000000000000, $precision);
			$suffix = 'T';
		}
	  // Remove unecessary zeroes after decimal. "1.0" -> "1"; "1.00" -> "1"
	  // Intentionally does not affect partials, eg "1.50" -> "1.50"
		if ( $precision > 0 ) {
			$dotzero = '.' . str_repeat( '0', $precision );
			$n_format = str_replace( $dotzero, '', $n_format );
		}
		return $n_format . $suffix;
	}

	public static function leadsGenerationSummery($args){
		
		$from 	= $args->get('from');
		$to 	= $args->get('to');

		$empTypes 	= 	EmployeeType::all()->lists('name','id');

		$mainData 	= 	[];
		$subData 	= 	[];

		foreach ($empTypes as $key => $value) {
			$tt = Employee::where('employee_type_id',$key)->select(DB::raw('CONCAT(first_name," ",last_name) as name'),'id')->get();

			$tt1 = array_pluck($tt,'id');

			$md = [];

			if(count($tt1)>0){

				$tmp = Inquiry::whereIn('created_by',$tt1);

				if(($from=="" && $to=="") && ($from=="" || $to!="") && ($from!="" || $to=="") || ($from==NULL || $to==NULL)){
					$monthFirst = date('Y-m')."-01";
					$today = date('Y-m-d');
					$tmp = $tmp->whereBetween('created_at',[$monthFirst,$today])->count();
				}elseif($from!="" && $to!=""){
					$tmp = $tmp->whereBetween('created_at',[$from,$to])->count();
				}
				
				if($tmp>0){
					$md['name'] 		= $value;
					$md['y'] 			= $tmp;
					$md['drilldown'] 	= $value;

					$mainData[] = $md;

					$dd['name'] 	= $value;
					$dd['id'] 		= $value;
					$dd['data'] 	= [];

					foreach ($tt as $value) {
						if($from!="" && $to!=""){
							$tmp1 = Inquiry::where('created_by',$value['id'])->whereBetween('created_at',[$from,$to])->count();
							$dd['data'][] = [$value['name'],$tmp1];
						}else{
							$monthFirst = date('Y-m')."-01";
							$today = date('Y-m-d');
							$tmp1 = Inquiry::where('created_by',$value['id'])->whereBetween('created_at',[$monthFirst,$today])->count();
							$dd['data'][] = [$value['name'],$tmp1];
						}
					}

					array_push($subData, $dd);
				}
			}
		}

		return ['main'=>$mainData,'sub'=>$subData];

	}

	public static function quotesGenerationSummery($args){
		
		$from 	= $args->get('from');
		$to 	= $args->get('to');

		$empTypes 	= 	EmployeeType::all()->lists('name','id');

		$mainData 	= 	[];
		$subData 	= 	[];

		foreach ($empTypes as $key => $value) {
			$tt = Employee::where('employee_type_id',$key)->select(DB::raw('CONCAT(first_name," ",last_name) as name'),'id')->get();

			$tt1 = array_pluck($tt,'id');

			$md = [];

			if(count($tt1)>0){

				$tmp = Quotation::whereIn('action_by',$tt1);

				if(($from=="" && $to=="") && ($from=="" || $to!="") && ($from!="" || $to=="") || ($from==NULL || $to==NULL)){
					$monthFirst = date('Y-m')."-01";
					$today = date('Y-m-d');
					$tmp = $tmp->whereBetween('quotation_date',[$monthFirst,$today])->count();
				}elseif($from!="" && $to!=""){
					$tmp = $tmp->whereBetween('quotation_date',[$from,$to])->count();
				}

				if($tmp>0){
					$md['name'] 		= $value;
					$md['y'] 			= $tmp;
					$md['drilldown'] 	= $value;

					$mainData[] = $md;

					$dd['name'] 	= $value;
					$dd['id'] 		= $value;
					$dd['data'] 	= [];

					foreach ($tt as $value) {

						if($from!="" && $to!=""){
							$tmp1 = quotation::where('action_by',$value['id'])->whereBetween('quotation_date',[$from,$to])->count();
							$dd['data'][] = [$value['name'],$tmp1];
						}else{
							$monthFirst = date('Y-m')."-01";
							$today = date('Y-m-d');
							$tmp1 = quotation::where('action_by',$value['id'])->whereBetween('quotation_date',[$monthFirst,$today])->count();
							$dd['data'][] = [$value['name'],$tmp1];
						}
					}

					array_push($subData, $dd);
				}
			}
		}

		return ['main'=>$mainData,'sub'=>$subData];

	}

	public static function poGenerationSummery($args){
		
		$from 	= $args->get('from');
		$to 	= $args->get('to');

		$empTypes 	= 	EmployeeType::all()->lists('name','id');

		$mainData 	= 	[];
		$subData 	= 	[];

		foreach ($empTypes as $key => $value) {
			$tt = Employee::where('employee_type_id',$key)->select(DB::raw('CONCAT(first_name," ",last_name) as name'),'id')->get();

			$tt1 = array_pluck($tt,'id');

			$md = [];

			if(count($tt1)>0){

				$tmp = CustomerPo::whereIn('employee_id',$tt1);

				if(($from=="" && $to=="") && ($from=="" || $to!="") && ($from!="" || $to=="") || ($from==NULL || $to==NULL)){
					$monthFirst = date('Y-m')."-01";
					$today = date('Y-m-d');
					$tmp = $tmp->whereBetween('created_at',[$monthFirst,$today])->count();
				}elseif($from!="" && $to!=""){
					$tmp = $tmp->whereBetween('created_at',[$from,$to])->count();
				}
				
				if($tmp>0){
					$md['name'] 		= $value;
					$md['y'] 			= $tmp;
					$md['drilldown'] 	= $value;

					$mainData[] = $md;

					$dd['name'] 	= $value;
					$dd['id'] 		= $value;
					$dd['data'] 	= [];

					foreach ($tt as $value) {

						if($from!="" && $to!=""){
							$tmp1 = CustomerPo::where('employee_id',$value['id'])->whereBetween('created_at',[$from,$to])->count();
							$dd['data'][] = [$value['name'],$tmp1];
						}else{
							$monthFirst = date('Y-m')."-01";
							$today = date('Y-m-d');
							$tmp1 = CustomerPo::where('employee_id',$value['id'])->whereBetween('created_at',[$monthFirst,$today])->count();
							$dd['data'][] = [$value['name'],$tmp1];
						}
					}

					array_push($subData, $dd);
				}
			}
		}

		return ['main'=>$mainData,'sub'=>$subData];

	}

	public static function milestoneDetails(){
		
		$inquiry_generation_lead_time = DB::select(DB::raw(
			'SELECT 
				avg(days) as inquiry_lead_time
			from
				(
					SELECT 
						DATEDIFF(NOW(),inquiries.created_at) as days
					FROM 
						inquiries 
					    	INNER JOIN 
					    inquiry_transaction it2 ON inquiries.id = it2.inquiry_id 
					WHERE inquiries.id NOT IN (SELECT inquiry_id  FROM `inquiry_transaction` it WHERE it.event_id != 1 AND it.event_id != 6)
					UNION ALL
					Select
						DATEDIFF(trans_b.created_at, trans_a.created_at) AS days
					from 
						inquiry_transaction trans_a
							join 
						inquiry_transaction trans_b on trans_a.inquiry_id=trans_b.inquiry_id
					where trans_a.event_id in (1,6) and trans_b.event_id=5
				) tb'
		));

		$quotation_submission_lead_time = DB::select(DB::raw(
			'SELECT
				AVG(DATEDIFF(tb.quotation_date,ta.created_at)) as quotation_lead_time
			FROM
				(SELECT
					*
				FROM
					inquiry_transaction
				WHERE 
					event_id=5) ta JOIN
			    (SELECT
			    	*,min(quotation_date)
			     FROM
			     	quotation
			     GROUp BY inquiry_id
			    ) tb ON ta.inquiry_id=tb.inquiry_id
			'
		));

		$po_received_lead_time = DB::select(DB::raw(
			'SELECT
				AVG(days) as po_received_lead_time
			FROM
			(SELECT 
				DATEDIFF(NOW(),quotation_date) as days
			from 
				(SELECT 
			     	po.id as po_id,
			     	q.quotation_date,
			     	po.created_at     
			     FROM 
			     	quotation q 
			     		left join 
			    	customer_po po on q.reference_no=po.reference_no
			    ) tx
			where 
				tx.po_id is NULL
			UNION ALL
			SELECT 
				DATEDIFF(created_at,quotation_date) as days
			from 
				(SELECT 
			     	po.id as po_id,
			     	q.quotation_date,
			     	po.created_at
			     FROM 
			     	quotation q 
			     		left join 
			    	customer_po po on q.reference_no=po.reference_no
			    ) tx
			where 
				tx.po_id IS NOT NULL
			group by po_id) tz
			'
		));

		$data = [];

		$dd=[
			'no'		=>	"M1",
			'name'		=>	"Inquiry Generation",
			'start'		=>	"Opportunity Identification Date",
			'end'		=>	"Sales Resource Allocated Date",
			'lead'		=>	number_format((float)$inquiry_generation_lead_time[0]->inquiry_lead_time, 2, '.', '')
		];

		$data[]=$dd;

		$dd=[
			'no'		=>	"M2",
			'name'		=>	"Quotation Submission",
			'start'		=>	"Sales Resource Allocated Date",
			'end'		=>	"Quotation Submission Date",
			'lead'		=>	number_format((float)$quotation_submission_lead_time[0]->quotation_lead_time, 2, '.', '')
		];

		$data[]=$dd;

		$dd=[
			'no'		=>	"M3",
			'name'		=>	"PO Received",
			'start'		=>	"Quotation Submission Date",
			'end'		=>	"PO Received Date",
			'lead'		=>	number_format((float)$po_received_lead_time[0]->po_received_lead_time, 2, '.', '')
		];

		$data[]=$dd;

		$dd=[
			'no'		=>	"M4",
			'name'		=>	"Invoicing",
			'start'		=>	"PO Received Date",
			'end'		=>	"Invoicing Date",
			'lead'		=>	0
		];

		$data[]=$dd;

		$dd=[
			'no'		=>	"M5",
			'name'		=>	"Delivery",
			'start'		=>	"Invoicing Date",
			'end'		=>	"Customer Delivered Date",
			'lead'		=>	0
		];

		$data[]=$dd;

		$dd=[
			'no'		=>	"M6",
			'name'		=>	"Collection of Payment",
			'start'		=>	"Customer Delivered Date",
			'end'		=>	"Payment Collected Date",
			'lead'		=>	0
		];

		$data[]=$dd;

		return $data;
	}

}
