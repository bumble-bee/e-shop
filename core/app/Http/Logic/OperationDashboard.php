<?php namespace App\Http\Logic;


use Core\MenuManage\Models\Menu;
use Core\UserManage\Models\User;
use Core\UserRoles\Models\UserRole;
use Core\Sector\Models\Sector;
use Core\EmployeeManage\Models\Employee;

use App\Models\Inquiry;
use App\Models\InquiryTransaction;
use App\Models\Quotation;
use App\Models\Invoice;
use App\Models\Milestone;
use App\Models\MilestoneDetail;

use App\Models\TargetSales;
use App\Models\TargetCollection;

use App\Classes\Functions;
use App\Events\NotificationEvent;

use DB;
use Sentinel;
use DateTime;
use DateTimeZone;

class OperationDashboard {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */

	public static function getLeads(){

		/*$inquiry 			= Inquiry::all();

		$pending_leads 		= InquiryTransaction::whereIn('event_id',[1,6])->get();
		$approved_leads 	= InquiryTransaction::whereIn('event_id',[2])->get();
		$cancel_leads 		= InquiryTransaction::whereIn('event_id',[3])->get();
		$reopened_leads 	= InquiryTransaction::whereIn('event_id',[4])->get();
		$assigned_leads 	= InquiryTransaction::whereIn('event_id',[5])->get();

		// $tmp_code['total_leads'] 	= count($inquiry);

		$dd=[];
		
		$tmp_code['label'] 	= 'Pending';
		$tmp_code['data'] 	= count($pending_leads);
		$tmp_code['color'] 	= '#CC0000';

		array_push($dd, $tmp_code);

		$tmp_code = [];

		$tmp_code['label'] 	= 'Approved';
		$tmp_code['data'] 	= count($approved_leads);
		$tmp_code['color'] 	= '#00C851';

		array_push($dd, $tmp_code);

		$tmp_code = [];

		// $tmp_code['cancel_leads'] 	= count($cancel_leads);
		// $tmp_code['reopened_leads'] = count($reopened_leads);
		$tmp_code['label'] 	= 'Assigned';
		$tmp_code['data'] 	= count($assigned_leads);
		$tmp_code['color'] 	= '#FF8800';

		array_push($dd, $tmp_code);*/

		$data = DB::select(
			"SELECT
				'DONE' as label,
				'#00C851' as color,
				COUNT(inq.id) as `data`
			FROM
				inquiries inq
			LEFT JOIN inquiry_transaction inqt ON inq.id = inqt.inquiry_id AND inqt.event_id = 5
			WHERE 
				inq.status_id != -1
			 AND inqt.created_at IS NOT NULL
			UNION
			 SELECT 
				'PENDING' as label,
				'#FF8800' as color,
			  COUNT(inq.id) as `data`
			FROM
				inquiries inq
			LEFT JOIN inquiry_transaction inqt ON inq.id = inqt.inquiry_id AND inqt.event_id = 5
			WHERE 
				inq.status_id != -1
			 AND inqt.created_at IS NULL
			 AND TIMESTAMPDIFF(HOUR,inq.created_at,NOW()) < 24
			UNION
			SELECT 
				'DELAYED' as label,
				'#CC0000' as color,
			  COUNT(inq.id) as `data`
			FROM
				inquiries inq
			LEFT JOIN inquiry_transaction inqt ON inq.id = inqt.inquiry_id AND inqt.event_id = 5
			WHERE 
				inq.status_id != -1
			 AND inqt.created_at IS NULL
			AND TIMESTAMPDIFF(HOUR,inq.created_at,NOW()) > 24");

		return $data;

	}

	public static function getQuotationSummary(){
		$data = DB::select(
			"SELECT
				'DONE' AS label,
				'#00C851' AS color,
				COUNT(q.id) AS `data`
			FROM
				quotation q
			WHERE
				approved_at IS NOT NULL
			UNION
				SELECT
					'PENDING' AS label,
					'#FF8800' AS color,
					COUNT(q.id) AS `data`
				FROM
					quotation q
				WHERE
					approved_at IS NULL
				AND TIMESTAMPDIFF(HOUR, q.created_at, NOW()) < 24
				UNION
					SELECT
						'DELAYED' AS label,
						'#CC0000' AS color,
						COUNT(q.id) AS `data`
					FROM
						quotation q
					WHERE
						approved_at IS NULL
					AND TIMESTAMPDIFF(HOUR, q.created_at, NOW()) > 24");

		return $data;	
	}

	public static function getQuotes(){
		
		$from 	= date('Y-m')."-01";
		$to 	= date('Y-m-d',strtotime('+1 day'));
		$dates 	= [$from , $to];

		$quotes = Quotation::whereNotIn('inquiry_id',[0])->whereBetween('quotation_date',$dates)->count();
		$tmp_code['quotes'] = $quotes;
		
		return $tmp_code;

	}

	public static function getPOCount(){
		
		$from 	= date('Y-m')."-01";
		$to 	= date('Y-m-d',strtotime('+1 day'));
		$dates 	= [$from , $to];

		$count = DB::select(
			"SELECT
				COUNT(*) AS count
			FROM
				quotation
			INNER JOIN customer_po ON quotation.id = customer_po.quotation_id where customer_po.created_at between '".$from."' and '".$to."'");

		return $count;

	}

	public static function getDirectQuotes(){

		$from 	= date('Y-m')."-01";
		$to 	= date('Y-m-d',strtotime('+1 day'));
		$dates 	= [$from , $to];

		$quotes = Quotation::where('inquiry_id',0)->whereBetween('quotation_date',$dates)->count();
		$tmp_code['quotes'] = $quotes;
		return $tmp_code;
	}

	public static function getInvoices(){

		$from 	= date('Y-m')."-01";
		$to 	= date('Y-m-d',strtotime('+1 day'));
		$dates 	= [$from , $to];

		$invoices = DB::select(
			"SELECT
				(
					SELECT
						COUNT(*) AS count
					FROM
						invoice
					WHERE
						invoice.quotation_no IS NULL AND invoice.invoice_date between '".$from."' and '".$to."'
				) AS ln,
				(
					SELECT
						COUNT(*) AS count
					FROM
						invoice
					WHERE
						invoice.quotation_no IS NOT NULL AND invoice.invoice_date between '".$from."' and '".$to."'
				) AS olead");

		return $invoices;
	}

	public static function getDeliveredInvoices(){

		$from 	= date('Y-m')."-01";
		$to 	= date('Y-m-d',strtotime('+1 day'));
		$dates 	= [$from , $to];

		$invoices = Invoice::where('status',1)->whereBetween('invoice_date',$dates)->get();

		$cc = 0;

		foreach ($invoices->pluck('total_amount') as $value) {
			$cc += $value;
		}

		$cc = self::number_format_short($cc);

		$tmp_code['invoices'] 	= count($invoices);

		$tmp_code['amount'] 	= $cc;		

		return $tmp_code;
	}

	public static function getCollectionDetails(){

		// $invoices = Invoice::where('status',1)->get();

		$cc = 0;

		// foreach ($invoices->pluck('total_amount') as $value) {
		// 	$cc += $value;
		// }

		$cc = self::number_format_short($cc);

		$tmp_code['pending_collection'] = $cc;

		$tmp_code['total_collection'] = $cc;		

		return $tmp_code;
	}	

	public static function getLeadsCurrentMonth(){

		$from 	= date('Y-m')."-01";
		$to 	= date('Y-m-d',strtotime('+1 day'));
		$dates 	= [$from , $to];			

		$data = DB::select(
			"SELECT
				'DONE' as label,
				'#00C851' as color,
				COUNT(inq.id) as `data`
			FROM
				inquiries inq
			LEFT JOIN inquiry_transaction inqt ON inq.id = inqt.inquiry_id AND inqt.event_id = 5
			WHERE 
				inq.status_id != -1
			 AND inqt.created_at IS NOT NULL
			 AND inq.created_at between '".$from."' and '".$to."'
			UNION
			 SELECT 
				'PENDING' as label,
				'#CC0000' as color,
			  COUNT(inq.id) as `data`
			FROM
				inquiries inq
			LEFT JOIN inquiry_transaction inqt ON inq.id = inqt.inquiry_id AND inqt.event_id = 5
			WHERE 
				inq.status_id != -1
			 AND inqt.created_at IS NULL
			 AND inq.created_at between '".$from."' and '".$to."'
			 AND (TIMESTAMPDIFF(HOUR,inq.created_at,NOW()) < 24 or TIMESTAMPDIFF(HOUR,inq.created_at,NOW()) > 24)");

		return $data;

	}

	public static function getQuotationCurrentMonthSummary(){

		$from 	= date('Y-m')."-01";
		$to 	= date('Y-m-d',strtotime('+1 day'));
		$dates 	= [$from , $to];

		$data = DB::select(
			"SELECT
				'DONE' AS label,
				'#00C851' AS color,
				COUNT(q.id) AS `data`
			FROM
				quotation q
			WHERE
				approved_at IS NOT NULL
				AND q.quotation_date between '".$from."' and '".$to."'
			UNION
				SELECT
					'PENDING' AS label,
					'#CC0000' AS color,
					COUNT(q.id) AS `data`
				FROM
					quotation q
				WHERE
					approved_at IS NULL
					AND q.quotation_date between '".$from."' and '".$to."'	
					AND (TIMESTAMPDIFF(HOUR, q.created_at, NOW()) < 24 or TIMESTAMPDIFF(HOUR, q.created_at, NOW()) > 24)");

		return $data;	
	}

}
