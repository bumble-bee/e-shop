<?php namespace App\Http\Controllers;

use App\Modules\AdminQuotationManage\Models\AdminQuotationManage;
use Core\UserManage\Models\User;
use Sentinel;
use Response;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use App\Classes\Functions;
use App\Models\AppUser;
use App\Models\AppVersion;
use App\Models\Inquiry;
use App\Models\Image;
use Core\Sector\Models\Sector;
use App\Models\Source;
use App\Models\MetaData;
use App\Models\InquiryTransaction;
use App\Models\Quotation;
use App\Models\QuotationDetails;
use App\Models\Vat;
use App\Models\VatType;
use App\Models\SVat;
use App\Models\Nbt;
use App\Models\DeliveryTerms;
use App\Modules\AdminProductCategory\Models\AdminProductCategory;
use App\Modules\AdminInquiryManage\Models\AdminInquiryManage;
use App\Modules\AdminProductManage\Models\Product;
use App\Modules\AdminTaxManage\Models\Tax;
use App\Modules\AdminFollowUpManage\Models\FollowUpType;
use App\Modules\AdminFollowUpManage\Models\FollowUp;
use App\Modules\AdminCustomerManage\Models\Customer;
use Core\UserRoles\Models\UserRole;
use App\Modules\AdminQuotationManage\Models\CustomerPo;
use App\Modules\AdminQuotationManage\Models\PoAttachments;

use App\Modules\AdminPriceManage\BusinessLogics\PriceBookLogic;
use App\Modules\AdminProductManage\BusinessLogics\ProductLogic;
use App\Modules\AdminQuotationManage\BusinessLogics\QuotationLogic;
use App\Modules\AdminMessageManage\Models\AdminMessageManage;
use Mail;
use File;
use Core\EmployeeManage\Models\Employee;

class ApiSalesAppController extends Controller {

    private $priceBook;
    private $productLogic;
    private $quotationLogic;

    public function __construct(
        PriceBookLogic $priceBook,
        ProductLogic $productLogic,
        QuotationLogic $quotationLogic)
    {
        $this->priceBook = $priceBook;
        $this->quotationLogic = $quotationLogic;
        $this->productLogic = $productLogic;
        $this->take = 5000;
        ini_set('memory_limit', '-1');

    }

	public function checkUserAuthorize(Request $request)
	{
		if (!isset($_REQUEST['username']) || !isset($_REQUEST['password'])) {
            return 'invalid request request parameters is in valid ';
        }

        $userName = $request->get('username');
        $passWord = $request->get('password');
        $response = array();
        $response["timestamp"] = Functions::unixTimePhpToJava(time());

        $result = AppUser::
                    selectRaw('
                        employee.first_name,
                        employee.last_name,
                        token,
                        app_user.username,
                        code as userCode,
                        nic,
                        address as userAddress,
                        password,
                        mobile as telephoneNo,
                        mobile as mobileNo,
                        employee.email,
                        employee.id as userId,
                        concat(employee.first_name," ",employee.last_name) as name'
                    )->where('app_user.username', $userName)
		            ->leftjoin('employee','app_user.employee_id','=','employee.id')
                    ->where('app_user.password', md5($passWord))
		            ->first();


        if (count($result) == 1) {
        	$response["result"] = 1;
        	$response["user"] = $result;
            $result->last_login =  date("Y/m/d H:i:s");
            $result->save();
            return response()->json($response);
        } else {
            $response["result"] = 0;
            return response()->json($response);
        }
	}


	public function getSources(Request $request)
	{
		if (!isset($_REQUEST['token']) && !isset($_REQUEST['inq_id'])) {
            return 'invalid request request parameters is in valid ';
        }

        $token = $request->get('token');

        if ($token != NULL && $this->isToken($token)) {
			 $timestamp  = $request->get('timestamp');
            $timestamp  = Functions::unixTimeJavaToPhp($timestamp);
            $timestamp  = strlen($timestamp) > 0 ? $timestamp : 0;
            $response   = array();
			$response["timestamp"]         = Functions::unixTimePhpToJava(time());
			$response["result"]            = 1;
			$user                          = $this->getTokenUser($token);

			$result  = Source::selectRaw('id,name,created_at')
                ->where(DB::raw('UNIX_TIMESTAMP(timestamp)'), '>', $timestamp)
                ->orWhere(DB::raw('UNIX_TIMESTAMP(timestamp)'), '=', $timestamp)
                ->get();
			$response["result"]    = count($result)>0?1:0;
			$response["sources"] = count($result)>0?$result:[];
            return response()->json($response);
        } else {
            $response["result"] = 0;
            return response()->json($response);
        }
	}

	public function getInquiry(Request $request)
	{
		if (!isset($_REQUEST['token'])) {
            return 'invalid request request parameters is in valid ';
        }

        $token = $request->get('token');

        if ($token != NULL && $this->isToken($token)) {
			$timestamp  = $request->get('timestamp');
            $timestamp  = Functions::unixTimeJavaToPhp($timestamp);
            $timestamp  = strlen($timestamp) > 0 ? $timestamp : 0;
            $response   = array();
			$response["result"]            = 1;
			$user                          = $this->getTokenUser($token);

			//INQUIRY LOGIC ASSIGN
			$result  = Inquiry::where(function($query) use ($timestamp){
                            $query->where(DB::raw('UNIX_TIMESTAMP(timestamp)'), '>', $timestamp)
                            ->orWhere(DB::raw('UNIX_TIMESTAMP(timestamp)'), '=', $timestamp);
                        })
                        ->where('assigned_user', $user->employee_id)
                        ->get();

			$response["result"]    = count($result)>0?1:0;
            $response["timestamp"] = Functions::unixTimePhpToJava(time());
			$response["inquiries"] = count($result)>0?$result:[];
            return response()->json($response);
        } else {
            $response["result"] = 0;
            return response()->json($response);
        }
	}


	public function getCategories(Request $request)
    {
        if (!isset($_REQUEST['token'])) {
            return 'invalid request request parameters is in valid ';
        }

        $token = $request->get('token');
        if ($token != NULL && $this->isToken($token)) {
            $timestamp     = $request->get('timestamp');
            $timestamp  = Functions::unixTimeJavaToPhp($timestamp);
            $timestamp  = strlen($timestamp) > 0 ? $timestamp : 0;
            $response   = array();
            $response["timestamp"]         = Functions::unixTimePhpToJava(time());
            $response["result"]            = 1;
            $user                          = $this->getTokenUser($token);

            $result  = AdminProductCategory::selectRaw('id,name,created_at,code,parent')
                ->where(DB::raw('UNIX_TIMESTAMP(timestamp)'), '>', $timestamp)
                ->orWhere(DB::raw('UNIX_TIMESTAMP(timestamp)'), '=', $timestamp)
                ->get();

            $response["result"]    = count($result)>0?1:0;
            $response["categories"] = $result;
            return response()->json($response);
        } else {
            $response["result"] = 0;
            return response()->json($response);
        }
    }

    public function getProductDetails(Request $request)
	{
		if (!isset($_REQUEST['token'])) {
            return 'invalid request request parameters is in valid ';
        }

        $token = $request->get('token');
        $product_id = $request->get('product_id');
        $customer_id = $request->get('customer_id');

        if ($token != NULL && $this->isToken($token)) {
			$timestamp  = $request->get('timestamp');
            $timestamp  = Functions::unixTimeJavaToPhp($timestamp);
            $timestamp  = strlen($timestamp) > 0 ? $timestamp : 0;
            $response   = array();
			$response["timestamp"]         = Functions::unixTimePhpToJava(time());
			$response["result"]            = 1;
			$user                          = $this->getTokenUser($token);

            $product = Product::with('mrpprice')->find($product_id);

			try{
			    $result  = $this->priceBook->getProductPriceForCustomer($product_id, $customer_id);
            }catch(\Exception $e){
                $result = null;
            }

            $data = new \StdClass;


            if($result){
                $data->product_id       = $result->id;
                $data->product_price    = $result->price;
                $data->max_discount     = $result->max_discount?$result->max_discount:0;
                $data->udp              = $result->udp?$result->udp:0;
                $data->cash_udp         = $result->cash_udp?$result->cash_udp:0;
                $data->is_special_price    = 1;
                $data->stock            = $result->stock?$result->stock:0;
                $data->product_vertical = $result->product_vertical;
            }elseif($product){
                $data->product_id       = $product->id;
                $data->product_price    = $product->mrp;
                $data->max_discount     = $product->max_discount?$product->max_discount:0;
                $data->udp              = $product->udp?$product->udp:0;
                $data->cash_udp         = $product->cash_udp?$product->cash_udp:0;
                $data->is_special_price    = 0;
                $data->stock            = $product->stock?$product->stock:0;
                $data->product_vertical = $product->product_vertical;
            }


			$result  = AdminProductCategory::selectRaw('id,name,created_at,code,parent')->where(DB::raw('UNIX_TIMESTAMP(timestamp)'), '>', $timestamp)
                ->orWhere(DB::raw('UNIX_TIMESTAMP(timestamp)'), '=', $timestamp)
                ->get();

			$response["result"]    = count($result)>0?1:0;
			$response["details"] = $data;
            return response()->json($response);
        } else {
            $response["result"] = 0;
            return response()->json($response);
        }
	}

	 //done
    public function getProducts(Request $request)
    {
        if (!isset($_REQUEST['token'])) {
            return 'invalid request request parameters is in valid ';
        }

        $token 		= $request->get('token');
        $rowCount 	= $request->get('rowCount');
        if ($token != NULL && $this->isToken($token)) {

            $timestamp 	= $request->get('timestamp');
            $timestamp 	= Functions::unixTimeJavaToPhp($timestamp);
            $timestamp 	= strlen($timestamp) > 0 ? $timestamp : 0;
            $response 	= array();
            $response["timestamp"] = Functions::unixTimePhpToJava(time());
            $response["result"] = 1;
            $response["endStatus"] = 0;

            if($timestamp > 0){
                 $result = Product::selectRaw('
                        id,
                        code,
                        description,
                        product_category_id,
                        IFNULL(mrp,0) as mrp,
                        IFNULL(max_discount,0) as max_discount,
                        IFNULL(udp,0) as udp,
                        IFNULL(cash_udp,0) as cash_udp,
                        IFNULL(is_vat,0) as is_vat,
                        IFNULL(is_nbt,0) as is_nbt,
                        IFNULL(stock,0) as stock,
                        product_vertical
                     ')
                    ->where('mrp','>',0)
                    ->where('max_discount','>',0)
                    ->where('udp','>',0)
                    ->where('cash_udp','>',0)
                    ->where(DB::raw('UNIX_TIMESTAMP(timestamp)'), '>', $timestamp)
                    ->orWhere(DB::raw('UNIX_TIMESTAMP(timestamp)'), '=', $timestamp)
                    ->get();
            }else{
                $result = Product::selectRaw('
                        id,
                        code,
                        description,
                        product_category_id,
                        IFNULL(mrp,0) as mrp,
                        IFNULL(max_discount,0) as max_discount,
                        IFNULL(udp,0) as udp,
                        IFNULL(cash_udp,0) as cash_udp,
                        IFNULL(is_vat,0) as is_vat,
                        IFNULL(is_nbt,0) as is_nbt,
                        IFNULL(stock,0) as stock,
                        product_vertical
                     ')
                    ->where('mrp','>',0)
                    ->where('max_discount','>',0)
                    ->where('udp','>',0)
                    ->where('cash_udp','>',0)
                    ->where(DB::raw('UNIX_TIMESTAMP(timestamp)'), '>', $timestamp)                    
                    ->orWhere(DB::raw('UNIX_TIMESTAMP(timestamp)'), '=', $timestamp)
                    ->take($this->take)
                    ->skip($rowCount)
                    ->get();
            }


            $response["result"] = 1;
            $response["products"] = $result;

            $response["count"] = count($result);

            if (count($result) < ($rowCount + $this->take)) {
                $response["endStatus"] = 1;
            }
            if(sizeof($result)==0){
                $response["endStatus"] = 1;
            }
            return response()->json($response);
        } else {
            $response["result"] = 0;
            return response()->json($response);
        }
    }

    public function getVat(Request $request)
    {
        if (!isset($_REQUEST['token']) || !isset($_REQUEST['timestamp'])) {
            return 'invalid request request parameters is in valid ';
        }

        $token = $request->get('token');
        if ($token != NULL && $this->isToken($token)) {
            $timestamp = $request->get('timestamp');
            $timestamp = Functions::unixTimeJavaToPhp($timestamp);
            $response = array();
            $response["timestamp"] = Functions::unixTimePhpToJava(time());
            $response["result"] = 1;

            $result = Tax::select(
                'id as vatId',
                'value',
                'type_id',
                'ended_at as endedAt',
                'created_at as createdAt'
            )->where(DB::raw('UNIX_TIMESTAMP(timestamp)'), '>', $timestamp)
                ->orWhere(DB::raw('UNIX_TIMESTAMP(timestamp)'), '=', $timestamp)
                ->get();

            $response["result"] = 1;
            $response["vat"] = $result;
            return response()->json($response);
        } else {
            $response["result"] = 0;
            return response()->json($response);
        }
    }

     public function getSector(Request $request)
    {
        if (!isset($_REQUEST['token']) || !isset($_REQUEST['timestamp'])) {
            return 'invalid request request parameters is in valid ';
        }

        $token = $request->get('token');
        if ($token != NULL && $this->isToken($token)) {
            $timestamp = $request->get('timestamp');
            $timestamp = Functions::unixTimeJavaToPhp($timestamp);
            $response = array();
            $response["timestamp"] = Functions::unixTimePhpToJava(time());
            $response["result"] = 1;

            $result  = Sector::selectRaw('id,name,description')
                ->where(DB::raw('UNIX_TIMESTAMP(timestamp)'), '>', $timestamp)
                ->orWhere(DB::raw('UNIX_TIMESTAMP(timestamp)'), '=', $timestamp)
                ->get();

            $response["result"] = 1;
            $response["sector"] = $result;
            return response()->json($response);
        } else {
            $response["result"] = 0;
            return response()->json($response);
        }
    }

    public function getFollowupTypes(Request $request)
    {
        if (!isset($_REQUEST['token']) || !isset($_REQUEST['timestamp'])) {
            return 'invalid request request parameters is in valid ';
        }

        $token = $request->get('token');
        if ($token != NULL && $this->isToken($token)) {
            $timestamp = $request->get('timestamp');
            $timestamp = Functions::unixTimeJavaToPhp($timestamp);
            $response = array();
            $response["timestamp"] = Functions::unixTimePhpToJava(time());
            $response["result"] = 1;
            $user  = $this->getTokenUser($token);

            $result  = FollowUpType::selectRaw('id,name')
                ->where(DB::raw('UNIX_TIMESTAMP(timestamp)'), '>', $timestamp)
                ->orWhere(DB::raw('UNIX_TIMESTAMP(timestamp)'), '=', $timestamp)
                ->get();

            $response["result"]    = count($result)>0?1:0;
            $response["followup_types"] = $result;
            return response()->json($response);
        } else {
            $response["result"] = 0;
            return response()->json($response);
        }
    }

    public function addQuotation(Request $request)
    {
        $data = file_get_contents('php://input');
        $data = str_replace("\xe2\x80\x8b", "", $data);
        $token = $request->get('token');
        $data = json_decode($data);
        $response = array();
        $response["timestamp"] = Functions::unixTimePhpToJava(time());
        $nbt_id=NULL;
        $vat_id=NULL;
        $svat_id=NULL;
        $user  = $this->getTokenUser($token);

        $vat =  Vat::whereNull('ended_at')->where('type_id', VAT)->first();
        $vat_id =  ($vat)? $vat->id: null;

        $nbt =  Vat::whereNull('ended_at')->where('type_id', NBT)->first();
        $nbt_id =  ($nbt)? $nbt->id: null;

        if ($token != NULL && $this->isToken($token)) {
            $response["result"] = 1;
            $response["sucsessfully"] = array('quotation' => array(), 'details' => array());
            try {
                foreach (json_decode($data) as $value) {
                    DB::beginTransaction();
                    $quotation1 = $value->purchaseOrder;

                    if($quotation1->isReviseQuotation>0){
                        $quote = Quotation::find($quotation1->reference_id);
                        if($quote){
                            $quote->update([
                                'status' => REVISED
                            ]);

                            if($quote->inquiry_id && $quote->inquiry_id > 0){
                                InquiryTransaction::create([
                                    'inquiry_id' => $quote->inquiry_id,
                                    'event_id' => 8,
                                    'action_by' => $user->employee->id,
                                    'reference' => '',
                                    'created_at' => date('Y-m-d H:i:s')
                                ]);
                            }
                        }
                    }                    


                    $q_date = Functions::formatDateToPhp($quotation1->quotation_date);

                    $quotation_status = 0;

                    $quotation = Quotation::create([
                        'reference_no'=>$quotation1->reference_no,
                        'reference_id'=>$quotation1->isReviseQuotation>0?$quotation1->reference_id:null,
                        'customer_id'=>$quotation1->customer_id,
                        'inquiry_id'=>$quotation1->inquiry_id,
                        'quotation_date'=> date('Y-m-d H:i:s'),
                        'total_amount'=>$quotation1->total_amount,
                        'total_discount'=>$quotation1->total_discount,
                        'delivery_term_id'=>$quotation1->delivery_term_id,
                        'vat_id'=> $vat_id,
                        'nbt_id'=> $nbt_id,
                        'vat_per'=>$vat->value,
                        'quotation_type'=>0,
                        'discount_type'=>($quotation1->discount_type=='credit')?1:2,
                        'status'=>0,
                        'action_by'=>$quotation1->action_by,
                        'vat_type'=>$quotation1->vat_type,
                        'attn'=>(isset($quotation1->attn))?$quotation1->attn:'',
                        //'approved_at'=>$quotation->approved_at,
                        'action_by'=>($user->employee)?$user->employee->id:0,
                        'is_revise_quotation'=>$quotation1->isReviseQuotation
                    ]);

                    //file qutation performainvoice

                    $neededApproval = false;
                    $approvalDiscount = 0;


                    foreach ($value->purchaseOrderDetails as $key => $details) {

                        $pro = $this->productLogic->getProductById($details->product_id);

                        if($details->discount > 0){
                            $neededApproval = true;
                        }

                        if($details->discount > $approvalDiscount){
                            $approvalDiscount = $details->discount;
                        }

                           $quotation_details = QuotationDetails::create([
                            'quotation_id'=>$quotation->id,
                            'product_id'=>$details->product_id,
                            'price'=>$details->price,
                            'discount'=>$details->discount,
                            'is_vat'=> $details->is_vat,
                            'is_nbt'=> $details->is_nbt,
                            'vat'=> $vat->value,
                            'nbt'=> $nbt->value,
                            'udp'=> ($quotation1->discount_type=='credit')?$details->udp:$details->discount_cash,
                            'discount_cash'=> (isset($details->discount_cash))?$details->discount_cash:0,
                            'qty'=>$details->qty,
                            'discount_price'=> 0 //FOR NEW REQ APP NOT SENDING TOTALS CALC IT REALTIME
                        ]);

                        array_push($response["sucsessfully"]['details'], [
                            'clientKey'=>$details->local_id,
                            'serverKey'=>$quotation_details->id
                        ]);
                    }

                    if(!$neededApproval){
                        $quotation->approved_at = date('Y-m-d H:i:s');
                        $quotation->approved_by = ($user->employee)?$user->employee->id:0;
                        $quotation->status = 1;
                        $quotation->save();

                        $quotation_status = 1;

                        if($quotation->id) {
                            $quotation = $this->quotationLogic->getQuotationById($quotation->id);

                            $qutationFileName = 'quotation/quotation-' . $quotation->id . '-' . date('YmdHis') . '.pdf';
                            $fileName1 =$qutationFileName;
                            $qutationFileName = $this->quotationLogic->generateQuotationPdf($quotation, $nbt->value, $vat->value, $qutationFileName, true);

                            $piFileName = 'quotation/pi/pi-' . $quotation->id . '-' . date('YmdHis') . '.pdf';
                            $fileName2  = $piFileName;
                            $piFileName = $this->quotationLogic
                                            ->generateProformaInvoicePdf($quotation, $nbt->value, $vat->value, $piFileName, true);

                            $quotation->quotation_file_path = $fileName1;
                            $quotation->pi_file_path = $fileName2;
                            $quotation->save();
                        }
                    }else{
                        $role = UserRole::where('discount','>=', $approvalDiscount)
                                ->orderBy('discount', 'asc')->first();

                        if($role){
                            $quotation->approval_role_id = $role->id;
                            $quotation->save();

                            $quotation = $this->quotationLogic->getQuotationById($quotation->id);

                            $emails = Employee::whereIn('id',function($query) use ($role){
                                $query->select('employee_id')
                                    ->from('users')
                                    ->whereIn('id',function($qry) use ($role){
                                        $qry->select('user_id')
                                            ->from('role_users')
                                            ->where('role_id',$role->id);
                                    });
                            })
                            ->whereNotNull('email')
                            ->lists('email')
                            ->toArray();

                            $emails = array_filter($emails);
                            

                            if(count($emails) > 0){
                                Mail::send('email.approval',
                                    ['quotation' => $quotation], 
                                    function($m) use ($emails){
                                    $m->from('projectsales@orelcorp.com', 'Project Sales');
                                    $m->to($emails)->subject('1-Project Sales Quotation Approval NEEDED');
                                });
                            }
                        }else{
                            $role = UserRole::where('discount','<', $approvalDiscount)
                                ->orderBy('discount', 'desc')->first();

                            if($role && $role->discount > 0){
                                $quotation->approval_role_id = $role->id;
                                $quotation->save();

                                $quotation = $this->quotationLogic->getQuotationById($quotation->id);

                                $emails = Employee::whereIn('id',function($query) use ($role){
                                    $query->select('employee_id')
                                        ->from('users')
                                        ->whereIn('id',function($qry) use ($role){
                                            $qry->select('user_id')
                                                ->from('role_users')
                                                ->where('role_id',$role->id);
                                        });
                                })
                                ->whereNotNull('email')
                                ->lists('email')
                                ->toArray();

                                $emails = array_filter($emails);

                                if(count($emails) > 0){
                                    Mail::send('email.approval',
                                        ['quotation' => $quotation], 
                                        function($m) use ($emails){
                                        $m->from('projectsales@orelcorp.com', 'Project Sales');
                                        $m->to($emails)->subject('2-Project Sales Quotation Approval NEEDED');
                                    });
                                }
                            }
                        }
                    }

                    array_push($response["sucsessfully"]['quotation'], [
                        'clientKey'=>$value->purchaseOrder->local_id,
                        'serverKey'=>$quotation->id,
                        'status'=>$quotation_status
                    ]);

                    DB::commit();
                }
                return response()->json($response);
            }catch(\Exception $e){
                DB::rollback();
                return $e;
                $response["result"] = 0;
                $response["message"] = $e->getMessage();
                return response()->json($response);
                //return $e->getMessage();
            }
        } else {
            $response["result"] = 0;
            return response()->json($response);
        }
    }

    public function sendFollowUp(Request $request){
        $data = file_get_contents('php://input');
        $data = str_replace("\xe2\x80\x8b", "", $data);
        $token = $request->get('token');
        $data = json_decode($data);
        $response = array();
        $response["timestamp"] = Functions::unixTimePhpToJava(time());

        $user  = $this->getTokenUser($token);

        if ($token != NULL && $this->isToken($token)) {
            $response["result"] = 1;
            $response["sucsessfully"] = array();
            foreach (json_decode($data) as $followup) {
                $fflw = FollowUp::create([
                    'title'=>$followup->followUp_title,
                    'remark'=>$followup->followUp_Remark,
                    'followup_date'=>$followup->followupDate,
                    'inquiry_id'=>$followup->inquiry_Id,
                    'followup_type_id'=>$followup->followUp_type,
                    'status'=>$followup->followUp_status,
                    'action_by'=>($user->employee)?$user->employee->id:0
                ]);

                array_push($response["sucsessfully"], [
                    'clientkey'=>$followup->localID,
                    'serverKey'=>$fflw->id
                ]);
            }
            return response()->json($response);
        } else {
            $response["result"] = 0;
            return response()->json($response);
        }
    }

    public function addCustomer(Request $request){
        $data = file_get_contents('php://input');
        $data = str_replace("\xe2\x80\x8b", "", $data);
        $token = $request->get('token');
        $data = json_decode($data);
        $response = array();
        $response["timestamp"] = Functions::unixTimePhpToJava(time());

        $user  = $this->getTokenUser($token);

        if ($token != NULL && $this->isToken($token)) {
            $response["result"] = 1;
            $response["sucsessfully"] = array();
            foreach (json_decode($data) as $custom) {
                $customer = Customer::create([
                    'first_name'=>$custom->first_name,
                    'last_name'=>$custom->last_name,
                    'address'=>$custom->address,
                    'contact_no'=>$custom->telephone,
                    'status' => 0,
                    'action_by'=>($user->employee)?$user->employee->id:0
                ]);

                array_push($response["sucsessfully"], [
                    'client_key'=>$custom->local_id,
                    'server_key'=>$customer->id
                ]);
            }
            return response()->json($response);
        } else {
            $response["result"] = 0;
            return response()->json($response);
        }
    }

    public function getFollowUps(Request $request)
    {
        if (!isset($_REQUEST['token']) && !isset($_REQUEST['inq_id'])) {
            return 'invalid request request parameters is in valid ';
        }

        $token = $request->get('token');

        if ($token != NULL && $this->isToken($token)) {
             $timestamp  = $request->get('timestamp');
            $timestamp  = Functions::unixTimeJavaToPhp($timestamp);
            $timestamp  = strlen($timestamp) > 0 ? $timestamp : 0;
            $response   = array();
            $response["timestamp"]         = Functions::unixTimePhpToJava(time());
            $response["result"]            = 1;
            $user                          = $this->getTokenUser($token);

            $result  = FollowUp::selectRaw('id,inquiry_Id,followup_type_id as followUp_type,title as followUp_title,remark as followUp_Remark,status as followUp_status,created_at as createdDate,followup_date as followupDate')
                ->where(DB::raw('UNIX_TIMESTAMP(timestamp)'), '>', $timestamp)
                ->orWhere(DB::raw('UNIX_TIMESTAMP(timestamp)'), '=', $timestamp)
                ->get();
            $response["result"]    = count($result)>0?1:0;
            $response["followups"] = count($result)>0?$result:[];
            return response()->json($response);
        } else {
            $response["result"] = 0;
            return response()->json($response);
        }
    }

	public function isToken($token)
	{
		return AppUser::where('token',$token)->count()>0;
	}

	public function getTokenUser($token)
	{
		return AppUser::with(['employee'])->where('token',$token)->first();
	}

    public function getCustomers(Request $request)
    {
        if (!isset($_REQUEST['token']) || !isset($_REQUEST['timestamp'])) {
            return 'invalid request request parameters is in valid ';
        }

        $token = $request->get('token');
        if ($token != NULL && $this->isToken($token)) {
            $timestamp = $request->get('timestamp');
            $timestamp = Functions::unixTimeJavaToPhp($timestamp);
            $response = array();
            $response["timestamp"] = Functions::unixTimePhpToJava(time());
            $response["result"] = 1;
            $user  = $this->getTokenUser($token);

            $result  = Customer::selectRaw('
                        id,
                        first_name,
                        last_name,
                        blocked,
                        ifnull(credit_limit,0) as credit_limit,
                        ifnull(is_credit_blocked,0) as is_credit_blocked,
                        ifnull(credit_period,0) as credit_period,
                        IFNULL(is_vat,0) as is_vat,
                        IF(IFNULL(is_vat,0) = 0,\'\',vat) as vat_type,
                        code as short_code')
                    ->where(DB::raw('UNIX_TIMESTAMP(timestamp)'), '>', $timestamp)
                    ->where('status',1)
                    ->where('code','like','BPR%')
                    ->orWhere(DB::raw('UNIX_TIMESTAMP(timestamp)'), '=', $timestamp)
                    ->get();

            $response["result"]    = count($result)>0?1:0;
            $response["customers"] = $result;
            return response()->json($response);
        } else {
            $response["result"] = 0;
            return response()->json($response);
        }
    }

    public function getQuotationPdf(Request $request){

        if (!isset($_REQUEST['token'])) {
            return 'invalid request request parameters is in valid ';
        }

        $token = $request->get('token');

        if ($token != NULL && $this->isToken($token)) {
            $response["timestamp"] = Functions::unixTimePhpToJava(time());
            $response["result"] = 1;

            $quotation = $this->quotationLogic->getQuotationById($request->quotation_id);

            if(!$quotation->approved_at){
                $response["result"] = 0;
                return response()->json($response); 
            }

            $nbt_ar = $this->quotationLogic->getCurrentNbt();
            $nbt = $nbt_ar[0]->value;

            $vat_ar = $this->quotationLogic->getCurrentVat();
            $vat = $vat_ar[0]->value;

            $withHeader = !$request->print;

            foreach(QUOTATION_FORMATS as $key => $value){

                if(strpos($key, 'Q') !== false){
                    if($key == 'NQ'){
                        $fileName = 'quotation/quotation-'.$quotation->id.'-NQ'.date('YmdHis').'.pdf';
                        $fileName = $this->quotationLogic->generateQuotationPdf($quotation,$nbt, $vat, $fileName,true,'N',$withHeader);
                    }elseif($key == 'NBQ'){
                        $fileName = 'quotation/quotation-'.$quotation->id.'-NBQ'.date('YmdHis').'.pdf';
                        $fileName = $this->quotationLogic->generateQuotationPdf($quotation,$nbt, $vat, $fileName,true,'NB',$withHeader);
                    }elseif($key == 'DQ'){
                        $fileName = 'quotation/quotation-'.$quotation->id.'-DQ'.date('YmdHis').'.pdf';
                        $fileName = $this->quotationLogic->generateQuotationPdf($quotation,$nbt, $vat, $fileName,true,'D',$withHeader);
                    }

                    $response['data'][] = [
                        'name' => $value,
                        'filePath' => url($fileName)
                    ];
                }

                if(strpos($key, 'P') !== false){
                    if($key == 'NP'){
                        $fileName = 'quotation/pi/pi-'.$quotation->id.'-NP'.date('YmdHis').'.pdf';
                        $fileName = $this->quotationLogic->generateProformaInvoicePdf($quotation,$nbt,$vat,$fileName,true,'N',$withHeader);
                    }elseif($key == 'NBP'){
                        $fileName = 'quotation/pi/pi-'.$quotation->id.'-NBP'.date('YmdHis').'.pdf';
                        $fileName = $this->quotationLogic->generateProformaInvoicePdf($quotation,$nbt,$vat,$fileName,true,'NB',$withHeader);
                    }elseif($key == 'DP'){
                        $fileName = 'quotation/pi/pi-'.$quotation->id.'-DP'.date('YmdHis').'.pdf';
                        $fileName = $this->quotationLogic->generateProformaInvoicePdf($quotation,$nbt,$vat,$fileName,true,'D',$withHeader);
                    }

                    $response['data'][] = [
                        'name' => $value,
                        'filePath' => url($fileName)
                    ];
                }
            }
            return response()->json($response);
        }else{
            $response["result"] = 0;
            return response()->json($response);
        }
    }

    public function getQuotations(Request $request){
        if (!isset($_REQUEST['token'])) {
            return 'invalid request request parameters is in valid ';
        }

        $token = $request->get('token');

        if ($token != NULL && $this->isToken($token)) {
            $timestamp  = $request->get('timestamp');
            $timestamp  = Functions::unixTimeJavaToPhp($timestamp);
            $timestamp  = strlen($timestamp) > 0 ? $timestamp : 0;
            $response   = array();
            $response["result"]            = 1;
            $user                          = $this->getTokenUser($token);

            $vat =  Vat::whereNull('ended_at')->where('type_id', VAT)->first();

            $nbt =  Vat::whereNull('ended_at')->where('type_id', NBT)->first();

            //INQUIRY LOGIC ASSIGN
            $result  = Quotation::select(
                            'id',
                            'reference_no',
                            'customer_id',
                            'inquiry_id',
                            'approval_role_id',
                            'quotation_date',
                            'total_amount',
                            'total_discount',
                            'vat_id',
                            'nbt_id',
                            'timestamp',
                            'vat_per',
                            'vat_type',
                            'quotation_type',
                            DB::raw('IF(discount_type=1,\'credit\',\'cash\') as discount_type'),
                            'reference_id',
                            'status',
                            'approved_by',
                            'approved_at',
                            'action_by',
                            'ended_at',
                            'created_at as createdAt',
                            'created_at as createdDate',
                            'updated_at',
                            'updated_at',
                            'deleted_at',
                            'attn',
                            'is_revise_quotation as isReviseQuotation'
                        )->with(['details' => function($qry) use ($vat, $nbt){
                            $qry->select(
                                '*', 
                                DB::raw('qty as product_qty'), 
                                DB::raw('\''.$vat->value.'\' as vat'),
                                DB::raw('\''.$nbt->value.'\' as nbt'));
                        }])
                        ->where('action_by', $user->employee_id)
                        ->where(DB::raw('UNIX_TIMESTAMP(timestamp)'), '>', $timestamp)
                        ->orWhere(DB::raw('UNIX_TIMESTAMP(timestamp)'), '=', $timestamp)
                        ->get();

            $response["timestamp"] = Functions::unixTimePhpToJava(time());

            $dds = [];

            if(count($result) > 0){
                foreach($result as $row){
                    $row = collect($row);
                    $details = $row['details'];
                    $dd['quotation'] = $row->except(['details']);
                    $dd['details'] = $details;
                    array_push($dds, $dd);
                }
            }

            $response["result"]    = count($result)>0?1:0;
            $response["data"] = [];
            $response["data"] = count($result)>0?$dds:[];
            return response()->json($response);
        } else {
            $response["result"] = 0;
            $response["data"] = [];
            return response()->json($response);
        }
    }

    public function getQuotationPdfForSaveFile(Request $request,$id){

        if (!isset($_REQUEST['token'])) {
            return 'invalid request request parameters is in valid ';
        }

        $token = $request->get('token');

        if ($token != NULL && $this->isToken($token)) {
            $response["timestamp"] = Functions::unixTimePhpToJava(time());
            $response["result"] = 1;

            $quotation = $this->quotationLogic->getQuotationById($id);

            $fileName = 'quotation/quotation-'.$quotation->id.'-'.date('YmdHis').'.pdf';

            $fileName = $this->quotationLogic->generateQuotationPdf($quotation, $fileName, true);

            $response['quotation'] = [
                'name' => $quotation->reference_no,
                'filePath' => url($fileName)
            ];

            $fileName = 'quotation/pi/pi-'.$quotation->id.'-'.date('YmdHis').'.pdf';
            $fileName = $this->quotationLogic->generateProformaInvoicePdf($quotation, $fileName, true);

            $response['pi'] = [
                'name' => $quotation->reference_no,
                'filePath' => url($fileName)
            ];

            return response()->json($response);
        }else{
            $response["result"] = 0;
            return response()->json($response);
        }
    }

    public function uploadPo(Request $request){

        $response = array();
        $data = $request->get('data');
        $data = json_decode($data);

        $token = $data->token;

        if ($token != NULL && $this->isToken($token)) {
            $response["timestamp"] = Functions::unixTimePhpToJava(time());
            $response["result"] = 1;

            $user  = $this->getTokenUser($token);
            try {
                DB::beginTransaction();

                $quotation = $this->quotationLogic->getQuotationById($data->quotation_id);

                CustomerPo::where('quotation_id',$data->quotation_id)
                    ->whereNull('ended_at')
                    ->update([
                        'ended_at' => date('Y-m-d H:i:s')
                    ]);

                $imag = CustomerPo::create([
                    'quotation_id'  => $data->quotation_id,
                    'expected_date' => $data->expected_date,
                    'reference_no'  => $data->reference_no,
                    'po_number'     => $data->po_number,
                    'verified'      => $data->verified,
                    'customer_id'   => $quotation->customer_id,
                    'employee_id'   => $user->employee->id
                ]);

                $quotation->status = CLOSED;
                $quotation->save(); 

                for($i=0;$i<count($_FILES['picture']['name']);$i++){
                    $file_name =$_FILES['picture']['name'][$i];
                    $file_ext = File::extension($_FILES['picture']['name'][$i]);

                    $destinationPath = storage_path('uploads/po/');
                    if (!file_exists($destinationPath)) {
                        mkdir($destinationPath, 0777, true);
                    }

                    $response["timestamp"] = Functions::unixTimePhpToJava(time());
                    $name = date("YmdHis").'_'.$imag->id.'_'.$i.'.'.$file_ext;

                    if(move_uploaded_file($_FILES['picture']['tmp_name'][$i],$destinationPath.$name)){
                        $fil = PoAttachments::create([
                            'po_id'    => $imag->id,
                            'file_ext' => $file_ext,
                            'file'     => 'uploads/po/'.$name
                        ]);                               
                    }
                }

                DB::commit();
                $response["result"] = 1;
                $response["successfully"] = $data->quotation_id;

            } catch (Exception $ex) {
                DB::rollback();
                $response["result"] = 0;
                return response()->json($response);
            }

            return response()->json($response);
        }else{
            $response["result"] = 0;
            return response()->json($response);
        }
    }

    public function sendQuotationMail(Request $request)
    {
        $data                  = file_get_contents('php://input');
        $data                  = str_replace("\xe2\x80\x8b", "", $data);
        $token                 = $request->get('token');
        $data                  = json_decode(json_decode($data));
        $yy                    = [];
        //$maildata    = json_decode(json_decode($data));
        $response              = array();
        $response["timestamp"] = Functions::unixTimePhpToJava(time());

        $user  = $this->getTokenUser($token);
        if ($token != NULL && $this->isToken($token)) {
            $response["result"]       = 1;
            $response["sucsessfully"] = array();            
            $yy['title']              = $data->title;
            $yy['message']            = $data->message;
            //$yy['attachment']       = $data->attachment;
            $yy['from']               = 'oits.webteam@gmail.com';
            $yy['to']                 = $data->sender_email;
            $yy['cc']                 = $data->cc;
            $quotation_id             = $request->id;            
            $quotation                =  Quotation::find($quotation_id);

            $task_history = AdminMessageManage::create([
                'quotation_id' => $quotation_id,
                'sender_email' => $yy['to'],
                'from_email'   => $yy['from'],
                'cc'           => $yy['cc'],
                'title'        => $yy['title'],
                'message'      => $yy['message'],
                'attachment'   => $quotation->quotation_file_path
            ]);


            Mail::send('EmailManage::index',['data'=>$yy], function($message) use ($request, $user,$data,$quotation)            {
                $message->from('oits.webteam@gmail.com', 'OITS Team');
                $message->to($data->sender_email, '')->to($data->cc, '')->subject($data->title);

                if($data->isQuotation>0){
                    $message->attach(url('/'.$quotation['quotation_file_path']));
                }

                if($data->isProforma>0){
                    $message->attach(url('/'.$quotation['pi_file_path']));
                }
                
            });

            return response()->json($response);
        } else {
            $response["result"] = 0;
            return response()->json($response);
        }

    }

    public function getVatType(Request $request)
    {
        if (!isset($_REQUEST['token']) || !isset($_REQUEST['timestamp'])) {
            return 'invalid request request parameters is in valid ';
        }

        $token = $request->get('token');
        if ($token != NULL && $this->isToken($token)) {
            $timestamp = $request->get('timestamp');
            $timestamp = Functions::unixTimeJavaToPhp($timestamp);
            $response = array();
            $response["timestamp"] = Functions::unixTimePhpToJava(time());
            $response["result"] = 1;

            $result = VatType::where(DB::raw('UNIX_TIMESTAMP(timestamp)'), '>', $timestamp)
                                ->orWhere(DB::raw('UNIX_TIMESTAMP(timestamp)'), '=', $timestamp)
                                ->get();

            $response["result"] = 1;
            $response["vat_type"] = $result;
            return response()->json($response);
        } else {
            $response["result"] = 0;
            return response()->json($response);
        }
    }

    public function getDeliveryTerms(Request $request)
    {
        if (!isset($_REQUEST['token']) || !isset($_REQUEST['timestamp'])) {
            return 'invalid request request parameters is in valid ';
        }

        $token = $request->get('token');
        if ($token != NULL && $this->isToken($token)) {
            $timestamp = $request->get('timestamp');
            $timestamp = Functions::unixTimeJavaToPhp($timestamp);
            $response = array();
            $response["timestamp"] = Functions::unixTimePhpToJava(time());
            $response["result"] = 1;

            $result = DeliveryTerms::where(DB::raw('UNIX_TIMESTAMP(timestamp)'), '>', $timestamp)
                ->orWhere(DB::raw('UNIX_TIMESTAMP(timestamp)'), '=', $timestamp)
                ->get();

            $response["result"] = 1;
            $response["delivery_terms"] = $result;
            return response()->json($response);
        } else {
            $response["result"] = 0;
            return response()->json($response);
        }
    }

    public function checkVersion(Request $request)
    {
        if (!isset($_REQUEST['ver'])) {
            return 'invalid request request parameters is in valid ';
        }

        $ver = $request->get('ver');
        if ($ver != NULL) {
            $response   = [];
            $appVersion = AppVersion::where('status',1)
                                        ->whereNull('ended_at')
                                        ->where('version','>=',$ver)
                                        ->get();

            if(count($appVersion)>0){
                $response["result"]       = 1;
            }else{
                $response["result"]       = 0;                
            }                          
                        
            return response()->json($response);
        } else {
            $response["result"]           = 0;
            return response()->json($response);
        }
    }

}
