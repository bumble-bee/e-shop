<?php namespace App\Http\Controllers;

use App\Modules\AdminQuotationManage\Models\AdminQuotationManage;
use Core\UserManage\Models\User;
use Sentinel;
use Response;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use App\Classes\Functions;
use App\Models\AppUser;
use App\Models\AppVersion;
use App\Models\Inquiry;
use App\Models\Image;
use Core\Sector\Models\Sector;
use App\Models\Source;
use App\Models\MetaData;
use App\Models\InquiryTransaction;
use App\Models\Quotation;
use App\Models\QuotationDetails;
use App\Models\Vat;
use App\Models\VatType;
use App\Models\SVat;
use App\Models\Nbt;
use App\Models\Invoice;
use App\Models\DeliveryTerms;
use App\Modules\BankMange\Models\Bank;
use App\Modules\AdminProductCategory\Models\AdminProductCategory;
use App\Modules\AdminInquiryManage\Models\AdminInquiryManage;
use App\Modules\AdminProductManage\Models\Product;
use App\Modules\AdminTaxManage\Models\Tax;
use App\Modules\AdminFollowUpManage\Models\FollowUpType;
use App\Modules\AdminFollowUpManage\Models\FollowUp;
use App\Modules\AdminCustomerManage\Models\Customer;
use Core\UserRoles\Models\UserRole;
use App\Modules\AdminQuotationManage\Models\CustomerPo;

use App\Modules\AdminPriceManage\BusinessLogics\PriceBookLogic;
use App\Modules\AdminProductManage\BusinessLogics\ProductLogic;
use App\Modules\AdminQuotationManage\BusinessLogics\QuotationLogic;
use App\Modules\AdminMessageManage\Models\AdminMessageManage;
use Mail;
use File;
use Core\EmployeeManage\Models\Employee;

class SColApiController extends Controller {


    public function __construct()
    {

    }

    public function isToken($token)
    {
        return AppUser::where('token',$token)->count()>0;
    }

    public function getTokenUser($token)
    {
        return AppUser::with(['employee'])->where('token',$token)->first();
    }

	public function checkUserAuthorize(Request $request)
	{
		if (!isset($_REQUEST['username']) || !isset($_REQUEST['password'])) {
            return 'invalid request request parameters is in valid ';
        }

        $userName = $request->get('username');
        $passWord = $request->get('password');
        $response = array();
        $response["timestamp"] = Functions::unixTimePhpToJava(time());

        $result = AppUser::
                    selectRaw('
                        employee.first_name as firstName,
                        employee.last_name as lastName,
                        token as userToken,
                        app_user.username,
                        code as userCode,
                        nic,
                        address as userAddress,
                        password,
                        mobile as telephoneNo,
                        mobile as mobileNo,
                        employee.email,
                        employee.id as userId,
                        concat(employee.first_name," ",employee.last_name) as name'
                    )->where('app_user.username', $userName)
		            ->leftjoin('employee','app_user.employee_id','=','employee.id')
                    ->where('app_user.password', md5($passWord))
		            ->first();


        if (count($result) == 1) {
        	$response["result"] = 1;
        	$response["user"] = $result;
            $result->last_login =  date("Y/m/d H:i:s");
            $result->save();
            return response()->json($response);
        } else {
            $response["result"] = 0;
            return response()->json($response);
        }
	}

    public function getCustomers(Request $request){
        if (!isset($_REQUEST['token'])) {
            return 'invalid request request parameters is in valid ';
        }

        $token = $request->get('token');
        if ($token != NULL && $this->isToken($token)) {
            $response = array();
            $response["timestamp"] = Functions::unixTimePhpToJava(time());
            $response["result"] = 1;
            $user  = $this->getTokenUser($token);

            $result  = Customer::selectRaw("
                        id,
                        first_name as name,
                        code as bpr"
                    )->where('status',1)
                     ->where('first_name','like','%'.$request->keyword.'%')
                     ->orwhere('code','like','%'.$request->keyword.'%')
                     ->get();

            
            $response["result"]    = count($result)>0?1:0;
            $response["customers"] = $result;
            return response()->json($response);
        } else {
            $response["result"] = 0;
            return response()->json($response);
        }
    }

    public function getInvoices(Request $request){
        if (!isset($_REQUEST['token']) || !isset($_REQUEST['customer_id'])) {
            return 'invalid request request parameters is in valid ';
        }

        $token = $request->get('token');
        $customer_id = $request->get('customer_id');
        if ($token != NULL && $this->isToken($token)) {
            $response = array();

            $result  = Invoice::selectRaw("
                    id,
                    invoice_no as invoiceNo,
                    DATE_FORMAT(invoice_date, '%d-%m-%Y') as invoiceDate,
                    TRUNCATE(total_amount,2) as invoiceAmount,
                    0 as pendingCash,
                    0 as pendingCheque,
                    DATE_FORMAT(invoice_date, '%d') as invoiceDay, 
                    DATE_FORMAT(invoice_date, '%b') as invoiceMonth
                ")->where('customer_id',$customer_id)->get();

            $response["result"]    = count($result)>0?1:0;
            $response["invoices"] = $result;
            return response()->json($response);
        } else {
           return response()->json($response);
        }
    } 


    public function getQuotations(Request $request){
        if (!isset($_REQUEST['token']) || !isset($_REQUEST['customer_id'])) {
            return 'invalid request request parameters is in valid ';
        }

        $token = $request->get('token');
        $customer_id = $request->get('customer_id');
        if ($token != NULL && $this->isToken($token)) {
            $response = array();
            $user  = $this->getTokenUser($token);

            $result  = Quotation::selectRaw("
                id,
                reference_no as quotationNo,
                DATE_FORMAT(quotation_date, '%d-%m-%Y') as quotationDate,
                TRUNCATE(total_amount,2) as quotationAmount,
                0 as pendingCash,
                0 as pendingCheque,
                DATE_FORMAT(quotation_date, '%d') as quotationDay, 
                DATE_FORMAT(quotation_date, '%b') as quotationMonth
            ")->where('customer_id',$customer_id)->where('id',function($qry)
            {
                $qry->select('quotation_id')->from('customer_po')->limit(1);
            })->get();

            $response["result"]    = count($result)>0?1:0;
            $response["quotations"] = $result;
            return response()->json($response);
        } else {
            return response()->json($response);
        }
    } 

    public function getBanks(Request $request){
        if (!isset($_REQUEST['token'])) {
            return 'invalid request request parameters is in valid ';
        }

        $token = $request->get('token');
        if ($token != NULL && $this->isToken($token)) {
            $response = array();
            $response["timestamp"] = Functions::unixTimePhpToJava(time());
            $response["result"] = 1;
            $user  = $this->getTokenUser($token);

            $result  = Bank::where('status',1)->get();

            $response["result"]    = count($result)>0?1:0;
            $response["banks"] = $result;
            return response()->json($response);
        } else {
            $response["result"] = 0;
            return response()->json($response);
        }

    }

    


}
