<?php namespace App\Http\Controllers;

use Sentinel;

use Core\MenuManage\Models\Menu;
use Core\UserManage\Models\User;
use Core\UserRoles\Models\UserRole;
use Core\Sector\Models\Sector;
use Core\EmployeeManage\Models\Employee;

use App\Models\Inquiry;
use App\Models\Quotation;
use App\Models\Invoice;

use App\Models\Vat;

use App\Classes\Functions;
use App\Events\NotificationEvent;

use DB;
use Illuminate\Http\Request;
use App\Http\Logic\Dashboard;
use App\Http\Logic\OperationDashboard;
use App\Modules\AdminQuotationManage\BusinessLogics\QuotationLogic;

class WelcomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	
	protected $logic;
	public function __construct(QuotationLogic $logic)
	{	
		//$this->middleware('guest');
		$this->logic = $logic;
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function admin(Request $request)
	{

		$from 	= $request->get('from');
		$to 	= $request->get('to');

		if(($from=="" && $to=="") && ($from=="" || $to!="") && ($from!="" || $to=="") || ($from==NULL || $to==NULL)){
			$from 	= date('Y-m')."-01";
			$to 	= date('Y-m-d',strtotime('+1 day'));
		}elseif($from!="" && $to!=""){
			$from 	= $from;
			$to 	= $to;
		}

		$dates = [$from , $to];

		$inquiry 						= Dashboard::getInquiries($dates);
		$quotes 						= Dashboard::getQuotes($dates);
		$quotes_direct 					= Dashboard::getDirectQuotes($dates);
		$invoices 						= Dashboard::getInvoices($dates);
		$invoices_delivered 			= Dashboard::getDeliveredInvoices($dates);
		$collection 					= Dashboard::getCollectionDetails($dates);

		$perform_chart_overroll			= Dashboard::performChartSectorOverroll($dates);
		$perform_chart_overroll_amount	= Dashboard::performChartSectorOverrollAmountWise($dates);

		$perform_chart_user 			= Dashboard::performChart($dates);

		$sector_perform_chart 			= Dashboard::sectorPerformance($dates);		

		$leads_approval 				= Dashboard::approvedLeads();
		$to_do 							= Dashboard::toDoList();
		$quotes_approval 				= Dashboard::approvalQuotes();
		// $milestone 						= Dashboard::milestone();

		$milestone 						= Dashboard::milestoneDetails();
		// return $temp 					= Dashboard::temp();		

		return view('dashboard')->with([
        	'leads' 						=> 	$inquiry,
        	'quotes' 						=> 	$quotes,
        	'quotes_direct' 				=> 	$quotes_direct,
        	'invoices' 						=> 	$invoices,
        	'invoices_delivered' 			=> 	$invoices_delivered,
        	'collection' 					=> 	$collection,

        	'perform_chart_overroll'		=>	$perform_chart_overroll,
        	'perform_chart_overroll_amount'	=>	$perform_chart_overroll_amount,
        	'perform_chart_user'			=>	$perform_chart_user,
        	'sector_perform_chart'			=>	$sector_perform_chart,
        	// 'sales_recap'				=>	$sales_recap,

        	'leads_approval'				=>	$leads_approval,
        	'to_do'							=>	$to_do,
        	'quotes_approval'				=>	$quotes_approval,

        	'milestone'						=>	$milestone,

        	'old'							=> ['from'=>$from,'to'=>$to]
        ]);

	}

	public function test(){	

		/*$quotation = $this->logic->getQuotationById(156);

        $vat = Vat::find($quotation->vat_id);
        $nbt = Vat::find($quotation->nbt_id);

        $qutationFileName = 'quotation/quotation-' . $quotation->id . '-' . date('YmdHis') . '.pdf';
        $fileName1 = $qutationFileName;
        $qutationFileName = $this->logic->generateQuotationPdf($quotation, $nbt->value, $vat->value, $qutationFileName, true);

        $piFileName = 'quotation/pi/pi-' . $quotation->id . '-' . date('YmdHis') . '.pdf';
        $fileName2  = $piFileName;
        $piFileName = $this->logic->generateProformaInvoicePdf($quotation, $nbt->value, $vat->value, $piFileName, true);

        $quotation->quotation_file_path = $fileName1;
        $quotation->pi_file_path = $fileName2;
        $quotation->save();*/

		/*$user = Sentinel::getUser();
		event(new NotificationEvent($user));*/
		return view('email.customer_block');
	}

	public function privacy(){
		return view('privacy');
	}

	public function adminTest(){

		$leads 				= OperationDashboard::getLeads();
		$quotes 			= OperationDashboard::getQuotationSummary();
		$leadQuotesCount 	= OperationDashboard::getQuotes();
		$directQuotesCount 	= OperationDashboard::getDirectQuotes();
		$poCount 			= OperationDashboard::getPOCount();
		$invoices 			= OperationDashboard::getInvoices();

		return view('test')->with([
			'leads'				=>$leads, 
			'quotes'			=>$quotes,
			'leadQuotesCount'	=>$leadQuotesCount, 
			'directQuotesCount'	=>$directQuotesCount,
			'poCount'			=>$poCount,
			'invoices'			=>$invoices
		]);
	}

	public function monthAnalize(){

		$leads 				= OperationDashboard::getLeadsCurrentMonth();
		$quotes 			= OperationDashboard::getQuotationCurrentMonthSummary();
		$leadQuotesCount 	= OperationDashboard::getQuotes();
		$directQuotesCount 	= OperationDashboard::getDirectQuotes();
		$poCount 			= OperationDashboard::getPOCount();
		$invoices 			= OperationDashboard::getInvoices();

		return view('dashboard.month-summery-analize')->with([
			'leads'				=>$leads, 
			'quotes'			=>$quotes, 
			'leadQuotesCount'	=>$leadQuotesCount, 
			'directQuotesCount'	=>$directQuotesCount,
			'poCount'			=>$poCount,
			'invoices'			=>$invoices
		]);
	}

	public function getLeadsData(){
		return $leads = OperationDashboard::getLeads();		
	}

	public function adminDashboard2(Request $request){

		$from 	= $request->get('from');
		$to 	= $request->get('to');

		if(($from=="" && $to=="") && ($from=="" || $to!="") && ($from!="" || $to=="") || ($from==NULL || $to==NULL)){
			$from 	= date('Y-m')."-01";
			$to 	= date('Y-m-d',strtotime('+1 day'));
		}elseif($from!="" && $to!=""){
			$from 	= $from;
			$to 	= $to;
		}

		$dates = [$from , $to];

		$inquiry 						= Dashboard::getInquiries($dates);
		$quotes 						= Dashboard::getQuotes($dates);
		$quotes_direct 					= Dashboard::getDirectQuotes($dates);
		$invoices 						= Dashboard::getInvoices($dates);
		$invoices_delivered 			= Dashboard::getDeliveredInvoices($dates);
		$collection 					= Dashboard::getCollectionDetails($dates);

		$perform_chart_overroll			= Dashboard::performChartSectorOverroll($dates);

		$perform_chart_user 			= Dashboard::performChart($dates);

		$sector_perform_chart 			= Dashboard::sectorPerformance($dates);		

		$leads_approval 				= Dashboard::approvedLeads();
		$to_do 							= Dashboard::toDoList();
		$quotes_approval 				= Dashboard::approvalQuotes();
		// $milestone 						= Dashboard::milestone();

		$milestone 						= Dashboard::milestoneDetails();
		// return $temp 					= Dashboard::temp();		

		return view('dashboard2')->with([
        	'leads' 					=> 	$inquiry,
        	'quotes' 					=> 	$quotes,
        	'quotes_direct' 			=> 	$quotes_direct,
        	'invoices' 					=> 	$invoices,
        	'invoices_delivered' 		=> 	$invoices_delivered,
        	'collection' 				=> 	$collection,

        	'perform_chart_overroll'	=>	$perform_chart_overroll,
        	'perform_chart_user'		=>	$perform_chart_user,
        	'sector_perform_chart'		=>	$sector_perform_chart,
        	// 'sales_recap'				=>	$sales_recap,

        	'leads_approval'			=>	$leads_approval,
        	'to_do'						=>	$to_do,
        	'quotes_approval'			=>	$quotes_approval,

        	'milestone'					=>	$milestone
        ]);
	}

	public function adminSalesRecap(Request $request){

		$leadsGenerationSummery		= Dashboard::leadsGenerationSummery($request);
		$quotesGenerationSummery	= Dashboard::quotesGenerationSummery($request);		
		$poGenerationSummery		= Dashboard::poGenerationSummery($request);

		$from 	= $request->get('from');
		$to 	= $request->get('to');

		if(($from=="" && $to=="") && ($from=="" || $to!="") && ($from!="" || $to=="") || ($from==NULL || $to==NULL)){
			$from 	= date('Y-m')."-01";
			$to 	= date('Y-m-d');
		}elseif($from!="" && $to!=""){
			$from 	= $from;
			$to 	= $to;
		}

		return view('dashboard.sector-recap')->with([        	
        	'leads_generation_summery'	=>	$leadsGenerationSummery,
        	'quotes_generation_summery'	=>	$quotesGenerationSummery,
        	'po_generation_summery'		=>	$poGenerationSummery,
        	'old'	=> ['from'=>$from,'to'=>$to]			
        ]);
	}

	public function getSalesCap(Request $request){
		$dates = $request->get('dates');
		return $sales_recap = Dashboard::salesRecap($dates);
	}
}
