<?php namespace App\Http\Controllers;

use Core\UserManage\Models\User;
use Sentinel;
use Response;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use App\Classes\Functions;
use App\Models\AppUser;
use App\Models\Inquiry;
use App\Models\Image;
use Core\Sector\Models\Sector;
use App\Models\Source;
use App\Models\InquiryTransaction;
use Core\EmployeeManage\Models\Employee;
use Core\EmployeeManage\Models\EmployeeType;

class IOSApiController extends Controller {
	
	public function checkUserAuthorize(Request $request)
	{
		if (!isset($_REQUEST['username']) || !isset($_REQUEST['password'])) {
            return 'invalid request request parameters is in valid ';
        }

        $userName = $request->get('username');
        $passWord = $request->get('password');
        $response = array();
        $response["timestamp"] = Functions::unixTimePhpToJava(time());

        $result = AppUser::selectRaw('*,id as userId')->where('username', $userName)
		            ->where('password', md5($passWord))
		            ->first();


        if (count($result) == 1) {
        	$response["result"] = 1;
        	$response["user"] = $result;
            $result->last_login =  date("Y/m/d H:i:s");
            $result->save();
            return response()->json($response);
        } else {
            $response["result"] = 0;
            return response()->json($response);
        }
	}

	public function getInquiryList(Request $request)
	{
		if (!isset($_REQUEST['token'])) {
            return 'invalid request request parameters is in valid ';
        }

        $token = $request->get('token');
        if ($token != NULL && $this->isToken($token)) {
			$response                      = array();
			$response["timestamp"]         = Functions::unixTimePhpToJava(time());
			$response["result"]            = 1;
			$user                          = $this->getTokenUser($token);
			
			$result  = DB::table('inquiries')
						->select('inquiries.*','sectors.name as sectorName','sources.name as source')
						->leftJoin('sectors', 'sectors.id', '=', 'inquiries.sector')
						->leftJoin('sources', 'sources.id', '=', 'inquiries.source_id')
						->where('created_by',$user->employee_id)
			            ->get();

			$response["result"]    = count($result)>0?1:0;
			$response["inquiries"] = $result;
            return response()->json($response);
        } else {
            $response["result"] = 0;
            return response()->json($response);
        }
	}

	public function getSectors(Request $request)
	{
		if (!isset($_REQUEST['token'])) {
            return 'invalid request request parameters is in valid ';
        }

        $token = $request->get('token');
        if ($token != NULL && $this->isToken($token)) {
			$response                      = array();
			$response["timestamp"]         = Functions::unixTimePhpToJava(time());
			$response["result"]            = 1;
			$user                          = $this->getTokenUser($token);
			
			$result  = Sector::get();

			$response["result"]    = count($result)>0?1:0;
			$response["sectors"] = $result;
            return response()->json($response);
        } else {
            $response["result"] = 0;
            return response()->json($response);
        }
	}

	public function getSources(Request $request)
	{
		if (!isset($_REQUEST['token'])) {
            return 'invalid request request parameters is in valid ';
        }

        $token = $request->get('token');
        if ($token != NULL && $this->isToken($token)) {
			$response                      = array();
			$response["timestamp"]         = Functions::unixTimePhpToJava(time());
			$response["result"]            = 1;
			$user                          = $this->getTokenUser($token);
			
			$result  = Source::get();

			$response["result"]    = count($result)>0?1:0;
			$response["sources"] = $result;
            return response()->json($response);
        } else {
            $response["result"] = 0;
            return response()->json($response);
        }
	}

	public function getMasterData(Request $request)
	{
		if (!isset($_REQUEST['token'])) {
            return 'invalid request request parameters is in valid ';
        }

        $token = $request->get('token');
        if ($token != NULL && $this->isToken($token)) {
			$response                      = array();
			$response["timestamp"]         = Functions::unixTimePhpToJava(time());
			$response["result"]            = 1;
			$user                          = $this->getTokenUser($token);
			
			$sources  = Source::get();
			$sectors  = Sector::get();

			$response["result"]  = (count($sources)+count($sectors))>0?1:0;
			$response["sources"] = $sources;
			$response["sectors"] = $sectors;
            return response()->json($response);
        } else {
            $response["result"] = 0;
            return response()->json($response);
        }
	}

	public function saveInquiry(Request $request)
	{
		if (!isset($_REQUEST['token'])) {
            return 'invalid request request parameters is in valid ';
        }

        //return json_encode($request->all());

        $token = $request->get('token');
        $user  = $this->getTokenUser($token);
        if ($token != NULL && $this->isToken($token)) {			
        	try{
        		DB::beginTransaction();
	        	$aa = Inquiry::create([
		            'title'                 =>  $request->title, 
		            'address'               =>  $request->address,
		            'sector'                =>  $request->sector,
                    'contact_person'        =>  $request->contact_name,
                    'contact'               =>  $request->contact,
                    'created_date'          =>  date('Y-m-d H:i:s'),
                    'created_by'            =>  $user->employee->id,
                    'created_source'        =>  'MOBILE (IOS)',
                    'designation'        	=>  $request->designation,
                    'project_value'        	=>  $request->project_value,
		            'city'                  =>  "",
		            'district'              =>  "",
		            'developer'             =>  "",
		            'constructor'           =>  "",
		            'architect'             =>  "",
		            'developer_contact'     =>  "",
		            'constructor_contact'   =>  "",
		            'architect_contact'     =>  "",
		            'source_id'             =>  "",
		            'created_at'			=>  date('Y-m-d H:i:s'),
		            'updated_at'			=>  date('Y-m-d H:i:s'),
		            'timestamp'				=>  date('Y-m-d H:i:s')

		        ]);
		        $aa->status_id = 0;
		        $aa->inquiry_code = "INQ".str_pad($aa->id, 6, '0', STR_PAD_LEFT);
		        $aa->save();

		        InquiryTransaction::where('inquiry_id',$aa->id)->delete();

	            $trans=InquiryTransaction::create([
	                'inquiry_id'    =>  $aa->id,
	                'event_id'      =>  6,
	                'action_by'     =>  $user->employee->id,
	                'reference'     =>  'Mobile'
	            ]);

		        $response["result"] = 1;
		        $inq  = DB::table('inquiries')
						->select('inquiries.*','sectors.name as sectorName')
						->join('sectors', 'sectors.id', '=', 'inquiries.sector')
			            ->where('inquiries.id',$aa->id)->first();
				$response["inquiry"] = $inq;

				$file_name = $_FILES['file']['name'];
		        $file_ext = explode('.',$file_name);

		        $destinationPath = storage_path('uploads/images/project/');
		        if (!file_exists($destinationPath)) {
		            mkdir($destinationPath, 0777, true);
		        }

		        $response["timestamp"] = Functions::unixTimePhpToJava(time());
		        $name = date("Y_m_d_H_i_s").'.'.$file_ext[1];

		        if(move_uploaded_file($_FILES['file']['tmp_name'],$destinationPath.$name)){	
		         $imag = Image::create([
		            	'inquiry_id' 	=> $aa->id,
		            	'path' 			=> $destinationPath.$name,
		            	'filename' 		=> $name,
		            ]);	          
		        }else{
		            DB::rollback();
		        }

		        DB::commit();
		        $response["result"] = 1;
		    }catch(Exception $ex){
		    	DB::rollback();
		    	$response["result"] = 0;
		    }
            return response()->json($response);
        } else {
            $response["result"] = 0;
            return response()->json($response);
        }
	}


	public function getInquiryDetails(Request $request)
	{

		if (!isset($_REQUEST['token']) && !isset($_REQUEST['inq_id'])) {
            return 'invalid request request parameters is in valid ';
        }

		$token  = $request->get('token');
		$inq_id = $request->get('inq_id');

        if ($token != NULL && $this->isToken($token)) {
			$response                      = array();
			$response["timestamp"]         = Functions::unixTimePhpToJava(time());
			$response["result"]            = 1;
			$user                          = $this->getTokenUser($token);


			$result  = DB::table('inquiries')
						->select('inquiries.*','sectors.name as sectorName',DB::raw('ifnull(sources.name,"") as source'))
						->leftJoin('sectors', 'sectors.id', '=', 'inquiries.sector')				
						->leftJoin('sources', 'sources.id', '=', 'inquiries.source_id')				
						->where('created_source','MOBILE')
						->where('created_by',$user->employee_id)
						->where('inquiries.id',$inq_id)
			            ->get();

			$reject_reason=InquiryTransaction::where('inquiry_id',$inq_id)->where('event_id',3)->first();

			if($reject_reason){
				$comment=$reject_reason->reference;
			}else{
				$comment='-';
			}

			$response["result"]    = count($result)>0?1:0;
			$response["inquiry"] = count($result)>0?$result[0]:new \stdClass();
			if(count($result)>0){
				$response["inquiry"]->reject_reason=$comment;				
			}
            return response()->json($response);
        } else {
            $response["result"] = 0;
            return response()->json($response);
        }
	}
	
	

	public function userSignup(Request $request)
	{
       
		$response  = array();

		$data = file_get_contents('php://input');
    	$data = str_replace("\xe2\x80\x8b", "", $data);
    	
    	$inquiry = json_decode($data);
    	try{
    		DB::beginTransaction();
        	$aa = AppUser::create([
	            'username'      =>  $inquiry->userName,
	            'password'      =>  md5($inquiry->password),
	        ]);

	        $empType=EmployeeType::where('name','like','%Inquiry User%')->first();

        	$emp=Employee::create([
				'employee_type_id' =>	$empType->id,
				'first_name'       =>  $inquiry->first_name, 
				'last_name'        =>  $inquiry->last_name,
				'username'         =>  $inquiry->userName,
				'email'            =>  $inquiry->emailAddress,
				'password'         =>  md5($inquiry->password),
				'code'             =>  'GUEST',
	        ]);		        

			$aa->token          = md5($aa->id);
			$aa->employee_id    = $emp->id;
			$aa->save();
			$response["result"] = 1;
	        DB::commit();
	    }catch(Exception $ex){
	    	DB::rollback();
	    	$response["result"] = 0;
	    }
        return response()->json($response);
        
		
	}

	public function getInquiryTimeline(Request $request)
	{
		if (!isset($_REQUEST['token']) && !isset($_REQUEST['inq_id'])) {
            return 'invalid request request parameters is in valid ';
        }

		$token  = $request->get('token');
		$inq_id = $request->get('inq_id');

        if ($token != NULL && $this->isToken($token)) {
			$response                      = array();
			$response["timestamp"]         = Functions::unixTimePhpToJava(time());
			$response["result"]            = 1;
			$user                          = $this->getTokenUser($token);
			
			$result  = InquiryTransaction::selectRaw("
                inquiry_transaction.id,
                inquiry_id,
                reference as description,
                concat(employee.first_name,' ',employee.last_name) as name,
                events.name as title,
                DATE_FORMAT(inquiry_transaction.created_at,'%d') as day,
                DATE_FORMAT(inquiry_transaction.created_at,'%b') as month,
                DATE_FORMAT(inquiry_transaction.created_at,'%Y') as year,
                DATE_FORMAT(inquiry_transaction.created_at,'%I:%i %p') as time,
                CASE
                    WHEN event_id = 1 THEN 'Pending'
                    WHEN event_id = 2 THEN 'Approved'
                    WHEN event_id = 3 THEN 'Rejected'
                    WHEN event_id = 4 THEN 'Reopened'
                    WHEN event_id = 5 THEN 'Assigned'
                    WHEN event_id = 6 THEN 'Pending'
                    ELSE NULL
                END AS status"
            )
            ->leftJoin('employee','inquiry_transaction.reference','=','employee.id')
            ->leftJoin('events','inquiry_transaction.event_id','=','events.id')
            ->where('inquiry_id',$inq_id)
            ->orderBy('inquiry_transaction.created_at','desc')
            ->withTrashed()
            ->get();

			$response["result"]    = count($result)>0?1:0;
			$response["inquiry"] = count($result)>0?$result:[];
            return response()->json($response);
        } else {
            $response["result"] = 0;
            return response()->json($response);
        }
	}

	public function isToken($token)
	{	
		return AppUser::where('token',$token)->count()>0;
	}

	public function getTokenUser($token)
	{	
		return AppUser::where('token',$token)->first();
	}



	
}
